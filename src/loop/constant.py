
from pathlib import Path

MODULE_DIR = Path(__file__).parents[2]
ICON_DIR = MODULE_DIR / 'resources' / 'icons'