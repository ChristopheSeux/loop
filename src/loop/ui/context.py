

class Context:
    def __init__(self, app):

        self.app = app
        self.buffer_size = 60

    @property
    def main_window(self):
        return self.app.main_window

    @property
    def clip_editor_widget(self):
        return self.main_window.clip_editor_widget

    @property
    def timeline_widget(self):
        return self.clip_editor_widget.timeline_widget

    @property
    def timeline_view(self):
        return self.timeline_widget.view

    @property
    def video_surface(self):
        return self.clip_editor_widget.video_surface

    @property
    def audio_player(self):
        return self.clip_editor_widget.audio_player

    @property
    def buffer_ready(self):
        return self.clip_editor_widget.buffer_ready()

    @property
    def buffering_mode(self):
        return self.timeline_widget.buffering.mode

    @property
    def channel_views(self):
        return self.timeline_widget.channels_widget.channel_views

    @property
    def clip_items(self):
        return [i for c in self.channel_views for i in c.items()]

    @property
    def video_clip_items(self):
        return [item for item in self.clip_items if item.type == 'video']

    @property
    def audio_clip_items(self):
        return [item for item in self.clip_items if item.type == 'audio']

    @property
    def edit(self):
        return self.app.edit
    
    @property
    def nb_frames(self):
        return self.edit.nb_frames
    
    @property
    def duration(self):
        return self.edit.duration

    @property
    def frame_current(self):
        return self.timeline_widget.frame_current()
    
    @property
    def previous_frame_current(self):
        return self.timeline_widget.previous_frame_current()

    @property
    def displayed_frame_index(self):
        return self.video_surface.frame_index

    @property
    def frame_start(self):
        return self.edit.frame_start

    @property
    def frame_end(self):
        return self.edit.frame_end
    
    @property
    def is_playing(self):
        return self.timeline_widget.is_playing()

    @property
    def fps(self):
        return self.edit.fps

    @property
    def active_video_clip_items(self):
        
        return self.video_clip_items

        return [
                clip_item for clip_item in self.video_clip_items if
                clip_item.clip.edit_frame_start >= self.frame_current - (self.buffer_size*0.75) or
                clip_item.clip.edit_frame_end <= self.frame_current + (self.buffer_size*0.75)
        ]
        
    @property
    def is_play_reversed(self):
        return self.timeline_widget.is_play_reversed

    @property
    def is_forward(self):
        return self.frame_current > self.previous_frame_current

    @property
    def video_enabled(self):
        return self.clip_editor_widget.tool_bar.video_bool_widget.is_checked()

    @property
    def audio_enabled(self):
        return self.clip_editor_widget.tool_bar.audio_bool_widget.is_checked()