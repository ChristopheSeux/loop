


from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject, QIODevice, QByteArray)
from PySide6.QtGui import (QImage,)
from PySide6.QtMultimedia import QAudioSink,  QAudioFormat

#from PySide6 import QtNetwork

from __feature__ import snake_case#, true_property

import av
from av.audio.resampler import AudioResampler
import numpy as np
from math import ceil

from loop import ctx, signals
import time
import logging
import sys
from collections import deque

'''
class AudioFramePlayerSignals(QObject):
    #finished = Signal()
    #frame_decoded = Signal(int)
    started = Signal()
    finished = Signal()


class AudioFramePlayer(QRunnable):
    def __init__(self, buffering, frame_index) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True) 
        self.signals = AudioFramePlayerSignals()
        self.is_running = True
        self.frame_index = frame_index
        #self.second = 

    def run(self):
        self.signals.started.emit()

        #second = ctx.timeline_widget.frame_to_second(self.frame_index)
        
        #print('START SOUND RUNNABLE')

        for clip_item in ctx.audio_clip_items:
            stream = clip_item.clip.stream
            #clip_item.cache

            #print('----')
            #print(self.frame_index, clip_item.cache.keys())

            #audio_frame_index = stream.second_to_frame(second)
            audio_frame = clip_item.cache.get(self.frame_index)

            if audio_frame is None:
                print('AUDIO CFRAME NOT IN CACHE')
                continue

            print(audio_frame)

            #print(clip_item.cache.keys())

            #sample_rate = 44100
            #sd.play(audio_frame[0], samplerate=stream.fps)
            #sd.wait()

        #sd.play(signal, sample_rate, blocking=True)


class AudioPlayer(QThreadPool) :
    #runnable_added = Signal()

    def __init__(self) :
        super().__init__()

        self.set_max_thread_count(1)
        self.mode = 'idle'
        self.queue = []

        signals.frame_current_changed.connect(self.play_audio_frame)

    def play_audio_frame(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_current
        
        runnable = AudioFramePlayer(self, frame_index)

        self.start(runnable)
'''

class AudioDecoderSignals(QObject):
    #finished = Signal()
    #frame_decoded = Signal(int)
    started = Signal()
    finished = Signal()


class AudioDecoder(QRunnable):
    def __init__(self, buffering) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True)
        self.signals = AudioDecoderSignals()
        #self.signals.started.connect(lambda : self.buffering.set_state(''))

    def clip_items_at(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_index

        return [c for c in ctx.audio_clip_items if c.clip.frame_in_range(frame_index)]

    def convert_frames(self, stream, frames, start, end=None):

        dtype = self.buffering.dtype
        nb_channels = self.buffering.nb_channels


        #return np.concatenate([f.to_ndarray() for f in frames], axis=1)

        chunk_nb_samples = sum(f.samples for f in frames)
        chunk_start = stream.pts_to_second(frames[0].pts)
        trim_start = int((start - chunk_start)*stream.fps)

        chunk_end = chunk_start + (chunk_nb_samples / stream.fps)
        trim_end = int((chunk_end - end)*stream.fps)

        # print(f"{trim_start=}")
        # print(f"{trim_end=}")

        audio_chunk = np.zeros((nb_channels, chunk_nb_samples), dtype=dtype, order='F')

        # for frame in frames:
        #     sample_offset = 0

        #     for converted_frame in self.buffering.resampler.resample(frame):
        #         for channel_index, plane in enumerate(converted_frame.planes):
        #             audio_chunk[channel_index:channel_index+1, sample_offset: sample_offset+frame.samples] = np.frombuffer(
        #                 plane, dtype=dtype, count=converted_frame.samples)
            
        #     sample_offset += frame.samples

        sample_offset = 0
        for frame in frames:

            for channel_index, plane in enumerate(frame.planes):
                audio_chunk[channel_index:channel_index+1, sample_offset: sample_offset+frame.samples] = np.frombuffer(
                    plane, dtype=dtype, count=frame.samples)
            
            sample_offset += frame.samples
        
        #print(len(audio_chunk[0]), trim_start)
        #print(f'{chunk_nb_samples=}, start={float(start)}, end={float(end)}, {chunk_start=}, {chunk_end=}, {trim_start=}, {trim_end=}', len(audio_chunk[0]), len(audio_chunk[:,trim_start:-trim_end][0]))
        #print((end-start)*stream.fps)
        
        #return audio_chunk

        return audio_chunk[:,trim_start:-trim_end]
        #return audio_chunk[:,trim_start:-trim_end]
        #return audio_chunk[:,trim_start:-trim_end]

    def run(self):
        self.signals.started.emit()

        #cache = self.buffering.cache

        audio_stack = []


        #cache_
        #forward_index = self.buffering.frame_index
        #nb_cache_frames = self.buffering.cache_size - (forward_index - ctx.frame_current)#self.buffering.cache_size - len(cache)

        ##print('nb_cache_frames', nb_cache_frames)
        #print('forward_index', forward_index)
        #print(f'{nb_cache_frames=}')
        #print(f'cache_len={len(cache)}')
        #print('Is cache full', self.buffering.cache_size)
        while not self.buffering.is_cache_full:
            if self.buffering.frame_index == ctx.frame_end:
                break

            #for i in range(nb_cache_frames):
            #print('Buffering Index', self.buffering.frame_index)

            #print(cache_frame_index, len(cache))
            #edit_frame_index = forward_index  + cache_frame_index

            #print(self, 'self.buffering.frame_index', self.buffering.frame_index)
            #clip_items = 

            for clip_item in self.clip_items_at(self.buffering.frame_index):
                stream = clip_item.clip.stream
                clip_frame_index = clip_item.frame_current(self.buffering.frame_index)

                #second_start = None
                #if clip_item.time_current(stream.current_time) < ctx.time_current:
                second_start = ctx.timeline_widget.frame_to_second(clip_frame_index)
                second_end = ctx.timeline_widget.frame_to_second(clip_frame_index+1)

                #print(f'second_start={float(second_start)}', stream.current_time)
                stream.seek(second_start)

                frames = []
                for frame in stream.decode_to(second_end):
                    if frame is None:
                        break

                    frames.append(frame)

                #frames = [stream.decode_next_frame(), stream.decode_next_frame()]

                #print(float(second_start), float(second_end),  frames)

                audio_stack.append((clip_item, self.convert_frames(stream, frames, start=second_start, end=second_end)))

            chunk = ctx.audio_player.mix_down(audio_stack)

            #print(chunk)
            self.buffering.cache_chunk(chunk)
            #np_frame = frame.to_ndarray().T
             # Append the chunk transposed and convert to bytes to cache
            self.buffering.frame_index += 1

        self.signals.finished.emit()


class AudioBuffering(QThreadPool):
    #ready = Signal()

    def __init__(self) :
        super().__init__()

        #self.queue = [] # Store runnables

        self.cache = QByteArray()#deque()
        self.max_cache_size = 2 # in megabytes
        self.frame_index = 0
        #self.frame_size = 0.02

        self.set_max_thread_count(1)

        self.dtype = 'float32'
        self.nb_channels = 2
        self.resampler = AudioResampler('s32', 'stereo', 48000)
        self.is_running = False
    
    @property
    def is_ready(self):
        return self.cache.size() > 2048
    
    @property
    def cache_size(self):
        return self.cache.size() * 0.000001

    @property
    def is_cache_full(self):
        return self.cache_size > self.max_cache_size

    def cache_chunk(self, chunk):
        #emit_signal = False
        #if not self.cache:
        #    emit_signal = True
        
        #print('Add to Cache')

        self.cache.append(chunk.T.tobytes())

        #if emit_signal:
        #    self.ready.emit()

    def set_running(self, is_running):
        self.is_running = is_running

    def decode(self):
        if self.is_running:
            return
            
        runnable = AudioDecoder(self)
        runnable.signals.started.connect(lambda : self.set_running(True))
        runnable.signals.finished.connect(lambda : self.set_running(False))
        #runnable.signals.finished.connect(lambda : self.remove(runnable))

        self.start(runnable)

        return runnable

    # def remove(self, runnable):
    #     if runnable in self.queue:
    #         self.queue.remove(runnable)

class AudioDevice(QIODevice):
    def __init__(self, player):
        super().__init__()

        self.player = player
        self.open(QIODevice.ReadOnly)

    @property
    def cache(self):
        return self.player.cache

    def read_data(self, maxlen):
        data = self.cache.left(maxlen)
        self.cache.remove(0, maxlen)

        return data.data()

    def write_data(self, data):
        return 0

    def bytes_available(self):
        return self.cache.size() + super().bytes_available()


class AudioPlayer(QObject):
    ready = Signal()
    def __init__(self) :
        super().__init__()

        #self.frame_index = 0
        #self.start_time = time.time()
        
        self.device = AudioDevice(self)
        self.buffering = AudioBuffering()

        self.format = QAudioFormat()
        self.format.set_sample_rate(48000)
        self.format.set_channel_count(2)
        self.format.set_sample_format(QAudioFormat.Float)

        self.output = QAudioSink(self.format)

        #self.cache = deque()
        #self.buffering.ready.connect(self.ready.emit)
        
        self.output.stateChanged.connect(self.handle_state_changed)
        signals.frame_current_changed.connect(self.buffering.decode)

    @property
    def cache(self):
        return self.buffering.cache

    @property
    def is_ready(self):
        return self.buffering.is_ready

    def handle_state_changed(self, state):
        print(f"{state=}")

    def volume(self):
        return self.output.volume()

    def set_volume(self, volume):
        self.output.set_volume(volume)

    def debug_print(self):
        return
        print(time.time()- self.start_time)
    
    def play(self):
        print('Start Audio')
        #self.output.start(self.buffer)

        self.device.open(QIODevice.ReadOnly)
        print('cache_size', self.cache.size())
        self.output.start(self.device)

        #runnable = self.buffering.decode()
        #runnable.signals.finished.connect(lambda: self.output.start(self.device))
        #self.output.start(self)
        #self.start_time = time.time()

    def pause(self):
        self.device.close()
    
    def seek(self, second):
        self.device.close()
        self.cache.clear()

        self.buffering.frame_index = ctx.timeline_widget.second_to_frame(second)
        self.buffering.decode()
        
        #for clip_item in self.clip_items_at(second):
        # for clip_item in ctx.audio_clip_items:
        #     clip_second = ctx.timeline_widget.frame_to_second(clip_item.clip.edit_frame_start)
        #     if clip_item.clip.frame_in_range(ctx.frame_index):
        #         clip_second += second

        #     clip_item.clip.stream.seek(clip_second)
        
        #self.second = ctx.timeline_widget.frame_to_second(ctx.frame_current)
        #self.pts_current = second * self.stream.samplerate

        #self.play()
        
    #def pause(self):
    #    self.stream.stop()
    '''
    def callback(self, outdata, frames, time, status):
        if status:
            print(status, file=sys.stderr)
        
        #print(time.outputBufferDacTime - time.currentTime, (time.outputBufferDacTime - time.currentTime)*ctx.fps)
        #print('-', time.inputBufferAdcTime*ctx.fps)
        #time_offset = time.inputBufferAdcTime*ctx.fps

        #print('---', frames)
        
        audio_stack = self.decode_frame(self.second_current)

        if not audio_stack:
            outdata.fill(0)
            return

        audio_chunk = self.mix_down(audio_stack)
        outdata[:] = audio_chunk.T

        self.pts_current += self.stream.blocksize
        '''



    # def convert_frame(self, frame, dtype):
    #     #time.sleep(0.01)

    #     #print(frame.pts)
    #     nb_channels = len(frame.layout.channels)
    #     audio_chunk = np.zeros((nb_channels, self.stream.blocksize), dtype=dtype, order='F')
    #     sample_index = 0

    #     for channel_index, plane in enumerate(frame.planes):
    #         audio_chunk[channel_index:channel_index+1, 0: frame.samples] = np.frombuffer(
    #             plane, dtype=dtype, count=frame.samples)

    #     return audio_chunk

    def mix_down(self, stack):
        #stack.sort(key=lambda x : x[0].channel)

        for clip_item, audio_chunk in stack:
            pass
            #pass # Apply opacity transform and blend mode

        return audio_chunk


