

from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject)
from PySide6.QtGui import (QImage,)

from __feature__ import snake_case#, true_property

import av
import numpy as np
from math import ceil

from loop import ctx, signals
import time
import logging


class VideoDecoderSignals(QObject):
    started = Signal()
    finished = Signal()
    frame_decoded = Signal(int)


class VideoDecoder(QRunnable):
    def __init__(self, buffering) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True) 
        self.signals = VideoDecoderSignals()
        self.mode = 'base'
        self.is_running = True

    def stop(self):
        self.is_running = False

    def clip_items_in_range(self, frame_start, frame_end):
       return [ c for c in ctx.video_clip_items if frame_start <= c.clip.edit_frame_start < frame_end or
                frame_start <= c.clip.edit_frame_end < frame_end or
                c.clip.edit_frame_start <= frame_start < c.clip.edit_frame_end or
                c.clip.edit_frame_start <= frame_end < c.clip.edit_frame_end
            ]

    def clip_items_at(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_current
        
        return [c for c in ctx.video_clip_items if c.clip.edit_frame_start <= frame_index < c.clip.edit_frame_end]

    def decode_clip_to(self, clip_item, frame_index):
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)
        second = stream.frame_to_second(frame_index)

        #print('decode_clip_to', stream.frame_index, frame_index)

        for video_frame in stream.decode_to(second):
            #print(stream.frame_index)
            yield video_frame

    def seek_clip_to(self, clip_item, frame_index):
        '''Place The stream index if necessary'''
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)

        #if stream.frame_index == frame_index: # Already well placed
        #   return

        if frame_index <= 0:
            stream.start()
            return

        if stream.frame_index not in range(frame_index-20, frame_index):
            print('REEL SEEK', stream.frame_index, frame_index)
            seek_second = stream.frame_to_second(frame_index-1)
            stream.seek_keyframe(seek_second)

        #print('seek clip to', frame_index-1)
        #seek_second = stream.frame_to_second(frame_index-1)
        #stream.seek_keyframe(seek_second)

        #target_second = stream.frame_to_second(frame_index)
        #for video_frame in stream.decode_to(target_second):
        #    yield video_frame


class ForwardVideoDecoder(VideoDecoder):
    def __init__(self, buffering) :
        super().__init__(buffering)

        self.mode = 'forward'

    def discard_image(self, cached_index):
        if not ctx.video_surface.is_cache_full():
            return

        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current
        buffer_size = int(ctx.buffer_size*0.75)

        frame_index = max(cache)
        if frame_index < frame_current + buffer_size or frame_index == cached_index:
            frame_index = min(cache)

        ctx.video_surface.discard_image(frame_index)

    def run(self):
        self.signals.started.emit()
        buffer_size = int(ctx.buffer_size*0.25)
        cache = ctx.video_surface.cache

        # Find forward frame not in the cache already
        forward_index = next((ctx.frame_current+i for i in range(buffer_size) if ctx.frame_current+i not in cache), None)

        if forward_index is None or forward_index > ctx.frame_end: # The cache is full
            self.signals.frame_decoded.emit(ctx.frame_current)
            return self.signals.finished.emit()

        #Check is the clips need seeking
        for clip_item in self.clip_items_at(forward_index):
            if clip_item.clip.stream.frame_index == forward_index:
                continue
                
            self.seek_clip_to(clip_item, forward_index)
            for _ in self.decode_clip_to(clip_item, forward_index-1):
                if self.buffering.mode != self.mode:
                    return self.signals.finished.emit()

        last_index = min(ctx.frame_current + buffer_size, ctx.frame_end+1) # Last frame of the range to put in cache
        for frame_index in range(forward_index, ctx.frame_current + buffer_size):
            frame_stack = []

            for clip_item in self.clip_items_at(frame_index):
                video_frame = None
                
                #print()
                for video_frame in self.decode_clip_to(clip_item, frame_index):
                    if self.buffering.mode != self.mode:
                        return self.signals.finished.emit()

                if not video_frame:
                    print('NO VIDEO FRAME', clip_item.clip.name)
                    continue
                    #return self.signals.finished.emit()

                frame_stack.append((clip_item, video_frame))
                
                #ctx.audio_player.mix_down(audio_stack, frame_index)
            #ctx.video_surface.compose(frame_stack, frame_index)

            #print('frame_index', frame_index)

            #if not frame_stack:
            #    raise Exception('No Frame Stack')

            #print(frame_stack)
            
            image_stack = ctx.video_surface.convert_frames(frame_stack)
            image = ctx.video_surface.compose(image_stack)
            ctx.video_surface.cache_image(frame_index, image)

            self.discard_image(frame_index)
            self.signals.frame_decoded.emit(frame_index)

        self.signals.finished.emit()
        #print(f'Forward Buffering finished', frame_index)


class BackwardVideoDecoder(VideoDecoder):
    def __init__(self, buffering) :
        super().__init__(buffering)

        self.mode = 'backward'

    def discard_image(self, cached_index):
        if not ctx.video_surface.is_cache_full():
            return

        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current
        buffer_size = int(ctx.buffer_size*0.75)

        frame_index = min(cache)
        if frame_index >= frame_current-buffer_size or frame_index == cached_index:
            frame_index = max(cache)

        ctx.video_surface.discard_image(frame_index)

    def run(self):
        self.signals.started.emit()
        buffer_size = int(ctx.buffer_size*0.75)
        cache = ctx.video_surface.cache



        #print(sorted(ctx.video_surface.cache.items()))

        # Align video decode cursor
        backward_index = ctx.frame_current-buffer_size #next((ctx.frame_current-i for i in range(buffer_size) if ctx.frame_current-i not in cache), None)

        clip_items = self.clip_items_in_range(backward_index, ctx.frame_current)
        if not clip_items:
            print('No Clip Items')
            return self.signals.finished.emit()
        #print('clip_items', clip_items)

        for clip_item in clip_items:
            self.seek_clip_to(clip_item, backward_index)
            for _ in self.decode_clip_to(clip_item, backward_index-1):
                if self.buffering.mode != self.mode:
                    return self.signals.finished.emit()

        #print(sorted(cache.keys()))
        #print(backward_index, backward_index)
        for frame_index in range(backward_index, ctx.frame_current+1):
            if frame_index in cache:
                #print(frame_index, 'in video Cache')
                continue
            
            #print('Backward Buffer Frame index', frame_index)

            frame_stack = []

            for clip_item in self.clip_items_at(frame_index):
                video_frame = None
                #print('decode_clip_to', frame_index, clip_item.clip.stream.frame_index)
                for video_frame in self.decode_clip_to(clip_item, frame_index):

                    if self.buffering.mode != self.mode:
                        #print('Changing Mode')
                        return self.signals.finished.emit()

                if video_frame:
                    frame_stack.append((clip_item, video_frame))
                else:
                    continue


            #image = ctx.video_surface.decode_frames(frame_stack)

            image_stack = ctx.video_surface.convert_frames(frame_stack)
            image = ctx.video_surface.compose(image_stack)
            ctx.video_surface.cache_image(frame_index, image)

            self.discard_image(frame_index)

            #ctx.video_surface.cache_image(frame_index, image)
            #self.discard_image(frame_index)

            self.signals.frame_decoded.emit(frame_index)

        #ctx.video_surface.show_image(frame_index, image)
        self.signals.frame_decoded.emit(frame_index)
        self.signals.finished.emit()





class RandomVideoDecoder(VideoDecoder):
    def __init__(self, buffering) :
        super().__init__(buffering)
    
    def discard_image(self, cached_index):
        #return
        if not ctx.video_surface.is_cache_full():
            return

        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current

        frame_index = min(cache)#max(cache, key=lambda x : abs(x-frame_current))
        if frame_index == frame_current or frame_index == cached_index:
            frame_index = max(cache)

        ctx.video_surface.discard_image(frame_index)

    def run(self):
        self.signals.started.emit()

        frame_current = ctx.frame_current
        if frame_current in ctx.video_surface.cache:
            return self.signals.finished.emit()

        clip_items = self.clip_items_at(frame_current)

        if not clip_items:
            return self.signals.finished.emit()

        #print(ctx.edit.fps)

        #Check is the clips need seeking
        for clip_item in self.clip_items_at(frame_current):
            stream = clip_item.clip.stream

            print('--', stream.frame_index, frame_current)
            if stream.frame_index == frame_current:
               continue


            #print(stream.frame_to_second(frame_current-5), "second")

            # stream.seek_keyframe(stream.frame_to_second(frame_current-2))
            # for _ in stream.decode_to(stream.frame_to_second(frame_current-2)):
            #     print('--', clip_item.clip.stream.frame_index)
            #     if self.buffering.mode != self.mode:
            #         return self.signals.finished.emit()
            self.seek_clip_to(clip_item, frame_current)
            for _ in self.decode_clip_to(clip_item, frame_current-1):
                #print('--', clip_item.clip.stream.frame_index)
                if self.buffering.mode != self.mode:
                    return self.signals.finished.emit()
            
            #print(clip_item.clip.stream.frame_index)
        
        frame_stack = [(c, c.clip.stream.decode_next_frame()) for c in clip_items]

        image_stack = ctx.video_surface.convert_frames(frame_stack)
        image = ctx.video_surface.compose(image_stack)
        ctx.video_surface.cache_image(frame_current, image)

        self.discard_image(frame_current)

        ctx.video_surface.show_image(frame_current, image)
        #ctx.video_surface.cache_image(frame_index, image)
        #self.discard_image(frame_index)

        #self.signals.frame_decoded.emit(frame_index)

        self.signals.finished.emit()





class VideoBuffering(QThreadPool) :
    #runnable_added = Signal()

    def __init__(self) :
        super().__init__()

        self.set_max_thread_count(1)
        self.mode = 'idle'
        self.queue = []

        signals.about_to_quit.connect(lambda : self.set_mode('idle'))

        #self.
    
    def add_runnable(self, runnable, start=True, connect=None):
        if connect:
            runnable.signals.frame_decoded.connect(connect, Qt.SingleShotConnection)

        if start:
            self.start(runnable)

        return runnable

    def decode_forward(self, start=True, connect=None):
        print('decode_forward', self.mode, connect)

        if self.mode == 'forward':
            return# connect.emit()

        return self.add_runnable(ForwardVideoDecoder(self), start=start, connect=connect)

    def decode_backward(self, start=True, connect=None):
        if self.mode == 'backward':
            return# connect.emit()

        return self.add_runnable(BackwardVideoDecoder(self), start=start, connect=connect)

    def decode_random(self, start=True, connect=None):
        return self.add_runnable(RandomVideoDecoder(self), start=start, connect=connect)

    def remove(self, runnable):
        if runnable in self.queue:
            self.queue.remove(runnable)

    def start(self, runnable):
        self.clear()

        runnable.signals.started.connect(lambda : self.set_mode(runnable.mode))
        runnable.signals.finished.connect(lambda : self.set_mode('idle'))
        runnable.signals.finished.connect(lambda : self.remove(runnable))

        #runnable.connect('started', lambda : self.set_mode(runnable.mode))
        #runnable.connect('finished', lambda : self.set_mode('idle'))
        #runnable.connect('finished', lambda : self.remove(runnable))

        #print('Buffer Added', runnable)

        self.mode = runnable.mode
        self.queue.append(runnable)

        super().start(runnable)

        #print('QUEUE LEN', len(self.queue))


    def set_mode(self, mode):
        self.mode = mode

    #def stop(self):



'''

class Buffering(QThread):
    frame_decoded = Signal((int, np.ndarray))

    def __init__(self):
        super().__init__()

        #self.video_streams = []#set()
        self.is_running = True
        self.buffering_mode = 'fast '
    
        #self.start()
    #def get_current_videos(self):
    #    return (v for v in ctx.edit.video_clips if v.edit_frame_start < ctx.frame_current < v.edit_frame_end)

    def stop(self) :
        self.is_running = False
        self.wait()

    def run(self):
        while self.is_running:

            #get all active video
            video_clip_items = ctx.active_video_clip_items#set(clip for clip in ctx.clip_items_at if clip.type=='video')

            #print('video_clip_items', video_clip_items)

            if not video_clip_items:
                time.sleep(0.015)
                continue
            
            #print(ctx.buffering_mode)

            for video_clip_item in video_clip_items:
                if ctx.buffering_mode == 'forward':
                    video_clip_item.forward_buffering()
                elif ctx.buffering_mode == 'backward':
                    video_clip_item.backward_buffering()
                elif ctx.buffering_mode == 'fast':
                    video_clip_item.random_buffering(precise=False)
                    ctx.timeline_widget.buffering_mode = 'forward'
                elif ctx.buffering_mode == 'precise':
                    video_clip_item.random_buffering(precise=True)
                    ctx.timeline_widget.buffering_mode = 'forward'

            if all(not wgt.is_buffering for wgt in video_clip_items):
                time.sleep(0.015)
                continue

'''