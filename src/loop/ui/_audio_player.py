

from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject)
from PySide6.QtGui import (QImage,)

from __feature__ import snake_case#, true_property

import av
import numpy as np
from math import ceil

from loop import ctx, signals
import time
import logging
import sounddevice as sd
import sys

'''
class AudioFramePlayerSignals(QObject):
    #finished = Signal()
    #frame_decoded = Signal(int)
    started = Signal()
    finished = Signal()


class AudioFramePlayer(QRunnable):
    def __init__(self, buffering, frame_index) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True) 
        self.signals = AudioFramePlayerSignals()
        self.is_running = True
        self.frame_index = frame_index
        #self.second = 

    def run(self):
        self.signals.started.emit()

        #second = ctx.timeline_widget.frame_to_second(self.frame_index)
        
        #print('START SOUND RUNNABLE')

        for clip_item in ctx.audio_clip_items:
            stream = clip_item.clip.stream
            #clip_item.cache

            #print('----')
            #print(self.frame_index, clip_item.cache.keys())

            #audio_frame_index = stream.second_to_frame(second)
            audio_frame = clip_item.cache.get(self.frame_index)

            if audio_frame is None:
                print('AUDIO CFRAME NOT IN CACHE')
                continue

            print(audio_frame)

            #print(clip_item.cache.keys())

            #sample_rate = 44100
            #sd.play(audio_frame[0], samplerate=stream.fps)
            #sd.wait()

        #sd.play(signal, sample_rate, blocking=True)


class AudioPlayer(QThreadPool) :
    #runnable_added = Signal()

    def __init__(self) :
        super().__init__()

        self.set_max_thread_count(1)
        self.mode = 'idle'
        self.queue = []

        signals.frame_current_changed.connect(self.play_audio_frame)

    def play_audio_frame(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_current
        
        runnable = AudioFramePlayer(self, frame_index)

        self.start(runnable)
'''

class AudioPlayer:
    #runnable_added = Signal()

    def __init__(self) :
        super().__init__()

        #self.cache = {}
        self.pts_current = 0
        #self.second_current = 0

        self.stream = sd.OutputStream(
            channels=2, blocksize=1024,
            samplerate=48000, callback=self.callback,
            latency='low')

        #self.stream.start()
        #print(dir(self.stream))

        #print(self.stream.latency)
        self.start_time = time.time()

        signals.frame_current_changed.connect(lambda : self.debug_print())

        #signals.frame_current_changed.connect(self.add_frame)
    def debug_print(self):
        return
        print(time.time()- self.start_time)

    @property
    def second_current(self):
        return self.pts_current / self.stream.samplerate
    
    def start(self):
        print('Start Audio')
        self.stream.start()

        self.start_time = time.time()

    def pause(self):
        self.stream.stop()

    def seek(self, second):
        self.stream.abort()
        
        for clip_item in self.clip_items_at(second):

            clip_second = ctx.timeline_widget.frame_to_second(clip_item.clip.edit_frame_start) + second
            clip_item.clip.stream.seek(clip_second)
        
        #self.second = ctx.timeline_widget.frame_to_second(ctx.frame_current)
        self.pts_current = second * self.stream.samplerate
        
    #def pause(self):
    #    self.stream.stop()

    def callback(self, outdata, frames, time, status):
        if status:
            print(status, file=sys.stderr)
        
        #print(time.outputBufferDacTime - time.currentTime, (time.outputBufferDacTime - time.currentTime)*ctx.fps)
        #print('-', time.inputBufferAdcTime*ctx.fps)
        #time_offset = time.inputBufferAdcTime*ctx.fps

        #print('---', frames)
        
        audio_stack = self.decode_frame(self.second_current)

        if not audio_stack:
            outdata.fill(0)
            return

        audio_chunk = self.mix_down(audio_stack)
        outdata[:] = audio_chunk.T

        self.pts_current += self.stream.blocksize

    def clip_items_at(self, second=None):
        if second is None:
            second = ctx.second_current

        frame_index = ctx.timeline_widget.second_to_frame(second)
        
        #return [c for c in ctx.audio_clip_items if ctx.timeline_widget.frame_to_second(c.clip.edit_frame_start) <= frame_index < c.clip.edit_frame_end]
        return [c for c in ctx.audio_clip_items if c.clip.frame_in_range(frame_index)]

    def decode_frame(self, second=None):
        audio_stack = []
        
        #print('Decode Sound Frame', second, ctx.timeline_widget.second_to_frame(second))

        if second is None:
            second = ctx.second_current

        clip_items = self.clip_items_at(second)

        #print([(clip_item.clip.name, clip_item.clip.edit_frame_start, clip_item.clip.edit_frame_end) for clip_item in clip_items])

        if not clip_items:
            # Add Silence
            return

        for clip_item in clip_items:

            #print('Decode Frame of', clip_item.clip.name, ctx.timeline_widget.second_to_frame(second))    
            stream = clip_item.clip.stream
            #print(stream.sample_fmt)
            audio_frame = stream.decode_next_frame()

            frame_index = ctx.timeline_widget.second_to_frame(stream.current_time) + clip_item.clip.edit_frame_start
            print(frame_index)

            #print('frame_pts', audio_frame.pts)

            if not audio_frame:
                continue
            audio_stack.append((clip_item, self.convert_frame(audio_frame, dtype=stream.dtype)))
            #if frame_index < clip_item.clip.frame_start or frame_index > clip_item.clip.frame_end:
            #    continue

        #if not audio_stack:
        #    return self.decode_next_frame()

        #stream.seek(second)
        return audio_stack

    def convert_frame(self, frame, dtype):
        #time.sleep(0.01)

        #print(frame.pts)
        nb_channels = len(frame.layout.channels)
        audio_chunk = np.zeros((nb_channels, self.stream.blocksize), dtype=dtype, order='F')
        sample_index = 0

        for channel_index, plane in enumerate(frame.planes):
            audio_chunk[channel_index:channel_index+1, 0: frame.samples] = np.frombuffer(
                plane, dtype=dtype, count=frame.samples)

        return audio_chunk

    def mix_down(self, stack):
        #stack.sort(key=lambda x : x[0].channel)

        for clip_item, audio_chunk in stack:
            pass
            #pass # Apply opacity transform and blend mode

        return audio_chunk


