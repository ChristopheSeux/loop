
from PySide6.QtCore import (QObject, Signal, Qt, QRectF, QPointF, QLineF, QPoint, QByteArray)
from PySide6.QtGui import (QPainter, QImage, QPen, QBrush, QColor, QPolygonF)
from PySide6.QtWidgets import (QWidget, QVBoxLayout, QGraphicsItem, QGraphicsRectItem, QGraphicsTextItem)
from __feature__ import snake_case##, true_property

import logging
import numpy as np
from math import floor

from loop import ctx, signals


class ClipLabelItem(QGraphicsTextItem):
    """Widget holding a video clip and cache"""
    
    type = 'label'
    #frame_decoded = Signal(int, QImage)

    def __init__(self, clip_item):
        super().__init__()

        #self.set_brush(QBrush())
        #self.setPen()
        self.text_color = QColor(235, 235, 235)
        self.background_color = QColor(30, 30, 30)

        self.set_parent_item(clip_item)
        self.set_plain_text(clip_item.clip.name)

        self.set_flags(QGraphicsItem.ItemIgnoresTransformations)
        self.set_default_text_color(self.text_color)

        font = self.font()
        font.set_pixel_size(10)
        self.set_font(font)

        self.set_pos(0.5, 0)

    
    def paint(self, painter, option, widget):
        clip_item = self.parent_item()
        if clip_item.height() < clip_item.header_scene_height() or not clip_item.show_header:
            return
        
        super().paint(painter, option, widget)

        # Draw Clip Name in the header
        # font = painter.font()
        # font.set_pixel_size(10)
        # painter.set_font(font)

        # clip = self.parent_item()

        # painter.set_pen(QColor(0, 0, 0, 60))
        # painter.draw_text(header_rect.adjusted(6, 2, 0, 0), Qt.AlignLeft, clip.name)

        # painter.set_pen(self.text_color)
        # painter.draw_text(header_rect.adjusted(5, 1, 0, 0), Qt.AlignLeft, clip.name)

class ClipSignals(QObject):
    pass
    #preview_updated = Signal()


class ClipItem(QGraphicsRectItem):
    """Widget holding a video clip and cache"""
    
    type = 'clip'
    #ready = Signal()
    #frame_decoded = Signal(int, QImage)

    def __init__(self, clip, parent=None):
        super().__init__(0, 0, 100, 40)

        #self.set_brush(QBrush())
        #self.setPen()
        self.signals = ClipSignals()
        self.show_header = False
        self.text_color = QColor(255, 255, 255, 210)
        self.background_color = QColor(40, 40, 40)
        #self.header_color = QColor(70, 70, 70, 128)

        self.color = QColor(77, 104, 144)
        self.header_height = 20

        #self.set_flag(QGraphicsItem.ItemIsMovable)
        self.set_flag(QGraphicsItem.ItemIsSelectable, True)
        self.set_flag(QGraphicsItem.ItemSendsGeometryChanges, True)

        self.is_ready = False
        self.clip = clip
        self.is_buffering = True
        self.cache = {}
        self.preview_cache = {}

        #print(clip, clip.edit_frame_start)
        # print(clip.channel)

        #self.channel_size = 55
        self.set_pos(clip.edit_frame_start, 0)
        self.set_rect(QRectF(0, 0, self.clip.duration, 100))

        self.label_item = ClipLabelItem(self)

        #self.signals.preview_updated.connect(self.update)
        #self.signals.preview_updated.connect(lambda : print(self, 'preview updated'))

    def view(self):
        views = self.scene().views()
        if views:
            return views[0]

    def vertical_scaling(self):
        return self.view().vertical_scaling()

    def horizontal_scaling(self):
        return self.view().horizontal_scaling()

    def header_scene_height(self):
        view = self.view()
        if not view:
            return self.header_height

        return self.header_height  / self.vertical_scaling() #view.map_to_scene(QPoint(0, self.header_height)).y() / self.vertical_scaling()

    def map_to_screen(self, value):
        return self.view().map_from_scene(QPoint(value, 0)).x()

    def width_screen(self):
        return self.view().map_from_scene(QPoint(self.rect().width(), 0)).x() #* self.horizontal_scaling()

    def height_screen(self):
        return self.view().map_from_scene(QPoint(self.rect().height(), 0)).x() #* self.vertical_scaling()

    def width(self):
        return self.rect().width() #/ self.horizontal_scaling()

    def height(self):
        return self.rect().height() #/ self.vertical_scaling()

    def draw_header(self, painter):
        # Draw Header Background
        header_color = QColor(self.color)
        rect = self.rect()

        header_height = self.header_scene_height()

        header_rect = rect
        header_color.set_alpha(56)
        if self.height() > 2 * header_height:
            header_color.set_alpha(48)
            header_rect = rect.adjusted(0, 0, 0, -rect.height() + header_height)

        painter.set_brush(header_color)
        painter.draw_rect(header_rect)

        # Draw Header Line
        #print(self.screen_height())
        if self.height() > 2 * header_height:
            painter.set_brush(Qt.NoBrush)
            pen = QPen(self.color, 1,  Qt.SolidLine, Qt.FlatCap)
            pen.set_cosmetic(True)

            header_shadow_height = (self.header_height +1 ) / self.vertical_scaling()
            pen.set_color(QColor(0, 0, 0, 64))
            painter.set_pen(pen)
            painter.draw_line(QLineF(0, header_shadow_height, rect.width(), header_shadow_height))

            pen.set_color(self.color)
            painter.set_pen(pen)
            painter.draw_line(QLineF(0, header_height, rect.width(), header_height))

    def draw_background(self, painter):

        painter.set_render_hint(QPainter.Antialiasing, False)

        header_height = self.header_scene_height()

        #print(header_height)

        pen_width = 1
        rect = self.rect()#.adjusted(*(pen_width*0.5,)*4)
        painter.set_pen(Qt.NoPen)

        # Draw Background
        painter.set_brush(self.background_color)
        painter.draw_rect(rect)

        header_color = QColor(self.color)
        header_color.set_alpha(12)
        if self.show_header:
            header_color.set_alpha(8)
        painter.set_brush(header_color)
        painter.draw_rect(rect)

        if self.is_selected():
            painter.set_brush(QColor(255, 255, 255, 16))
            painter.draw_rect(rect)

        if self.show_header:
            self.draw_header(painter)

    def draw_foreground(self, painter):

        #print('DRAWW CLIP', self)

        painter.set_render_hint(QPainter.Antialiasing, False)

        header_height = self.header_scene_height()
        pen_width = 1
        rect = self.rect()#.adjusted(*(pen_width*0.5,)*4)
        header_rect = rect.adjusted(0, 0, 0, -rect.height() + header_height)

        # Draw Clip Name in the header
        
        font = painter.font()
        font.set_pixel_size(10)
        painter.set_font(font)

        # painter.set_pen(QColor(0, 0, 0, 60))
        # painter.draw_text(header_rect.adjusted(6, 2, 0, 0), Qt.AlignLeft, self.clip.name)

        # painter.set_pen(self.text_color)
        # painter.draw_text(header_rect.adjusted(5, 1, 0, 0), Qt.AlignLeft, self.clip.name)

        # Outline
        painter.set_brush(Qt.NoBrush)

        pen = QPen(QColor(0, 0, 0), 1)
        pen.set_cosmetic(True)
        if self.is_selected():
           pen.set_color(QColor(255, 255, 255))

        painter.set_pen(pen)
        painter.draw_rect(rect.adjusted(0, 0, -0.1, -0.1))

    def paint(self, painter, option, widget):
        self.draw_background(painter)
        self.draw_foreground(painter)

    def item_change(self, change, value):
        if change == QGraphicsItem.ItemSelectedChange:
            if value:
                self.setZValue(1)
            else:
                self.setZValue(-1)
        
        #print(self.view().scale().x())
        #self.header_heigt = 16 / self.view().scale().x()


        if change == QGraphicsItem.ItemPositionChange:
            #channel = round(value.y() / self.channel_size)
            return QPointF(value.x(), 0)

        return super().item_change(change, value) 

    def image(self, frame_index):
        return self.cache.get(self.frame_current(frame_index))

    def frame_current(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_current

        return (frame_index - self.clip.edit_frame_start)

    def second_current(self, second_current=None):
        if second_current is None:
            second_current = ctx.second_current
        
        return (second_current - self.clip.edit_frame_start)


# class AudioClipSignals(QObject):
#     waveform_updated = Signal()


class VideoClipItem(ClipItem):
    """Widget holding a video clip and cache"""
    
    type = 'video'
    #frame_decoded = Signal(int, QImage)

    def __init__(self, video_clip, parent=None):
        super().__init__(clip=video_clip)

        self.thumbnails = {}
        self.cached_frame = None # The active frame is kept in memory for fast tweak

    def closest_thumbnail(self, value):

        thumbnail_indexes = sorted(self.thumbnails.keys())
        index = np.searchsorted(thumbnail_indexes, value, side="left")
        if index == len(thumbnail_indexes):
            index = -1
        
        #print(len(thumbnail_indexes))
        thumbnail_index = thumbnail_indexes[index]
        return self.thumbnails[thumbnail_index]#[thumbnail_indexes[index]]

    def draw_background(self, painter):
        super().draw_background(painter)

        header_height = self.header_scene_height()
        if not self.thumbnails or self.height() < header_height:
            return

        painter.set_render_hint(QPainter.SmoothPixmapTransform, True)
        painter.set_clip_rect(self.rect()) 

        # Draw waveform
        #painter.set_render_hint(QPainter.Antialiasing, False)
        thumbnail = self.thumbnails[0]
        thumbnail_height = self.height()
        thumbnail_ratio = thumbnail_height / thumbnail.height() / self.horizontal_scaling() * self.vertical_scaling()
        thumbnail_width = thumbnail.width() * thumbnail_ratio

        thumbnail_y = 0
        if self.show_header:
            thumbnail_y = header_height 

        #print(len(self.thumbnails))
        #print(self.thumbnails)

        #nb_thumbnail = floor(self.width() / (thumbnail_width+1))
        thumbnail_lines = []
        for thumbnail_index in np.arange(0, self.width(), thumbnail_width):
            thumbnail = self.closest_thumbnail(thumbnail_index)

            thumbnail_rect = QRectF(thumbnail_index, thumbnail_y, thumbnail_width, thumbnail_height)

            #print(rect)
            painter.draw_image(thumbnail_rect, thumbnail)

            thumbnail_lines.append(QLineF(thumbnail_index, 0, thumbnail_index, self.height()))
        
        pen = QPen(QColor(0, 0, 0, 128), 1,  Qt.SolidLine, Qt.FlatCap)
        pen.set_cosmetic(True)

        painter.set_brush(Qt.NoBrush)
        painter.set_pen(pen)

        painter.draw_lines(thumbnail_lines)

        '''
        for frame_index, image in self.thumbnails.items():
            image_height = self.height()
            image_y = 0
            if self.show_header:
                image_height -= header_height
                image_y = header_height 

            
            rect = QRectF(frame_index, image_y, image.width()*ratio, image_height)

            #print(rect)
            painter.draw_image(rect, image)

        '''
# class AudioClipSignals(QObject):
#     waveform_updated = Signal()


class AudioClipItem(ClipItem):
    """Widget holding a audio clip and cache"""
    
    type = 'audio'
    #waveform_updated = Signal()
    #frame_decoded = Signal(int, QImage)

    def __init__(self, audio_clip, parent=None):
        super().__init__(clip=audio_clip)

        self.color = QColor(68, 128, 128)
        self.waveform_color = QColor(150, 150, 150)
        self.waveform = []
        self.cache = QByteArray()

        
        #self.set_brush(QBrush(QColor(68, 128, 128)))

    def draw_background(self, painter):
        super().draw_background(painter)

        if not self.waveform or self.height() < self.header_scene_height():
            return

        painter.save()

        # Draw waveform
        painter.set_render_hint(QPainter.Antialiasing, False)

        #waveform_center = self.header_height + (self.rect().height()-self.header_height) * 0.5
        #stream = self.clip.stream

        #points = [
        #    QPointF(ctx.timeline_widget.second_to_frame(stream.pts_to_second(pts)), waveform_center + y*100) 
        #    for pts, y in list(self.preview_cache.items())]
        
        painter.set_brush(Qt.NoBrush)
        pen = QPen(self.waveform_color, 1,  Qt.SolidLine, Qt.FlatCap)
        pen.set_cosmetic(True)

        painter.set_pen(pen)


        #painter.translate(QPointF(0, waveform_center))
        scale = self.rect().height() #/ 50
        offset = 1 * self.vertical_scaling()

        #print('--- ', scale)

        #painter.scale((self.rect().width()-1.5)/(self.rect().width()), scale)
        #painter.translate(QPointF(0, self.rect().height()))
        #painter.translate(QPointF(0.75, self.rect().height()/scale -0.5))

        painter.translate(QPointF(0, self.rect().height()))
        painter.scale(1, -scale)

        #print(len(self.waveform))
        #print(self.screen_width())
        padding = int(len(self.waveform) / self.width_screen() *0.75)

        padding = max(1, padding)
        #print('-----', padding, len(self.waveform))
        #print(self.screen_width(), len(self.waveform))
        #print(padding)
        #print(self.screen_width(), len(self.waveform[::padding]))
        #print(self.waveform[::padding])
        #painter.draw_lines(self.waveform[::padding])

        painter.draw_lines(self.waveform[::padding])

        painter.restore()



class SubtitleClipItem(ClipItem):
    """Widget holding a subtitle clip and cache"""
    
    type = 'subtitle'
    #frame_decoded = Signal(int, QImage)

    def __init__(self, subtitle_clip, parent=None):
        super().__init__(clip=subtitle_clip)

        self.color = QColor(48, 68, 68)
        #self.set_brush(QBrush(QColor('48, 68, 68')))