
from PySide6.QtWidgets import (QSlider, QWidget, QGraphicsView, QGraphicsScene, 
QGraphicsRectItem, QGraphicsTextItem, QFrame, QGraphicsItem)
from PySide6.QtCore import (QTimeLine, Qt, QRect, QRectF, Signal, QLine, QTimer, QPoint, QPointF)
from PySide6.QtGui import (QColor, QPainter, QBrush, QPen, QPixmap, QShortcut,
    QKeySequence)
from __feature__ import snake_case##, true_property

from loop import ctx, signals, app

from math import ceil, floor, log2, log
from operator import itemgetter
from itertools import groupby
import logging
import time


# class Scene()

class ChannelScene(QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        signals.frame_end_changed.connect(self.update_background)

        self.show_buffer = True
        self.show_section = True

        self.background_color = QColor(15, 15, 15)
        self.range_color = QColor(23, 23, 23)

        self.buffer_color = QColor(71, 200, 71, 128)
        self.accent_color = QColor(56, 100, 159)#

    def update_background(self):
        #print('Ivalidata')
        #print('update_background')

        self.set_scene_rect(-1000, 0, ctx.frame_end + 2000, 10)

        self.invalidate(self.scene_rect(), QGraphicsScene.BackgroundLayer)
        self.set_background_brush(QBrush(self.range_color))


class ChannelView(QGraphicsView):
    def __init__(self, parent=None, scene=None):
        super().__init__(parent=parent)


        self.set_scene(ChannelScene(self))

        self.set_cache_mode(QGraphicsView.CacheBackground)

        #self.set_auto_fill_background(False)

        self.set_alignment(Qt.AlignTop|Qt.AlignLeft)

        #self.set_viewport_update_mode(QGraphicsView.FullViewportUpdate)

        self.set_horizontal_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        self.set_vertical_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        
        self.set_frame_style(QFrame.NoFrame)

        self.set_minimum_height(20)

        signals.frame_end_changed.connect(lambda : self.fit_view())

    #def map_to_screen(self, value):
    #    return self.view().map_from_scene(QPoint(value, 0)).x()

    def vertical_scaling(self):
        return self.transform().m22()

    def horizontal_scaling(self):
        return self.transform().m11()

    def text_width(self, widget, text):
        return widget.font_metrics().bounding_rect(str(text)).width() + 2
    
    def range_padding(self):
        return self.text_width(self, ctx.frame_end) #+ self.range_margin

    def fit_view(self):
        view_padding = self.range_padding()
        ratio = ctx.frame_end / (self.width() + 2*view_padding)

        #padding = self.range_padding() / self.horizontal_scaling()
        screen_padding = view_padding * ratio

        rect = QRect(-screen_padding, 0, ctx.frame_end+2*screen_padding, 100)

        self.fit_in_view(rect)

    def resize_event(self, event):
        super().resize_event(event)
        self.fit_view()