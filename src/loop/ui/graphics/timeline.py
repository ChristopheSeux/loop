
from PySide6.QtWidgets import (QSlider, QWidget, QGraphicsView, QGraphicsScene, 
QGraphicsRectItem, QGraphicsTextItem, QFrame, QGraphicsItem)
from PySide6.QtCore import (QTimeLine, Qt, QRect, QRectF, Signal, QLine, QTimer, QPoint, QPointF)
from PySide6.QtGui import (QColor, QPainter, QBrush, QPen, QPixmap, QShortcut,
    QKeySequence)
from __feature__ import snake_case##, true_property

from loop import ctx, signals, app

from math import ceil, floor, log2, log
from operator import itemgetter
from itertools import groupby
import logging
import time


# class Scene()

class ChannelScene(QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent=parent)


class ChannelView(QGraphicsView):
    def __init__(self, parent=None, scene=None):
        super().__init__(parent=parent)


        self.set_scene(ChannelScene(self))

        self.set_cache_mode(QGraphicsView.CacheBackground)

        #self.set_auto_fill_background(False)

        self.set_alignment(Qt.AlignTop|Qt.AlignLeft)

        #self.set_viewport_update_mode(QGraphicsView.FullViewportUpdate)

        self.set_horizontal_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        self.set_vertical_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        
        self.set_frame_style(QFrame.NoFrame)


class TimelineScene(QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
    
        self.show_buffer = True
        self.show_section = True

        self.background_color = QColor(15, 15, 15)
        self.range_color = QColor(23, 23, 23)

        #self.set_auto_fill_background(False)

        #self.set_background_brush(QBrush(self.background_color))

        self.buffer_color = QColor(71, 200, 71, 128)#
        #self.accent_color = QColor(255, 255, 255)#71, 114, 179
        self.accent_color = QColor(56, 100, 159)#

        self.main_sections = tuple()
        self.in_between_sections = tuple()

        self.section_space = 100
        #self.number_section = 0
        self.section_ratio = 1

        self.range_margin = 4
        self.range_height = 0
        self.frame_width = 1

        #signals.buffer_changed.connect(self.update_foreground)
        signals.frame_end_changed.connect(self.update_background)

        #self.set_scene_rect(-1000, )

    def view(self):
        views = self.views()
        if views:
            return views[0]

    def vertical_scaling(self):
        return self.view().vertical_scaling()

    def horizontal_scaling(self):
        return self.view().horizontal_scaling()

    def update_background(self):
        #print('Ivalidata')
        #print('update_background')

        self.set_scene_rect(-1000, 52.5, ctx.frame_end + 2000, 260)

        self.invalidate(self.scene_rect(), QGraphicsScene.BackgroundLayer)
        self.set_background_brush(QBrush(self.range_color))
        #self.update()

    def update_foreground(self):
        #return
        #print('Ivalidata')
        self.invalidate(self.scene_rect(), QGraphicsScene.ForegroundLayer)


    def draw_range(self, painter):
        painter.set_pen(Qt.NoPen)

        #origin = self.map_to_scene(QPoint(0, 0))
        #range_padding = self.range_padding()

        # Draw frame range lighter
        painter.set_brush(QBrush(self.range_color, Qt.SolidPattern))
        painter.set_pen(Qt.NoPen)
        painter.draw_rect(
            0, 0, 
            ctx.frame_end, 1000
        )

        '''

        # Draw frame checker
        painter.set_brush(QBrush(QColor(255, 255, 255, 10), Qt.SolidPattern))
        painter.set_pen(QPen(QColor(0, 0, 0, 75), 1))
        if self.frame_width >= 3:
            for i in range(ctx.nb_frames):
                if i%2 != 0:
                    continue
                x = i*self.frame_width + self.range_padding() - self.frame_width*0.5
                painter.draw_rect(
                    x, -1, 
                    self.frame_width, self.range_height+2
                )
        '''

    def draw_sections(self, painter):

        if self.frame_width >= 3:
            return

        # Draw black section line for first and end frame
        painter.set_pen(QPen(QColor(0, 0, 0), 1))
        line_start = QLine(
            self.range_padding(), 0, 
            self.range_padding(), self.height()
        )
        line_end = QLine(
            self.width()-self.range_padding(), 0, 
            self.width()-self.range_padding(), self.height()
        )

        painter.draw_lines((line_start, line_end))

        main_section_lines = tuple(QLine(x, 0, x, self.height()) for i, x in self.main_sections)
        painter.set_pen(QPen(QColor(0,0,0, 128), 1))
        painter.draw_lines(main_section_lines)

        in_between_section_lines = tuple(QLine(x, 0, x, self.height()) for i, x in self.in_between_sections)
        painter.set_pen(QPen(QColor(0,0,0, 80), 1))
        painter.draw_lines(in_between_section_lines)

    def get_buffer_ranges(self):
        """Get a range list of contigus buffered frames"""

        buffer_ranges = [] 

        cached_frames = sorted(ctx.video_surface.cache.keys())

        # Loop through the list of ints, and break it into lists of contiguous ints
        for k, g in groupby(enumerate(cached_frames), key=lambda x: x[0]-x[1]):
            buffer_range = list(map(itemgetter(1), g))
            buffer_ranges.append([buffer_range[0], buffer_range[-1]])

        return buffer_ranges


    def draw_buffer(self, painter):
        buffer_color = QColor(self.accent_color)
        buffer_color.set_alpha(200)

        painter.set_brush(buffer_color)
        painter.set_pen(Qt.NoPen)

        #range_ratio = self.frame_width
        buffer_height = 0

        rects = []
        for buffer_range in self.get_buffer_ranges():
            rect = QRectF(
                buffer_range[0], 0,
                buffer_range[-1]-buffer_range[0], 70
            )

            rects.append(rects)
            painter.draw_rect(rect)

        #print(f'Draw {len(rects)} Rects')
        # buffer_rects = tuple(
        #     QRectF(
        #         buffer_range[0], 0,
        #         buffer_range[-1]-buffer_range[0], 10
        #     )
        #     for buffer_range in self.get_buffer_ranges()
        # )
        # print(buffer_lines)
        # painter.draw_lines(buffer_lines)

    def draw_cursor(self, painter, rect):
        #return
        #range_width = ctx.clip_editor.range_width()
        #range_padding = ctx.clip_editor.range_padding()

        #range_ratio =  range_width / (ctx.nb_frames-1)

        #x = rect.x() + ctx.frame_current * range_ratio + range_padding

        # Draw line of Current frame
        painter.set_render_hint(QPainter.Antialiasing, False)
        painter.set_brush(Qt.NoBrush)

        pen = QPen(self.accent_color, 2, Qt.SolidLine, Qt.FlatCap)
        pen.set_cosmetic(True)
        painter.set_pen(pen)

        painter.draw_line(ctx.frame_current, 0, ctx.frame_current, self.height())

    def draw_background(self, painter, rect):
        #print('draw_background')
        
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.draw_rect(self.scene_rect())


        self.draw_range(painter)
        

        #print(rect)
        #print(QRect(rect)
        #painter.draw_pixmap(rect.to_rect(), self.pixmap_cache)
        #self.draw_range(painter)
        #self.draw_sections(painter)

        #if self.show_buffer:
        #    self.draw_buffer(painter)

    def draw_foreground(self, painter, rect):
        #print('draw_foreground')

        if self.show_buffer:
            #print('Draw Buffer')
            self.draw_buffer(painter)

        self.draw_cursor(painter, rect.to_rect())  


class TimelineView(QGraphicsView):
    resizing = Signal()

    def __init__(self, parent=None, scene=None):
        super().__init__(parent=parent)


        self.set_scene(scene)

        self.set_cache_mode(QGraphicsView.CacheBackground)

        #self.set_auto_fill_background(False)

        self.set_alignment(Qt.AlignTop|Qt.AlignLeft)

        #self.set_viewport_update_mode(QGraphicsView.FullViewportUpdate)

        self.set_horizontal_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        self.set_vertical_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        
        self.set_frame_style(QFrame.NoFrame)
        
        self.show_buffer = True
        self.show_section = True

        self.background_color = QColor(15, 15, 15)
        self.range_color = QColor(35, 35, 35)

        self.buffer_color = QColor(71, 200, 71, 128)#
        #self.accent_color = QColor(255, 255, 255)#71, 114, 179
        self.accent_color = QColor(56, 100, 159)#

        self.main_sections = tuple()
        self.in_between_sections = tuple()

        self.section_space = 100
        #self.number_section = 0
        self.section_ratio = 1

        self.range_margin = 4
        self.range_height = 0
        self.frame_width = 1

        self.unit = 'frame'

        self.set_minimum_height(32)
        #self.pixmap_cache = QPixmap()

        #self.set_background_brush(QBrush(self.background_color))
        #self.buffering.mode = 'forward'
        #self.is_play_reversed = False

        #self.fps = 25
        # self.timer = QTimer()
        # self.timer.set_timer_type(Qt.PreciseTimer)
        # self.timer.timeout.connect(self.frame_change)

        #signals.frame_current_changed.connect(lambda: logging.debug(f'frame_change={self.frame_current}'))
        #signals.frame_current_changed.connect(lambda : self.viewport().update())
        #signals.buffer_changed.connect(lambda : self.repaint())
        

        signals.frame_end_changed.connect(lambda : self.fit_view())
        #signals.clip_added.connect(lambda : self.fit_view())
        #signals.frame_end_changed.connect(lambda : self.adjust_size())
        #signals.frame_end_changed.connect(lambda : setattr(self, 'frame_end', ctx.frame_end))
        #signals.frame_end_changed.connect(self.compute_metrics)
        #self.resizing.connect(self.compute_metrics)

        #self.pixmap_cache = QPixmap(self.width(), self.height())
        #self.buffering.start()
        #self.time = time.perf_counter()

    @property
    def viewport(self):
        return super().viewport()

    @property
    def scene(self):
        return super().scene()

    def vertical_scaling(self):
        return self.transform().m22()

    def horizontal_scaling(self):
        return self.transform().m11()

    def fit_view(self):
        #rect = QRect(500, 0, ctx.frame_end+1000, 132)

        #print(self.width())

        view_padding = self.range_padding()
        ratio = ctx.frame_end / (self.width() + 2*view_padding)

        #padding = self.range_padding() / self.horizontal_scaling()
        screen_padding = view_padding * ratio

        rect = QRect(-screen_padding, 0, ctx.frame_end+2*screen_padding, 111)
        #self.set_scene_rect(0, 0, ctx.frame_end, 110)
        #print(rect)
        #self.translate(500, 0)
        self.fit_in_view(rect)

        #self.set_scene_rect(self.scene_rect().adjusted(0, -2, 0, 0))

        #self.repaint()

        #self.set_scene_rect(-200, 0, self.frame_size().width(), self.frame_size().height())

    # def show_event(self, event):
    #     super().show_event(event)

    #     print('SHOW EVENT')

    #     self.fit_view()

    def resize_event(self, event):
        super().resize_event(event)

        #self.scene.set_scene_rect(self.rect())

        #scale = ctx.frame_end / self.rect().width()

        #print(scale)
        #self.scale(scale, scale)
        #view_range_padding = ctx.timeline_widget.range_padding()
        #scene_range_padding = self.map_to_scene(QPoint(view_range_padding, 0)).x()
        #width = ctx.frame_end - 2* scene_range_padding

        self.fit_view()
        #self.scale(width, width)



        #scale = self.transform().m11() #Horizontal scaling
        #scale = self.transform().m22() #Vertical scaling
        #for item in self.scene.items():
        #    item.resize(scale)

        #self.pixmap_cache = QPixmap(self.width(), self.height())
        self.resizing.emit()
    

    def text_width(self, widget, text):
        return widget.font_metrics().bounding_rect(str(text)).width() + 2
    
    def range_padding(self):
        return self.text_width(self, ctx.frame_end) + self.range_margin
    '''
    def range_width(self):
        return self.width() - (2*self.range_padding())

    def compute_metrics(self):
        #logging.debug('compute_metrics')
        self.frame_end = ctx.frame_end

        rect = self.rect().translated(-self.range_padding(), -10)#.adjusted(0, 0, 0, 0)
        self.scene.set_scene_rect(rect)

        duration = ctx.nb_frames-1

        self.range_height = self.height()
        self.frame_width = self.range_width() / duration

        nb_frames = 2**(ceil(log(duration, 2)))
        range_width = nb_frames / duration * self.range_width()

        self.section_width = range_width
        while self.section_space < self.section_width and self.section_width > self.frame_width+1:# > self.frame_width:
            self.section_width *= 0.5
        
        nb_sections = int(range_width / self.section_width)
        self.nb_frames_section = max(1, int(nb_frames / nb_sections))

        #print("nb_sections", nb_sections)
        #print("duration", duration)
        #print("nb_sections", nb_sections)
        #print("nb_frames_section", self.nb_frames_section)

        self.main_sections = tuple((i * self.nb_frames_section, round(i*self.section_width) + self.range_padding()) for i in range(1, nb_sections))

        #self.show_sections = nb_sections < duration
        #print('show_sections', self.show_sections)
        if self.frame_width >= 3:
            self.in_between_sections = tuple((i * self.nb_frames_section,
                (i * self.section_width) + self.range_padding() + (self.section_width*0.5))
                for i in range(0, nb_sections)
            )
        else:
            self.in_between_sections =()

        #painter = QPainter(self.pixmap_cache)
        #self.draw_range(painter)
        #self.draw_sections(painter)
        #self.draw_sections_digit(painter)

        self.repaint()

    def text_width(self, widget, text):
        return widget.font_metrics().bounding_rect(str(text)).width() + 2

    
    def get_buffer_ranges(self):
        """Get a range list of contigus buffered frames"""

        buffer_ranges = [] 

        cached_frames = set(i for clip in ctx.clip_items for i in clip.cache)
        cached_frames = sorted(list(cached_frames))

        # Loop through the list of ints, and break it into lists of contiguous ints
        for k, g in groupby(enumerate(cached_frames), key=lambda x: x[0]-x[1]):
            buffer_range = list(map(itemgetter(1), g))
            buffer_ranges.append([buffer_range[0], buffer_range[-1]])

        return buffer_ranges

    def draw_range(self, painter):
        painter.set_pen(Qt.NoPen)

        #origin = self.map_to_scene(QPoint(0, 0))
        range_padding = self.range_padding()

        #draw Background
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.draw_rect(
            0-range_padding, -10, 
            self.width() + range_padding, self.height()
        )

        # Draw frame range lighter
        painter.set_brush(QBrush(self.range_color, Qt.SolidPattern))
        painter.set_pen(Qt.NoPen)
        painter.draw_rect(
            0, 0, 
            self.range_width(), self.range_height
        )

        # Draw frame checker
        painter.set_brush(QBrush(QColor(255, 255, 255, 10), Qt.SolidPattern))
        painter.set_pen(QPen(QColor(0, 0, 0, 75), 1))
        if self.frame_width >= 3:
            for i in range(ctx.nb_frames):
                if i%2 != 0:
                    continue
                x = i*self.frame_width + self.range_padding() - self.frame_width*0.5
                painter.draw_rect(
                    x, -1, 
                    self.frame_width, self.range_height+2
                )
    
    def draw_sections(self, painter):

        if self.frame_width >= 3:
            return

        # Draw black section line for first and end frame
        painter.set_pen(QPen(QColor(0, 0, 0), 1))
        line_start = QLine(
            self.range_padding(), 0, 
            self.range_padding(), self.height()
        )
        line_end = QLine(
            self.width()-self.range_padding(), 0, 
            self.width()-self.range_padding(), self.height()
        )

        painter.draw_lines((line_start, line_end))

        main_section_lines = tuple(QLine(x, 0, x, self.height()) for i, x in self.main_sections)
        painter.set_pen(QPen(QColor(0,0,0, 128), 1))
        painter.draw_lines(main_section_lines)

        in_between_section_lines = tuple(QLine(x, 0, x, self.height()) for i, x in self.in_between_sections)
        painter.set_pen(QPen(QColor(0,0,0, 80), 1))
        painter.draw_lines(in_between_section_lines)

    def draw_buffer(self, painter):
        buffer_color = QColor(self.accent_color)
        buffer_color.set_alpha(128)
        painter.set_pen(QPen(buffer_color, 4, Qt.SolidLine, Qt.FlatCap))

        range_ratio = self.frame_width
        buffer_lines = tuple(
            QLine(
                buffer_range[0]*range_ratio + self.range_padding(), 4,
                buffer_range[-1]*range_ratio + self.range_padding(), 4
            )
            for buffer_range in self.get_buffer_ranges()
        )
        painter.draw_lines(buffer_lines)

    def draw_cursor(self, painter, rect):

        range_ratio = self.range_width() / (ctx.nb_frames-1)

        x = rect.x() + ctx.frame_current * range_ratio + self.range_padding()

        # Draw line of Current frame 
        painter.set_pen(QPen(self.accent_color, 2, Qt.SolidLine, Qt.FlatCap))
        painter.draw_line(x, 0, x, self.height())

    def draw_background(self, painter, rect):

        #print(rect)
        #print(QRect(rect)
        #painter.draw_pixmap(rect.to_rect(), self.pixmap_cache)
        self.draw_range(painter)
        self.draw_sections(painter)

        if self.show_buffer:
            self.draw_buffer(painter)

    def draw_foreground(self, painter, rect):
        
        self.draw_cursor(painter, rect.to_rect())

    '''
    def draw_background(self, painter, rect):
        
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.draw_rect(self.scene_rect())

        super().draw_background(painter, rect)


