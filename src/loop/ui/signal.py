
from PySide6.QtCore import QObject, Signal
from pathlib import Path


class Signals(QObject) :
    launched = Signal()
    about_to_quit = Signal()

    connected = Signal()
    frame_current_changed = Signal()
    frame_start_changed = Signal()
    frame_end_changed = Signal()
    fps_changed = Signal()
    full_screen_toogled = Signal()
    clip_added = Signal()

    buffer_changed = Signal()
    files_dropped = Signal(list)

    frame_decoded = Signal(int, list, bool) # frame_index, frame_stack, add_to_cache
