

from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject)
from PySide6.QtGui import (QImage,)

from __feature__ import snake_case#, true_property

import av
import numpy as np
from math import ceil

from loop import ctx, signals
import time
import logging


# class RunnableSignals(QObject):
#     """
#     Defines the signals available from a running worker thread.
#     """
    
#     started = Signal()
#     finished = Signal()
#     error = Signal(str)
#     result = Signal(object)
#     progress = Signal(int)
#     updated = Signal()

class BufferingSignals(QObject):
    #finished = Signal()
    frame_decoded = Signal(int)
    started = Signal()
    finished = Signal()


class BaseBuffering(QRunnable):
    def __init__(self, buffering) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True) 
        self.signals = BufferingSignals()
        self.mode = 'base'
        self.is_running = True

    def connect(self, signal, callable, single_shot=False):
        signal = getattr(self.signals, signal)
        args = [callable]
        if single_shot:
            args += [Qt.SingleShotConnection]

        signal.connect(*args)

    def cache_frame(self, clip_item, index, frame):
        logging.debug(f'Frame Cached {index}')
        clip_item.cache[index] = frame
        signals.buffer_changed.emit()

    # def cache_video_frame(self, video_clip_item, index, frame):
    #     if frame is None:
    #         return
        
    #     clip = video_clip_item.clip

    #     w, h = clip.width, clip.height
    #     image = QImage(frame.to_ndarray(format='rgb24'), w, h,  QImage.Format_RGB888)

    #     logging.debug(f'Frame Cached {index}')
    #     video_clip_item.cache[index] = image
    #     signals.buffer_changed.emit()

    #     return image
    
    # def cache_audio_frame(self, audio_clip_item, index, frame):
    #     if frame is None:
    #         return
        
    #     dtype = audio_clip_item.clip.stream.dtype
    #     count = frame.samples
    #     audio_chunk = np.empty((count, len(frame.planes)), dtype=dtype)
    #     for i, plane in enumerate(frame.planes):
    #        audio_chunk[:, i] = np.frombuffer(plane, dtype=dtype, count=count)

    #     logging.debug(f'Frame Cached {index}')
    #     audio_clip_item.cache[index] = audio_chunk
    #     signals.buffer_changed.emit()

    #     #print(audio_chunk)

    #     return audio_chunk

    def stop(self):
        self.is_running = False

    def active_clip_items(self):
        return [clip_item for clip_item in ctx.clip_items if clip_item.type=='video']

        return [
            clip_item for clip_item in ctx.clip_items if clip_item.type=='video' and
            (clip_item.clip.edit_frame_start >= ctx.frame_current - (ctx.buffer_size*0.75) or
            clip_item.clip.edit_frame_end <= ctx.frame_current + (ctx.buffer_size*0.75))
        ]

    def process_audio_frames(self, clip_item, frame_index, frames):
        """Cut audio frames to have the same length as a video frame"""

        stream = clip_item.clip.stream

        video_second_start = ctx.timeline_widget.frame_to_second(frame_index)
        video_second_end = video_second_start + (1 / ctx.fps)

        audio_second_start = clip_item.clip.stream.pts_to_second(frames[0].pts)
        audio_second_end = clip_item.clip.stream.pts_to_second(frames[-1].pts)

        cut_start = int((video_second_start - audio_second_start) / ctx.fps)
        cut_end = ceil((video_second_end - audio_second_end) / ctx.fps)

        #buffer[:, frame.pts:frame.pts + frame.samples] = np_frame

        audio_chunk = np.empty((stream.nb_channels, sum(f.samples for f in frames)), dtype=stream.dtype)
        sample_index = 0
        for frame in frames:
            for i, plane in enumerate(frame.planes):
                audio_chunk[i:i+1, sample_index: sample_index+frame.samples] = np.frombuffer(plane, dtype=stream.dtype, count=frame.samples)
            
            sample_index += frame.samples

        audio_chunk = audio_chunk[:, cut_start: -cut_end]

        #print(frame)
        #second = clip_item.clip.stream.pts_to_second(frame.pts)
        #frame_index = ctx.timeline_widget.second_to_frame(second)
        #logging.debug(f'process frame: {frame_index}'  )

        #frame_index = clip_item.clip.stream.second_to_frame(second)
        self.cache_frame(clip_item, frame_index, audio_chunk)
        self.discard_frame(clip_item)
        self.signals.frame_decoded.emit(frame_index)

    def process_video_frame(self, clip_item, frame):
        clip = clip_item.clip
        #print(frame)
        second = clip.stream.pts_to_second(frame.pts)
        frame_index = ctx.timeline_widget.second_to_frame(second)
        logging.debug(f'process frame: {frame_index}'  )

        w, h = clip.width, clip.height
        image = QImage(frame.to_ndarray(format='rgb24'), w, h,  QImage.Format_RGB888)

        self.cache_frame(clip_item, frame_index, image)
        self.discard_frame(clip_item)
        self.signals.frame_decoded.emit(frame_index)


class ForwardBuffering(BaseBuffering):
    def __init__(self, buffering) :
        super().__init__(buffering)

        self.mode = 'forward'

    def discard_frame(self, video_clip_item):
        cache = video_clip_item.cache
        frame_current = video_clip_item.frame_current()

        if len(cache) > ctx.buffer_size: #Delete the most far away frame
            frame_index = min(cache)
            if frame_index >= frame_current-1:
                frame_index = max(cache)

            logging.debug(f'Remove from buffer: {frame_index}')
            del cache[frame_index]               
            signals.buffer_changed.emit()

    def buffer_video(self):
        pass

    def buffer_audio(self, clip_item):
        clip = clip_item.clip
        stream = clip.stream
        cache = clip_item.cache
        frame_current = clip_item.frame_current() # Frame Global converted to clip time
        buffer_size = int(ctx.buffer_size*0.25)

        #stream_frame_index = ctx.timeline_widget.frame_to_second(forward_index)
        #stream_frame

        # Find first forward frame not in cache
        forward_index = next((frame_current+i for i in range(buffer_size) if frame_current+i not in cache), None)
        if forward_index is None or forward_index >= clip.frame_end: # The cache is full
            logging.debug('The cache is full')
            return

        # If the stream frame index is missplaced, need seeking
        #if stream.frame_index != forward_index-1:
        #    second_forward = ctx.timeline_widget.frame_to_second(forward_index)
        #    logging.debug(f'The stream index: {stream.frame_index} is missplaced, {forward_index}')

        second_forward = ctx.timeline_widget.frame_to_second(forward_index)
        stream.seek(second_forward)

        buffer_index = ceil(forward_index + buffer_size-(forward_index-frame_current))
        second_forward = ctx.timeline_widget.frame_to_second(buffer_index)


        audio_frames = []
        #frame_index = buffer_index
        for audio_frame in stream.decode_to(second_forward):
            frame_index = ctx.timeline_widget.second_to_frame(stream.current_time)
            #if frame_index in clip_item.cache:
            #    break

            if self.buffering.mode != 'forward':
                return self.signals.finished.emit()

            audio_frames.append(audio_frame)

            if frame_index != buffer_index:
                self.process_audio_frames(clip_item, frame_index, audio_frames)
                buffer_index = frame_index
                audio_frames.clear()




        self.signals.finished.emit()

    def run(self):
        self.signals.started.emit()
        buffer_size = int(ctx.buffer_size*0.25)

        #return self.signals.finished.emit()

        for clip_item in ctx.clip_items:
            if clip_item.type == 'audio':
                self.buffer_audio(clip_item)
                continue

            clip = clip_item.clip
            stream = clip.stream
            cache = clip_item.cache
            frame_current = clip_item.frame_current() # Frame Global converted to clip time

            # Find first forward frame not in cache
            forward_index = next((frame_current+i for i in range(buffer_size) if frame_current+i not in cache), None)
            if forward_index is None or forward_index >= clip.frame_end: # The cache is full
                logging.debug('The cache is full')
                continue

            # If the stream frame index is missplaced, need seeking
            if stream.frame_index != forward_index-1:
                second_forward = ctx.timeline_widget.frame_to_second(forward_index)
                logging.debug(f'The stream index: {stream.frame_index} is missplaced, {forward_index}')

                stream.seek_keyframe(second_forward)

                for video_frame in stream.decode_to(second_forward):
                    if self.buffering.mode != self.mode:
                        return self.signals.finished.emit()

                self.process_video_frame(clip_item, video_frame)

            logging.debug(f'{forward_index}, {forward_index}')

            buffer_index = ceil(forward_index + buffer_size-(forward_index-frame_current))
            second_forward = ctx.timeline_widget.frame_to_second(buffer_index)
            for video_frame in stream.decode_to(second_forward):
                if stream.frame_index in clip_item.cache:
                    break
                self.process_video_frame(clip_item, video_frame)

                if self.buffering.mode != 'forward':
                    return self.signals.finished.emit()

        self.signals.finished.emit()
        #print(f'Forward Buffering finished', frame_index)


class BackwardBuffering(BaseBuffering):
    def __init__(self, buffering) :
        super().__init__(buffering)

        self.mode = 'backward'

    def discard_frame(self, video_clip_item):
        cache = video_clip_item.cache
        frame_current = video_clip_item.frame_current()

        if len(cache) > ctx.buffer_size:
            frame_index = max(cache)
            if frame_index <= frame_current+1:
                frame_index = min(cache)

            logging.debug(f'Remove from buffer: {frame_index}')
            del cache[frame_index]               
            signals.buffer_changed.emit()

    def run(self):
        self.signals.started.emit()

        buffer_size = int(ctx.buffer_size*0.75)

        for clip_item in self.active_clip_items():                    
            clip = clip_item.clip
            stream = clip.stream
            cache = clip_item.cache
            frame_current = clip_item.frame_current() # Frame Global converted to clip time

            backward_index = next((frame_current-i for i in range(buffer_size) if frame_current-i not in cache), None)
            if backward_index is None or backward_index < clip.frame_start:
                logging.debug('The cache is full')
                continue

            # Seek to previous keyframe
            #print("backward_index", backward_index)
            second_backward = ctx.timeline_widget.frame_to_second(backward_index)
            stream.seek_keyframe(second_backward)

            for video_frame in stream.decode_to(second_backward):
                if stream.frame_index in clip_item.cache:
                    break
                self.process_video_frame(clip_item, video_frame)

                if self.buffering.mode != 'backward':
                    return self.signals.finished.emit()

        self.signals.finished.emit()
        #print(f'Backward Buffering finished', frame_index)


class RandomBuffering(BaseBuffering):
    def __init__(self, buffering) :
        super().__init__(buffering)
        
        #self.keyframe = keyframe
        #self.mode = 'keyframe' if keyframe else 'precise'
        #self.buffering.runnable_added.connect(self.stop)

    def discard_frame(self, video_clip_item):
        cache = video_clip_item.cache
        stream = video_clip_item.clip.stream
        frame_current = video_clip_item.frame_current()

        if len(cache) > ctx.buffer_size: 
            frame_index = min(cache)#max(cache, key=lambda x : abs(x-frame_current))
            if frame_index == frame_current or frame_index == stream.frame_index:
                frame_index = max(cache)

            logging.debug(f'Remove from buffer: {frame_index}')
            del cache[frame_index]               
            signals.buffer_changed.emit()

    def run(self):
        self.signals.started.emit()

        #print('RandomBuffering start', self.mode, self.keyframe)

        for clip_item in self.active_clip_items():
            frame_current = clip_item.frame_current()
            second_current = ctx.timeline_widget.frame_to_second(frame_current)

            clip = clip_item.clip
            stream = clip.stream

            # Avoid seeking if the frame is less than 30 frame away
            gop_size = stream.codec.gop_size
            clip_frame_index = stream.frame_index

            #print(stream.frame_index, frame_current)
            #print(gop_size == 1, stream.frame_index > frame_current+gop_size, stream.frame_index < frame_current-2*gop_size)
            if gop_size == 1 or clip_frame_index > frame_current+gop_size or clip_frame_index < frame_current-gop_size:
                stream.seek_keyframe(second_current)
            #else:
            #    print('\n----SEEKING AVOIDED')
            
            if self.mode == 'keyframe':
                video_frame = stream.decode_next_frame()

                #print('KEYFRAME DECODED', frame_index, )

                # Don't display image if the keyframe if far away than the current frame
                #print(frame_index, ctx.displayed_frame_index, frame_current)
                if abs(stream.frame_index - frame_current) > abs(ctx.displayed_frame_index - frame_current):
                    continue
            else:
                for video_frame in stream.decode_to(second_current):
                    if self.buffering.mode != self.mode:
                        return self.signals.finished.emit()

            self.process_video_frame(clip_item, video_frame)

        #print(f'Random Buffering finished, keyframe={self.keyframe}', frame_index)
        #print("")

        self.signals.finished.emit()

    
    def run(self):
        self.signals.started.emit()

        for clip_item in self.active_clip_items():
            frame_current = clip_item.frame_current()
            second_current = ctx.timeline_widget.frame_to_second(frame_current)

            clip = clip_item.clip
            stream = clip.stream

            # Avoid seeking if the frame is less than the gope size away
            gop_size = stream.codec.gop_size
            clip_frame_index = stream.frame_index

            if gop_size == 1 or clip_frame_index > frame_current+gop_size or clip_frame_index < frame_current:
                stream.seek_keyframe(second_current)

            for video_frame in stream.decode_to(second_current):
                if self.buffering.mode != self.mode or self is not self.buffering.queue[-1]:
                    break
            
            # Don't show frame if it's more far away than the current frame displayed
            if abs(stream.frame_index - frame_current) > abs(ctx.displayed_frame_index - frame_current):
                continue

            self.process_video_frame(clip_item, video_frame)

            #if not self.is_running:
            #    break

        self.signals.finished.emit()
            
            





class Buffering(QThreadPool) :
    #runnable_added = Signal()

    def __init__(self) :
        super().__init__()

        self.set_max_thread_count(1)
        self.mode = 'idle'
        self.supported_modes = ('forward', 'backward', 'random')
        self.queue = []

        signals.about_to_quit.connect(lambda : self.set_mode('idle'))

    def add(self, mode='forward', start=False):

        logging.debug(f'add buffer({mode}), mode={self.mode}')

        if mode not in self.supported_modes:
            raise Exception(f'Mode {mode} not supported, supported mode are {self.supported_modes}')

        if mode == self.mode and mode in ('forward', 'backward'):
            return

        logging.debug(f'Decode Mode: {mode}')

        if mode == 'forward':
            runnable = ForwardBuffering(self)
        elif mode == 'backward':
            runnable = BackwardBuffering(self)
        elif mode == 'random':
            runnable = RandomBuffering(self)
        else:
            raise Exception(f'Could not create runnable from mode {mode}')

        if start:
            self.start(runnable)

        return runnable

    def remove(self, runnable):
        if runnable in self.queue:
            self.queue.remove(runnable)

    def start(self, runnable):
        self.clear()

        runnable.connect('started', lambda : self.set_mode(runnable.mode))
        runnable.connect('finished', lambda : self.set_mode('idle'))
        runnable.connect('finished', lambda : self.remove(runnable))

        #print('Buffer Added', runnable)

        self.mode = runnable.mode
        self.queue.append(runnable)

        super().start(runnable)

        print('QUEUE LEN', len(self.queue))


    def set_mode(self, mode):
        self.mode = mode

    #def stop(self):



'''

class Buffering(QThread):
    frame_decoded = Signal((int, np.ndarray))

    def __init__(self):
        super().__init__()

        #self.video_streams = []#set()
        self.is_running = True
        self.buffering_mode = 'fast '
    
        #self.start()
    #def get_current_videos(self):
    #    return (v for v in ctx.edit.video_clips if v.edit_frame_start < ctx.frame_current < v.edit_frame_end)

    def stop(self) :
        self.is_running = False
        self.wait()

    def run(self):
        while self.is_running:

            #get all active video
            video_clip_items = ctx.active_video_clip_items#set(clip for clip in ctx.active_clip_items if clip.type=='video')

            #print('video_clip_items', video_clip_items)

            if not video_clip_items:
                time.sleep(0.015)
                continue
            
            #print(ctx.buffering_mode)

            for video_clip_item in video_clip_items:
                if ctx.buffering_mode == 'forward':
                    video_clip_item.forward_buffering()
                elif ctx.buffering_mode == 'backward':
                    video_clip_item.backward_buffering()
                elif ctx.buffering_mode == 'fast':
                    video_clip_item.random_buffering(precise=False)
                    ctx.timeline_widget.buffering_mode = 'forward'
                elif ctx.buffering_mode == 'precise':
                    video_clip_item.random_buffering(precise=True)
                    ctx.timeline_widget.buffering_mode = 'forward'

            if all(not wgt.is_buffering for wgt in video_clip_items):
                time.sleep(0.015)
                continue

'''