

from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject, QPointF, QLineF)
from PySide6.QtGui import (QImage, QPolygonF)

from __feature__ import snake_case#, true_property

import av
import numpy as np
from math import ceil

from loop import ctx, signals
import time
import logging

#from pydub import AudioSegment
#import io

# class RunnableSignals(QObject):
#     """
#     Defines the signals available from a running worker thread.
#     """
    
#     started = Signal()
#     finished = Signal()
#     error = Signal(str)
#     result = Signal(object)
#     progress = Signal(int)
#     updated = Signal()

class PreviewerSignals(QObject):
    #finished = Signal()
    frame_decoded = Signal(int)
    preview_updated = Signal()
    started = Signal()
    finished = Signal()


class BasePreviewer(QRunnable):
    def __init__(self, thread_pool, clip_item) :
        super().__init__()

        self.thread_pool = thread_pool
        self.clip_item = clip_item
        self.clip = clip_item.clip

        self.set_auto_delete(True) 
        self.signals = PreviewerSignals()

    def connect(self, signal, callable, single_shot=False):
        signal = getattr(self.signals, signal)
        args = [callable]
        if single_shot:
            args += [Qt.SingleShotConnection]

        signal.connect(*args)
    

class VideoPreviewer(BasePreviewer):
    def __init__(self, thread_pool, clip_item) :
        super().__init__(thread_pool, clip_item)

        self.resolution = 256
        self.second_sample = 10

    def run(self):
        self.signals.started.emit()


        stream = self.clip.stream.copy()
        t0 = time.perf_counter()
        #resampler = av.audio.resampler.AudioResampler('flt', 'mono', 4096)
        
        self.clip_item.thumbnails.clear()

        #i = 0

        #for packet in stream.container.demux(video=0):
        #    for frame in packet.decode():
        #seconds = (i*self.second_sample for i in range(int(stream.duration / self.second_sample)))
        
        #stream.stream.codec_context.skip_frame = "NONKEY"

        i = 1
        
        for second in np.arange(0, stream.duration, self.second_sample):

            stream.seek(second)

            frame = stream.decode_next_frame()
            #frame = frame.reformat(width=int(frame.width * self.resolution / frame.height), height=self.resolution)
            #frame = frame.reformat().to_ndarray(format='rgb24')
            image = QImage(frame.to_ndarray(format='rgb24'), frame.width, frame.height,  QImage.Format_RGB888)
            
            image_width = int(frame.width * self.resolution / frame.height)
            #image = image.scaled(image_width, self.resolution, Qt.IgnoreAspectRatio, Qt.SmoothTransformation)
            image = image.scaled(image_width, self.resolution)
            self.clip_item.thumbnails[stream.frame_index-1] = image

            #print(second)
            #if i % 10 == 0:
            #    #break
            #    self.clip_item.signals.preview_updated.emit()

                #return self.clip_item.signals.preview_updated.emit()

            i += 1
        
        self.signals.preview_updated.emit()
        self.signals.finished.emit()
        #print(f'Forward Buffering finished', frame_index)

        print("Preview Thumbnail took", time.perf_counter()-t0)

        


class AudioPreviewer(BasePreviewer):
    def __init__(self, thread_pool, clip_item) :
        super().__init__(thread_pool, clip_item)

        self.sampling = 10
        self.smooth_strength = 100

    def run(self):
        self.signals.started.emit()

        stream = self.clip.stream.copy()
        t0 = time.perf_counter()
        #resampler = av.audio.resampler.AudioResampler('flt', 'mono', 4096)
        
        self.clip_item.waveform.clear()

        #print('Start Audio Previewer', self.clip_item)
        audio_samples  = []#np.array(dtype=stream.dtype)
        sampling = self.sampling

        i = 0
        for packet in stream.container.demux(audio=0):
            #samples = 0
            for frame in packet.decode():

                #print(dir(frame), frame.time_base)
                frame_second = stream.pts_to_second(frame.pts)
                frame_samples = np.frombuffer(frame.planes[0], dtype=stream.dtype, count=frame.samples)
                #print('FRAME SIZE', len(value))
                #print(stream.stream.frame_size)
                #nb_samples = len(samples)
                #padding = int(frame.samples/self.sample_rate)
                #print(frame_samples)
                audio_samples.append(frame_samples[::sampling])
                #frame_samples = samples[::padding]
                #samples = np.absolute(samples) *50 +1

                #indexes = np.arange(0.0, frame.samples, padding)
                #indexes *= float(stream.stream.time_base) #in second
                #indexes += frame_second#float(frame.pts)
                #indexes *=  float(ctx.fps) #to frame index

                #print('----', len(indexes), len(samples))
                #waveform = += [(index, value) for index, value in zip(indexes, samples)]
                #self.clip_item.waveform += [QLineF(index, 0, index, -value) for index, value in zip(indexes, samples)]

            #if i %5000 == 0:
            #    self.clip_item.signals.preview_updated.emit()

            i += 1
        

        audio_samples = np.concatenate(audio_samples)
        audio_samples = np.absolute(audio_samples)

        # Smooth the samples
        kernel = np.ones(self.smooth_strength) / self.smooth_strength
        audio_samples = np.convolve(audio_samples, kernel, mode='same')

        # Normalise between 0 and 1
        audio_samples = audio_samples / np.max(audio_samples)

        #### Amplify small values
        #audio_samples += 1 / np.exp(audio_samples*10) *0.1
        amplification = 0.01
        audio_samples += -np.sqrt(audio_samples) *amplification + amplification
        audio_samples *= -np.sqrt(audio_samples) +2
        #\left(-\sqrt{\left(x\right)}\right)\cdot0.1+0.1
        #\frac{1}{\exp\left(x\cdot10\right)}\cdot0.1

        #print(stream.duration / float(ctx.fps))
        nb_frames = stream.duration * float(ctx.fps) -1
        
        indexes = np.linspace(0, nb_frames, len(audio_samples))
        #indexes = #np.arange(0, len(audio_samples)*self.sampling, self.sampling) #* float(stream.time_base) * float(ctx.fps)
        #print(ctx.frame_end)
        #print(max(indexes))
        #indexes *= float(stream.stream.time_base) #in second
        #indexes += frame_second#float(frame.pts)
        #indexes *=  float(ctx.fps) #to frame index

        self.clip_item.waveform = [QLineF(index, 0, index, value) for index, value in zip(indexes, audio_samples)]

        #print('Emit', self.clip_item, 'preview updated')
        self.signals.preview_updated.emit()
        #self.clip_item.signals.preview_updated.emit()
        '''
                for i in range(self.sample_rate):
                    sample_index = int(i/self.sample_rate * frame.samples)

                    continue
                    sample_value = samples[sample_index]


                    value = abs(sample_value) * 10
                    second = stream.pts_to_second(frame.pts) + stream.pts_to_second(sample_index)
                    frame_index = second * ctx.fps
                    #print(second)

                    line = QLineF(frame_index, 0, frame_index, -value)
                    self.clip_item.waveform.append(line)
                '''
            
            #point = QPointF(second, -value) 
            #self.clip_item.waveform_polygon.append(point)

            #plane = frame.planes[0]

            #print(plane)
            #print(dir(plane))
            #value = value.astype(np.float32) / np.iinfo(value.dtype).max
            #data = bytes(packet)
            #segment = AudioSegment.from_raw(io.BytesIO(data), format="aac", sample_width=stream.stream.frame_size, frame_rate=stream.stream.sample_rate, channels=2)


            #samples = segment.get_array_of_samples()

            # Convert the samples to a floating-point representation and normalize to [-1, 1]
            #samples = np.array(samples, dtype=np.float32) / np.iinfo(samples.dtype).max
            #value = samples[0]
            #value = np.frombuffer(frame.planes[0], dtype=float) * 32767
            #value = np.mean(value)
            #print(value)
            #break
            #print('value', value)
            
            #self.clip_item.preview_cache[frame.pts] = value
        # for chunk in self.clip.stream.remaining_chunks:
        #     #for sample in chunk.samples:
        #     #frame = resampler.resample(chunk)[0]
        #     #value = np.mean(frame.to_ndarray())
        #     pts = chunk.pts
        #     value = chunk.to_ndarray()[0][0]

        #     self.clip_item.preview_cache[pts] = value

        print("Reading waveform took", time.perf_counter()-t0)

        
        self.signals.finished.emit()
        #print(f'Forward Buffering finished', frame_index)


class ClipPreviewer(QThreadPool) :
    def __init__(self) :
        super().__init__()

        self.set_max_thread_count(2)

    def add(self, clip_item):

        logging.debug(f'add previewer {clip_item}')

        if clip_item.type == 'video':
            runnable = VideoPreviewer(self, clip_item)
        elif clip_item.type == 'audio':
            runnable = AudioPreviewer(self, clip_item)
        else:
            return

        self.start(runnable)

        return runnable

