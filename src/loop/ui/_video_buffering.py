

from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject)
from PySide6.QtGui import (QImage,)

from __feature__ import snake_case#, true_property

import av
import numpy as np
from math import ceil

from loop import ctx, signals
import time
import logging


# class RunnableSignals(QObject):
#     """
#     Defines the signals available from a running worker thread.
#     """
    
#     started = Signal()
#     finished = Signal()
#     error = Signal(str)
#     result = Signal(object)
#     progress = Signal(int)
#     updated = Signal()

class VideoDecoderSignals(QObject):
    started = Signal()
    finished = Signal()
    frame_decoded = Signal(int)


class VideoDecoder(QRunnable):
    def __init__(self, buffering) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True) 
        self.signals = VideoDecoderSignals()
        self.mode = 'base'
        self.is_running = True

    # def connect(self, signal, callable, single_shot=False):
    #     signal = getattr(self.signals, signal)
    #     args = [callable]
    #     if single_shot:
    #         args += [Qt.SingleShotConnection]

    #     signal.connect(*args)

    def stop(self):
        self.is_running = False

    def clip_items_in_range(self, frame_start, frame_end):
       return [ c for c in ctx.video_clip_items if frame_start <= c.clip.edit_frame_start < frame_end or
                frame_start <= c.clip.edit_frame_end < frame_end ]

    def clip_items_at(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_current
        
        #print('clip_items_at')
        #print([c.clip.name for c in ctx.video_clip_items if c.clip.edit_frame_start < frame_index <= c.clip.edit_frame_end])
        #print([(c.clip.name, c.clip.edit_frame_start, c.clip.edit_frame_end) for c in ctx.video_clip_items])

        return [c for c in ctx.video_clip_items if c.clip.edit_frame_start <= frame_index < c.clip.edit_frame_end]

    '''
    def seek_clip_to(self, clip_item, frame_index):
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)

        second = stream.frame_to_second(frame_index)
        stream.seek_keyframe(stream.frame_to_second(frame_index-1))
    def decode_clip_to(self, clip_item, frame_index):
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)

        if stream.frame_index == frame_index:
            yield stream.decode_next_frame()
        else:
            second = stream.frame_to_second(frame_index)
            for video_frame in stream.decode_to(second):
                yield video_frame
    '''

    '''
    def seek_clip_to(self, clip_item, frame_index):
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)

        if stream.frame_index == frame_index: # Already well placed
            return

        if frame_index <= 0:
            stream.start()
            return

        seek_second = stream.frame_to_second(frame_index-2)
        stream.seek_keyframe(seek_second)

        target_second = stream.frame_to_second(frame_index-1)
        for video_frame in stream.decode_to(target_second):
            #print(stream.frame_index)
            pass

    def decode_clip_to(self, clip_item, frame_index):
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)

        #print('Decode', clip.name, frame_index, stream.frame_index)

        #print('##########', stream.frame_index, frame_index)
        #if frame_index == stream.frame_index:
        #    print('No NEED DECODE')
        #"fjytjyffjhytjhtfdgg"    return
            #yield stream.decode_next_frame()

        if stream.frame_index == frame_index-1:
            yield stream.decode_next_frame()

        elif frame_index == 0:
            stream.start()
            yield stream.decode_next_frame()
        
        else:
            # If the stream frame index is missplaced, need seeking

            print('REEL SEEK to', frame_index-1)


            #stream.seek_keyframe(ctx.timeline_widget.frame_to_second(frame_index-1))

            #if stream.frame_index not in range(frame_index-20, frame_index):
            #    print('REEL SEEK', stream.frame_index, frame_index)
            #    logging.debug(f'The stream index: {stream.frame_index} is missplaced, {frame_index}')
            #    stream.seek_keyframe(stream.frame_to_second(frame_index-1))

            stream.seek_keyframe(stream.frame_to_second(frame_index-2))
            second = stream.frame_to_second(frame_index)
            for video_frame in stream.decode_to(second):
                print(stream.frame_index)
                yield video_frame
                #if self.buffering.mode != self.mode:
                #    break

        #yield video_frame
        '''

    def decode_clip_to(self, clip_item, frame_index):
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)
        second = stream.frame_to_second(frame_index)

        print('decode_clip_to', stream.frame_index, frame_index)

        for video_frame in stream.decode_to(second):
            #print(stream.frame_index)
            yield video_frame

    def seek_clip_to(self, clip_item, frame_index):
        '''Place The stream index if necessary'''
        clip = clip_item.clip
        stream = clip.stream

        frame_index = clip_item.frame_current(frame_index)

        #if stream.frame_index == frame_index: # Already well placed
        #    return

        print('seek_clip_to', 'frame_index', frame_index)

        if frame_index <= 0:
            stream.start()
            return

        #if stream.frame_index not in range(frame_index-20, frame_index):
        #    seek_second = stream.frame_to_second(frame_index-1)
        #    stream.seek_keyframe(seek_second)

        print('seek clip to', frame_index-1)
        seek_second = stream.frame_to_second(frame_index-1)
        stream.seek_keyframe(seek_second)


        #target_second = stream.frame_to_second(frame_index)
        #for video_frame in stream.decode_to(target_second):
        #    yield video_frame


class ForwardVideoDecoder(VideoDecoder):
    def __init__(self, buffering) :
        super().__init__(buffering)

        self.mode = 'forward'

    def discard_image(self, cached_index):
        if not ctx.video_surface.is_cache_full():
            return

        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current
        buffer_size = int(ctx.buffer_size*0.75)

        frame_index = max(cache)
        if frame_index < frame_current + buffer_size or frame_index == cached_index:


            frame_index = min(cache)
        #if frame_index >= frame_current-buffer_size:
        #    frame_index = max(cache)

        ctx.video_surface.discard_image(frame_index)

    def run(self):
        self.signals.started.emit()
        buffer_size = int(ctx.buffer_size*0.25)
        cache = ctx.video_surface.cache

        forward_index = next((ctx.frame_current+i for i in range(buffer_size) if ctx.frame_current+i not in cache), None)
        #print('forward_index', forward_index)

        if forward_index is None:
            #print('Forward index is None')
            return self.signals.finished.emit()

        #Check is the clips need seeking
        for clip_item in self.clip_items_at(forward_index):
           self.seek_clip_to(clip_item, forward_index-2)
           for _ in self.decode_clip_to(clip_item, forward_index-1):
               if self.buffering.mode != self.mode:
                   return self.signals.finished.emit()

            #print('REEL SEEK', stream.frame_index, frame_index)

        #for clip_item in self.clip_items_at(ctx.frame_current):
        #    self.seek_clip_to(clip_item, forward_index)


        #print([c.clip.name for c in self.clip_items_at(31)])

        print(forward_index, forward_index)

        for frame_index in range(forward_index, ctx.frame_current + buffer_size):
            #if frame_index in ctx.video_surface.cache:
            #    continue
                #print()
            
            #print('Buffer Frame index', frame_index)

            frame_stack = []


            for clip_item in self.clip_items_at(frame_index):
                video_frame = None
                
                #print()
                for video_frame in self.decode_clip_to(clip_item, frame_index):
                    if self.buffering.mode != self.mode:
                        return self.signals.finished.emit()

                if not video_frame:
                    print('NO VIDEO FRAME', clip_item.clip.name)
                    continue
                    #return self.signals.finished.emit()

                frame_stack.append((clip_item, video_frame))
                
                #ctx.audio_player.mix_down(audio_stack, frame_index)
            #ctx.video_surface.compose(frame_stack, frame_index)

            
            
            image = ctx.video_surface.decode_frames(frame_stack)
            ctx.video_surface.cache_image(frame_index, image)

            self.discard_image(frame_index)
            self.signals.frame_decoded.emit(frame_index)

        self.signals.finished.emit()
        #print(f'Forward Buffering finished', frame_index)


class BackwardVideoDecoder(VideoDecoder):
    def __init__(self, buffering) :
        super().__init__(buffering)

        self.mode = 'backward'

    '''
    def run(self):
        self.signals.started.emit()

        buffer_size = int(ctx.buffer_size*0.75)
        
        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current
        backward_index = next((frame_current-i for i in range(buffer_size) if frame_current-i not in cache), None)

        if backward_index is None:
            logging.debug('The cache is full')
            self.signals.finished.emit()
            return

        second_backward = ctx.timeline_widget.frame_to_second(backward_index)

        for clip_item in self.clip_items_at():                    
            clip = clip_item.clip
            stream = clip.stream
            clip_frame_index = clip_item.frame_current(backward_index) # Frame Global converted to clip time

            # TODO Check if really working
            #if clip_frame_index < clip.frame_start

            backward_index = next((frame_current-i for i in range(buffer_size) if frame_current-i not in cache), None)
            if backward_index is None or backward_index < clip.frame_start:
                logging.debug('The cache is full')
                continue

            # Seek to previous keyframe
            #print("backward_index", backward_index)
            second_backward = ctx.timeline_widget.frame_to_second(backward_index)
            stream.seek_keyframe(second_backward)

            for video_frame in stream.decode_to(second_backward):
                if stream.frame_index in clip_item.cache:
                    break
                self.process_video_frame(clip_item, video_frame)

                if self.buffering.mode != 'backward':
                    return self.signals.finished.emit()

        self.signals.finished.emit()
        #print(f'Backward Buffering finished', frame_index)
    '''
    def discard_image(self, cached_index):
        return

        if not ctx.video_surface.is_cache_full():
            return

        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current
        buffer_size = int(ctx.buffer_size*0.75)

        frame_index = min(cache)
        if frame_index >= frame_current-buffer_size or frame_index == cached_index:
            frame_index = max(cache)

        ctx.video_surface.discard_image(frame_index)

    def run(self):
        self.signals.started.emit()
        buffer_size = int(ctx.buffer_size*0.75)
        cache = ctx.video_surface.cache



        #print(sorted(ctx.video_surface.cache.items()))

        # Align video decode cursor
        backward_index = ctx.frame_current-buffer_size #next((ctx.frame_current-i for i in range(buffer_size) if ctx.frame_current-i not in cache), None)
        for clip_item in self.clip_items_in_range(backward_index, ctx.frame_current):
            self.seek_clip_to(clip_item, backward_index-2)
            for _ in self.decode_clip_to(clip_item, backward_index-1):
                if self.buffering.mode != self.mode:
                    return self.signals.finished.emit()

            #for frame in self.decode_clip_to(clip_item, backward_index-1):
            #    pass
            


        #print('+++++Backward Buffering', backward_index)


        for frame_index in range(backward_index, ctx.frame_current+1):

            #print(frame_index)

            if frame_index in ctx.video_surface.cache:
                continue
            
            #print('Backward Buffer Frame index', frame_index)

            frame_stack = []

            for clip_item in self.clip_items_at(frame_index):
                video_frame = None
                for video_frame in self.decode_clip_to(clip_item, frame_index):
                    if self.buffering.mode != self.mode:
                        print('Changing Mode')
                        return self.signals.finished.emit()

                frame_stack.append((clip_item, video_frame))

                #print(clip_item.clip.stream.frame_index)
                
                #ctx.audio_player.mix_down(audio_stack, frame_index)
            #print(frame_stack)

            image = ctx.video_surface.decode_frames(frame_stack)
            ctx.video_surface.cache_image(frame_index, image)
            self.discard_image(frame_index)


            self.signals.frame_decoded.emit(frame_index)


        self.signals.finished.emit()
        #print(f'Forward Buffering finished', frame_index)


class RandomVideoDecoder(VideoDecoder):
    def __init__(self, buffering) :
        super().__init__(buffering)
    
    '''
    def run(self):
        self.signals.started.emit()

        frame_stack = []
        frame_index = ctx.frame_current
        for clip_item in self.clip_items_at():
            frame_current = clip_item.frame_current()
            second_current = ctx.timeline_widget.frame_to_second(frame_current)

            clip = clip_item.clip
            stream = clip.stream

            # Avoid seeking if the frame is less than the gope size away
            gop_size = stream.codec.gop_size
            clip_frame_index = stream.frame_index

            if gop_size == 1 or clip_frame_index > frame_current+gop_size or clip_frame_index < frame_current:
                stream.seek_keyframe(second_current)

            for video_frame in stream.decode_to(second_current):
                if self.buffering.mode != self.mode or self is not self.buffering.queue[-1]:
                    continue
            
            frame_index = stream.frame_index


            frame_stack.append((clip_item, video_frame))

        # Don't show frame if it's more far away than the current frame displayed
        if abs(frame_index - ctx.frame_current) > abs(ctx.displayed_frame_index - frame_current):
            self.signals.finished.emit()
            return

        #ctx.video_surface.compose(frame_stack, frame_index)

        self.signals.frame_decoded.emit(frame_index)

        self.signals.finished.emit()
        '''

    def discard_image(self, cached_index):
        #return
        if not ctx.video_surface.is_cache_full():
            return

        cache = ctx.video_surface.cache
        frame_current = ctx.frame_current

        frame_index = min(cache)#max(cache, key=lambda x : abs(x-frame_current))
        if frame_index == frame_current or frame_index == cached_index:
            frame_index = max(cache)

        ctx.video_surface.discard_image(frame_index)

    def run(self):
        self.signals.started.emit()

        #print(self.clip_items_at())
        frame_current = ctx.frame_current
        previous_frame_current = ctx.previous_frame_current
        is_forward = frame_current > previous_frame_current
        
        if frame_current in ctx.video_surface.cache:
            self.signals.finished.emit()


        clip_items = self.clip_items_at(frame_current)

        if not clip_items:
            return


        for clip_item in clip_items:
            print('SEEK CLIP ITEM')
            self.seek_clip_to(clip_item, frame_current-1)

        clip_datas = [
            {
                'clip_item' : clip_item,
                'frame_iterator' : self.decode_clip_to(clip_item, frame_current),
                'frame': None,
                'frame_index': -2
            }
            for clip_item in clip_items
        ]

        #frame_stack = []
        decoded_frame_nb = 0
        while all(clip_data['frame_index'] < frame_current for clip_data in clip_datas):
            #frame_stack.clear()
            for clip_data in clip_datas:
                video_frame = next(clip_data['frame_iterator'], clip_data['frame'])
                clip_data['frame_index'] = clip_data['clip_item'].clip.stream.frame_index + clip_data['clip_item'].clip.edit_frame_start
                clip_data['frame'] = video_frame

            #print(max(clip_data['frame_index'] for clip_data in clip_datas))
            #if 
            #print(decoded_frame_nb)

            # if is_forward and decoded_frame_nb > 12:
            #     if ((self.buffering.queue and self is not self.buffering.queue[-1]) or 
            #         (self.buffering.mode != self.mode)):
            #     #print('This runnable is not the lastest', time.time())
            #         break
            
            decoded_frame_nb += 1

                #frame_stack.append((clip_data['clip_item'], video_frame))

        #print(clip_datas)

        frame_stack = [(clip_data['clip_item'], clip_data['frame']) for clip_data in clip_datas]
        frame_index = max(clip_data['frame_index'] for clip_data in clip_datas)
        #add_to_cache = all(clip_data['frame_index'] == ctx.frame_current for clip_data in clip_datas)

        #print(frame_index)
        #print(frame_stack)

        if frame_index is None:
            print('----Frame index is None')
            return self.signals.finished.emit()
  
        if is_forward and frame_index < ctx.displayed_frame_index:
            print('Seeking Forward More far away')
            return self.signals.finished.emit()

        '''
        print(f'{frame_current=} {previous_frame_current=}', 'BACKWARD' if frame_current <= previous_frame_current else 'FORWARD')
        
        #print(f'{frame_current=} {ctx.displayed_frame_index=}, {frame_index=}', 'BACKWARD' if frame_current <= ctx.displayed_frame_index else 'FORWARD')

        if frame_index != frame_current:
            if frame_current < previous_frame_current and frame_index > ctx.displayed_frame_index: # Seeking Backward 
                print('Seeking Backward More far away')
                return self.signals.finished.emit()

            elif frame_current > previous_frame_current and frame_index < ctx.displayed_frame_index:
                print('Seeking Forward More far away')
                return self.signals.finished.emit()
        '''

        # #Don't show frame if it's more far away than the current frame displayed
        # if abs(frame_index - frame_current) > abs(ctx.displayed_frame_index - frame_current):
        #     #print('Frame is far away', frame_index, ctx.displayed_frame_index)
        #     return self.signals.finished.emit()


        
        #print('Add to cache', add_to_cache)

        image = ctx.video_surface.decode_frames(frame_stack)
        ctx.video_surface.cache_image(frame_index, image)
        self.discard_image(frame_index)

        ctx.video_surface.show_image(frame_index, image)
        #ctx.video_surface.cache_image(frame_index, image)
        #self.discard_image(frame_index)

        #self.signals.frame_decoded.emit(frame_index)

        self.signals.finished.emit()





class VideoBuffering(QThreadPool) :
    #runnable_added = Signal()

    def __init__(self) :
        super().__init__()

        self.set_max_thread_count(1)
        self.mode = 'idle'
        self.queue = []

        signals.about_to_quit.connect(lambda : self.set_mode('idle'))

        #self.
    
    def add_runnable(self, runnable, start=True, connect=None):
        if connect:
            runnable.signals.frame_decoded.connect(connect, Qt.SingleShotConnection)

        if start:
            self.start(runnable)

        return runnable

    def decode_forward(self, start=True, connect=None):
        #print('decode_forward', self.mode, connect)

        if self.mode == 'forward':
            return

        return self.add_runnable(ForwardVideoDecoder(self), start=start, connect=connect)

    def decode_backward(self, start=True, connect=None):
        if self.mode == 'backward':
            return

        return self.add_runnable(BackwardVideoDecoder(self), start=start, connect=connect)

    def decode_random(self, start=True, connect=None):
        return self.add_runnable(RandomVideoDecoder(self), start=start, connect=connect)

    def remove(self, runnable):
        if runnable in self.queue:
            self.queue.remove(runnable)

    def start(self, runnable):
        self.clear()

        runnable.signals.started.connect(lambda : self.set_mode(runnable.mode))
        runnable.signals.finished.connect(lambda : self.set_mode('idle'))
        runnable.signals.finished.connect(lambda : self.remove(runnable))

        #runnable.connect('started', lambda : self.set_mode(runnable.mode))
        #runnable.connect('finished', lambda : self.set_mode('idle'))
        #runnable.connect('finished', lambda : self.remove(runnable))

        #print('Buffer Added', runnable)

        self.mode = runnable.mode
        self.queue.append(runnable)

        super().start(runnable)

        #print('QUEUE LEN', len(self.queue))


    def set_mode(self, mode):
        self.mode = mode

    #def stop(self):



'''

class Buffering(QThread):
    frame_decoded = Signal((int, np.ndarray))

    def __init__(self):
        super().__init__()

        #self.video_streams = []#set()
        self.is_running = True
        self.buffering_mode = 'fast '
    
        #self.start()
    #def get_current_videos(self):
    #    return (v for v in ctx.edit.video_clips if v.edit_frame_start < ctx.frame_current < v.edit_frame_end)

    def stop(self) :
        self.is_running = False
        self.wait()

    def run(self):
        while self.is_running:

            #get all active video
            video_clip_items = ctx.active_video_clip_items#set(clip for clip in ctx.clip_items_at if clip.type=='video')

            #print('video_clip_items', video_clip_items)

            if not video_clip_items:
                time.sleep(0.015)
                continue
            
            #print(ctx.buffering_mode)

            for video_clip_item in video_clip_items:
                if ctx.buffering_mode == 'forward':
                    video_clip_item.forward_buffering()
                elif ctx.buffering_mode == 'backward':
                    video_clip_item.backward_buffering()
                elif ctx.buffering_mode == 'fast':
                    video_clip_item.random_buffering(precise=False)
                    ctx.timeline_widget.buffering_mode = 'forward'
                elif ctx.buffering_mode == 'precise':
                    video_clip_item.random_buffering(precise=True)
                    ctx.timeline_widget.buffering_mode = 'forward'

            if all(not wgt.is_buffering for wgt in video_clip_items):
                time.sleep(0.015)
                continue

'''