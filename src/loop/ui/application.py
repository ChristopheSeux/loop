
import logging
from PySide6.QtWidgets import QApplication
from PySide6 import QtMultimedia
from PySide6.QtGui import QFont

from __feature__ import snake_case#, true_property

import loop
from loop.constant import ICON_DIR
from loop.ui.signal import Signals
from loop.ui.context import Context
from loop.core.edit import Edit

from canvas.ui.application import Application

import sys


class LoopApplication(Application):
    def __init__(self, medias=None, debug=False):
        super().__init__()

        self.medias = medias
        self.debug = debug
        self.is_running = True

        debug_level = logging.INFO
        if debug:
            debug_level = logging.DEBUG
        logging.basicConfig(level=debug_level, format='%(levelname)s: %(message)s')
        
        font = QFont()
        font.set_pixel_size(13)
        font.set_weight(QFont.DemiBold)
        #font.set_point_size(9)
        #font.set_hinting_preference(QFont.PreferFullHinting)
        #font.set_style_strategy(QFont.PreferQuality|QFont.PreferAntialias)

        self.set_font(font)

        
        #QFont V ("Verdana", 14);
        #QApplication::setFont(V);

        self.context = Context(self)
        self.signals = Signals()
        self.edit = Edit()

        loop.app = self
        loop.signals = self.signals
        loop.ctx = self.context

        sys.modules.update( {
            'loop.app' : self,
            'loop.signals' : self.signals,
            'loop.ctx' : self.context,
            'loop.edit' : self.edit
        } )

        # Import module after adding artifial module to loop
        from loop.ui.widget.main_window import MainWindow
        #from loop.ui.buffering import Buffering
        #from loop.ui.audio_player import AudioPlayer
        from loop.ui.thread_pool import ThreadPool

        self.main_window = MainWindow()
        self.thread_pool = ThreadPool()
        #self.buffering = Buffering()        
        #self.audio_player = AudioPlayer()
        self.signals.launched.emit()

        if medias:
            self.main_window.clip_editor_widget.add_medias(medias)
            #self.main_window.clip_editor_widget.timeline_view.play()

        self.about_to_quit.connect(self.quit)

    def quit(self):
        self.is_running = False
        self.signals.about_to_quit.emit()
        #self.buffering.stop()#.is_running = False#.stop()

        print('Closing Loop Player')
