from PySide6.QtCore import (Signal, Qt, QRectF, QRect)
from PySide6.QtGui import (QPainter, QImage, QPen, QBrush, QPixmap)
from PySide6.QtWidgets import (QWidget, QVBoxLayout, QLabel, QFrame)
from __feature__ import snake_case#, true_property

from PySide6.QtOpenGLWidgets import QOpenGLWidget#, GLWidget

from canvas.ui.layout import HBoxLayout, VBoxLayout
from canvas.ui.frame import VBoxFrame

import time
from loop import ctx, signals
from pathlib import Path
import logging


class VideoSurface(VBoxFrame):
    """Edit Render all videos with effects apply"""
    #frame_current_changed = Signal(int)
    image_cached = Signal(int)
    image_discarded = Signal(int)
    #ready = Signal()
    #cache = {}
    #updated = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)

        #self.is_ready = False

        self.label = QLabel(self)
        self.add_widget(self.label)

        self.cache = {}
        #self.image_stack = []
        self.frame_index = 0
        self.last_cached_index = 0

        #self.fps = 25
        #self.updated.connect(self.update)

        self.image = QImage()

        self.set_accept_drops(True)

        signals.frame_current_changed.connect(self.change_frame)
        #self.image_cached.connect(self.discard_frame)

        #signals.frame_decoded.connect(self.on_frame_decoded)

    @property
    def is_ready(self):
        if ctx.frame_current in self.cache:
            return True
        
        return False

    def drag_enter_event(self, event):
        logging.debug(f'Drag Enter Event, has urls : {event.mime_data().has_urls()}')
        print()
        if event.mime_data().has_urls():
            event.accept()

    def drop_event(self, event):
        logging.debug('Drop Event')

        paths = [Path(url.to_local_file()) for url in event.mime_data().urls()]
        logging.debug(paths)

        signals.files_dropped.emit(paths)      

    def convert_frame(self, frame):
        if not frame:
            return
            
        return QImage(frame.to_ndarray(format='rgb24'), frame.width, frame.height,  QImage.Format_RGB888)

    def convert_frames(self, frame_stack):

        if not frame_stack:
            print('Frame stack is empty')
            return

        return [(clip_item, self.convert_frame(frame)) for clip_item, frame in frame_stack]
        #image = self.compose(image_stack)

        #if cache_image:
        #    self.discard_frame(frame_index)
        #    self.cache_image(frame_index, image)
        # else:
        #     self.show_image(image)

        #return image

    def cache_image(self, frame_index, image):

        print('cache_image', frame_index)

        self.cache[frame_index] = image
        self.last_cached_index = frame_index
        self.image_cached.emit(frame_index)

        signals.buffer_changed.emit()

    def is_cache_full(self):
        return len(self.cache) >= ctx.buffer_size

    def discard_image(self, frame_index):
        del self.cache[frame_index]
        self.image_discarded.emit(frame_index)



    def compose(self, image_stack):
        """Mix down all video clip image with transformation, filter and fusion mode applied"""
        #image_stack.sort(key=lambda x : x[0].channel)
        if not image_stack:
            return

        #for clip_item, frame in image_stack:
        #    image = self.convert_frame(frame)
            #continue # Apply opacity transform and blend mode

        image = image_stack[0][1] # For now take the first clip image


        return image

        #return QPixmap.from_image(image)

    def show_image(self, frame_index, image):
        
        #print('show_image', image)

        self.image = image
        self.frame_index = frame_index
        self.update()
        #self.updated.emit()

    def change_frame(self, frame_index=None):
        if frame_index is None:
            frame_index = ctx.frame_current

        #if frame_index in self.cache:
        #    self.frame_index = frame_index
            #self.show_image(self.cache[self.frame_index])


        if frame_index in self.cache:
            self.show_image(frame_index, self.cache[frame_index])

        #else:
        #    print(f'Image  {frame_index} not in the cache', self.cache.keys())

    def image_rect(self, image):
        h_ratio = self.width() / image.width()
        w_ratio =  self.height() / image.height()

        if h_ratio < w_ratio:
            w = self.width()
            h = image.height() * h_ratio
            x = 0
            y = (self.height()/2) - (h / 2)

        else:
            w = image.width() * w_ratio
            h = self.height()
            x = (self.width()/2) - (w / 2)
            y = 0

        return QRect(x, y, w, h)
        #return image.scaled(w, h, Qt.IgnoreAspectRatio, Qt.SmoothTransformation)

    # def cache_image(self):
    #     if self.frame_index not in self.cache:
    #         return

    #     return self.cache[self.frame_index]

    def draw_image(self, painter, image):

        #print('draw_image', image)

        if not image:
            # print('Image not in the cache')
            return

        rect = self.image_rect(image)
        #scaled_image = image.scaled(rect.width(), rect.height(), Qt.IgnoreAspectRatio)

        painter.draw_image(rect, image)
        #pixmap = QPixmap.from_image(image)
        #painter.draw_pixmap(image.rect(), image)

    def paint_event(self, event):
        painter = QPainter(self)


        # Draw Background
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(Qt.black, Qt.SolidPattern))
        painter.draw_rect(0, 0, self.width(), self.height())

        #print('Draw image', self.image)
        self.draw_image(painter, self.image)