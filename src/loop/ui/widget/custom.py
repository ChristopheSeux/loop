
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, 
    QHBoxLayout, QVBoxLayout, QWidget, QFrame, QApplication, QMenu, QSizePolicy)
from PySide6.QtCore import (Qt, QRectF, QMargins, QCoreApplication, QSize, QPoint)
from PySide6.QtGui import (QColor, QPainter, QBrush, QPen, QIcon, QAction, QShortcut, QKeySequence)
from __feature__ import snake_case#, true_property

import logging
import time

from canvas.ui.layout import HBoxLayout, VBoxLayout
from canvas.ui.property import IntWidget, BoolWidget, StringWidget
from canvas.ui.property import PushButton
from canvas.ui.window import Menu
from canvas.ui.frame import Splitter, Frame, Separator, HBoxFrame

from loop import app, edit, ctx, signals
from loop import ctx, signals, app



class AudioMenu(Menu):
    def __init__(self, parent=None):
        super().__init__(parent)    

        self.aboutToShow.connect(self.set_ui)
        parent.set_menu(self)
    
    def set_ui(self):
        self.layout().clear()
        self.layout().add_widget(QPushButton(text='Audio'))
        self.layout().add_widget(QPushButton(text='Audio2'))


class VideoMenu(Menu):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.aboutToShow.connect(self.set_ui)
        parent.set_menu(self)

    def set_ui(self):
        self.layout().clear()
        self.layout().add_widget(QPushButton(text='Video1'))
        self.layout().add_widget(QPushButton(text='Video2'))


class SubtitleMenu(Menu):
    def __init__(self, parent=None):
        super().__init__(parent)   

        self.aboutToShow.connect(self.set_ui) 
        parent.set_menu(self)

    def set_ui(self):
        self.layout().clear()
        self.layout().add_widget(QPushButton(text='Subtitle1'))
        self.layout().add_widget(QPushButton(text='Subtitle2'))


class ToolBarWidget(HBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent, margin=(4, 2))

        #self.set_minimum_height(50)

        self.background_color = QColor(45, 45, 45)
        self.background_color = QColor(30, 30, 30)

        icons = app.icons()

        #Separator(frame_layout, size=(20, 1))

        self.layout().add_spacing(14)

        self.subtitle_bool_widget = BoolWidget(parent=self.layout(), icon=icons['comment_fill'], value=True)
        self.subtitle_bool_widget.set_tool_tip('Enable / Disable Subtitles')
        self.subtitle_menu_button = PushButton(parent=self.layout(), icon=icons['arrow_down_small'])
        self.subtitle_menu_button.set_fixed_width(20)
        SubtitleMenu(self.subtitle_menu_button)
        #self.subtitle_menu_button.set_menu(subtitle_menu)

        #print(self.subtitle_menu_button.menu())

        #Separator(self.layout(), size=(5, 1))
        self.layout().add_spacing(5)

        self.video_bool_widget = BoolWidget(parent=self.layout(), icon=icons['movie_fill'], value=True)
        self.video_bool_widget.set_tool_tip('Enable / Disable Video')
        self.video_menu_button = PushButton(parent=self.layout(), icon=icons['arrow_down_small'])
        self.video_menu_button.set_fixed_width(20)
        VideoMenu(self.video_menu_button)
        #print( self.video_menu_button.menu())
        self.layout().add_spacing(5)

        self.audio_bool_widget = BoolWidget(parent=self.layout(), icon=icons['sound_on_fill'], value=True)
        self.audio_bool_widget.set_tool_tip('Enable / Disable Audio')
        self.audio_menu_button = PushButton(parent=self.layout(), icon=icons['arrow_down_small'])
        self.audio_menu_button.set_fixed_width(20)

        AudioMenu(self.audio_menu_button)
        #print( self.sound_menu_button.menu())


        '''
        workspace_layout = HBoxLayout(self.layout(), margin=(0, 1, 0, 0))

        self.player_button = BoolWidget(parent=workspace_layout, icon=icons['player'], emboss=False)
        #self.player_button = QPushButton(text='Play')
        self.player_button.set_icon_size(QSize(24, 24))
        self.player_button.set_fixed_size(QSize(30, 30))
        #workspace_layout.add_widget(self.player_button)
        #self.player_button.set_minimum_size(QSize(32, 32))

        self.review_button = BoolWidget(parent=workspace_layout, icon=icons['review'], emboss=False)
        self.review_button.set_fixed_size(QSize(32, 30))
        self.review_button.set_icon_size(QSize(24, 24))

        self.edit_button = BoolWidget(parent=workspace_layout, icon=icons['cut'], emboss=False)
        self.edit_button.set_fixed_size(QSize(32, 30))
        self.edit_button.set_icon_size(QSize(24, 24))

        self.compose_button = BoolWidget(parent=workspace_layout, icon=icons['nodes'], emboss=False)
        self.compose_button.set_fixed_size(QSize(32, 30))
        self.compose_button.set_icon_size(QSize(24, 24))

        self.export_button = BoolWidget(parent=workspace_layout, icon=icons['file_export'], emboss=False)
        self.export_button.set_fixed_size(QSize(32, 30))
        self.export_button.set_icon_size(QSize(24, 24))
        '''

        self.layout().add_stretch()
        #layout.add_stretch()

        self.button_widget = HBoxFrame(self, margin=(0, 0, 0, 0))
        self.button_widget.set_size_policy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.previous_track_button = PushButton(parent=self.button_widget, icon=icons['previous_track_fill'], emboss=False)
        self.next_track_button = PushButton(parent=self.button_widget, icon=icons['next_track_fill'], emboss=False)
        #self.stop_button = PushButton(parent=self, icon=icons['stop'], emboss=False)
        self.layout().add_spacing(10)

        self.play_button = PushButton(parent=self.button_widget, icon=icons['play_fill'], emboss=False, size=(40, 25))
        #print(self.play_button.size())
        #print(self.play_button.minimum_size())
        #print(self.play_button.maximum_size())
        #self.play_button.set_fixed_size(QSize(40, 25))
        #self.play_button.set_size((40, 25))

        self.pause_button = PushButton(parent=self.button_widget, icon=icons['pause_fill'], emboss=False, size=(40, 25))
        #self.pause_button.set_fixed_size(QSize(40, 25))
        self.pause_button.hide()

        self.previous_frame_button = PushButton(parent=self.button_widget, icon=icons['previous_frame_fill'], emboss=False, size=(30, 25))
        #self.previous_frame_button.set_fixed_size(QSize(30, 25))

        self.next_frame_button = PushButton(parent=self.button_widget, icon=icons['next_frame_fill'], emboss=False, size=(30, 25))
        #self.next_frame_button.set_fixed_size(QSize(30, 25))

        self.layout().add_stretch()

        self.frame_current_widget = IntWidget(parent=self)
        self.frame_current_widget.set_fixed_width(85)

        self.layout().add_spacing(10)

        frame_layout = HBoxLayout(self, align='HCenter')
        self.frame_start_widget = IntWidget(parent=frame_layout, name='Start')
        self.frame_end_widget = IntWidget(parent=frame_layout, name='End')
        self.loop_widget = BoolWidget(parent=frame_layout, icon=icons['loop_fill'])

        frame_layout.collapse()
        self.layout().collapse()

        #frame_layout.set_collapsed()

        #print(self.loop_widget, self.loop_widget.metrics().corners)

        #self.custom_range_button = BoolWidget(parent=frame_layout, icon=icons['timer'])
        #self.frame_start_int_widget = IntWidget(parent=frame_layout, text='Start')
        #self.frame_end_int_widget = IntWidget(parent=frame_layout, text='End')
        



        #self.separator_frame = Frame(self, size=(10, 1))
        #self.separator_frame.set_fixed_width(10)

        signals.launched.connect(self.connect_signals)

    def set_frame(self):
        #TODO Implement a context manager like this
        #with signal_blocked(self.frame_current_int_widget) as w:
        #    w.set_value(ctx.frame_index)
        self.frame_current_widget.block_signals(True)
        self.frame_current_widget.set_value(ctx.frame_current)
        self.frame_current_widget.block_signals(False)

    def connect_signals(self):
        ctx.timeline_widget.paused.connect(self.toogle_play_pause_button)
        ctx.timeline_widget.played.connect(self.toogle_play_pause_button)
        #self.stop_button.clicked.connect(lambda : ctx.timeline_widget.stop())
        self.pause_button.clicked.connect(lambda : ctx.timeline_widget.pause())
        self.play_button.clicked.connect(lambda : ctx.timeline_widget.play())
        self.previous_frame_button.clicked.connect(lambda: ctx.timeline_widget.jump_frame(-1))
        self.next_frame_button.clicked.connect(lambda: ctx.timeline_widget.jump_frame(1))

        ctx.timeline_widget.paused.connect(self.set_frame)
        signals.frame_current_changed.connect(self.set_frame)
        self.frame_current_widget.value_changed.connect(ctx.timeline_widget.set_frame_current)


        #self.video_bool_widget.state_changed.connect()

    def toogle_play_pause_button(self):
        #print('Toolge play pause')
        if ctx.timeline_widget.is_playing():
            self.play_button.hide()
            self.pause_button.show()
        else:
            self.pause_button.hide()
            self.play_button.show()

    def paint_event(self, event):
        painter = QPainter(self)

        # Draw Background
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.draw_rect(0, 0, self.width(), self.height())


