

from PySide6.QtCore import (Signal, QTimer, Qt, QRect, QMargins)
from PySide6.QtGui import (QKeySequence, QShortcut)
from PySide6.QtWidgets import (QApplication, QMainWindow, QWidget, QPushButton, 
    QVBoxLayout, QHBoxLayout)

from __feature__ import snake_case#, true_property

from pathlib import Path
import sys
import logging
import time

from canvas.ui.frame import Splitter, Frame

from loop.ui.widget.clip_editor import ClipEditorWidget
from loop import app, edit, ctx, signals





class MainWindow(QMainWindow):
    #frame_current_changed = Signal(int)
    #ready = Signal()

    def __init__(self):
        super().__init__()

        self.set_window_title('Loop')
        self.set_geometry(100, 100, 800, 600)

        self.clip_editor_widget = ClipEditorWidget(self)
        self.set_central_widget(self.clip_editor_widget)
 
        self.show()
    

        

