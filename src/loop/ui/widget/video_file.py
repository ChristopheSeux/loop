
from PySide6.QtCore import (Signal, Qt, QRectF)
from PySide6.QtGui import (QPainter, QPen, QBrush)
from PySide6.QtWidgets import (QWidget, QVBoxLayout)
from __feature__ import snake_case#, true_property

from loop import ctx, signals



class VideoFileWidget(QWidget):
    """Widget holding a single video file"""

    def __init__(self, video_file, parent=None):
        super().__init__(parent)

        self.video_file = video_file

        # self.layout = (QVBoxLayout(self))
        # self.layout().contents_margins = (0, 0, 0, 0)

        # self.video_surface = VideoSurface()
        # self.layout().add_widget(self.video_surface)

        #signals.frame_current_changed.connect(self.video_surface.repaint)