
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, 
    QHBoxLayout, QVBoxLayout, QWidget, QFrame, QApplication, QMenu, QGraphicsScene, QStackedLayout)
from PySide6.QtCore import (Qt, QRectF, QMargins, QCoreApplication, QSize, QPoint, QPointF, Signal, 
    QTimeLine, QLine, QRect, QRectF, QTimer, QThread)
from PySide6.QtGui import (QColor, QPainter, QBrush, QPen, QIcon, QAction, 
    QShortcut, QKeySequence, QTransform)
from __feature__ import snake_case#, true_property

import logging
import time
from functools import partial
from operator import itemgetter
from itertools import groupby

from loop import app, edit, ctx, signals
from loop.ui.widget.video_surface import VideoSurface
from loop.ui.graphics.clip import VideoClipItem, AudioClipItem, SubtitleClipItem
#from loop.ui.graphics.timeline import TimelineView, TimelineScene
from loop.ui.graphics.channel import ChannelView

from canvas.ui.property import PushButton
from canvas.ui.frame import Splitter, Frame, Separator, VBoxFrame, StackedFrame
from canvas.ui.layout import HBoxLayout, VBoxLayout, StackedLayout
from canvas.ui.property import IntWidget, BoolWidget

from loop.ui.widget.custom import ToolBarWidget
from loop import ctx, signals, app

from loop.ui.video_buffering import VideoBuffering
from loop.ui.audio_player import AudioPlayer
from loop.ui.clip_previewer import ClipPreviewer


class TimelineHeaderWidget(QWidget):
    played = Signal()
    paused = Signal()
    stopped = Signal()

    def __init__(self, parent):
        super().__init__(parent)

        self.timeline_widget = parent

        self._is_down = False
        self._is_dragged = False
        self._is_playing = False # When clicked on the timeline

        self.text_color = QColor(200, 200, 200)
        self.header_color = QColor(20, 20, 20)
        self.accent_color = QColor(56, 100, 159)#
        self.header_height = 20

        self.set_minimum_height(20)
        

    def text_width(self, widget, text):
        return widget.font_metrics().bounding_rect(str(text)).width() + 2

    

    def paint_event(self, event):
        painter = QPainter(self)

        #Draw Header
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(self.header_color, Qt.SolidPattern))
        painter.draw_rect(
            0, 0, 
            self.width(), self.height()
        )

        painter.set_pen(QPen(QColor(0, 0, 0, 150), 1))
        line_top = QLine(
            0, 0, 
            self.width(), 0
        )
        line_bottom = QLine(
            0, self.height()-1,
            self.width(), self.height()-1
        )

        painter.draw_lines((line_top, line_bottom))

        #self.draw_sections_digit(painter)
        self.draw_cursor(painter)

    def draw_cursor(self, painter):
        frame_text = str(ctx.frame_current)
        text_width = len(frame_text) * self.text_width(painter, '0') + 8
        text_width = max(text_width, 20)
        #round_text_width = round(text_width / 2) * 2
        #range_padding = self.timeline_widget.range_padding()
        #range_width = self.timeline_widget.range_width()

        #range_ratio = range_width / (ctx.nb_frames-1)

        transform = self.timeline_widget.view_transform()
        x = transform.map(QPointF(ctx.frame_current, 0)).x()

        #x = ctx.frame_current * range_ratio + range_padding
        y = 1

        # Draw line of Current frame 
        #painter.set_pen(QPen(self.accent_color, 2, Qt.SolidLine, Qt.FlatCap))
        #painter.draw_line(x, y, x, self.height())

        frame_square_x = x - text_width*0.5
        #round_frame_square_x = round(frame_square_x / 2) * 2

        cursor_rect = QRectF(frame_square_x, y, text_width, self.height()-y-1)

        painter.set_render_hint(QPainter.Antialiasing, True)
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(self.accent_color, Qt.SolidPattern))
        #painter._draw_rounded_rect(cursor_rect, 3, 3)

        painter.set_pen(QColor(255, 255, 255))
        painter.draw_text(cursor_rect,
            Qt.AlignCenter, frame_text
        )

    def draw_sections_digit(self, painter):
        range_padding = self.timeline_widget.range_padding()
        range_width = self.timeline_widget.range_width()

        painter.set_pen(self.text_color)
        font = painter.font()
        font.set_pixel_size(11)
        painter.set_font(font)
        
        # Draw first frame section
        text_width = self.text_width(painter, ctx.frame_start)
        painter.draw_text(
            range_padding - round(text_width*0.5), 0, # x, y coord
            text_width, self.header_height, #width and height of the text rect
            Qt.AlignLeft|Qt.AlignVCenter, str(ctx.frame_start)
        )

        # Draw end frame section
        text_width = self.text_width(painter, ctx.frame_end)
        painter.draw_text(
            self.width() - round(text_width*0.5) - range_padding, 0, # x, y coords
            text_width, self.header_height, #width and height of the text rect
            Qt.AlignLeft|Qt.AlignVCenter, str(ctx.frame_end)
        )

        text_color = QColor(self.text_color)
        text_color.set_alpha(175)

        painter.set_pen(text_color)

        for i, x in ctx.timeline_view.main_sections:
            frame_text = f'{i}'
            text_width = self.text_width(painter, frame_text)

            if x > range_width + range_padding - text_width*1.5:
                continue

            painter.draw_text(
                x - round(text_width*0.5), 0, 
                text_width, self.header_height, 
                Qt.AlignLeft|Qt.AlignVCenter, frame_text
            )

    def frame_from_pos(self, pos):
        range_padding = self.timeline_widget.range_padding()
        range_width = self.timeline_widget.range_width()

        ratio = (pos - range_padding) / range_width
        return round(ratio * (ctx.nb_frames-1))

    def mouse_press_event(self, event):
        if event.button() != Qt.LeftButton:
            return
        
        self._is_playing = ctx.is_playing
        #print('----', ctx.is_playing)
        
        pos = event.pos().x()
        frame_index = self.frame_from_pos(pos)

        if not ctx.frame_start <= frame_index <= ctx.frame_end:
            return

        self._is_down = True

        #print('mouse_press_event', frame_index)

        ctx.timeline_widget.seek(frame_index)
        ctx.timeline_widget.pause()

        #self.buffering.mode = 'forward'
 
    def mouse_move_event(self, event):
        if not self._is_down:
            return
        
        self._is_dragged = True

        pos = event.pos().x()
        frame_index = self.frame_from_pos(pos)
        if not ctx.frame_start <= frame_index <= ctx.frame_end:
            return

        ctx.timeline_widget.seek(frame_index)

    def mouse_release_event(self, event):
        self._is_down = False
        #if self._is_dragged:
        #    pos = event.pos().x()
        #    frame_index = self.frame_from_pos(pos)
        #    ctx.timeline_widget.seek(frame_index, keyframe=False)

        self._is_dragged = False

        if self._is_playing:
            ctx.timeline_widget.play()


class ChannelsWidget(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.timeline_widget = parent
        self.channel_views = []

        self.add_channel()
        self.add_channel()

    def add_channel(self):

        channel_view = ChannelView()
        self.add_widget(channel_view)
        self.channel_views.append(channel_view)

    def add_clip(self, clip_item):
        if clip_item.type == 'audio':
            channel_index = 0
        elif clip_item.type == 'video':
            channel_index = 1

        self.channel_views[channel_index].scene().add_item(clip_item)


class CacheVisualizerWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.timeline_widget = parent
        self.color = QColor(80, 80, 80)
        self.set_fixed_height(2)

        signals.buffer_changed.connect(self.update)

    def get_cache_ranges(self):
        """Get a range list of contigus buffered frames"""

        cache_ranges = [] 
        cached_frames = sorted(ctx.video_surface.cache.keys())

        # Loop through the list of ints, and break it into lists of contiguous ints
        for k, g in groupby(enumerate(cached_frames), key=lambda x: x[0]-x[1]):
            cache_range = list(map(itemgetter(1), g))
            cache_ranges.append([cache_range[0], cache_range[-1]])

        return cache_ranges

    def paint_event(self, event):
        painter = QPainter(self)

        painter.save()

        painter.set_render_hint(QPainter.Antialiasing, False)

        # Align the view painter to the scene matrix

        channel_view = self.timeline_widget.channels_widget.channel_views[0]
        start = channel_view.map_from_scene(QPointF(ctx.frame_start, 0)).x()
        #end = channel_view.map_from_scene(QPointF(ctx.frame_end, 0)).x()

        #print('-------')
        #print(channel_view.horizontal_scaling())
        #print(start, end)
        #print((end-start)/self.width(), self.width())

        painter.translate(QPointF(start, 0))
        painter.scale(channel_view.horizontal_scaling(), 1)

        buffer_color = QColor(self.color)
        #buffer_color.set_alpha(200)

        painter.set_brush(buffer_color)
        painter.set_pen(Qt.NoPen)

        #range_ratio = self.frame_width
        buffer_height = 0

        rects = []
        for cache_range in self.get_cache_ranges():
            rect = QRectF(
                cache_range[0], 0,
                cache_range[-1]-cache_range[0], self.height()
            )

            #print(rect)

            rects.append(rects)
            painter.draw_rect(rect)

        painter.restore()




class TimelineOverlayWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.timeline_widget = parent
        self.accent_color = QColor(56, 100, 159)
        #self.set_attribute(Qt.WA_OpaquePaintArea)
        # self.set_attribute(Qt.WA_NoSystemBackground)
        # self.set_auto_fill_background(False)
        # self.set_attribute(Qt.WA_TranslucentBackground)
        # self.set_window_flags(Qt.FramelessWindowHint)
        # self.set_attribute(Qt.WA_TransparentForMouseEvents)
        # self.set_window_opacity(0)

        # self.set_style_sheet("background-color: rgba(255, 0, 0, 0);")

    def paint_event(self, event):
        painter = QPainter(self)

        transform = self.timeline_widget.view_transform()
        painter.set_transform(transform)

        # painter.set_pen(Qt.NoPen)
        # #painter.set_brush(Qt.transparent)
        # painter.set_brush(QColor(255,255,255,0))
        # painter.draw_rect(self.rect())

        #return

        #painter.set_render_hint(QPainter.Antialiasing, False)
        #painter.set_brush(Qt.NoBrush)

        pen = QPen(QColor(0, 0, 0, 150), 4, Qt.SolidLine, Qt.FlatCap)
        pen.set_cosmetic(True)
        painter.set_pen(pen)

        painter.draw_line(ctx.frame_current, 0, ctx.frame_current, self.height())

        pen = QPen(self.accent_color, 2, Qt.SolidLine, Qt.FlatCap)
        pen.set_cosmetic(True)
        painter.set_pen(pen)

        painter.draw_line(ctx.frame_current, 0, ctx.frame_current, self.height())


class TimelineWidget(VBoxFrame):
    paused = Signal()
    played = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)

        #self.set_layout(StackedLayout(self, stacking_mode='StackAll'))

        self.header_widget = TimelineHeaderWidget(self)
        self.add_widget(self.header_widget)

        self.stacked_widget = StackedFrame(stacking_mode='StackAll')
        self.add_widget(self.stacked_widget)

        self.underlay_widget = VBoxFrame()

        self.cache_visualizer_widget = CacheVisualizerWidget(self)
        self.channels_widget = ChannelsWidget(self)

        self.underlay_widget.add_widget(self.cache_visualizer_widget)
        self.underlay_widget.add_widget(self.channels_widget)


        self.overlay_widget = TimelineOverlayWidget(self)

        self.stacked_widget.add_widget(self.overlay_widget)
        self.stacked_widget.add_widget(self.underlay_widget)

        #self.scene = TimelineScene()
        #self.view = TimelineView(self, self.scene)

        #print(self.view.scene)


        #self.layout().add_widget(self.view)

        self.time = 0
        self.timer = QTimer()
        self.timer.set_timer_type(Qt.PreciseTimer)
        self.timer.timeout.connect(self.frame_change)

        self._frame_start = 0
        self._frame_end = 0
        self._frame_current = 0
        self._previous_frame_current = 0

        self.video_buffering = VideoBuffering()
        #self.audio_player = AudioPlayer()
        self.clip_previewer = ClipPreviewer()

        self._fps = 25
        self.set_frame_range(0, 10)

        signals.frame_current_changed.connect(self.header_widget.update)
        signals.frame_current_changed.connect(self.overlay_widget.update)
        #signals.frame_current_changed.connect(self.scene.update_foreground)

        self.range_margin = 4
        self.range_height = 0
        self.frame_width = 1
        self.background_color = QColor(15, 15, 15)

    def view_transform(self):
        channel_view = self.channels_widget.channel_views[0]
        start = channel_view.map_from_scene(QPointF(ctx.frame_start, 0)).x()

        transform = QTransform()

        transform.translate(start, 0)
        transform.scale(channel_view.horizontal_scaling(), 1)

        return transform

    def text_width(self, widget, text):
        return widget.font_metrics().bounding_rect(str(text)).width() + 2

    def range_padding(self):
        return self.text_width(self, ctx.frame_end) + self.range_margin

    def range_width(self):
        return self.width() - (2*self.range_padding())

    def set_fps(self, fps):
        self._fps = fps
        #self.timeline.set_update_interval(1000/fps)

    def fps(self):
        return self._fps#1 / self.timeline.updateInterval()

    def frame_current(self):
        return self._frame_current

    def previous_frame_current(self):
        return self._previous_frame_current

    def second_current(self):
        return self.frame_to_second(self.frame_current())

    def frame_start(self):
        return self._frame_start

    def frame_end(self):
        return self._frame_end

    def set_frame_start(self, frame_start):
        if self._frame_end != frame_start:
            signals.frame_start_changed.emit()

        self._frame_start = frame_start

    def set_frame_end(self, frame_end):
        #print('set_frame_end', frame_end)
        if self._frame_end != frame_end:
            signals.frame_end_changed.emit()
        
        self._frame_end = frame_end

    def set_frame_range(self, start, end):
        self.set_frame_start(start)
        self.set_frame_end(end)

    def is_playing(self):
        return self.timer.is_active()

    def frame_duration(self): # in seconds
        #print('frame_duration',  float(1 / ctx.fps))
        
        return 1 / self.fps()
        #return 1/32

    def start_timer(self):
        print('start_timer')
        self.timer.start(float(self.frame_duration() * 1000))
        
        ctx.audio_player.play()
        self.played.emit()

    def play(self):
        print('play')
        #     if self.frame_current >= self.frame_end:
        #         self.set_frame_current(self.frame_start)

        #print('Play', ctx.buffer_ready)

        #if ctx.buffer_ready:
        #    self.buffering.add(mode='forward', start=True)
        #    self.start_timer()
        #else:
        ctx.audio_player.buffering.decode()
        self.video_buffering.decode_forward()

        ctx.clip_editor_widget.wait_until_ready(connect=self.start_timer)
        #self.start_timer()

        #self.video_buffering.decode_forward(connect=lambda: self.start_timer())

    def pause(self):
        self.timer.stop()
        ctx.audio_player.pause()
        self.paused.emit()
        signals.frame_current_changed.emit()

    def stop(self):
        self.timer.stop()
        #self.stopped.emit()

    def toogle_play_pause(self):
        logging.debug('toogle_play_pause')
        if self.is_playing():
            self.pause()
        else:
            self.play()

    def seek(self, frame_index):
        #print('seek', frame_index, self.clamp_frame_index(frame_index))
        #print(self.frame_start(), self.frame_end())

        frame_index = self.clamp_frame_index(frame_index)

        if frame_index == self.frame_current():
            return

        self.set_frame_current(frame_index)
        #print(frame_index, self.frame_to_second(frame_index))
        ctx.audio_player.seek(self.frame_to_second(frame_index))
        self.video_buffering.decode_random(connect=ctx.video_surface.change_frame)

        #buffer_runnable = self.video_buffering.add(mode='random')
        #buffer_runnable.connect('frame_decoded', ctx.video_surface.change_frame)

        #self.video_buffering.start(buffer_runnable)

    def clamp_frame_index(self, frame_index):

        frame_index = max(frame_index, self.frame_start())
        frame_index = min(frame_index, self.frame_end())

        return round(frame_index)
    
    def frame_to_second(self, frame_index):
        return frame_index / self.fps()
    
    def second_to_frame(self, second, rounding=round):
        return rounding(second * self.fps())

    def set_time_current(self, time): # in second
        logging.debug('set_time_current', time * 1000)
        self.timeline.set_current_time(round(time*1000))

    def set_frame_current(self, frame_index):
        self._previous_frame_current = self._frame_current
        
        frame_index = self.clamp_frame_index(frame_index)

        if frame_index == self.frame_current():
            return
        
        self._frame_current = frame_index

        signals.frame_current_changed.emit()

        #if ctx.buffer_ready or not self.is_playing:
        #    return signals.frame_current_changed.emit()

        #app.thread_pool.add_callable(
        #    ctx.clip_editor_widget.wait_bufferring,
        #    connect=lambda : signals.frame_current_changed.emit())

    def jump_frame(self, delta):
        self.pause()

        frame_index = self.frame_current() + delta
        change_frame = lambda : self.set_frame_current(frame_index)

        print('change_framé', delta)

        ctx.audio_player.seek(self.frame_to_second(frame_index))

        if delta > 0:
            self.video_buffering.decode_forward(connect=change_frame)
        else:
            self.video_buffering.decode_backward(connect=change_frame)

        #self.is_play_reversed = delta < 0

        #return self.set_frame_current(self.frame_current() + delta)

    def frame_change(self):
        #current_time = time.perf_counter()
        #print(self.frame_current, current_time-self.time)
        new_time = time.perf_counter()

        #print(new_time-self.time)

        self.time = time.perf_counter()

        if self.frame_current() == ctx.frame_end:
            self.pause()
            signals.frame_current_changed.emit()
            return

        signals.frame_current_changed.emit()

        #self.buffering.mode = 'forward'
        self.video_buffering.decode_forward()
        self._frame_current += 1

    def paint_event(self, event):
        painter = QPainter(self)

        # Draw Background
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.draw_rect(self.rect())


class ClipEditorWidget(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__()


        #self.layout().set_contents_margins(0, 0, 0, 0)

        self.audio_player = AudioPlayer()
        self.video_surface = VideoSurface(self)

        #self.timeline_header_widget = TimelineHeaderWidget(self)
        self.timeline_widget = TimelineWidget(self)

        self.splitter = Splitter("Vertical", parent=self)

        self.add_widget(self.splitter)
        self.splitter.add_widget(self.video_surface)
        self.splitter.add_widget(self.timeline_widget)

        #self.splitter.sizes = ([2, 1])
        self.splitter.set_stretch_factor(0, 1)
        self.splitter.set_stretch_factor(2, 1)

        self.tool_bar = ToolBarWidget(self)
        self.separator_frame = Frame(self, color=(40, 40, 40))
        self.separator_frame.set_fixed_height(1)

        self.add_widget(self.separator_frame)
        self.add_widget(self.tool_bar)

        signals.files_dropped.connect(self.clear)
        signals.files_dropped.connect(self.add_medias)
        signals.full_screen_toogled.connect(self.toogle_timeline_visibility)

        self.set_shortcuts()

        self._is_dragged = False
        self._is_down = False
        self._frame_index = 0
        self._pos = QPoint()

    # def paint_event(self, event):
    #     painter = QPainter(self)


    #     # Draw Background
    #     painter.set_brush(QBrush(Qt.black, Qt.SolidPattern))
    #     painter.draw_rect(0, 0, self.width(), self.height())

    def set_shortcuts(self):
        toogle_play_shorcut = QShortcut(QKeySequence(" "), self)
        toogle_play_shorcut.activated.connect(self.timeline_widget.toogle_play_pause)

        jump_right_shorcut = QShortcut(QKeySequence(Qt.Key.Key_Right), self)
        jump_right_shorcut.activated.connect(lambda : self.timeline_widget.jump_frame(1))

        jump_left_shorcut = QShortcut(QKeySequence(Qt.Key.Key_Left), self)
        jump_left_shorcut.activated.connect(lambda : self.timeline_widget.jump_frame(-1))

        toogle_full_screen = QShortcut(QKeySequence.Cancel, self)
        toogle_full_screen.activated.connect(self.exit_full_screen)

        toogle_full_screen = QShortcut(QKeySequence.FullScreen, self)
        toogle_full_screen.activated.connect(self.toogle_full_screen)

    def clip_items(self):
        return self.timeline_widget.range_view.scene().items()

    def is_full_screen(self):
        return self.window().window_state() is Qt.WindowFullScreen
    
    def set_full_screen(self):
        self.window().set_window_state(self.window().window_state() | Qt.WindowFullScreen)
        signals.full_screen_toogled.emit()
    
    def exit_full_screen(self):
        self.window().set_window_state(self.window().window_state() ^ Qt.WindowFullScreen)
        signals.full_screen_toogled.emit()

    def toogle_full_screen(self):
        if self.is_full_screen():
            self.exit_full_screen()
        else:
            self.set_full_screen()

    # def mouse_double_click_event(self, event):
    #     #print('Double Click')
    #     self.toogle_full_screen()

    # def mouse_press_event(self, event):
    #     self._is_down = True
    #     self._pos = event.pos()
    #     self._frame_index = self.timeline_widget.frame_index

    # def mouse_move_event(self, event):
    #     if not self._is_down:
    #         return

    #     offset = (event.x() - self._pos.x()) / self.timeline_widget.frame_duration()

    #     self.timeline_widget.seek(self._frame_index + offset, keyframe=True)

    #     self._is_dragged = True

    # def mouse_release_event(self, event):
    #     self._is_down = False

    #     if self._is_dragged:
    #         offset = (event.x() - self._pos.x()) / self.timeline_widget.frame_duration()
    #         self.timeline_widget.seek(self._frame_index + offset, keyframe=False)

    #     self._is_dragged = False

    # def is_dragged(self):
    #     return self._is_dragged

    def toogle_timeline_visibility(self):
        self.timeline_widget.set_visible(not self.timeline_widget.is_visible())
        self.tool_bar.set_visible(not self.tool_bar.is_visible())

    def clear(self):
        logging.debug(f'Clear Edit')

        ctx.edit.clear()

        for wgt in self.clip_items:
            wgt.delete_later()
            wgt.set_parent(None)
        
        self.clip_items.clear()
        self.timeline_widget.pause()
        self.timeline_widget.frame_index = 0
        signals.frame_current_changed.emit()

    def add_medias(self, medias):
        logging.debug(f'Add Medias: {medias}')

        frame_end = ctx.frame_end
        fps = ctx.fps

        clip_wgts = []

        frame_start = ctx.edit.frame_current
        for media in medias:

            #print('media', media, frame_start)
            for clip in ctx.edit.clips.new(media, edit_frame_start=frame_start):

                #print(clip, clip.type)

                #clip_widget = next(wgt for wgt in self.media_widgets if wgt.clip is clip)
                #if not clip_widget:
                if clip.type == 'video':
                    #continue
                    clip_item = VideoClipItem(clip)
                elif clip.type == 'audio':
                    clip_item = AudioClipItem(clip)
                elif clip.type == 'subtitle':
                    clip_item = SubtitleClipItem(clip)
                else:
                    raise Exception(f'Type: {type} not supported')

                
                clip_previewer = self.timeline_widget.clip_previewer.add(clip_item)
                clip_previewer.signals.preview_updated.connect(partial(clip_item.update))
                #clip_previewer.connect('finished', lambda: clip_item.prepare_geometry_change())

                #elif clip.type == 'audio':
                #    clip_wgt = AudioClipGraphics(clip)

                #self.clip_items.append(clip_wgt)
                #self.set_active_video_clip_items()
                self.timeline_widget.channels_widget.add_clip(clip_item)
                clip_wgts.append(clip_item)

            frame_start += clip.duration
        
        signals.clip_added.emit()

        #ctx.edit.set_frame_end()

        self.timeline_widget.set_frame_end(edit.frame_end)
        self.timeline_widget.set_fps(edit.fps)
        #self.timeline_widget.set_fps(24)

        if fps != ctx.edit.fps:
            signals.fps_changed.emit()

        self.timeline_widget.video_buffering.decode_forward(connect=ctx.video_surface.change_frame)
        #runnable.decoded.connect(self.show_image(frame_index, self.cache[frame_index]))

        #print('EDIT FPS', ctx.edit.fps)
        #print('FPS', ctx.fps)
        #print('FPS', ctx.clip_editor_widget.frame_duration())

        #self.timeline_widget.play()

        return clip_wgts

    @property
    def is_ready(self):
        return (self.video_surface.is_ready and self.audio_player.is_ready)

    def wait_until_ready(self, connect):
        print('wait_until_ready')

        # while not self.is_ready:
        #     #print('Waiting Buffer...', 'Audio Ready', self.audio_player.is_ready, 'Video Ready', self.video_surface.is_ready)

        #     #if not self.video_surface.is_ready:
        #     #    print(self.video_surface.cache.keys())
        #     #print('Audio Ready', self.audio_player.is_ready)

        #     time.sleep(0.0025)
        
        # connect()

        # return

        class WaitUntilReady(QThread):
            def __init__(self, parent):
                super().__init__()
                self.parent = parent

            def run(self):
                

                while not self.parent.is_ready:
                    if not app.is_running:
                        break
                
                    if not self.parent.is_ready:
                        print(f'Waiting Buffer... video_ready={self.parent.video_surface.is_ready}, audio_ready={self.parent.audio_player.is_ready}')
                        #print(self.parent.video_surface.cache.keys())
                        #print('Audio Ready', self.parent.audio_player.is_ready)

                        time.sleep(0.0025)

        self.wait_thread = WaitUntilReady(self)
        self.wait_thread.finished.connect(connect)
        self.wait_thread.start()


    # def set_active_video_clip_items(self):
    #     self.active_video_clip_items.clear()
    #     self.active_video_clip_items.update(
    #             clip_wgt for clip_wgt in self.clip_items if
    #             clip_wgt.clip.type == 'video' and
    #             clip_wgt.clip.edit_frame_start >= ctx.frame_current - (ctx.buffer_size*0.5) or
    #             clip_wgt.clip.edit_frame_end <= ctx.frame_current + (ctx.buffer_size*0.5)
    #         )


