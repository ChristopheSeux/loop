from PySide6.QtCore import QObject, Signal, QRunnable, QThreadPool
from __feature__ import snake_case#, true_property

import time
import subprocess
import traceback

import logging
import uuid


class RunnableSignals(QObject):
    """
    Defines the signals available from a running worker thread.
    """
    
    started = Signal()
    finished = Signal()
    error = Signal(str)
    result = Signal(object)
    progress = Signal(int)
    updated = Signal()


class Runnable(QRunnable):
    def __init__(self, callable, *args, connect=None, env=None, **kargs) :
        super().__init__()

        self.signals = RunnableSignals()
        self.callable = callable
        self.args = args
        self.kargs = kargs
        self.created_at = time.time_ns()
        self.id = str(uuid.uuid1())

        self.is_started = False
        self.is_error = False
        self.is_finished = False

        if connect:
            if not isinstance(connect, list) :
                connect = [connect]

            for c in connect:
                self.signals.result.connect(c)

        logging.debug( f'thead_pool.add_callable({callable.__module__}.{callable.__name__}, {args}, {kargs})')

    @classmethod
    def from_cmd(cls, cmd, connect=None, env=None):
        return cls(subprocess.call, cmd, connect=connect, env=env)

    def run(self):
        self.is_started = True
        self.signals.started.emit()

        try:
            result = self.callable(*self.args, **self.kargs)
        except Exception:
            traceback.print_exc()
            self.is_error = True 
            self.signals.error.emit(traceback.format_exc())
        else:
            self.signals.result.emit(result) # Return the result of the processing
        finally:
            self.is_finished = True
            self.signals.finished.emit() 


class ThreadPool(QThreadPool) :
    def __init__(self) :
        super().__init__()

        self.queue = []
        self.progress = 0

        self.set_max_thread_count(1)

    def add_runnable(self, runnable, signal=None):
        runnable.signals.finished.connect(self.update_progress)
        runnable.signals.error.connect(lambda x: logging.error(x))
        runnable.signals.error.connect(lambda: logging.error('Failed to Execute Function (Scroll up for detail)'))

        if signal:
           signal.connect(lambda : self.start(runnable))
        else:
            self.start(runnable)

        self.queue.append(runnable)
        self.update_progress()

        return runnable

    def add_callable(self, instance, *args, connect=None, signal=None, **kargs):
        runnable = Runnable(instance, *args, connect=connect, **kargs)
        return self.add_runnable(runnable, signal)

    def add_command(self, cmd, *args, connect=None, signal=None, **kargs):
        runnable = Runnable.from_cmd(cmd, *args, connect=connect, **kargs)
        return self.add_runnable(runnable, signal)        

    def stop(self) :
        for runnable in self.queue:
            runnable.is_running = False

    @property
    def remaining(self):
        return [r for r in self.queue if not r.is_finished]

    def update_progress(self) :

        stack_size = len(self.remaining)

        if stack_size == 0 :
            progress = 0
        elif stack_size == 1 :
            progress = 85
        else :
            progress = 100 / (1+(stack_size/50) )**1.25

        if progress != self.progress :
            self.progress = progress
