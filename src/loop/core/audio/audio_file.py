
from pathlib import Path
import av
import logging

from loop.core.media_file import MediaFile


class AudioFile(MediaFile):
    type = 'audio'

    def __init__(self, media_path):
        super().__init__(media_path)

        #self.filepath = Path(media_path)
        #self.name = self.filepath.name

        #self.stream = self.container.streams.video[channel]
        #self.duration = self.stream.frames

    # @property
    # def nb_frames(self):
    #     frames = self.stream.frames
    #     if not frames: # Guess frame number from duration
    #         frames = round(self.duration * self.fps)

    #     return frames

    # @property
    # def duration(self):
    #     """Get duration of the video in seconds"""

    #     #return self.stream.duration
    #     pts = self.stream.duration
    #     return float(pts * self.stream.time_base) / 1000