
from loop.core.clip import Clip
from loop.core.audio.audio_stream import AudioStream



class AudioClip(Clip):
    type = 'audio'

    def __init__(self, audio_file, edit_frame_start=0, parent=None, channel=1):
        super().__init__(edit_frame_start, parent=parent)

        self.channel = channel
        self.file = audio_file
        self.name = audio_file.name
        self.stream = AudioStream(audio_file)
        self.duration = self.stream.nb_frames

        #print('SOUND DURATION', self.duration)

    @property
    def filepath(self):
        return self.file.filepath
