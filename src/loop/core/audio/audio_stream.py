

#from __feature__ import snake_case#, true_property

import av
import numpy as np
import logging
from loop.core.stream import Stream


class AudioStream(Stream):
    def __init__(self, audio_file):
        super().__init__(audio_file)

        self.stream = audio_file.streams.audio[0]

        #self._chunk_iter = iter([])
        self.last_decoded_frame = None
        self.start()

        self.dtype = np.dtype(self.format_dtypes[self.stream.format.name])

    @property
    def nb_channels(self):
        return self.stream.channels

    def iter_decoder(self):
        return self.container.decode(self.stream)

    def pts_to_second(self, pts):
        return pts / self.fps

    def second_to_frame(self, second):
        return int(second * self.fps)

    @property
    def remaining_frames(self):
        return self._decoder_iter

    @property
    def fps(self):
        return self.stream.sample_rate

    def frame_end_pts(self, frame):
        '''Calculate the pts at the end of the frame'''
        return round(frame.pts + (frame.samples / frame.sample_rate) *  self.stream.time_base)
