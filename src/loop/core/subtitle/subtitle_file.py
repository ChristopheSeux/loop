from pathlib import Path
import av
import logging
from loop.core.media_file import MediaFile


class SubtitleFile(MediaFile):
    type = 'subtitle'
    
    def __init__(self, media_path):
        super().__init__(media_path)

    # @property
    # def language(self):
    #     return self.stream.language

    # @property
    # def format(self):
    #     return self.stream.type

