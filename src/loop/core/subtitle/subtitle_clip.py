
from loop.core.clip import Clip


class SubtitleClip(Clip):
    type = 'subtitle'

    def __init__(self, audio_file, edit_frame_start=0, parent=None):
        super().__init__(edit_frame_start, parent=parent)

        self.channel = 0
        self.file = audio_file
        self.name = audio_file.name

    @property
    def filepath(self):
        return self.file.filepath