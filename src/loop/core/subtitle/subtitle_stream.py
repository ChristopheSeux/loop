

import av
import numpy as np
import logging
from gadget.core.stream import Stream


class Caption:
    """"Extract information from a subtitle packet"""
    def __init__(self, packet):

        self.text = packet.text
        self.position = 'bottom'
        self.start_frame = packet.pts
        self.duration = packet.duration
        self.end_frame = self.start_frame + self.duration


class SubtitleStream(Stream):
    def __init__(self, subtitle_file):
        super().__init__(file=subtitle_file)

    def iter_decoder(self):
        return self.container.demux(self.stream)

    def seek(self, frame_index):
        '''Seek to the closest subtitle of the frame index requested'''
            #print('Real seek')
        logging.debug(f'Seek {self} to {frame_index}')

        pts = self.frame_to_pts(frame_index)
        self.container.seek(stream=self.stream, offset=pts)

    def decode_next_caption(self):
        caption_packet = next(self._decoder_iter, None)

        if caption_packet is None:
            self.restart()
            return
        
        caption = Caption(caption_packet)
            
        self.frame_index = self.pts_to_frame(caption_packet.pts)
        return self.frame_index, caption