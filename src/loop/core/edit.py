
from pathlib import Path

from loop.core.video.video_file import VideoFile
from loop.core.video.video_clip import VideoClip
from loop.core.audio.audio_file import AudioFile
from loop.core.audio.audio_clip import AudioClip
from loop.core.subtitle.subtitle_file import SubtitleFile
from loop.core.subtitle.subtitle_clip import SubtitleClip

from loop.core.clip import Clip
from canvas.core import Container
from loop.core.media_file import MediaFile


class Clips(Container):
    def __init__(self, edit):
        super().__init__()
        
        self.edit = edit
        self.container = None
        self.child_type = Clip
    
    def get(self, name=None, filepath=None, fallback=None):
        if filepath is not None:
            filepath = Path(filepath)

        return super().get(name, filepath=filepath, fallback=fallback)

    def new(self, media_path, edit_frame_start=None, check_existing=True):
        '''Will create a video clip and a audio strip from the first channel found'''

        if edit_frame_start is None:
            edit_frame_start = self.edit.frame_current

        #video_file = self.edit.sources.video_files.get(filepath=media_path)

        media_file = self.container.new(media_path, check_existing=check_existing)    
        media_clip = self.child_type(media_file, edit_frame_start=edit_frame_start, parent=self)

        # Set the current fps to the media fps
        #self.fps = media_clip.file.fps
        #self.frame_end = media_clip.file.nb_frames -1

        self.add(media_clip)

        #self.edit.set_frame_end()
        #self.edit.fps = media_clip.fps

        return media_clip

        #print(1000/self.fps)
        #video_clip.video.ready.connect(lambda : self.timer.start(int(1000/self.fps)))


class VideoClips(Clips):
    def __init__(self, edit):
        super().__init__(edit)
        
        self.container = self.edit.sources.videos
        self.child_type = VideoClip


class AudioClips(Clips):
    def __init__(self, edit):
        super().__init__(edit)
        
        self.container = self.edit.sources.audios
        self.child_type = AudioClip
        

class SubtitleClips(Clips):
    def __init__(self, edit):
        super().__init__(edit)
        
        self.container = self.edit.sources.subtitles
        self.child_type = SubtitleClip


class MediaFiles(Container):
    """Base Container for medias"""
    def __init__(self, sources): 
        super().__init__(sources)

        self.sources = sources
        self.edit = sources.edit
        self.child_type = MediaFile

    def get(self, name=None, filepath=None, fallback=None):
        if filepath is not None:
            filepath = Path(filepath)

        return super().get(name, filepath=filepath, fallback=fallback)

    def new(self, media_path, check_existing=True):
        media_file = None
        if check_existing:
            media_file = self.get(filepath=media_path)

        if not media_file:
            media_file = self.add(self.child_type(media_path))
        
        return media_file


class VideoFiles(MediaFiles):
    """Container for all videos"""
    def __init__(self, sources): 
        super().__init__(sources)

        self.child_type = VideoFile


class AudioFiles(MediaFiles):
    """Container for all audio files"""
    def __init__(self, edit): 
        super().__init__(edit)

        self.child_type = AudioFile


class SubtitleFiles(MediaFiles):
    """Container for all subtitle files"""
    def __init__(self, edit): 
        super().__init__(edit)

        self.child_type = SubtitleFile


class ImageFiles(MediaFiles):
    """Container for all image files"""
    def __init__(self, edit): 
        super().__init__(edit)

        self.child_type = None


class EditClips(Clips):
    """Container for all clips, video audio or subtitle"""
    def __init__(self, edit): 
        super().__init__(edit)

        self.edit = edit

        self.videos = VideoClips(edit)
        self.audios = AudioClips(edit)
        self.subtitles = SubtitleClips(edit)
        # self.images = []
        # self.effects = []
        # self.solids = []
    
    @property
    def _values(self):
        return (self.videos._values + self.audios._values)

    @property
    def clips(self):
        return self.audio_clips._values + self.video_clips._values# + self.image_clips + self.effect_clips + self.solid_clips

    def clear(self):
        self.videos.clear()
        self.audios.clear()

    def new(self, media_path, edit_frame_start=None, check_existing=True, clip_type='all'):
        
        media_type = self.edit.sources.get_media_type(media_path)

        #print('\n----media_type', media_type, media_type == 'video')

        if clip_type == 'all':
            clips = []

            video = None

            media_file = MediaFile(media_path).open()
            if media_file.streams.video:
                video = self.videos.new(media_path, edit_frame_start=edit_frame_start, check_existing=check_existing)
                clips.append(video)

            if media_file.streams.audio:
                audio = self.audios.new(media_path, edit_frame_start=edit_frame_start, check_existing=check_existing)
                clips.append(audio)
                if video:
                    audio.duration = video.duration
                    #print('Audio and VIdeo', video.frame_end, audio.frame_end)

            #if media_file.streams.subtitles:
                #print('SUBTITLE ADDED')
            #    clips.append(self.subtitles.new(media_path))

            media_file.close()

        elif media_type == 'video':
            clips = self.videos.new(media_path, edit_frame_start=edit_frame_start, check_existing=check_existing)
        elif media_type == 'audio':
            clips = self.audios.new(media_path, edit_frame_start=edit_frame_start, check_existing=check_existing)
        
        self.edit.set_frame_end()
        self.edit.set_fps()

        return clips


class Sources(Container):
    """Container for all source files"""
    def __init__(self, edit): 
        super().__init__(edit)

        self.edit = edit

        self.videos = VideoFiles(self)
        self.audios = AudioFiles(self)
        self.subtitles = SubtitleFiles(self)
        self.images = ImageFiles(self)

        self.video_extensions = {
            '.mov', '.dv', '.avi', '.mp4', '.mts', '.xvid', '.m2v', '.m2t', 
            '.ts', '.r3d', '.avs', '.flc', '.divx', '.ogv', '.wmv', '.movie', 
            '.flv', '.m4v', '.mv', '.m2ts', '.mpeg', '.vob', '.ogg', '.mkv', 
            '.mpg2', '.webm', '.mxf', '.mpg'
        }
        self.image_extensions = {
            '.psb', '.rgba', '.tif', '.bmp', '.sgi', '.dpx', '.jpeg', '.jp2', 
            '.webp', '.jpg', '.tga', '.tx', '.dds', '.j2c', '.cin', '.exr', 
            '.rgb', '.psd', '.hdr', '.tiff', '.png', '.pdd'
        }
        self.audio_extensions = {
            '.ac3', '.aif', '.aiff', '.wma', '.oga', '.eac3', '.ogg', '.wav', 
            '.mp3', '.mp2', '.aac', '.m4a', '.mka', '.flac'
        }
        self.subtitle_extensions = {
            '.srt', '.ass'
        }


    @property
    def _values(self):
        return self.video_files.data + self.audio_files.data

    def clear(self):
        self.video_files.clear()
        self.audio_files.clear()

    def get_media_type(self, media_path):
        media_path = Path(media_path)

        ext = media_path.suffix.lower()

        if ext in self.video_extensions:
            return 'video'

        elif ext in self.audio_extensions:
            return 'audio'

        # elif ext in self.video_extensions:
        #     return self.new_image(media_path)

        #else:

        #    raise Exception(f'Not Supported media {media_path}')


    def new(self, media_path, check_existing=True):
        '''Will create a video clip a audio clip or an image clip depending on the extension'''

        media_type = self.get_media_type(media_path)

        if media_type == 'video':
            return self.video_files.new(media_path, check_existing=check_existing)

        elif media_type == 'audio':
            return self.audio_files.new(media_path, check_existing=check_existing)

        # elif ext in self.video_extensions:
        #     return self.new_image(media_path)

        else:
            raise Exception(f'Not Supported media {media_path}')


class Edit:
    def __init__(self):
        #super().__init__()

        self.frame_current = 0
        self.frame_image = None
        self.fps = 25
        #self.is_fps_default = True
        self.auto_frame_end = True

        self.sources = Sources(self)
        self.clips = EditClips(self)

        self.frame_start = 0
        self.frame_end = 11

    
    # @property
    # def clips(self):
    #     return self.audio_clips._data + self.video_clips._data# + self.image_clips + self.effect_clips + self.solid_clips

    @property
    def duration(self):
        return self.nb_frames * self.fps

    @property
    def nb_frames(self):
        return (self.frame_end - self.frame_start) + 1

    def clear(self):
        self.clips.clear()
        self.sources.clear()

    def set_frame_end(self):
        if not self.clips or not self.auto_frame_end:
            return

        self.frame_end = max(clip.edit_frame_end for clip in self.clips) - 1 # Because Frame range is inclusive

    def set_fps(self):
        if not self.clips.videos or not self.auto_frame_end:
            return

        self.fps = max(clip.fps for clip in self.clips.videos)
    
