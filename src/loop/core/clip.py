

class Clip:
    type = 'clip'
    def __init__(self, edit_frame_start=0, parent=None):

        self.frame_index = 0
        self.frame_start = 0
        self.frame_end = 100
        self.edit_frame_start = edit_frame_start
        self.channel = 0
        self.parent = None
        self.set_parent(parent)

    @property
    def duration(self):
        #print('DURATION', )
        return (self.frame_end - self.frame_start) #+1

    @duration.setter
    def duration(self, value):
        self.frame_end = self.frame_start + value

    @property
    def edit_frame_end(self):
        return self.edit_frame_start + self.duration

    def set_parent(self, parent):
        if parent is not None:
            parent.add(self)

        self.parent = parent
    
    def frame_in_range(self, frame_index):
        return self.edit_frame_start <= frame_index < self.edit_frame_end