
from loop.core.clip import Clip
from loop.core.video.video_stream import VideoStream


class VideoClip(Clip):
    type = 'video'

    def __init__(self, video_file, edit_frame_start=0, parent=None, channel=2):
        super().__init__(edit_frame_start=edit_frame_start, parent=parent)

        self.channel = channel

        self.file = video_file
        self.name = video_file.name

        #self.frame_end = video_file.nb_frames -1

        self.stream = VideoStream(video_file)
        self.fps = self.stream.fps

        #print(self.stream.nb_frames)

        self.duration = self.stream.nb_frames
        self.width = self.stream.width
        self.height = self.stream.height

        #self.player.frame_current_changed.connect(self.frame_change)

    @property
    def filepath(self):
        return self.file.filepath

