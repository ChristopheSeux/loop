

#from __feature__ import snake_case#, true_property

import av
import numpy as np
import logging
from loop.core.stream import Stream


class VideoStream(Stream):
    def __init__(self, video_file):
        super().__init__(file=video_file)

        self.stream = video_file.streams.video[0]
        self.stream.thread_type = "AUTO"
        self.keyframes = set()
        self.start()

    @property
    def codec(self):
        return self.stream.codec_context

    @property
    def width(self):
        return self.codec.width

    @property
    def height(self):
        return self.codec.height

    @property
    def remaining_frames(self):
        return self.stream._decoder_iter

    def seek_keyframe(self, second):
        return self.seek(second)
        #if second > self.duration:
        #    return
        #pts = self.second_to_pts(second)

        #print('seek_keyframe', pts)
        #print(self.nb_frames, float(second), self.duration)

        #self.container.seek(stream=self.stream, offset=pts)
        
        #return self.decode_next_frame()

    # def seek(self, second):
    #     '''Return an iterator of decoded frames until it reach the asked time'''
    #         #print('Real seek')
    #     logging.debug(f'Seek to {second}')

    #     frame_index = self.second_to_frame(second)

    #     # Avoid seeking if the frame is less than 20 frame away
    #     if frame_index > self.frame_index + 20 or frame_index < self.frame_index:

    #         print('SEEK KEYFRAME')
    #         self.seek_keyframe(second)

        #def _decoder_iter():
        #    self.frame_index = -1

        #    while self.frame_index < frame_index:
        #        yield self.decode_next_frame()

        #return _decoder_iter()

    def frame_end_pts(self, frame):
        '''Calculate the pts at the end of the frame'''
        time_base = self.stream.time_base
        num, den = time_base.numerator, time_base.denominator
        return round(frame.pts + den / (num * self.fps))

    def decode_next_frame(self):
        frame = super().decode_next_frame()
        
        if frame and frame.key_frame:
            self.keyframes.add(self.frame_index)

        return frame
    