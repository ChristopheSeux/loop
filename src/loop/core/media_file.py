
from pathlib import Path
import av
import logging


class MediaFile:
    type = 'media'
    
    def __init__(self, media_path):
        super().__init__()

        self.filepath = Path(media_path)
        self.name = self.filepath.name
        self.container = None

        self.open()

    @property
    def streams(self):
        return self.container.streams

    def copy(self):
        return self.__class__(self.filepath)

    def open(self):
        if self.container:
            logging.debug(f'Closing container of {self}')
            self.container.close()

        logging.debug(f'Opening container of {self}')
        self.container = av.open(str(self.filepath))

        return self
    
    def close(self):
        self.container.close()