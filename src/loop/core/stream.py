import av
import numpy as np
import logging


class Stream:
    def __init__(self, file):

        self.file = file
        self._decoder_iter = iter([])
        self.pts = 0
        #self.frame_index = 0 # Last decoded frame index
        self.stream = None
        #self.start()

        self.format_dtypes = {
            'dbl': 'f8',
            'dblp': 'f8',
            'flt': 'f4',
            'fltp': 'f4',
            's16': 'i2',
            's16p': 'i2',
            's32': 'i4',
            's32p': 'i4',
            'u8': 'u1',
            'u8p': 'u1',
        }

    @property
    def container(self):
        return self.file.container

    @property
    def frame_index(self):
        return self.pts_to_frame(self.pts)
    
    @frame_index.setter
    def frame_index(self, frame_index):
        self.pts = self.frame_to_pts(frame_index)

    @property
    def current_time(self):
        return self.pts_to_second(self.pts)

    @property
    def time_base(self):
        return self.stream.time_base

    @property
    def fps(self):
        return self.stream.average_rate

    def copy(self):
        return self.__class__(self.file.copy())

    def decode(self):
        for frame in self.container.decode(self.stream):
            self.pts = frame.pts
            yield frame

    def decoder_iter(self):
        return (f for p in self.container.demux(self.stream) for f in p.decode())

    def start(self):
        self.pts = 0
        self.container.seek(stream=self.stream, offset=-1)
        self._decoder_iter = self.decoder_iter()
    
    def restart(self):
        self.start()

    def second_to_pts(self, second):
        num, den = self.time_base.numerator, self.time_base.denominator
        return round(second * (den / num))

    def pts_to_second(self, pts):
        return float(pts * self.time_base)

    def frame_to_second(self, frame_index):
        return float(frame_index / self.fps)

    def second_to_frame(self, second):
        return round(second * self.fps)

    def pts_to_frame(self, pts):
        num, den = self.time_base.numerator, self.time_base.denominator
        return round(pts * num / den * self.fps)

    def frame_to_pts(self, frame_index):
        num, den = self.time_base.numerator, self.time_base.denominator
        return round(frame_index / self.fps * den / num)

    @property
    def nb_frames(self):
        frames = self.stream.frames
        if not frames and self.duration is not None: # Guess frame number from duration
            frames = round(self.duration * self.fps)

        return frames

    @property
    def duration(self):
        """Get duration of the video in seconds"""
        #pts = self.container.duration

        if self.stream.duration:
            return float(self.stream.duration * self.time_base)

        return float(self.container.duration * self.time_base)

    '''
    def decode_to(self, second):
        frame_index = self.second_to_frame(second)
        self.frame_index = -1
        while self.frame_index < frame_index:
            yield self.decode_next_frame()

    def decode_next_frame(self):
        frame = next(self._decoder_iter, None)

        if frame is None:
            self.restart()
            return
        
        self.pts = frame.pts

        return frame
        
    '''

    def frame_end_pts(self, frame):
        raise Exception('You need to implement frame_end_pts')

    def decode_next_frame(self):
        frame = next(self._decoder_iter, None)

        if frame is None:
            self.restart()
            return None
        
        #frame.index = self.pts_to_frame(frame.pts)
        self.pts = self.frame_end_pts(frame)

        return frame

    def decode_to(self, second):
        target_pts = self.second_to_pts(second)
        if self.pts > target_pts:
            return #yield None

        while self.pts <= target_pts:
            yield self.decode_next_frame()

    def seek(self, second):
            #print('Real seek')
        logging.debug(f'Seek to {second}')

        self.restart()

        self.pts = self.second_to_pts(second)
        self.container.seek(stream=self.stream, offset=self.pts)