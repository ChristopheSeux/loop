

class Context:
    def __init__(self, app):

        self.app = app
        self.buffer_size = 50


    @property
    def edit(self):
        return self.app.edit
    
    @property
    def frame_current(self):
        return self.app.mainWindow.frameIndex

    @property
    def frame_end(self):
        return self.edit.frame_end


