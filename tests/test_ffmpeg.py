import subprocess
from time import perf_counter


input_movie_path = "c:/Users/seuxc/Downloads/01_pets_shading_edge_attributes-2023-03-14_105833-1080p.mp4"
output_movie_path = "c:/Users/seuxc/Downloads/test.mp4"


ffmpeg = 'ffmpeg'
ffmpeg = r"C:\Users\seuxc\Downloads\ffmpeg-master-latest-win64-gpl-shared\bin\ffmpeg.exe"

t0 = perf_counter()
cmd = ['ffmpeg', '-i', input_movie_path, output_movie_path, '-y']
subprocess.call(cmd)

print(f'FFMPEG Time Elapsed: {perf_counter()-t0}')