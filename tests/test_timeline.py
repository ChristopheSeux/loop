
from PySide6.QtCore import QTimeLine
from __feature__ import snake_case#, true_property


t = QTimeLine()
t.set_frame_range(0, 10)
t.set_current_time(120)

print(t.value_for_time(120))
print(t.frame_for_time(0.35))

print(t.current_time())