from PySide6.QtMultimedia import QMediaPlayer, QAudioOutput
from PySide6.QtWidgets import QApplication

from PySide6.QtMultimediaWidgets import QVideoWidget

app = QApplication([])
player = QMediaPlayer()

video_files = [
    #"C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/video (1080p).mp4",
    #"C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/pexels_videos_1722591 (1080p).mp4",
    "C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/pexels_videos_1722591 (1080p).mp4",
    "C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/man_adjusting_the_control_panel (1080p).mp4"
    ]

i = 0
def play_next_video(status_changed=None):
    global i
    if status_changed is not None and status_changed != QMediaPlayer.EndOfMedia:
        return
    
    player.stop()

    if video_files:
        video_path = video_files[i]
        print("play_next_video", video_path)
        #player.setSource('')
        player.setSource(video_path)
        player.play()

        i += 1
    else:
        print("All videos played.")

videoWidget = QVideoWidget()


audioOutput = QAudioOutput()
player.setAudioOutput(audioOutput)
audioOutput.setVolume(50)

player.setVideoOutput(videoWidget)



videoWidget.show()
#player.play()
player.mediaStatusChanged.connect(print)
#player.mediaStatusChanged.connect(play_next_video)
#play_next_video()

player.setSource(video_files[0])
player.play()
player.setSource(video_files[1])
player.play()

app.exec()