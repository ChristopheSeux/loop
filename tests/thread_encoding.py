

import av
from time import perf_counter
from PySide6.QtGui import QImage
from PySide6.QtWidgets import QApplication
from PySide6.QtCore import QThreadPool, QRunnable, QObject, Signal
from __feature__ import snake_case#, true_property
import cv2
import sys

from pathlib import Path
import numpy as np


input_movie_path = "c:/Users/seuxc/Downloads/01_pets_shading_edge_attributes-2023-03-14_105833-1080p.mp4"
input_container = av.open(input_movie_path)
in_video_stream = input_container.streams.video[0]
in_video_stream.thread_type = "AUTO"

output_movie_path = "c:/Users/seuxc/Downloads/test.mp4"
output_container = av.open(output_movie_path, 'w')

out_video_stream = output_container.add_stream('libx264', in_video_stream.average_rate)
out_video_stream.width = in_video_stream.width
out_video_stream.height = in_video_stream.height
out_video_stream.pix_fmt = 'yuv420p'

out_video_stream.thread_type = "AUTO"
#out_video_stream.options = {'preset': 'faster'}


class EncodeFrameRunnable(QRunnable):
    def __init__(self, thread_pool, video_frame) :
        super().__init__()

        self.thread_pool = thread_pool
        self.video_stream = thread_pool.video_stream
        self.container = self.video_stream.container
        self.video_frame = video_frame

    def run(self):
        packets = self.video_stream.encode(self.video_frame)

        #print('Muxing', packets)
        self.container.mux(packets)


class EncodingPool(QThreadPool):
    def __init__(self, video_stream) :
        super().__init__()

        self.video_stream = video_stream
        self.set_max_thread_count(1)


class EditFrameRunnable(QRunnable):
    def __init__(self, thread_pool, video_frame) :
        super().__init__()

        self.thread_pool = thread_pool
        self.video_stream = thread_pool.video_stream
        self.video_frame = video_frame

    def run(self):
        frame = self.video_frame

        #print('Editing frame', frame)

        np_frame = frame.to_ndarray(format='rgb24')

        width, height, channel = frame.width, frame.height, 3

        qimage = QImage(np_frame, frame.width, frame.height, QImage.Format_RGB888)

        ptr = qimage.const_bits()
        arr = np.asarray(ptr).reshape(height, width, channel)  #  Copies the data

        # Convert RGB to YUV, faster than reformat
        yuv_array = cv2.cvtColor(arr, cv2.COLOR_RGB2YUV_I420)

        output_video_frame = av.VideoFrame.from_ndarray(yuv_array, format='yuv420p')
        
        self.thread_pool.encode_frame(output_video_frame)
        


class EditingPool(QThreadPool):
    def __init__(self, in_video_stream, out_video_stream) :
        super().__init__()

        self.video_stream = in_video_stream
        #self.out_video_stream = out_video_stream

        self.encoding_pool = EncodingPool(out_video_stream)        
        self.set_max_thread_count(4)

    def edit_frame(self, frame):
        runnable = EditFrameRunnable(self, frame)
        self.start(runnable)

    def encode_frame(self, frame):
        runnable = EncodeFrameRunnable(self.encoding_pool, frame)
        self.encoding_pool.start(runnable)


editing_pool = EditingPool(in_video_stream, out_video_stream)

t0 = perf_counter()


frame_iter = (frame for packet in input_container.demux(in_video_stream) for frame in packet.decode())
for frame in frame_iter:
    editing_pool.edit_frame(frame)


editing_pool.encoding_pool.wait_for_done()

input_container.close()
output_container.close()


print(output_movie_path, Path(output_movie_path).exists())

print(f'AV Time Elapsed: {perf_counter()-t0}')

# import subprocess

# t0 = perf_counter()
# cmd = ['ffmpeg', '-i', input_movie_path, output_movie_path, '-y']
# subprocess.call(cmd)

# print(f'FFMPEG Time Elapsed: {perf_counter()-t0}')