import av
from time import perf_counter
from PySide6.QtGui import QImage, QColor, QPainter
from PySide6.QtWidgets import QApplication
from PySide6.QtCore import QThreadPool, Qt
from __feature__ import snake_case#, true_property
import cv2

import numpy as np


input_movie_path = "c:/Users/seuxc/Downloads/01_pets_shading_edge_attributes-2023-03-14_105833-1080p.mp4"
input_container = av.open(input_movie_path)
input_video_stream = input_container.streams.video[0]
input_video_stream.thread_type = "AUTO"

output_movie_path = "c:/Users/seuxc/Downloads/test.mp4"
output_container = av.open(output_movie_path, 'w')

output_video_stream = output_container.add_stream('libx264', input_video_stream.average_rate)
output_video_stream.width = input_video_stream.width
output_video_stream.height = input_video_stream.height
output_video_stream.pix_fmt = 'yuv420p'

output_video_stream.thread_type = "AUTO"
#output_video_stream.options = {'preset': 'faster'}

frame_iter = (frame for packet in input_container.demux(input_video_stream) for frame in packet.decode())

t0 = perf_counter()

app = QApplication([])


for frame in frame_iter:
    #print(frame)
    #break

    np_frame = frame.to_ndarray(format='rgb24')

    width, height, channel = frame.width, frame.height, 3

    qimage = QImage(np_frame, frame.width, frame.height, QImage.Format_RGB888)


    painter = QPainter(qimage)

    font = painter.font()
    font.set_pixel_size(32)
    painter.set_pen(QColor(255, 255, 255))
    painter.set_font(font)

    for i in range(0, 20):

        painter.draw_text(50, i*32, 'fnzioeuhizuhbizquehblquebhl')

    painter.end()

    ptr = qimage.const_bits()
    arr = np.asarray(ptr).reshape(height, width, channel)  #  Copies the data

    # Convert RGB to YUV, faster than reformat
    yuv_array = cv2.cvtColor(arr, cv2.COLOR_RGB2YUV_I420)

    output_video_frame = av.VideoFrame.from_ndarray(yuv_array, format='yuv420p')

    packet = output_video_stream.encode(output_video_frame)
    output_container.mux(packet)

input_container.close()
output_container.close()

print(f'AV Time Elapsed: {perf_counter()-t0}')

# import subprocess

# t0 = perf_counter()
# cmd = ['ffmpeg', '-i', input_movie_path, output_movie_path, '-y']
# subprocess.call(cmd)

# print(f'FFMPEG Time Elapsed: {perf_counter()-t0}')
