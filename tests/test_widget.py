
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication)
from PySide6.QtCore import (Qt, QRect, QMargins)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon

from __feature__ import snake_case, true_property

from pathlib import Path

class PushButton(QPushButton):
    def __init__(self, parent=None, emboss=True, icon=None):
        super().__init__(parent=parent, text='Play')

        self.background_color = QColor(40, 40, 40)#QColor(32, 32, 34)
        self.emboss = emboss

        if icon and not isinstance(icon, QIcon):
            #icon_path = icon
            #icon = QIcon(icon_path)
            #print(icon, type(icon))
            #icon.add_file(icon_path)
            #self.icon.add_file(icon)
            self.set_icon(QIcon(str(icon)))
        
        #self._icon = QIcon(icon_path)

        self.set_minimum_height(30)
        self.set_contents_margins(4, 2, 4, 2)

        #print(self.contents_margins)
        
    def paint_event(self, event):
        painter = QPainter(self)

        icon_width, icon_height = self.icon_size().width(), self.icon_size().height()
        
        # Draw Background
        if not self.emboss:
            painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
            painter.set_pen(QPen(QColor(0, 0, 0), 4, Qt.SolidLine, Qt.FlatCap))
            painter.draw_rect(
                0, 0, 
                self.width(), self.height()
            )

        #print(self.icon_rect())
        margins = self.contents_margins()
        icon_rect = QRect(
            0, 0,
            self.width(), self.height()
        )

        #print(self.icon(), icon_rect)
        self.icon().paint(painter, icon_rect)
        #painter.draw_pixmap(self.icon_rect(), self.icon().pixmap(self.icon_size()))

import sys

app = QApplication()


icon = Path(r'C:\Users\Christophe\Documents\Travail\Tools\Python\loop\resources\icons\default\build\build_32.png')
print(icon.exists())
btn = PushButton(icon=icon, emboss=False)
btn.show()

sys.exit(app.exec())