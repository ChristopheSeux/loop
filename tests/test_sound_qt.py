import numpy as np
import av
import time
import io

from PySide6.QtCore import QIODevice, QBuffer, QByteArray
from PySide6.QtMultimedia import QAudioFormat, QAudioOutput, QAudioSink, QMediaDevices 
from PySide6.QtWidgets import QApplication, QPushButton


filename = r"C:\Users\Christophe\Downloads\GE_H264.mp4"

container = av.open(filename)
audio_stream = container.streams.audio[0]

frame_size = audio_stream.frame_size
nb_channel = audio_stream.channels
sample_rate = audio_stream.sample_rate

print('layout', audio_stream.layout)
print('nb_channel', nb_channel)
print('sample_rate', audio_stream.sample_rate)
print('frame_size', frame_size)

# Create a QBuffer to store the audio data
audio_data = QBuffer()
audio_data.open(QBuffer.ReadWrite)


for packet in container.demux(audio_stream):
    for frame in packet.decode():
        np_frame = frame.to_ndarray().T#.astype(np.float64)
        #print(np_frame.dtype)
        #np_frame = np_frame.astype(np.int16, order='C') * 32768.0
        #np_frame = np.frombuffer(frame.planes[0], dtype=np.float32, count=frame.samples)
        
        #byte_data = QByteArray(np_frame.tobytes())
        
        #audio_data.setData(byte_data)
        audio_data.write(np_frame.tobytes())
        #audio_data.write(np_frame.tobytes())

audio_data.seek(0)
audio_data.open(QIODevice.ReadOnly)

# Create a QAudioFormat for the audio data
audio_format = QAudioFormat()
audio_format.setSampleRate(sample_rate)
audio_format.setChannelCount(nb_channel)
audio_format.setSampleFormat(QAudioFormat.Float)
#audio_format.setChannelConfig(QAudioFormat.ChannelConfigStereo)

print('bytesPerFrame', audio_format.bytesPerFrame())

import sys

app = QApplication()
# Create a QAudioOutput and set its format
app.audio_output = QAudioSink(audio_format)

#app.audio_output.setVolume(0.01)

btn = QPushButton(text='test Audio')
btn.clicked.connect(lambda: app.audio_output.start(audio_data))

btn.show()


sys.exit(app.exec())
