import cupy as cp
import numpy as np

def rgb24_to_yuv420p_gpu(rgb_frame):
    height, width, _ = rgb_frame.shape

    # Move the data to the GPU
    rgb_frame_gpu = cp.asarray(rgb_frame)

    # Create empty YUV420p frame on the GPU
    yuv_frame_gpu = cp.zeros((height + height // 2, width, 3), dtype=cp.uint8)

    # Convert RGB to YUV on the GPU
    yuv_frame_gpu[:height, :, 0] = 0.299 * rgb_frame_gpu[:, :, 0] + 0.587 * rgb_frame_gpu[:, :, 1] + 0.114 * rgb_frame_gpu[:, :, 2]
    yuv_frame_gpu[:height, :, 1] = -0.14713 * rgb_frame_gpu[:, :, 0] - 0.288862 * rgb_frame_gpu[:, :, 1] + 0.436 * rgb_frame_gpu[:, :, 2]
    yuv_frame_gpu[:height, :, 2] = 0.615 * rgb_frame_gpu[:, :, 0] - 0.51498 * rgb_frame_gpu[:, :, 1] - 0.10001 * rgb_frame_gpu[:, :, 2]

    # Subsample U and V components on the GPU
    for i in range(0, height, 2):
        yuv_frame_gpu[height + i // 2, :, 1] = 0.25 * (yuv_frame_gpu[i, :, 1] + yuv_frame_gpu[i + 1, :, 1])
        yuv_frame_gpu[height + i // 2, :, 2] = 0.25 * (yuv_frame_gpu[i, :, 2] + yuv_frame_gpu[i + 1, :, 2])

    # Move the result back to the CPU
    yuv_frame_cpu = cp.asnumpy(yuv_frame_gpu)

    return yuv_frame_cpu.astype(np.uint8)

image = np.array([[255, 255, 0], [255, 255, 255], [255, 255, 0], [255, 2, 2]]).reshape(2, 2, 3)
new_im = rgb24_to_yuv420p_gpu(image)

print(new_im)