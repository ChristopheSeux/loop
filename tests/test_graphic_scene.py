from PySide6.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsView, QGraphicsItem, QGraphicsRectItem, QGraphicsTextItem
from PySide6.QtGui import QBrush, QColor, QPen
from PySide6.QtCore import Qt, QRectF, QPointF

class VideoClip(QGraphicsRectItem):
    def __init__(self, x, y, w, h, color, parent=None):
        super().__init__(x, y, w, h, parent)
        self.setBrush(QBrush(QColor(color)))
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            channel = round(value.y() / 100)
            return QPointF(value.x(), channel*100)

        return super().itemChange(change, value) 

    # def mousePressEvent(self, event):
    #     super().mousePressEvent(event)
    #     self.setCursor(Qt.ClosedHandCursor)

    # def mouseReleaseEvent(self, event):
    #     super().mouseReleaseEvent(event)
    #     self.setCursor(Qt.OpenHandCursor)

    # def mouseMoveEvent(self, event):
    #     super().mouseMoveEvent(event)
    #     # limit movement to x-axis only
    #     self.setPos(self.pos().x(), self.y())

class Timeline(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)

        scene = QGraphicsScene(self)
        view = QGraphicsView(scene, self)

        clip1 = VideoClip(0, 0, 100, 100, 'red')
        clip2 = VideoClip(100, 0, 100, 100, 'green')
        clip3 = VideoClip(200, 0, 100, 100, 'blue')

        scene.addItem(clip1)
        scene.addItem(clip2)
        scene.addItem(clip3)

        # add channel names
        channel_names = ['Channel 1', 'Channel 2', 'Channel 3']
        channel_height = 100
        channel_width = 100
        for i, channel_name in enumerate(channel_names):
            text = QGraphicsTextItem(channel_name)
            text.setPos(0, i * channel_height)
            scene.addItem(text)

            line = scene.addLine(channel_width, i * channel_height, scene.width(), i * channel_height, QPen(Qt.black))

        self.setCentralWidget(view)

        print(self.centralWidget)

        self.resize(400, 200)
    
    @property
    def centralWidget(self):
        return super().centralWidget()

if __name__ == '__main__':
    app = QApplication([])
    timeline = Timeline()
    timeline.show()
    app.exec_()