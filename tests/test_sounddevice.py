import sounddevice as sd

import numpy as np
import av

filename = r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\sh003.mp4"
filename = r"C:\Users\Christophe\Downloads\GE_H264.mp4"

container = av.open(filename)
audio_stream = container.streams.audio[0]

frame_size = audio_stream.frame_size
num_channels = audio_stream.channels

print('layout', audio_stream.layout)
print('num_channels', num_channels)
print('sample_rate', audio_stream.sample_rate)
print('frame_size', frame_size)

buffer = np.zeros((num_channels, audio_stream.sample_rate * frame_size), dtype=np.float32)

for packet in container.demux(audio_stream):
    for frame in packet.decode():
        #np_frame = resampler.resample(frame)
        np_frame = frame.to_ndarray()
        #print(np_frame)
        #print(len(np_frame[0]))
        buffer[:, frame.pts:frame.pts + frame.samples] = np_frame

#print(buffer.T)

sd.play(buffer.T, samplerate=audio_stream.sample_rate)
sd.wait()