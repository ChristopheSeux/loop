


from PySide6.QtCore import (Qt, QRunnable, QThreadPool, Signal, QObject, QIODevice, QBuffer, QByteArray)
from PySide6.QtGui import (QImage,)
from PySide6.QtMultimedia import QAudioSink,  QAudioFormat
from PySide6.QtWidgets import QApplication, QPushButton

#from PySide6 import QtNetwork

from __feature__ import snake_case#, true_property

import av
from av.audio.resampler import AudioResampler
import numpy as np
from math import ceil

import time
import logging
import sys
from collections import deque
from pathlib import Path

class MediaFile:
    type = 'media'
    
    def __init__(self, media_path):
        super().__init__()

        self.filepath = Path(media_path)
        self.name = self.filepath.name
        self.container = None

        self.open()

    @property
    def streams(self):
        return self.container.streams

    def copy(self):
        return self.__class__(self.filepath)

    def open(self):
        if self.container:
            logging.debug(f'Closing container of {self}')
            self.container.close()

        logging.debug(f'Opening container of {self}')
        self.container = av.open(str(self.filepath))

        return self
    
    def close(self):
        self.container.close()

class Stream:
    def __init__(self, file):

        self.file = file
        self._decoder_iter = iter([])
        self.pts = 0
        #self.frame_index = 0 # Last decoded frame index
        self.stream = None
        #self.start()

        self.format_dtypes = {
            'dbl': 'f8',
            'dblp': 'f8',
            'flt': 'f4',
            'fltp': 'f4',
            's16': 'i2',
            's16p': 'i2',
            's32': 'i4',
            's32p': 'i4',
            'u8': 'u1',
            'u8p': 'u1',
        }

    @property
    def container(self):
        return self.file.container

    @property
    def frame_index(self):
        return self.pts_to_frame(self.pts)
    
    @property
    def current_time(self):
        return self.pts_to_second(self.pts)

    @frame_index.setter
    def frame_index(self, frame_index):
        self.pts = self.frame_to_pts(frame_index)

    def copy(self):
        return self.__class__(self.file.copy())

    def decode(self):
        for frame in self.container.decode(self.stream):
            self.pts = frame.pts
            yield frame

    def iter_decoder(self):
        return (f for p in self.container.demux(self.stream) for f in p.decode())

    def start(self):
        self.frame_index = -1
        self.container.seek(stream=self.stream, offset=-1)
        self._decoder_iter = self.iter_decoder()
    
    def restart(self):
        self.start()
    '''
    def restart(self):
        print(self.file.name, 'restarted')
        self.seek(-1)
        #self.pts = 0
        self.frame_index = -1
        self._decoder_iter = self.iter_decoder()
        '''

    def second_to_pts(self, second):
        time_base = self.stream.time_base
        num, den = time_base.numerator, time_base.denominator

        return round(second * (den / num))

    def pts_to_second(self, pts):
        return float(pts * self.stream.time_base)

    def frame_to_second(self, frame_index):
        return float(frame_index / self.fps)

    def second_to_frame(self, second):
        return round(second * self.fps)

    def pts_to_frame(self, pts):
        fps = self.fps
        time_base = self.stream.time_base
        num, den = time_base.numerator, time_base.denominator

        return round(pts * num / den * fps)

    def frame_to_pts(self, frame_index):
        fps = self.fps
        time_base = self.stream.time_base
        num, den = time_base.numerator, time_base.denominator

        return round(frame_index / fps * den / num)

    @property
    def nb_frames(self):
        frames = self.stream.frames
        if not frames: # Guess frame number from duration
            frames = round(self.duration * self.fps)

        return frames

    @property
    def duration(self):
        """Get duration of the video in seconds"""
        #pts = self.container.duration
        return float(self.stream.duration * self.stream.time_base)

    def decode_to(self, second):
        frame_index = self.second_to_frame(second)
        self.frame_index = -1
        while self.frame_index < frame_index:
            yield self.decode_next_frame()

    def decode_next_frame(self):
        frame = next(self._decoder_iter, None)

        

        if frame is None:
            self.restart()
            return
        
        self.pts = frame.pts

        return frame

    def seek(self, second):
            #print('Real seek')
        logging.debug(f'Seek to {second}')

        pts = self.second_to_pts(second)
        self.container.seek(stream=self.stream, offset=pts)

class AudioStream(Stream):
    def __init__(self, audio_file):
        super().__init__(audio_file)

        self.stream = audio_file.streams.audio[0]

        #self._chunk_iter = iter([])
        self.start()

        self.dtype = np.dtype(self.format_dtypes[self.stream.format.name])

    @property
    def nb_channels(self):
        return self.stream.channels

    def iter_decoder(self):
        return self.container.decode(self.stream)

    def second_to_frame(self, second):
        return int(second * self.fps)

    @property
    def remaining_frames(self):
        return self._decoder_iter

    @property
    def fps(self):
        return self.stream.sample_rate


class AudioDecoderSignals(QObject):
    #finished = Signal()
    #frame_decoded = Signal(int)
    started = Signal()
    finished = Signal()


class AudioDecoder(QRunnable):
    def __init__(self, buffering) :
        super().__init__()

        self.buffering = buffering
        self.set_auto_delete(True) 
        self.signals = AudioDecoderSignals()

    '''
    def convert_frames(self, stream, frames, start, end):


        return frames[0].to_ndarray().T
        #audio_data.write(np_frame.tobytes())

        dtype = self.buffering.dtype
        nb_channels = self.buffering.nb_channels

        chunk_nb_samples = sum(f.samples for f in frames)
        chunk_start = stream.pts_to_second(frames[0].pts)
        chunk_end = chunk_start + (chunk_nb_samples / stream.fps)

        trim_start = int((start - chunk_start)*stream.fps)
        trim_end = int((chunk_end - end)*stream.fps)

        # print(f"{trim_start=}")
        # print(f"{trim_end=}")

        audio_chunk = np.zeros((nb_channels, chunk_nb_samples), dtype=dtype, order='F')

        for frame in frames:
            sample_offset = 0

            for converted_frame in self.buffering.resampler.resample(frame):
                for channel_index, plane in enumerate(converted_frame.planes):
                    audio_chunk[channel_index:channel_index+1, sample_offset: sample_offset+frame.samples] = np.frombuffer(
                        plane, dtype=dtype, count=converted_frame.samples)
            
            sample_offset += frame.samples

        return audio_chunk[:,trim_start:trim_end]

    def mix_down(self, stack):
        #stack.sort(key=lambda x : x[0].channel)

        for audio_chunk in stack:
            pass
            #pass # Apply opacity transform and blend mode

        return audio_chunk

    def run(self):

        print('Start')
        self.signals.started.emit()

        cache = self.buffering.cache
        fps = self.buffering.fps

        audio_stack = []

        #cache_
        nb_cache_frames = self.buffering.cache_size - len(cache)
        forward_index = self.buffering.frame_current + len(cache)
        #print(f'{nb_cache_frames=}')
        #print(f'cache_len={len(cache)}')

        for cache_frame_index in range(nb_cache_frames):

            print(cache_frame_index, len(cache))
            frame_index = forward_index  + cache_frame_index

            second_start = frame_index / fps
            second_end = (frame_index+1) / fps

            stream = self.buffering.stream
            stream.seek(second_start)

            frames = []
            for frame in stream.decode_to(second_end):
                frames.append(frame)

            audio_stack.append((self.convert_frames(stream, frames, start=second_start, end=second_end)))

            chunk = self.mix_down(audio_stack)
            cache.append(chunk)

            self.buffering.player.write(chunk.tobytes())

        self.signals.finished.emit()
        '''


    def run(self):
        buffer = self.buffering.buffer
        stream = self.buffering.stream

        for i in range(self.buffering.cache_size):

            frame = stream.decode_next_frame()
            if not frame:
                return

            np_frame = frame.to_ndarray().T#.flatten()#.T
            #print(np_frame)
            
            #cache.append(np_frame.tobytes())
            #print('write frame', stream.frame_index)
            buffer.append(np_frame.tobytes())
            #self.buffering.c.write(np_frame.tobytes())
        
        #self.buffering.buffer.seek(0)
        self.signals.finished.emit()




class AudioBuffering(QThreadPool):
    def __init__(self, player) :
        super().__init__()

        #self.queue = [] # Store runnables
        #self.buffer = QBuffer()
        #self.buffer.open(QIODevice.ReadWrite)
        self.buffer = QByteArray()

        self.player = player
        #self.cache = deque()
        self.cache_size = 20 # in frames
        #self.frame_size = 0.02

        self.set_max_thread_count(1)

        self.frame_current = 0

        #audio_file = MediaFile( r"C:\Users\Christophe\Documents\Travail\Tools\Python\loop\resources\videos\mono.mp4")
        audio_file = MediaFile( r"C:\Users\Christophe\Downloads\GE_H264.mp4")
        self.stream = AudioStream(audio_file)

        self.fps = 24
        self.dtype = 'float32'
        self.nb_channels = 1
        self.resampler = AudioResampler('s32', 'stereo', 48000)
    
    def decode(self):

        print('Decode')
        runnable = AudioDecoder(self)
        #runnable.signals.finished.connect(lambda : self.remove(runnable))

        self.start(runnable)

        return runnable

    # def remove(self, runnable):
    #     if runnable in self.queue:
    #         self.queue.remove(runnable)

class AudioDevice(QIODevice):
    def __init__(self, player):
        super().__init__()

        self.player = player

    @property
    def buffer(self):
        return self.player.buffer

    @buffer.setter
    def buffer(self, value):
        self.player.buffer = value

    def read_data(self, maxlen):
        print('read_data', maxlen)
        #if maxlen > len(self.buffer):
        #    raw_data = self.frame.to_bytes()
            #self.buffer += QByteArray(raw_data)

        #data = self.buffer[:maxlen]

        data = self.buffer.left(maxlen)

        #print('data', data.size(), self.buffer.size())

        # Remove read data from the buffer
        self.buffer.remove(0, maxlen)
        #self.buffer = self.buffer[maxlen:]

        #print(data.data())

        return data.data()

    def write_data(self, data):
        return 0

    def bytes_available(self):
        #return 100

        print('bytes_available', self.buffer.size())
        return self.buffer.size() + super().bytes_available()


class AudioPlayer():
    def __init__(self) :
        super().__init__()

        #self.frame_index = 0
        #self.start_time = time.time()
        

        self.buffering = AudioBuffering(self)

        self.format = QAudioFormat()
        self.format.set_sample_rate(48000)
        self.format.set_channel_count(2)
        self.format.set_sample_format(QAudioFormat.Float)

        self.device = AudioDevice(self)
        self.output = QAudioSink(self.format)

        self.output.stateChanged.connect(self.handle_state_changed)

        self.device.open(QIODevice.ReadOnly)

        #self.cache = deque()
        
        #signals.frame_current_changed.connect(self.buffering.decode)

    @property
    def buffer(self):
        return self.buffering.buffer

    @buffer.setter
    def buffer(self, value):
        self.buffering.buffer = value

    def handle_state_changed(self, state):
        print(f"{state=}")

    def volume(self):
        return self.output.volume()

    def set_volume(self, volume):
        self.output.set_volume(volume)

    def debug_print(self):
        return
        print(time.time()- self.start_time)
    

    def toto(self):
        print('READY READ')

    def start(self):
        print('Start Audio')
        #self.output.start(self.buffer)
        
        #self.device.wait_for_ready_read()
        
        #self.device.seek(0)
        self.output.start(self.device)
        print(self.device.is_readable())
        #self.device.readyRead.connect(self.toto)

        #lambda : self.output.start(self.device)

        #self.start_time = time.time()

    def pause(self):
        self.close()
    '''
    def seek(self, second):
        self.stream.abort()
        
        for clip_item in self.clip_items_at(second):

            clip_second = ctx.timeline_widget.frame_to_second(clip_item.clip.edit_frame_start) + second
            clip_item.clip.stream.seek(clip_second)
        
        #self.second = ctx.timeline_widget.frame_to_second(ctx.frame_current)
        self.pts_current = second * self.stream.samplerate
        '''
    #def pause(self):
    #    self.stream.stop()

    def play(self):
        #self.start()
        
        runnable = self.buffering.decode()
        runnable.signals.finished.connect(self.start)

        for i in range(0, 20):

            runnable = self.buffering.decode()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    #player = MusicPlayer(r"C:\Users\Christophe\Downloads\Flow_Animatic_23_light.mov")
    audio_player = AudioPlayer()


    btn = QPushButton(text='test Audio')
    btn.clicked.connect(audio_player.play)

    btn.show()



    sys.exit(app.exec())

