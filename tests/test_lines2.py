
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QStackedLayout
from PySide6.QtGui import QPainter, QPen, QColor
from PySide6.QtCore import Qt
from __feature__ import snake_case#, true_property

class LineWidget(QWidget):
    def paint_event(self, event):
        painter = QPainter(self)
        painter.set_pen(QPen(QColor(Qt.GlobalColor.red)))
        painter.draw_line(0, 0, self.width(), self.height())
        painter.draw_line(self.width(), 0, 0, self.height())

app = QApplication([])
#app.setStyle('Fusion')

window = QWidget()
stacked_layout = QStackedLayout()
window.set_layout(stacked_layout)

label = QLabel("Bottom Widget")

line_widget = LineWidget()
#line_widget.setAttribute(Qt.WidgetAttribute.WA_TransparentForMouseEvents)

stacked_layout.add_widget(line_widget)
stacked_layout.add_widget(label)
stacked_layout.set_stacking_mode(QStackedLayout.StackingMode.StackAll)

window.show()
app.exec()