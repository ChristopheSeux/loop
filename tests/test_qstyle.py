from PySide6.QtWidgets import (QApplication, QStyle, QProxyStyle, QWidget, QVBoxLayout, 
    QSpinBox, QStyleOptionSpinBox, QStylePainter, QPushButton)
from PySide6.QtCore import Qt, QRect
from PySide6.QtGui import QColor, QPalette

COLOR = QColor(255,0,0)

class BlenderStyle(QProxyStyle):
    def drawControl(self, element, option, painter, widget=None):
        if element == QStyle.CE_PushButton:
            self.drawCustomPushButton(element, option, painter, widget)
        else:
            super().drawControl(element, option, painter, widget)
            
    def drawCustomPushButton(self, element, option, painter, widget):
        # Call the base class drawControl to maintain other default drawing


        #if isinstance(widget, QPushButton):
        # Modify the text color of the push button
        text_color = Qt.blue
        painter.setPen(text_color)
        #text_rect = self.subControlRect(element, option, QStyle.SC_ToolButton, widget)
        #painter.drawText(QRect(), Qt.AlignCenter, widget.text())
        #super().drawControl(element, option, painter, widget)

        self.drawItemText(painter, option.rect, Qt.AlignCenter, widget.palette(), True,
                             widget.text(), QPalette.ButtonText)


    def drawBlenderSpinBox(self, option, painter, spinbox):
        #painter.save()

        # Draw the background

        painter = QStylePainter(spinbox)
        #painter.setBrush(COLOR)
        #painter.setPen(COLOR)
        option.palette.setColor(QPalette.Text, Qt.red)#palette().color(QPalette.Background)
        painter.drawControl(QStyle.CE_PushButton, option)

        #super().drawComplexControl(QStyle.CC_SpinBox, option, painter, spinbox)

        # # Draw the value
        # text = str(spinbox.value())
        # metrics = spinbox.fontGeometry()
        # text_rect = option.rect.adjusted(4, 0, -4, 0)
        # painter.drawText(text_rect, Qt.AlignCenter, text)

        #painter.restore()

class Window(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setLayout(QVBoxLayout())


        self.layout().addWidget(QPushButton(text='Test'))

def main():
    app = QApplication([])
    
    blender_style = BlenderStyle()
    app.setStyle(blender_style)

    window = Window()
    window.show()

    # Create and show your application's widgets here
    
    app.exec()

if __name__ == "__main__":
    main()