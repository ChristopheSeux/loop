from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QStackedLayout
from PySide6.QtGui import QPainter, QPen, QColor
from PySide6.QtCore import Qt

class LineWidget(QWidget):
    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setPen(QPen(QColor(Qt.GlobalColor.red)))
        painter.drawLine(0, 0, self.width(), self.height())
        painter.drawLine(self.width(), 0, 0, self.height())

app = QApplication([])
app.setStyle('Fusion')

window = QWidget()
stacked_layout = QStackedLayout()
window.setLayout(stacked_layout)

#layout = QVBoxLayout()
#window.setLayout(layout)

label = QLabel("Bottom Widget")
#layout1 = QVBoxLayout()
#layout1.addWidget(label)

line_widget = LineWidget()
#line_widget.setFixedSize(200, 200)
line_widget.setAttribute(Qt.WidgetAttribute.WA_TransparentForMouseEvents)
#layout2 = QVBoxLayout()
#layout2.addWidget(line_widget)




stacked_layout.addWidget(label)
stacked_layout.addWidget(line_widget)
stacked_layout.setStackingMode(QStackedLayout.StackAll)


window.show()
app.exec()