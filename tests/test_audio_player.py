import sys
import sounddevice as sd
import av
from PySide6.QtWidgets import (QApplication, QMainWindow, QWidget, QHBoxLayout,
                               QVBoxLayout, QSlider, QLabel, QPushButton)
from PySide6.QtCore import Qt, QBuffer

class MusicPlayer(QMainWindow):
    def __init__(self, filename):
        super().__init__()

        self.filename = filename
        self.format = None
        self.container = None
        self.stream = None
        self.player = None
        self.timer = None

        self.init_ui()
        self.init_audio()

    def init_ui(self):
        self.setWindowTitle("Music Player")
        self.setGeometry(100, 100, 500, 200)

        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        h_layout = QHBoxLayout(central_widget)

        self.play_button = QPushButton("Play")
        self.play_button.clicked.connect(self.play_audio)

        self.stop_button = QPushButton("Stop")
        self.stop_button.clicked.connect(self.stop_audio)

        self.seek_slider = QSlider()
        self.seek_slider.setOrientation(Qt.Horizontal)
        self.seek_slider.valueChanged.connect(self.seek_audio)

        self.time_label = QLabel("0:00")

        v_layout = QVBoxLayout()
        v_layout.addWidget(self.play_button)
        v_layout.addWidget(self.stop_button)
        v_layout.addStretch()
        v_layout.addWidget(self.seek_slider)
        v_layout.addWidget(self.time_label)

        h_layout.addLayout(v_layout)
        h_layout.addStretch()

    def audio_callback(self, outdata, frames, time, status):
        if status:
            print(status, file=sys.stderr)
        # Read the audio chunk from the file and convert it to stereo
        
        frame = next(self.container.decode(stream=self.stream))
        #frame = frame.reformat(format='s16', layout='stereo')
        # Copy the audio data to the output buffer and play it
        outdata[:] = frame.to_ndarray().T


    def init_audio(self):
        self.container = av.open(self.filename)
        self.stream = next(s for s in self.container.streams if s.type == 'audio')
        #self.format = sd.Format(channels=self.stream.channels, samplerate=self.stream.rate)
        self.player = sd.OutputStream(channels=self.stream.channels, samplerate=self.stream.rate, callback=self.audio_callback)

        self.seek_slider.setRange(0, int(self.container.duration / 1000000))

    def update_timer(self, *_):
        time_in_seconds = int(self.player.time)
        minutes = time_in_seconds // 60
        seconds = time_in_seconds % 60
        self.time_label.setText(f"{minutes}:{seconds:02}")

    def play_audio(self):
        if not self.player.active:
            self.player.start()
            self.timer = sd.CallbackTimer(self.update_timer, 100)
        elif self.player.paused:
            self.player.stop()
            self.timer.resume()

    def stop_audio(self):
        if self.player.active:
            self.player.stop()
            self.timer.stop()

    def seek_audio(self, value):
        timestamp = value * 1000000
        self.player.seek(timestamp, whence='time')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    player = MusicPlayer(r"C:\Users\Christophe\Downloads\Flow_Animatic_23_light.mov")
    player.show()
    sys.exit(app.exec_())