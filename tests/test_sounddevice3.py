import av
import sounddevice as sd
import time

filename = r"C:\Users\Christophe\Downloads\GE_H264.mp4"
chunk_size = 1024

# Open the audio file and get its properties
container = av.open(filename)
stream = container.streams.audio[0]
samplerate = stream.sample_rate
channels = stream.channels
duration = container.duration / av.time_base

print(f"Playing {filename} - {duration:.2f} seconds")

frame = next(container.decode(audio=0))

# Create a callback function to play audio chunks
def audio_callback(outdata, frames, time, status):
    if status:
        print(status, file=sys.stderr)
    # Read the audio chunk from the file and convert it to stereo
    
    frame = next(container.decode(audio=0))
    #frame = frame.reformat(format='s16', layout='stereo')
    # Copy the audio data to the output buffer and play it
    outdata[:] = frame.to_ndarray().T

# Create a SoundDevice stream and start playing audio
stream = sd.OutputStream(channels=channels, blocksize=chunk_size,
                          samplerate=samplerate, callback=audio_callback)
stream.start()
stream.stop()
stream.start()
time.sleep(10)
#stream.start()
#with stream:
#    sd.sleep(int(duration * 200))