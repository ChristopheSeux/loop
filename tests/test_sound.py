import numpy as np
import av
import time
import io 
from pydub import AudioSegment

from PySide6.QtCore import QIODevice, QBuffer
from PySide6.QtMultimedia import QAudioFormat, QAudioOutput, QAudioSink, QMediaDevices 
from PySide6.QtWidgets import QApplication, QPushButton

# Load the audio data from the movie file using PyAV
#filename = r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\sh001.wav"
filename = r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\sh003.mp4"
#filename = r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\sh003_test_44100.mp40001-0145.mov"
filename = r"C:\Users\Christophe\Downloads\GE_H264.mp4"

container = av.open(filename)
audio_stream = container.streams.audio[0]

frame_size = audio_stream.frame_size
nb_channel = audio_stream.channels
sample_rate = audio_stream.sample_rate

print('layout', audio_stream.layout)
print('nb_channel', nb_channel)
print('sample_rate', audio_stream.sample_rate)
print('frame_size', frame_size)
print('codec')


print()

resampler = av.AudioResampler(
    format=av.AudioFormat('s16').packed,
    layout=audio_stream.layout,
    rate=44100)


format_dtypes = {
    'dbl': 'f8',
    'dblp': 'f8',
    'flt': 'f4',
    'fltp': 'f4',
    's16': 'i2',
    's16p': 'i2',
    's32': 'i4',
    's32p': 'i4',
    'u8': 'u1',
    'u8p': 'u1',
}

#buffer = np.zeros((2, sample_rate * frame_size), dtype=np.float32)

#audio_stream.thread_type = "AUTO"

t0 = time.perf_counter()
dtype = np.dtype(format_dtypes[audio_stream.format.name])
print()
print(audio_stream.format.name, dtype)
'''
for frame in container.decode(audio=0):
    #plane = frame.planes[0]
    import numpy as np


    #if frame.format.is_planar:
    #    count = frame.samples
    #else:
    #    count = frame.samples * len(frame.channels)
    count = frame.samples
    #print('samples', frame.samples)
    #print(frame.frame_size)

    #data = np.empty((frame.samples, len(frame.planes)), dtype=dtype)
    #for i, p in enumerate(frame.planes):
    #    #print('shape', np.frombuffer(p, dtype=dtype).shape)
    #    data[:, i] = np.frombuffer(p, dtype=dtype, count=count)
    
    #data.resize([frame.channels, frame.samples])

    data = np.vstack([np.frombuffer(p, dtype=dtype, count=count) for p in frame.planes])
    
    #print(len(data[0]))




    #data = frame.to_ndarray()
    #print(len(data[0]))
    #print(data)

    #print(dir(plane))
    #print(frame.planes[0])
    #frame.to_bytes()
    #break
    '''
'''
buffer = np.zeros((2, sample_rate * frame_size), dtype=np.float32)
for packet in container.demux(audio_stream):
    #data = bytes(packet)
    #AudioSegment.from_raw(io.BytesIO(data), format="aac", sample_width=frame_size, frame_rate=audio_stream.sample_rate, channels=2)
    #[bytes(frame) for frame in packet.decode()]
    #pass

    for frame in packet.decode():
        #np_frame = resampler.resample(frame)
        np_frame = frame.to_ndarray()
        #print(np_frame)
        #print(len(np_frame[0]))
        buffer[:, frame.pts:frame.pts + frame.samples] = np_frame
'''

print('decoding time', time.perf_counter()-t0)
#raise Exception()
# Create a QBuffer to store the audio data
audio_data = QBuffer()
audio_data.open(QBuffer.ReadWrite)


for packet in container.demux(audio_stream):
    for frame in packet.decode():
        np_frame = frame.to_ndarray()
        audio_data.write(np_frame.tobytes())

audio_data.seek(0)

# Create a QAudioFormat for the audio data
audio_format = QAudioFormat()
audio_format.setSampleRate(sample_rate)
audio_format.setChannelCount(nb_channel)
audio_format.setSampleFormat(QAudioFormat.Float)
audio_format.setChannelConfig(QAudioFormat.ChannelConfigStereo)

#info = QMediaDevices.defaultAudioInput()
#print(info.preferredFormat())
#print('Is Supported', info.isFormatSupported(audio_format))

print('Is Valid', audio_format.isValid())

import sys

app = QApplication()
# Create a QAudioOutput and set its format
app.audio_output = QAudioSink(audio_format)

print('BufferSize', app.audio_output.bufferSize())

app.audio_output.start(audio_data)

# Connect the QBuffer to the QAudioOutput


btn = QPushButton(text='test Audio')
btn.show()


sys.exit(app.exec())



# Play the audio
#audio_device.seek(0)