from PySide6.QtMultimedia import QMediaPlayer
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton, QSlider
from PySide6.QtMultimediaWidgets import QVideoWidget
from PySide6.QtCore import Qt

video_files = [
    #"C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/video (1080p).mp4",
    #"C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/pexels_videos_1722591 (1080p).mp4",
    "C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/pexels_videos_1722591 (1080p).mp4",
    "C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/pexels_videos_1722591 (1080p).mp4",
    "C:/Users/seuxc/Documents/Professionnel/Programmation/loop/resources/videos/man_adjusting_the_control_panel (1080p).mp4"
    ]

class MediaPlayer(QMediaPlayer):
    def __init__(self, source):
        super().__init__()

        self.setSource(source)
        self.mediaStatusChanged.connect(self.status_changed)
        self.play()

    def status_changed(self, status_changed):
        print("status_changed", status_changed)
        if status_changed == QMediaPlayer.BufferedMedia:
            self.pause()


class VideoPlayer(QWidget):
    def __init__(self):
        super(VideoPlayer, self).__init__()

        self.video_widget = QVideoWidget()
        self.media_player = None
        self.media_index = 0

        self.medias_players = []
        for video_file in video_files:
            self.medias_players.append(MediaPlayer(video_file))

        #self.media_player.setVideoOutput(self.video_widget)

        layout = QVBoxLayout(self)
        layout.addWidget(self.video_widget)

        self.play_button = QPushButton("Play", self)
        self.play_button.clicked.connect(self.change_media_source)
        layout.addWidget(self.play_button)

        self.change_source_button = QPushButton("Change Source", self)
        self.change_source_button.clicked.connect(self.change_media_source)
        layout.addWidget(self.change_source_button)

        self.seek_slider = QSlider(Qt.Horizontal)
        self.seek_slider.setMaximum(1000)  # Setting the maximum value for better precision
        self.seek_slider.valueChanged.connect(self.set_media_position)
        self.seek_slider.sliderPressed.connect(self.slider_pressed)
        self.seek_slider.sliderReleased.connect(self.slider_released)

        layout.addWidget(self.seek_slider)

    def slider_pressed(self):
        self.media_player.pause()

    def slider_released(self):
        self.media_player.play()

    def set_media_position(self, position):
        self.media_player.setPosition(position)

    def status_changed(self, status_changed):
        print("status_changed", status_changed)
        if status_changed == QMediaPlayer.EndOfMedia:
            self.change_media_source()

    #def play_video(self):
    #    self.media_player.setSource(video_files[0])
    #    self.media_player.play()

    def change_media_source(self):
        # Change the media source to a new video file=

        self.media_player = self.medias_players[self.media_index]
        self.media_player.setVideoOutput(self.video_widget)
        #self.media_player.setSource(video_files[1])
        #print(self.media_player.source())
        self.media_player.mediaStatusChanged.connect(self.status_changed)
        self.media_player.play()

        self.media_index += 1

if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)

    player = VideoPlayer()
    player.resize(1280, 720)
    player.show()

    sys.exit(app.exec())