import sys
import argparse
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).parent/'src'))
sys.path.append(str(Path(__file__).parent/'lib'))

if __name__ == '__main__':
    # For allowing Python subprocess when Loop is Compiled
    if len(sys.argv) > 1 and sys.argv[1].endswith('.py') :
        sys.argv = sys.argv[1:]
        exec( open(sys.argv[0]).read() )

    else :
        parser = argparse.ArgumentParser(description='Loop Player',
        formatter_class= argparse.ArgumentDefaultsHelpFormatter)

        parser.add_argument("--medias", nargs="?")
        parser.add_argument('--debug', action='store_true')

        args = parser.parse_args()

        #args.medias = [
        #    r"C:\Users\Christophe\Downloads\GE_H264.mp4",
        #]

        #args.medias = [
        #    r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\test_loop\sh001.mp4",
        #    r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\test_loop\sh002.mp4"
        #]
        # args.medias = [
        #     r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\test_loop_cube\cube.mp4",
        #     r"C:\Users\Christophe\Documents\Travail\Flow\pipe\test_transcoder\render\test_loop_cube\suzanne.mp4",
        # ]

        args.medias = [r"C:\Users\seuxc\Downloads\pexels-hans-muggesen-1722591-1920x1080-30fps_2.mp4", r"C:\Users\seuxc\Downloads\pexels-pixabay-854948-1920x1080-30fps.mp4"]

        #args.medias = [r"C:\Users\Christophe\Downloads\The.Mandalorian.S03E06.VOSTFR.1080p.DSNP.WEB-DL.DDP5.1.H264-Wawacity.uno.mkv"]

        from loop.ui.application import LoopApplication

        LoopApplication(**vars(args)).exec()




