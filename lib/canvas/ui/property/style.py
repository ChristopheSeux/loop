
from PySide6.QtCore import QRect, QSize, Qt, QPoint, QMargins, Signal
from PySide6.QtWidgets import QApplication, QSizePolicy, QStyleOption, QLabel, QWidget, QGraphicsDropShadowEffect
from PySide6.QtGui import QPainter, QColor, QPixmap, QPalette, QImage, QFont, QBrush, QPen

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.style import WidgetStyle, ShadowStyle, BorderStyle
from canvas.ui.property.geometry import (PropertyGeometry, BoolGeometry, ColorGeometry, EditableGeometry, 
    IntGeometry, FloatGeometry, PathGeometry, ComboboxGeometry, EnumGeometry, ArrayGeometry)

from canvas.ui.property.state import (PropertyState, BoolState, ColorState, EditableState,
    IntState, FloatState, PathState, ComboboxState, EnumState, ArrayState)



class PropertyStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = PropertyGeometry(self)
        self._state = PropertyState(self)

        self.drop_shadow = ShadowStyle(widget, parent=self)
        self.text_shadow = ShadowStyle(widget.label_widget(), parent=self)

        #self.drop_shadow.set_theme(QApplication.instance().theme().widgets.shadow.drop)
        #self.text_shadow.set_theme(QApplication.instance().theme().widgets.shadow.text)

        self.background_color = QColor(0, 0, 0, 0)
        self.text_color = QColor(200, 200, 200, 255)
        self.value_color = QColor(210, 210, 210, 255)
        self.accent_color = QColor(56, 100, 159)

        self._property_split = None
        self.split_percentage = 0.4

    def update(self):
        self.drop_shadow.update()
        self.text_shadow.update()

        self.geometry.update()
        self.widget.update()

    @property
    def icon(self):
        return self.widget.icon()

    @property
    def emboss(self):
        return self.widget.emboss()

    # @property
    # def name(self):
    #     return self.widget.name()

    # @property
    # def value(self):
    #     return self.widget.value()

    @property
    def property_split(self):
        property_split = self._property_split

        #print(self.layout, hasattr(self.layout, 'property_split'))
        if property_split is None and hasattr(self.layout, 'property_split'):
            property_split = self.layout.property_split()

        return property_split

    #def update(self):
    #    super().update()

    def draw_frame(self, painter, color=None):

        geo = self.geometry

        if color is None: # Determine by the status
            color = QColor(self.background_color)
            if self.state.is_down:
                color = QColor(0, 0, 0, 120)
            elif self.state.hover_edit:
                #(lightness**2 + lightness*0.5)*0.5 * 255
                color = color.lighter(150) # TODO Better way of find best hover color

            elif not self.emboss: # Normal State
                return
        
        geo.rect_shape.paint(painter, brush=color, pen=self.border.color, borders=geo.borders,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    def draw_icon(self, painter):
        if self.icon:
            self.icon.paint(painter, self.geometry.icon_rect)

    def draw_name(self, painter, name=None):
        if name is None:
            name = self.widget.name()
        
        if not name or self.property_split:
            return

        color = QColor(self.text_color)
        if not self.state.under_mouse:
            color = color.darker(110)

        self.draw_text(painter, self.geometry.text_rect, color=color, text=str(name), align=('Left', 'VCenter'))

    def draw_value(self, painter):
        align = (self.text_alignment, 'VCenter')
        if self.widget.name() and not self.property_split:
            align = ('Right', 'VCenter')

        value = self.widget.value()
        self.draw_text(painter, self.geometry.text_rect, text=str(value), align=align,
            color=self.value_color)
    
    def draw_background(self, painter):
        painter.save()
        painter.set_clip_rect(self.geometry.clip_rect)

        self.draw_frame(painter)
        self.draw_icon(painter)

        painter.restore()

    def draw_foreground(self, painter):
        self.draw_name(painter)
        self.draw_value(painter)


class BoolStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = BoolGeometry(self)
        self._state = BoolState(self)

        self.set_theme(QApplication.instance().theme().widgets.bool)
        

    @property
    def show_checkbox(self):
        if self.emboss == 'Auto':
            if self.icon:
                return False
        
        if self.icon:
            return False

        return True
    
    @property
    def collapsible(self):
        return self.emboss

    @property
    def has_emboss(self):
        if self.emboss == 'Auto':
            if self.icon:
                return True
        return False

    def draw_frame(self, painter):
        if not self.has_emboss:
            return

        geo = self.geometry
        color = self.background_color
        #color = QColor(24,24,24)
        if self.widget.value():
            color = QColor(self.enabled_color)
            #color.set_hsv(color.hue(), color.saturation()*0.0, color.value()*0.5)
            color.set_alpha(215)
        elif self.state.under_mouse:
            #(lightness**2 + lightness*0.5)*0.5 * 255
            color = color.lighter(150) # TODO Better way of find best hover color

        geo.rect_shape.paint(painter, brush=color, pen=self.border.color, borders=geo.borders,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    def draw_icon(self, painter):
        if self.show_checkbox:
            return
        
        painter.save()
        if not self.widget.value() and self.has_emboss:
            painter.set_opacity(0.66)
            #print(painter.opacity())

        #painter.draw_pixmap(self.geometry.icon_rect, self.icon.pixmap(self.geometry.icon_size))

        self.icon.paint(painter, self.geometry.icon_rect)

        painter.restore()

    def draw_checkbox(self, painter):
        if not self.show_checkbox:
            return
        #rect.move_center(QPoint(self._content_rect.x(), self.height()*0.5))

        color = self.box_color
        if self.widget.value():
            color = self.enabled_color
            #print(icons[theme.check_icon])
        elif self.state.under_mouse:
            color = color.lighter(125)

        self.geometry.box_shape.paint(painter, brush=color, pen=self.border.color,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

        if self.widget.value():
            self.icons[self.check_icon].paint(painter, self.geometry.box_rect)

    def draw_value(self, painter):
        return

    def draw_name(self, painter, name=None):
        if name is None:
            name = self.widget.name()
        
        if not name or self.property_split:
            return

        color = QColor(self.text_color)
        if not self.state.under_mouse and not self.widget.value():#self.state.is_down or not self.state.under_mouse:
            color = color.darker(110)
        
        align = self.text_alignment
        if align == 'Auto':
            align = 'Left'
            if not self.show_checkbox:
                align = 'Center'

        self.draw_text(painter, self.geometry.text_rect, color=color, text=str(name), align=(align, 'VCenter'))

    def draw_background(self, painter):
        self.draw_frame(painter)
        self.draw_checkbox(painter)
        self.draw_icon(painter)

    def draw_foreground(self, painter):
        self.draw_name(painter)


class ColorStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = ColorGeometry(self)
        self._state = ColorState(self)

        self.box_size = QSize(40, 20)
        self.set_theme(QApplication.instance().theme().widgets.color)
    
    def draw_frame(self, painter):
        if not self.widget.value():
            print('has no widget value', self.widget.name(), self.widget, self.widget.parent())
            return

        color = self.widget.value()
        #color = QColor(24,24,24)
        
        self.geometry.rect_shape.paint(painter, brush=color, pen=self.border.color,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    def draw_name(self, painter, name=None):
        if name is None:
            name = self.widget.name()
        
        #print( self, self.property_split)
        if not name or self.property_split:
            return

        color = QColor(self.text_color)
        if not self.state.under_mouse and not self.widget.value():#self.state.is_down or not self.state.under_mouse:
            color = color.darker(110)
        
        align = self.text_alignment
        if align == 'Auto':
            align = 'Left'

        self.draw_text(painter, self.geometry.text_rect, color=color, text=str(name), align=(align, 'VCenter'),
            font=self.font_size)

    def draw_background(self, painter):
        self.draw_frame(painter)

    def draw_foreground(self, painter):
        self.draw_name(painter)


class EditableStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = EditableGeometry(self)
        self._state = EditableState(self)

        self.set_theme(QApplication.instance().theme().widgets.string)
        self.text_alignment = 'Left'

    def draw_frame(self, painter):
        if self.state.is_edited:

            #self.geometry.rect_shape.paint(painter, brush=self.edit_color, pen=self.border.color,
            #    lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

            self.geometry.rect_shape.paint(painter, brush=self.edit_color)
        else:
            super().draw_frame(painter)


class IntStyle(EditableStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = IntGeometry(self)
        self._state = IntState(self)

        self.button_color = QColor()
        self.button_size = 16

        self.set_theme(QApplication.instance().theme().widgets.int)
    
    @property
    def arrows(self):
        return self.widget.arrows()

    @property
    def has_range(self):
        return self.widget.has_range()

    @property
    def minimum(self):
        return self.widget.minimum()

    @property
    def maximum(self):
        return self.widget.maximum()

    @property
    def value(self):
        return self.widget.value()

    def draw_value(self, painter):
        align = ('Center', 'VCenter')
        if self.widget.name() and not self.property_split:
            align = ('Right', 'VCenter')

        value = self.widget.value()
        if unit := self.widget.unit():
            value = f'{value} {unit}'

        self.draw_text(painter, self.geometry.text_rect, text=str(value), align=align,
            color=self.value_color)

    def draw_arrows(self, painter):
        if not self.arrows or not self.state.under_mouse:
            return
        
        geo = self.geometry

        # Left Arrow
        btn_color = self.button_color#.copy()
        if self.state.hover_left_button:
            btn_color = btn_color.lighter(150)
        
        #border_radius = self.geometry.border_radius
        #radius = (border_radius[0], 0, 0, border_radius[-1])
        #arrow_fill = RectShape(rect=self.geometry.left_button_rect, border_radius=radius)
        #clip = QRect(0, 0, geometry.arrow_size, geometry.height)
        #geo.rect_shape.paint(painter, brush=btn_color, clip=geo.left_button_rect)

        geo.rect_shape.paint(painter, brush=btn_color, pen=self.border.color, borders=geo.borders, clip=geo.left_button_rect,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

        left_icon_rect = QRect(geo.left_button_rect)
        left_icon_rect.set_size(QSize(16, 16))
        left_icon_rect.move_center(geo.left_button_rect.center() - QPoint(0, 1))
        self.icons['arrow_left_small'].paint(painter, left_icon_rect)

        # Right Arrow
        btn_color = self.button_color#.copy()
        if self.state.hover_right_button:
            btn_color = btn_color.lighter(150)

            #btn_color = QColor(255,0,0)

        geo.rect_shape.paint(painter, brush=btn_color, pen=self.border.color, borders=geo.borders, clip=geo.right_button_rect,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

        right_icon_rect = QRect(geo.right_button_rect)
        right_icon_rect.set_size(QSize(16, 16))
        right_icon_rect.move_center(geo.right_button_rect.center() - QPoint(0, 1))
        self.icons['arrow_right_small'].paint(painter, right_icon_rect)

    def draw_slider(self, painter):
        if not self.has_range:
            return
         
        geo = self.geometry
        slider_width = (self.value -self.minimum) / (self.maximum - self.minimum) * geo.width

        self.geometry.rect_shape.paint(painter, brush=self.slider_color, pen=self.border.color, borders=geo.borders,
            clip=QRect(0, 0, slider_width, geo.height),
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    def draw_background(self, painter):
        painter.save()
        painter.set_clip_rect(self.geometry.clip_rect)

        self.draw_frame(painter)

        if self.state.is_edited:
            painter.restore()
            return
        
        self.draw_slider(painter)
        self.draw_arrows(painter)
        self.draw_icon(painter)

        painter.restore()
    
    def draw_foreground(self, painter):
        self.draw_name(painter)
        self.draw_value(painter)


class FloatStyle(IntStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = FloatGeometry(self)
        self._state = FloatState(self)

        self.set_theme(QApplication.instance().theme().widgets.float)


class PathStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = PathGeometry(self)
        self._state = PathState(self)

        self.path_background_color = QColor()
        self.set_theme(QApplication.instance().theme().widgets.path)

        #self.accent_color = 

        
    def draw_value(self, painter):
        value = str(self.widget.value())
        text_rect = self.geometry.text_rect
        elided_text = painter.font_metrics().elided_text(value, Qt.ElideMiddle, text_rect.width())

        self.draw_text(painter, text_rect, text=elided_text, align=('Left', 'VCenter'),
            color=self.value_color)


    def draw_path_icon(self, painter):
        painter.save()
        geo = self.geometry

        path_icon_rect = QRect(geo.path_icon_rect)

        #print(path_icon_rect)

        path_icon_rect.set_size(QSize(16, 16))
        path_icon_rect.move_center(geo.path_icon_rect.center())

        icon = self.icons['folder_fill']

        # icon_hover = icon.pixmap(QSize(16, 16))
        # icon_painter = QPainter(icon_hover)
        # icon_painter.set_composition_mode(QPainter.CompositionMode_SourceIn)
        # icon_painter.fill_rect(icon_hover.rect(), self.accent_color)
        # icon_painter.end()
        # painter.draw_pixmap(path_icon_rect, icon_hover)

        if not self.state.under_path_icon:
            painter.set_opacity(0.6)

        icon.paint(painter, path_icon_rect)

        #painter.end();


        painter.restore()

    def draw_background(self, painter):
        self.draw_frame(painter)
        if not self.state.is_edited:
            self.draw_path_icon(painter)


class ComboboxStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = ComboboxGeometry(self)
        self._state = ComboboxState(self)

        self.set_theme(QApplication.instance().theme().widgets.combobox)
        self.text_alignment = 'Left'
        self.checked_color = QColor(self.accent_color)

    @property
    def text(self):
        return self.widget.text()

    def draw_frame(self, painter):
        state = self.state
        geo = self.geometry
        #border_radius = style.border_radius

        color = self.background_color

        if state.is_down:
            color = self.checked_color
        elif state.under_mouse:
            color = self.background_color.lighter(130)

        geo.rect_shape.paint(painter, brush=color, pen=self.border.color, borders=geo.borders,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    def draw_arrow(self, painter):
        #icon_size = QSize(16, 16)        
        self.icons['arrow_down_small'].paint(painter, self.geometry.arrow_rect)

    def draw_name(self, painter):
        if self.property_split:
            return

        self.draw_text(painter, self.geometry.text_rect, color=self.text_color, text=self.text, 
            align=('Left', 'VCenter'))

    def draw_value(self, painter):
        if self.text:
            align = ('Right', 'VCenter')
        else:
            align = (self.text_alignment, 'VCenter')

        value = self.widget.value()
        self.draw_text(painter, self.geometry.text_rect, text=value, align=align,
            color=self.value_color)
    
    def draw_background(self, painter):
        self.draw_frame(painter)
        self.draw_arrow(painter)
    
    def draw_foreground(self, painter):
        self.draw_name(painter)
        self.draw_value(painter)


class EnumStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._geometry = EnumGeometry(self)
        self._state = EnumState(self)

        self.set_theme(QApplication.instance().theme().widgets.enum)

    def draw_item_frame(self, painter, item):

        #state = item.state()
        #border_radius = self.border_radius
        index = item.index()

        #rect = rect.adjusted(0, 0, -1, -1)
        #rect.move_center(QPoint(self._content_rect.x(), self.height()*0.5))

        color = self.item_color
        # if self.value():
        #     color = theme.enabled_color
        #     #print(icons[theme.check_icon])
        #     icons[theme.check_icon].paint(painter, rect)
        #print(item, self._item_under_mouse, item is self._item_under_mouse)

        if item.is_checked():
            color = self.checked_color

        elif item.under_mouse():
            color = color.lighter(125)

        #fill = RectShape(rect, border_radius=border_radius)
        rect_shape = self.geometry.item_rect_shapes[index]
        rect_shape.paint(painter, brush=color, pen=QColor(80, 80, 80, 255))


        #if self.value():
        #    icons[theme.check_icon].paint(painter, rect.adjusted(0, 1, 0, 1))

    def draw_item_text(self, painter, item):
        index = item.index()
        item_rect = self.geometry.item_rects[index].adjusted(0, -4, 0, 0)
        #print(item.text(),  item_rect, style.text_color)
        elided_text = painter.font_metrics().elided_text(item.text(), Qt.ElideRight, item_rect.width()-2)
        self.draw_text(painter, item_rect, text=elided_text, align=('Center', 'VCenter'), color=self.text_color)

    def draw_background(self, painter):
        for item in self.widget.items():
            self.draw_item_frame(painter, item)

    def draw_foreground(self, painter):
        for item in self.widget.items():
            self.draw_item_text(painter, item)


class ArrayStyle(PropertyStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._state = ArrayState(self)
        self._geometry = ArrayGeometry(self)

        self.set_theme(QApplication.instance().theme().widgets.int_vector)