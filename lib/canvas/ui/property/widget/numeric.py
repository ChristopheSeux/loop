
from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt
from PySide6.QtGui import QPainter, QCursor, QColor, QFont
from PySide6.QtWidgets import QApplication

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.shape import RectShape#, TextShape

from canvas.ui.property.widget.string import EditableWidget
from canvas.ui.property.style import IntStyle, FloatStyle

import math


class IntWidget(EditableWidget):
    _decimals = 0
    _type = int

    value_changed = Signal(int)

    def __init__(self, parent=None, value=0, min=None, max=None, step=1,
            unit='', name='', emboss=True, slider=False, arrows=True, icon=None, offset=True):

        super().__init__(parent=parent, name=name, value=value, emboss=emboss, icon=icon)

        #self._state = NumericState(self)
        #self._geometry = NumericGeometry(self)
        self._style = IntStyle(self)

        self._arrows = arrows
        self._slider = slider
        self._offset = offset
        self._unit = ''
        self._step = 1
        self._minimum = None
        self._maximum = None

        self.set_value(value)
        self.set_unit(unit)
        self.set_step(step)
        self.set_range(min, max)

        self.clicked.connect(self.edit_entering)

        self.set_mouse_tracking(True)

    def set_arrows(self, arrows):
        self._arrows = arrows
    
    def arrows(self):
        return self._arrows

    def has_range(self):
        return (self.minimum() != -2147483648 and self.maximum() != 2147483647)

    def set_unit(self, unit):
        self._unit = unit
    
    def unit(self):
        return self._unit

    def set_step(self, step):
        self._step = step
    
    def step(self):
        return self._step

    def set_range(self, min, max):
        if min is None:
            min = -2147483648
        if max is None:
            max = 2147483647

        self._minimum = min
        self._maximum = max

    def minimum(self):
        return self._minimum

    def set_minimum(self, minimum):
        self._minimum = minimum

    def maximum(self):
        return self._maximum

    def set_maximum(self, maximum):
        self._maximum = maximum

    def eval_string(self, string):
        math_func = ['acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'cosh',
            'degrees', 'e', 'exp', 'fabs', 'floor', 'fmod', 'frexp', 'hypot', 
            'ldexp', 'log', 'log10', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 
            'sqrt', 'tan', 'tanh']

        #use the list to filter the local namespace
        env = {k: getattr(math, k) for k in math_func}
        #add any needed builtins back in.
        env['abs'] = abs

        try:
            return eval(string, {"__builtins__": None}, env)
        except Exception:
            print(f'Cannot evaluate {string}')

    def normalize_value(self, value):
        if isinstance(value, str):
            value = self.eval_string(value)
            if value is None:
                return self._value

        if self.minimum() is not None:
            value = max(value, self._minimum)
        
        if self.maximum() is not None:
            value = min(value, self._maximum)

        value = round(value, self._decimals)
        return self._type(value)

    def set_value(self, value):
        if value is None:
            return

        self._value = self.normalize_value(value)
        self.value_changed.emit(self._value)
        self.update()

    def slider(self):
        return self._slider
    
    def set_slider(self, slider):
        self._slider = slider

    def set_offset(self, offset):
        self._offset = offset
    
    def offset(self):
        return self._offset

    #def enter_event(self, event):
    #    self.set_cursor(Qt.SizeHorCursor)
    #    super().enter_event(event)

    def mouse_press_event(self, event):
        state = self.state()

        state.is_right_down = state.hover_right_button
        state.is_left_down = state.hover_left_button

        state.pos = event.pos()
        state.press_value = self.value()

        super().mouse_press_event(event)

    def mouse_move_event(self, event):
        pos = event.pos()
        state = self.state()
        geo = self.style().geometry

        state.is_right_down = False
        state.is_left_down = False

        if state.is_down:
            state.is_dragged = True
            self.set_cursor(Qt.BlankCursor)

            offset = pos.x() - state.pos.x()
            pos = round(offset * 0.1) * self.step()
            self.set_value(state.press_value + pos)

        else:
            self.set_cursor(Qt.SizeHorCursor)
            state.hover_edit = True

            if self.arrows():
                state.hover_right_button = geo.right_button_rect.contains(pos)
                state.hover_left_button = geo.left_button_rect.contains(pos)

                if state.hover_right_button or state.hover_left_button:
                    self.set_cursor(Qt.ArrowCursor)
                    state.hover_edit = False

                self.update()

    def mouse_release_event(self, event):
        pos = event.pos()
        state = self.state()
        geo = self.style().geometry

        if state.is_right_down:
            self.set_value(self._value + self.step())

        elif state.is_left_down:
            self.set_value(self._value - self.step())
        
        else:
            super().mouse_release_event(event)
            self.set_cursor(Qt.SizeHorCursor)

        state.is_dragged = False
        state.is_down = False

        state.is_right_down = False
        state.is_left_down = False
        state.hover_right_button = geo.right_button_rect.contains(pos)
        state.hover_left_button = geo.left_button_rect.contains(pos)

        QCursor.set_pos(self.map_to_global(state.pos))
    
    '''
    def paint_event(self, event):
        painter = QPainter(self)

        if self.state().is_edited:
            self.style().draw_background(painter, color=self.style().edit_color)
            return

        self.style().draw_background(painter)
        self.style().draw_slider(painter)
        self.style().draw_arrows(painter)
        self.style().draw_icon(painter)
        #self.style().draw_name(painter)
        #self.style().draw_value(painter)
    '''




class FloatWidget(IntWidget):
    _type = float

    value_changed = Signal(float)

    def __init__(self, parent=None, name='', value=0.0, min=None, max=None, step=0.001, decimals=3, unit='',
            emboss=True, slider=False, arrows=True, icon=None, offset=True):

        super().__init__(parent=parent, name=name, emboss=emboss, slider=slider, icon=icon,
            min=min, max=max, step=step, unit=unit, offset=offset, arrows=arrows)

        self._decimals = decimals
        self._style = FloatStyle(self)

        self.set_value(value)

        #▲print(self, step, value, decimals)
        
    def set_decimals(self, decimals):
        self._decimals = decimals
    
    def decimals(self):
        return self._decimals