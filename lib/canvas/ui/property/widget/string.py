
from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt, QObject, QEvent
from PySide6.QtGui import QPainter, QCursor, QPalette, QColor
from PySide6.QtWidgets import QApplication, QLineEdit, QLabel, QWidget, QSizePolicy

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.frame import HBoxFrame
from canvas.ui.behaviour import FocusBlocker
from canvas.ui.text import LineEdit

from canvas.ui.property.style import EditableStyle
from canvas.ui.property.widget.base import PropertyWidget


import math


class LineEditWidget(LineEdit):
    """Widget to be integrated in another widget such as FloatWidget to enter a value"""

    editing_cancelled = Signal()

    def __init__(self, parent=None, emboss=True, text=''):
        super().__init__(parent=parent, emboss=emboss, text=text)

        self.set_frame(False)
    
        self._focus_blocker = FocusBlocker(self, install=True)

        # TODO replace with grab_mouse ?

        self.editing_cancelled.connect(self._focus_blocker.remove)
        self.editing_finished.connect(self._focus_blocker.remove)

        self._focus_blocker.focus_out.connect(self.editing_cancelled.emit)

        self.set_focus()
        self.select_all()
        
    def key_press_event(self, event):        
        if event.key() == Qt.Key_Escape:
            self.editing_cancelled.emit()
        
        super().key_press_event(event)


class EditableWidget(HBoxFrame, PropertyWidget):
    clicked = Signal()
    value_changed = Signal(str)

    def __init__(self, parent=None, value=None, name='', live_update=False, 
            emboss=True, icon=None):

        HBoxFrame.__init__(self)
        PropertyWidget.__init__(self, parent=parent, emboss=emboss, name=name,
            value=value, icon=icon)

        #self.theme().edit_color = QColor(17, 17, 17)

        #self._unit = unit
        #self._pos = QPoint()
        #self._state = EditableState(self)
        #self._geometry = EditableGeometry(self)
        self._style = EditableStyle(self)

        self._line_edit = None
        self._cursor = None
        self._live_update = live_update

        #self.set_contents_margins(4, 0, 4, 0)
        #self.set_mouse_tracking(True)
        #self.set_fixed_height(22)

        #self.set_policy(('Preferred', 'Fixed'))

        self.set_policy(('Preferred', 'Fixed'))

        #print(self.size_policy())

        #print('-----')
        #print(self.maximum_size())
        #self.size_policy().set_vertical_policy(QSizePolicy.Preferred)

    def minimum_size_hint(self):
        size = super().size_hint()
        return QSize(size.width(), 24)

    def live_update(self):
        return self._line_update
    
    def set_live_update(self, live_update):
        self._live_update = live_update

    def edit_entering(self):
        if self.state().is_edited:
            return
        #self.exec()
        #self.set_window_modality(Qt.ApplicationModal)


        self._label_widget.hide()
        self._line_edit = LineEditWidget(parent=self.layout(), emboss=False, text=str(self.value()))


        #self._line_edit.style().emboss = False

        self._line_edit.editing_cancelled.connect(self.edit_cancelled)
        self._line_edit.editing_finished.connect(self.edit_finishing)

        if self._live_update:
            self._line_edit.text_changed.connect(self.set_value)

        #↨self._line_edit.editing_finished

        if self._icon:
            self._line_edit.set_contents_margins(self.icon_size().width() + 2, 0, 0, 0)

        self._cursor = self.cursor()
        self.set_cursor(Qt.IBeamCursor)


        self.state().is_edited = True
        self.state().is_down = True
        self.update()
        

        #self.set_cursor(Qt.IBeamCursor)

        #{self.update()
        self._line_edit.set_focus()

    def edit_cancelled(self):
        self.state().is_edited = False
        self._line_edit.set_parent(None)
        self._label_widget.show()

        if self._cursor:
            self.set_cursor(self._cursor)
            self._cursor = None

        self.update()

    def edit_finishing(self):
        self.state().is_edited = False

        if not self._live_update:
            self.set_value(self._line_edit.text())

        if self._cursor:
            self.set_cursor(self._cursor)
            self._cursor = None

        self._line_edit.set_parent(None)
        self._label_widget.show()
        self.update()

    def mouse_move_event(self, event):
        if not self.state().is_down:
            return

        self.state().is_dragged = True

        super().mouse_move_event(event)

    def mouse_release_event(self, event):
        if event.button() == Qt.LeftButton and not self.state().is_dragged:
            self.clicked.emit()
        
        self.state().is_dragged = False
        self.state().is_down = False

        super().mouse_release_event(event)

    '''
    def paint_event(self, event):
        painter = QPainter(self)

        #self.painter().draw_shadow(painter)

        if self.state().is_edited:
            #fill = RectShape(self.painter().content_rect, border_radius=theme.border_radius)
            #fill.paint(painter, brush=theme.edit_color)

            self.style().draw_background(painter, color=self.style().edit_color)
            self.style().draw_icon(painter)
            return

        self.style().draw_background(painter)
        self.style().draw_icon(painter)
        #self.style().draw_name(painter)
        #self.style().draw_value(painter)
    '''

class StringWidget(EditableWidget):
    pressed = Signal()
    def __init__(self, parent=None, value='', name='', live_update=False, emboss=True, icon=None):
        super().__init__(parent=parent, name=name, value=value, live_update=live_update, emboss=emboss, icon=icon)

        self.set_value(value)
        self.pressed.connect(self.edit_entering)

    def mouse_press_event(self, event):
        super().mouse_press_event(event)

        self.pressed.emit()
