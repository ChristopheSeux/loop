

from PySide6.QtCore import Signal, QSize, QRect, QPoint, QMargins
from PySide6.QtWidgets import (QApplication, QStyleOptionButton, QStyle, QStyleOption, 
    QSizePolicy, QWidget, QColorDialog)
from PySide6.QtGui import QColor, QPainter, QIcon

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.property.widget.base import PropertyWidget
from canvas.ui.frame import HBoxFrame
#from canvas.ui.theme import Theme

from canvas.ui.property.style import ColorStyle


import math


class ColorWidget(HBoxFrame, PropertyWidget):
    value_changed = Signal(QColor)
    def __init__(self, parent=None, value=False, emboss=None, icon=None, name=''):

        HBoxFrame.__init__(self)
        PropertyWidget.__init__(self, parent=parent, emboss=emboss, icon=icon, name=name)

        #self._geometry = ColorGeometry(self)
        self._style = ColorStyle(self)
        
        self.set_icon(icon)
        self.set_value(value)
        self.set_contents_margins(4, 0, 4, 0)
        #self.set_mouse_tracking(True)

        self.set_policy(('Preferred', 'Fixed'))

    def minimum_size_hint(self):
        size = super().size_hint()
        return QSize(size.width(), 24)

    def pick_color(self):
        color = QColorDialog.get_color(initial=self._value, options=QColorDialog.ShowAlphaChannel)
        self.set_value(color)

    def mouse_press_event(self, event):
        self.pick_color()

        super().mouse_press_event(event)
