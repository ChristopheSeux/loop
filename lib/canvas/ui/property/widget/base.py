
from PySide6.QtCore import QRect, QSize, Qt, QPoint, QMargins, Signal
from PySide6.QtWidgets import (QApplication, QSizePolicy, QStyleOption, QLabel, 
    QGraphicsDropShadowEffect, QFrame)
from PySide6.QtGui import QPainter, QColor

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.utils import rename_signals
from canvas.ui.base import BaseWidget
from canvas.ui.property.style import PropertyStyle


class PropertyWidget(BaseWidget):
    value_changed = Signal(object)
    def __init__(self, parent=None, emboss=True, icon=None, name='', value=None, size=None, align=None, policy=None):
        super().__init__(parent=parent, size=size, align=align, policy=policy)

        self._label_widget = PropertyLabel(self)
        
        #self._state = PropertyState(self)
        self._style = PropertyStyle(self)
        #self._geometry = PropertyGeometry(self)

        self._emboss = True
        self._icon_size = QSize(16, 16)
        self._name = name
        self._default_value = value
        self._value = value
        self._icon = None

        self.set_icon(icon)
        self.set_emboss(emboss)

    def label_widget(self):
        return self._label_widget

    def emboss(self):
        return self._emboss
    
    def set_emboss(self, emboss):
        self._emboss = emboss

    def set_icon_size(self, size):
        self._icon_size = size

    def icon_size(self):
        return self._icon_size

    def set_value(self, value):
        self._value = value
        self.value_changed.emit(value)
    
    def value(self):
        return self._value

    def default_value(self):
        return self._default_value

    def set_name(self, name):
        self._name = name
    
    def name(self):
        return self._name

    def set_icon(self, icon):
        self._icon = icon
    
    def icon(self):
        return self._icon

    def size_hint(self):
        return QSize(80, 22)

    def enter_event(self, event):
        self.state().under_mouse = True
        self.state().hover_edit = True
        self.update()

    def leave_event(self, event):
        self.state().under_mouse = False
        self.state().hover_edit = False
        self.update()

    def mouse_press_event(self, event):
        #self._pos = event.pos()
        self.state().is_down = True
        self.update()

    def mouse_release_event(self, event):
        #super().mouse_release_event(event)
        self.state().is_down = False
        self.update()

    def paint_event(self, event):
        painter = QPainter(self)
        self.style().draw_background(painter)


class PropertyLabel(QLabel):
    def __init__(self, parent):
        super().__init__(parent)

        parent.layout().add_widget(self)
        self.property_widget = parent
        self.set_attribute( Qt.WA_TransparentForMouseEvents )

    def paint_event(self, event):
        painter = QPainter(self)
        
        widget = self.property_widget
        widget.style().draw_foreground(painter)