
from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt, QObject, QEvent
from PySide6.QtGui import QPainter, QCursor, QPalette, QColor
from PySide6.QtWidgets import QApplication, QMenu, QScrollArea

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.style import MenuStyle
from canvas.ui.layout import VBoxLayout

from canvas.ui.utils import rename_signals
from canvas.ui.property.core import PropertyGroup

from itertools import groupby
import math


class DropDownMenu(QMenu):
    def __init__(self, parent=None):
        super().__init__(parent)

        rename_signals(self)
        #if parent:
        #    parent.set_menu(self)
        
        #VBoxLayout(self)
        self.set_layout(VBoxLayout())

        #self._state = MenuState(self)
        self._style = MenuStyle(self)


        #self.layout().add_widget(QPushButton(text='test1'))
        #self.layout().add_widget(QPushButton(text='text2'))
        

        #self.set_window_flags(self.window_flags() | Qt.FramelessWindowHint)
        #self.set_auto_fill_background(False)
        #self.set_attribute(Qt.WA_TranslucentBackground)

    #def exec(self, actions, pos, at=None, parent=None):

    def state(self):
        return self._state

    def show_event(self, event):
        super().show_event(event)

        self.move(self.position())
        self.resize(self.parent().width(), self.height())

        self.state().shown = True


        #print(self.position())
        #pos = pos 
        #self.set_geometry(pos.x(), pos.y(), self.width(), self.height())

    def hide_event(self, event):
        super().hide_event(event)
        self.state().shown = False

        print('Hide Event')

    def position(self):
        """Return the position of the menu (above or bellow) the parent"""

        window = self.parent().window()

        y = self.parent().map_to(window, self.parent().rect().bottom_left()).y()
        if self.height() > window.height() - y: # Put menu on top

            return self.parent().map_to_global(QPoint(0, -self.height()))

        #if pos.y() > window.height() / 2:
        #    return self.parent().map_to_global(QPoint(-self.width() + self.parent().width(), - self.height()))
        
        #return self.parent().map_to_global(QPoint(-self.width() + self.parent().width(), 0))
        return self.parent().map_to_global(QPoint(0, self.parent().height()))

    ''' 
    def paint_event(self, event):
        #return super().paint_event(event)
        painter = QPainter(self)


        margins = self.contents_margins()
        #font_metrics = painter.font_metrics()
        content_x = margins.left()

        pen_width = 1
        half_pen_width = pen_width*0.5
        border_radius = 3
        #content_rect = QRectF(self.rect()) - QMarginsF(0, 0, 1, 1)
        #line_rect = QRectF(content_rect) - QMarginsF(*(half_pen_width, )*4)
        #text_rect = QRectF(content_rect) - QMarginsF(10, 0, 10, 1)
        
        painter.set_pen(Qt.NoPen)


        # Draw Shadow
        shadow_color = QColor(20, 20, 20, 230)
        painter.set_brush(shadow_color)
        painter._draw_rounded_rect(self.rect(), border_radius, border_radius)
    '''
