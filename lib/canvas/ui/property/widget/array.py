from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt
from PySide6.QtGui import QPainter, QCursor, QColor
from PySide6.QtWidgets import QApplication, QSizePolicy, QGraphicsDropShadowEffect

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.property.widget.base import PropertyWidget
from canvas.ui.property.widget.numeric import IntWidget,FloatWidget

from canvas.ui.property.style import ArrayStyle
from canvas.ui.frame import HBoxFrame, VBoxFrame
from canvas.ui.behaviour import signal_blocked

import math






class VectorItem:
    def __init__(self, value='', icon=None, tooltip=None, parent=None):
        
        self._value = value
        self._icon = icon
        self._tooltip = tooltip
        self._parent = parent

        #self._state = VectorItemState(self)

    #def state(self):
    #    return self._state

    def index(self):
        return self._parent.items().index(self)

    def parent(self):
        return self._parent

    def value(self):
        return self._value

    def icon(self):
        return self._icon

    def tooltip(self):
        return self._tooltip

    def __repr__(self):
        return f'VectorItem(value={self.value()}, index={self.index()})'


class IntArrayWidget(VBoxFrame, PropertyWidget):
    value_changed = Signal(list)
    def __init__(self, parent=None, value=[1, 1, 1], min=None, max=None, step=1,
            unit='', name='', emboss=True, slider=False, arrows=True, icon=None, offset=True):

        VBoxFrame.__init__(self)
        PropertyWidget.__init__(self, parent=parent, emboss=emboss, name=name,
            value=value, icon=icon)

        self.layout().clear()

        self._style = ArrayStyle(self)
        #self._state = ArrayState(self)
        #self._geometry = ArrayGeometry(self)

        #print(self.geometry())

        self._arrows = arrows
        self._slider = slider
        self._offset = offset
        self._step = 1
        self._unit = ''
        self._minimum = None
        self._maximum = None

        self.set_unit(unit)
        self.set_step(step)
        self.set_range(min, max)

        self.set_policy(('Preferred', 'Fixed'))
        self.set_items(value)

    def set_arrows(self, arrows):
        self._arrows = arrows
    
    def arrows(self):
        return self._arrows

    def has_range(self):
        return (self.minimum != -2147483648 and self.maximum != 2147483647)

    def set_unit(self, unit):
        self._unit = unit
    
    def unit(self):
        return self._unit

    def set_step(self, step):
        self._step = step
    
    def step(self):
        return self._step

    def set_range(self, min, max):
        return IntWidget.set_range(self, min, max)

    def minimum(self):
        return self._minimum

    def set_minimum(self, minimum):
        self._minimum = minimum

    def maximum(self):
        return self._maximum

    def set_maximum(self, maximum):
        self._maximum = maximum

    def slider(self):
        return self._slider
    
    def set_slider(self, slider):
        self._slider = slider

    def set_offset(self, offset):
        self._offset = offset
    
    def offset(self):
        return self._offset

    #def size_hint(self):
    #    return VBoxFrame.size_hint(self)

    #def minimum_size_hint(self):
    #    return VBoxFrame.minimum_size_hint(self)

    def set_value(self, values):
        if len(values) != len(self.default_value()):
            raise ValueError(f'Expected {len(self.default_value)} value got {len(values)}')

        for item, value in zip(self.items(), values):
            with signal_blocked(item):
                item.set_value(value)

        self.value_changed.emit(self.value())
        self.update()
    
    def value(self):
        return [item.value() for item in self.items()]

    def add_item(self, item):
        item = IntWidget(value=item, min=self.minimum(), max=self.maximum(), step=self.step(), unit=self.unit(),
            emboss=self.emboss(), slider=self.slider(), arrows=self.arrows(), icon=self.icon(),
            offset=self.offset(),  parent=self)
        
        item.value_changed.connect(lambda x : self.value_changed.emit(self.value()))

    def set_items(self, items):
        self.layout().clear()
        for item in items:
            self.add_item(item)
        
        self.layout().collapse()

    def items(self):
        return self.layout().widgets()

    def paint_event(self, painter):
        return
    #    #painter = QPainter(self)
    #    return
        #HBoxFrame.paint_event(self, painter)


    #def size_hint(self):
    #    size = super().size_hint()
    #    return QSize(size.width(), 22*len(self.items()))

    '''
    def paint_event(self, painter):
        painter = QPainter(self)

        #self.painter().draw_shadow(painter)
        painter.set_brush(QColor(0,0,0,255))

        for item in self.items():
            self.style().draw_item(painter, item)
    '''

class FloatArrayWidget(IntArrayWidget):
    value_changed = Signal(list)
    def __init__(self, parent=None, value=0, min=None, max=None, step=0.001, decimals=3,
            unit='', name='', emboss=True, slider=False, arrows=True, icon=None, offset=True):

        self._decimals = decimals


        super().__init__(parent=parent, value=value, min=min, max=max, step=step,
            unit=unit, name=name, emboss=emboss, slider=slider, arrows=arrows, icon=icon, offset=offset)

        #print(self.name(), self.step(), self.decimals(), self.minimum(), self.maximum())
        #print(step)
        #print(decimals)


    def set_decimals(self, decimals):
        self._decimals = decimals
    
    def decimals(self):
        return self._decimals

    def add_item(self, item):
        item = FloatWidget(value=item, min=self.minimum(), max=self.maximum(), step=self.step(), 
            decimals=self.decimals(), unit=self.unit(), emboss=self.emboss(), slider=self.slider(), 
            arrows=self.arrows(), icon=self.icon(), offset=self.offset(),  parent=self)
        
        item.value_changed.connect(lambda x : self.value_changed.emit(self.value()))