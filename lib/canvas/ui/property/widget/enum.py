
from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, QStringListModel, Qt, QEvent
from PySide6.QtGui import QColor, QIcon, QPainter, QPalette, QCursor
from PySide6.QtWidgets import (QPushButton, QApplication, QListView, QMenu, QScrollArea, 
    QListWidget, QListWidgetItem, QStyledItemDelegate, QStyleOptionViewItem, QStyle, QLineEdit, 
    QSizePolicy, QBoxLayout, QAbstractScrollArea, QLayout)

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.shape import RectShape
from canvas.ui.frame import HBoxFrame, VBoxFrame #, ScrollArea
from canvas.ui.container import ScrollBar
from canvas.ui.text import LineEdit
from canvas.ui.layout import VBoxLayout
from canvas.ui.behaviour import FocusBlocker
from canvas.ui.utils import rename_signals

from canvas.ui.property.style import ComboboxStyle, EnumStyle
from canvas.ui.property.widget.menu import DropDownMenu
from canvas.ui.property.widget.base import PropertyWidget

import math
#from canvas.ui.theme import Theme





class EnumItem:
    def __init__(self, text='', icon=None, tooltip=None, parent=None):
        
        self._text = text
        self._icon = icon
        self._tooltip = tooltip
        self._parent = parent
        #self._checked = False

        #self._state = EnumItemState(self)

    #def state(self):
    #    return self._state

    #def set_checked(self, checked):
    #    self._checked = checked

    def under_mouse(self):
        return self is self.parent().state().under_mouse

    def is_checked(self):
        return self in self.parent().state().checked_items

    def index(self):
        return self._parent.items().index(self)

    def parent(self):
        return self._parent

    def text(self):
        return self._text

    def icon(self):
        return self._icon

    def tooltip(self):
        return self._tooltip

    def __repr__(self):
        return f'EnumItem(text={self.text()}, checked={self.is_checked()})'





#class EnumListView()


# class EnumlistItem(QListWidgetItem):
#     def __init__(self, parent=None, text='', icon=None):
#         super().__init__(parent)

#         self.set_text(text)


#     def paint_event(self, event):
#         painter = QPainter(self)

class EnumItemDelegate(QStyledItemDelegate):
    # def init_style_option(self, option, index):
    #     super().init_style_option(option, index)
    #     # highlight color
    #     color = (
    #         QColor("gray")
    #         if index.row() % 2 == 0
    #         else QColor("salmon")
    #     )
    #     option.palette.set_color(
    #         QPalette.Normal, QPalette.Highlight, color
    #     )

    def theme(self):
        return QApplication.instance().theme().widgets.enum

    def size_hint(self, option, index):
        return QSize(64, 22)

    def parents(self, level):
        parent = self.parent()
        for i in range(level):
            parent = parent.parent()
        
        return parent
    
    def paint(self, painter, option, index):
        """Override the QStyledItemDelegate paint function.

        Args:
            painter: QPainter * painter
            option: const QStyleOptionViewItem & option
            index: const QModelIndex & index
        """
        opt = QStyleOptionViewItem(option)
        self.init_style_option(opt, index)
        #style = opt.widget.style()

        #print(opt.state)
        item = self.parent().item_from_index(index)
        
        cursor = QCursor.pos()
        pos = opt.widget.viewport().map_from_global(QCursor.pos())

        #print(pos)
        #theme = self.theme()
        style = self.parent().widget().style()
        border_radius = style.geometry.border_radius

        if opt.state & QStyle.State_MouseOver and option.rect.contains(pos) or item is self.parent().current_item():
            fill = RectShape(option.rect, radius=border_radius, pen_width=style.border.width)
            fill.paint(painter, brush=style.checked_color)


        #text_rect_ = style.sub_element_rect(QStyle.CT_ItemViewItem, opt, opt.widget)

        #text = TextShape(option.rect, item.text(), ('Left', 'VCenter'))
        style.draw_text(painter, option.rect.adjusted(5, 0, -5, 0), text=item.text(), align=('Left', 'VCenter'),
            color=style.text_color)

        # self._draw_background()
        # self._draw_icon()
        # self._draw_text(index)
        # self._draw_focus_rect()


class EnumListWidget(QListWidget):
    validated = Signal(str)
    def __init__(self, parent):
        super().__init__(parent)

        self.rename_signals()



        #self.viewport().set_size_policy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        #self.viewport().size_hint = self.minimum_size_hint
        #self.viewport().minimum_size_hint = self.minimum_size_hint
        # QAbstractScrollArea::AdjustIgnored	0	The scroll area will behave like before - and not do any adjust.
        # QAbstractScrollArea::AdjustToContents
        #self.set_size_adjust_policy(QAbstractScrollArea.AdjustIgnored)
        #self.set_maximum_viewport_size(QSize(0, 0))

        self.set_item_delegate(EnumItemDelegate(self))
        self.set_mouse_tracking(True)
        self.set_contents_margins(0, 0, 0, 0)
        self.viewport().set_auto_fill_background(False)
        #self.set_minimum_height(44)

        self.item_clicked.connect(self.validate)

        self.set_vertical_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        self.set_focus_proxy(self.vertical_scroll_bar())
        #self.vertical_scroll_bar().set_focus_policy(Qt.NoFocus)
        #self.set_vertical_scroll_bar(ScrollBar())
        self.item_entered.connect(self.set_current_item)

        self.set_auto_scroll(False)

        #self.set_maximum_height(22*4)


    # def minimum_size_hint(self):
    #     size = super().minimum_size_hint()
    #     height = self.size_hint_for_row(0)* min(self.count(), 4) #+ 2 * self.frame_width()
    #     print('size_hint', height)
    #     return QSize(size.width(), 66)

    def widget(self):
        return self.parent().widget()

    def validate(self, item):
        self.validated.emit(item.text())

    # def mouse_move_event(self, event):

    #     item = self.item_at(event.pos())
    #     if item:
    #         self.set_current_item(item)

    #     super().mouse_move_event(event)

    def filter_items(self, text):
        current_item = None
        text = text.lower()

        for i in range(self.count()):
            item = self.item(i)

            if text and text not in item.text().lower():
                item.set_hidden(True)
                continue
            
            item.set_hidden(False)
            if not current_item:
                current_item = item
        
        self.set_current_item(current_item)

    def _on_item_clicked(self, item):
        print(item)

    def rename_signals(self):
        for key, value in list(self.__dict__.items()):
            norm_key = ''.join([f'_{c.lower()}' if c.isupper() else c for c in key]).lstrip('_')
            setattr(self, norm_key, self.__dict__.pop(key))

    def show_event(self, event):
        height = self.size_hint_for_row(0)* min(self.count(), 4)
        self.set_fixed_height(height)
        self.viewport().set_fixed_height(height)

        self.scroll_to_item(self.item(0))

        super().show_event(event)
        self.set_current_item(self.item(0))


class EnumSearchWidget(LineEdit):
    validated = Signal(str)
    rejected = Signal()
    up = Signal()
    down = Signal()

    def __init__(self, parent):
        super().__init__(parent)

        self._drop_down = parent
        self._list_widget = parent.list_widget()

        icons = QApplication.instance().theme().icons
        
        #self.style().background_color = QColor(0, 0, 0, 0)
        self.add_action(QIcon(icons['search']), QLineEdit.LeadingPosition)

        #self.style().emboss = True

        self.return_pressed.connect(self.validate)
    
    def drop_down(self):
        return self._drop_down

    def list_widget(self):
        return self._list_widget

    def key_press_event(self, event):
        if (event.key() == Qt.Key_Escape):
            return self.rejected.emit()

        if event.key() in (Qt.Key_Return, Qt.Key_Enter):
            return self.validate()

        if (event.key() == Qt.Key_Up):
            return self.up.emit()

        if (event.key() == Qt.Key_Down):
            return self.down.emit()

        super().key_press_event(event)

    def validate(self):
        item = self.list_widget().current_item()
        if item:
            self.validated.emit(item.text())
        else:
            self.rejected.emit()


class EnumDropDown(QMenu):
    #item_clicked = Signal()
    validated = Signal(str)

    def __init__(self, parent):
        super().__init__(parent)

        rename_signals(self)

        self._widget = parent

        self.set_layout(VBoxLayout(margin=4, spacing=4))


        #self.layout().set_size_constraint(QLayout.SetNoConstraint)
        self.set_size_policy(QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Maximum))

        self._focus_blocker = FocusBlocker(self)
        self._focus_blocker.focus_out.connect(self.close)

        self._list_widget = EnumListWidget(self)
        self._search_widget = EnumSearchWidget(self)

        #for i in parent.items():
        #self.list_widget().add_items([i.text() for i in parent.items()])
        #self.list_widget().set_fixed_height(min(4, len(parent.items()))*22)
        #self.set_items()
        
        self.layout().add_widget(self.list_widget())

        #self.set_focus_proxy(self.search_widget())
        self.search_widget().hide()
        
        self.list_widget().validated.connect(self.validated.emit)
        self.search_widget().text_changed.connect(self.list_widget().filter_items)
        self.search_widget().validated.connect(self.validated.emit)
        self.search_widget().rejected.connect(self.close)

        self.search_widget().up.connect(self.select_previous_item)
        self.search_widget().down.connect(self.select_next_item)

        #parent.items_changed.connect(lambda : self.set_items())
        #self.set_maximum_height(22*)
    def widget(self):
        return self._widget

    def set_items(self):
        self.list_widget().clear()
        self.list_widget().add_items([i.text() for i in self._widget.items()])

    def select_next_item(self):
        current_row = self.list_widget().current_row()
        if current_row < self.list_widget().count() -1:
            new_row = current_row+1
            self.list_widget().set_current_row(new_row)
            self.list_widget().scroll_to_item(self.list_widget().item(new_row))

    def select_previous_item(self):
        current_row = self.list_widget().current_row()
        if current_row > 0:
            new_row = current_row-1
            self.list_widget().set_current_row(new_row)
            self.list_widget().scroll_to_item(self.list_widget().item(new_row))

    def list_widget(self):
        return self._list_widget

    def search_widget(self):
        return self._search_widget

    def key_press_event(self, event):
        if event.key() in (Qt.Key_Return, Qt.Key_Enter):
            return self.search_widget().validate()

        if (event.key() == Qt.Key_Escape):
            return self.close()

        self.search_widget().show()
        self.search_widget().set_focus()
        
        if event.text().isalnum():
            self.search_widget().set_text(event.text())

        self.move(self.position())

    def paint_event(self, event):
        style = self.parent().style()

        painter = QPainter(self)

        fill = RectShape(self.rect(), radius=style.geometry.border_radius)
        fill.paint(painter, brush=style.background_color)

    def hide_event(self, event):
        self._focus_blocker.remove()
        self.search_widget().hide()
        super().hide_event(event)

    def show_event(self, event):
        self._focus_blocker.install()

        self.search_widget().clear()

        self.set_focus()
        self.resize(self.parent().width(), self.height())
        self.adjust_size()
        self.move(self.position())

        super().show_event(event)

    def mouse_double_click_event(self, event):
        self.close()
        
    def position(self):
        """Return the position of the menu (above or bellow) the parent"""

        window = self.parent().window()
        height = self.height()
        if not self.search_widget().is_visible(): # Adding the height of the hided search_widget
            height += 28

        y = self.parent().map_to(window, self.parent().rect().bottom_left()).y()
        if height > window.height() - y: # Put menu on top

            self.layout().set_direction(QBoxLayout.BottomToTop)
            return self.parent().map_to_global(QPoint(0, - self.height()))

        self.layout().set_direction(QBoxLayout.TopToBottom)
        return self.parent().map_to_global(QPoint(0, self.parent().height()))


class Combobox(HBoxFrame, PropertyWidget):
    items_changed = Signal(list)
    value_changed = Signal(object)
    pressed = Signal()
    def __init__(self, parent=None, value=None, emboss=True, icon=None, name='', text='',
        items=None, exclusive=True):

        HBoxFrame.__init__(self)
        PropertyWidget.__init__(self, parent=parent, emboss=emboss, icon=icon, name=name)

        self._style = ComboboxStyle(self)

        self._items = []
        self._value = []
        self._exclusive = exclusive
        self._drop_down = None
        self._text = ''

        self.set_drop_down(EnumDropDown(self))
        self.set_items(items)
        self.set_value(value)
        self.set_text(text)
        self.set_policy(('Preferred', 'Fixed'))

    def set_text(self, text):
        self._text = str(text)

    def text(self):
        return self._text

    def minimum_size_hint(self):
        size = super().size_hint()
        return QSize(size.width(), 24)

    def drop_down(self):
        return self._drop_down

    def show_drop_down(self):
        #print('show_drop_down')
        self.drop_down().show()

    def close_drop_down(self):
        #print('close_drop_down')

        if self._drop_down.is_visible():
            self._drop_down.close()

        self.style().state.is_down = False
        self.update()

    def set_drop_down(self, drop_down):
        self._drop_down = drop_down
        drop_down.about_to_hide.connect(self.close_drop_down)
        drop_down.validated.connect(self.set_value)

    def mouse_press_event(self, event):
        if self.state().is_down:
           self.close_drop_down()
           return

        self.state().is_down = True
        #self.pressed.emit()
        self.update()
        self.show_drop_down()

    def set_value(self, value):
        if isinstance(value, (tuple, list)):
            value = set(value)
        else:
            value = {value}

        checked_items = self.state().checked_items
        checked_items.clear()

        checked_items.update(set(i for i in self.items() if i.text() in value))

        self.close_drop_down()

        new_value = self.value()
        if value != new_value:
            self.value_changed.emit(new_value)
        
    def value(self):
        value = [i.text() for i in self.state().checked_items]
        if self.exclusive():
            if value:
                return value[0]
            return ''
        else:
            return value

    def exclusive(self):
        return self._exclusive

    def set_exclusive(self, exclusive):
        self._exclusive = exclusive

    def set_items(self, items):
        value = self.value()

        items = list(dict.fromkeys(items).keys()) # Remove double item value

        self._items[:] = [EnumItem(text=i, parent=self) for i in items]

        if self.exclusive():
            if value not in items:
                value = items[0]
        else:
            value = set(self.value()) & set(items)
            if not value:
                value = items[0]

        self.set_value(value)
        self.drop_down().set_items()
        self.update()

    def items(self):
        return self._items
    

class EnumWidget(Combobox):
    def __init__(self, parent=None, value=None, emboss=True, icon=None, name='', items=None,
            exclusive=True, policy=None):

        super().__init__(value=value, parent=parent, emboss=emboss, icon=icon, name=name,
            items=items, exclusive=exclusive)

        self._style = EnumStyle(self)

        self.set_mouse_tracking(True)
        self.set_value(value)
    
    def leave_event(self, event):
        self.state().item_under_mouse = None
        super().leave_event(event)

    def mouse_dragged_event(self, event):
        item = self.state().item_under_mouse

        #print('mouse_dragged_event', item)

        if self.exclusive():
            self.state().checked_items.clear()
            if item:
                self.state().checked_items.add(item)
        else:
            if self.state().press_state:
                self.state().checked_items.add(item)
            elif item in self.state().checked_items:
                self.state().checked_items.remove(item)
        
    def mouse_move_event(self, event):
        super().mouse_move_event(event)

        state = self.state()

        pos_x = max(0, min(event.pos().x(), self.width()-1))
        index = int(pos_x / self.width() * len(self.items()))
        state.item_under_mouse = self.items()[index]

        #print(state.item_under_mouse, state.item_under_mouse.state().checked)

        if state.is_down:
            self.mouse_dragged_event(event)

        self.update()

    def mouse_press_event(self, event):
        item = self.state().item_under_mouse

        if not item:
            return

        self.state().press_state = not item.is_checked()
        self.state().press_value = self.value()

        if self.exclusive():
            self.state().checked_items.clear()
            if item:
                self.state().checked_items.add(item)
        else:
            if item in self.state().checked_items:
                self.state().checked_items.remove(item)
            else:
                self.state().checked_items.add(item)

        self.state().is_down = True

        self.update()
        #super().mouse_press_event(event)

    def mouse_release_event(self, event):
        state = self.state()
        value = self.value()
        #print(value, self._press_value)
        if state.press_value != value:
            self.value_changed.emit(self.value())

        state.press_value = None
        state.press_state =None

        state.is_down = False


