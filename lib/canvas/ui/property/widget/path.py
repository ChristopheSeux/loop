

from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt, QObject, QEvent
from PySide6.QtGui import QPainter, QCursor, QPalette, QColor
from PySide6.QtWidgets import QApplication, QLineEdit, QLabel, QWidget, QFileDialog

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.property.style import PathStyle
from canvas.ui.property.widget.string import EditableWidget

from canvas.ui.behaviour import FocusBlocker

from pathlib import Path


class PathWidget(EditableWidget):
    open_file_clicked = Signal()
    def __init__(self, parent=None, value=Path(), name='', file_mode='AnyFile', emboss=True):
        super().__init__(value=value, parent=parent, name=name, emboss=emboss)

        self.set_value(value)
        
        #self._state = PathState(self)
        #self._geometry = PathGeometry(self)
        self._style = PathStyle(self)

        self.clicked.connect(self.edit_entering)

        self.file_dialog = None

        self.open_file_clicked.connect(self.open_file_dialog)

        self.set_mouse_tracking(True)
        #self.pressed.connect(self.edit_entering)
        self._file_mode = file_mode
        #self.parent_to(parent)
    
    def file_mode(self):
        return self._file_mode
    
    def set_file_mode(self, file_mode):
        self._file_mode = file_mode

    def open_file_dialog(self):
        self.file_dialog = QFileDialog(self)
        self.file_dialog.file_selected = self.file_dialog.__dict__.pop('fileSelected') if hasattr(self.file_dialog, 'fileSelected') else self.file_dialog.file_selected

        self.file_dialog.set_file_mode(getattr(QFileDialog, self.file_mode()) )
        self.file_dialog.file_selected.connect(self.set_value)
        self.file_dialog.exec()

    def mouse_press_event(self, event):
        state = self.state()
        state.is_path_icon_down = state.under_path_icon
        if state.under_path_icon:
            print('under_path_icon')
            return

        super().mouse_press_event(event)

    def mouse_move_event(self, event):
        super().mouse_move_event(event)

        pos = event.pos()
        state = self.state()
        geo = self.style().geometry

        state.is_path_icon_down = False

        state.under_path_icon = geo.path_icon_rect.contains(pos)

        #if state.under_path_icon:
        self.update()

    def leave_event(self, event):
        super().leave_event(event)
        self.state().under_path_icon = False
        self.update()

    def mouse_release_event(self, event):

        state = self.state()

        if state.is_path_icon_down:
            print(state.is_path_icon_down)
            self.open_file_clicked.emit()
            state.is_path_icon_down = False
        else:
            super().mouse_release_event(event)

    def _norm_value(self, value):
        return Path(value)

    '''
    def paint_event(self, event):
        super().paint_event(event)

        #print(self, 'paint_event')

        painter = QPainter(self)

        if not self.state().is_edited:
            self.style().draw_path_icon(painter)
    '''