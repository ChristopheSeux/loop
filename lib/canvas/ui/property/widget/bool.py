

from PySide6.QtCore import Signal, QSize, QRect, QPoint, QMargins
from PySide6.QtWidgets import (QApplication, QStyleOptionButton, QStyle, QStyleOption, QSizePolicy, 
    QWidget, QGraphicsDropShadowEffect)
from PySide6.QtGui import QColor, QPainter, QIcon

from __feature__ import snake_case # pylint: disable=E0401, W0611

import math

from canvas.ui.layout import HBoxLayout

from canvas.ui.property.style import BoolStyle
from canvas.ui.property.widget.base import PropertyWidget
from canvas.ui.frame import HBoxFrame
#from canvas.ui.theme import Theme


class BoolWidget(HBoxFrame, PropertyWidget):
    value_changed = Signal(bool)
    def __init__(self, parent=None, value=False, emboss=None, icon=None, name='',
            align=None):

        HBoxFrame.__init__(self)        
        PropertyWidget.__init__(self, parent=parent, emboss=emboss,
            align=align, icon=icon, name=name)

        self._style = BoolStyle(self)
        #self._geometry = BoolGeometry(self)

        self.set_icon(icon)
        self.set_value(value)
        #self.set_contents_margins(4, 0, 4, 0)
        self.set_mouse_tracking(True)

        self.set_policy(('Preferred', 'Fixed'))
    
    def minimum_size_hint(self):
        size = super().size_hint()
        return QSize(size.width(), 24)

    def set_icon(self, icon):
        super().set_icon(icon)
        if icon and not self.name():
            self.set_fixed_width(self.height())

    def set_value(self, value):
        self._value = value
        self.value_changed.emit(value)

    def value(self):
        return self._value

    def mouse_press_event(self, event):
        self.set_value(not self.value())

        super().mouse_press_event(event)

