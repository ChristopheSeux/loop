from canvas.ui.property.widget.numeric import IntWidget, FloatWidget
from canvas.ui.property.widget.array import IntArrayWidget, FloatArrayWidget
from canvas.ui.property.widget.string import StringWidget
from canvas.ui.property.widget.bool import BoolWidget
from canvas.ui.property.widget.enum import EnumWidget, Combobox
from canvas.ui.property.widget.path import PathWidget
from canvas.ui.property.widget.color import ColorWidget