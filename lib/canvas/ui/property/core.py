from PySide6.QtGui import QColor, QIcon
from PySide6.QtCore import Signal, QObject

from __feature__ import snake_case # pylint: disable=E0401, W0611

from math import ceil

from pathlib import Path
import json
from functools import partial

from canvas.ui.behaviour import signal_blocked
#from canvas.ui.property import (IntWidget, FloatWidget, StringWidget, BoolWidget, 
#    EnumWidget, Combobox, PathWidget, ColorWidget)
#from canvas.ui.property import IntWidget, FloatWidget, StringWidget, BoolWidget, EnumWidget, Combobox

class PropertyGroupSignals(QObject):
    value_changed = Signal(str, object)


class PropertyGroup:
    def __init__(self, parent=None, id=None, **kargs):
        super().__init__()
        
        self.__dict__['parent'] = parent
        #self.set_cache_dir(path)

        self._signals = PropertyGroupSignals()
        self._id = id

        for key, value in kargs.items():
            setattr(self, key, value)

    @property
    def value_changed(self):
        return self._signals.value_changed
    
    def to_widgets(self, parent=None):
        parent = parent or self.parent
        for prop in self:
            prop.to_widget(parent)

    def clear(self):
        for prop in self.keys():
            del self.__dict__[prop]

    def items(self):
        return self.to_dict().items()

    def keys(self):
        return self.to_dict().keys()

    def values(self):
        return self.to_dict().values()

    def to_dict(self):
        return {k: v.value for k, v in self.__dict__.items() if isinstance(v, Property)}

    def save(self):
        parents = [obj := self]
        while obj := obj.parent:
            parents.append(obj)
        
        #print([p.__class__.__name__ for p in parents])

    def __getattribute__(self, key):

        attr = super().__getattribute__(key)
        if isinstance(attr, Property):
            return attr.value
        else:
            return attr

    def __setattr__(self, key, value):
        if isinstance(value, (Property, PropertyGroup)):
            value.value_changed.connect(lambda x: self.value_changed.emit(key, x))
            if hasattr(value, "name") and value.name is None:
                value.name = key.replace('_', ' ').title()

        elif isinstance(value, PropertyGroup):
            value.value_changed.connect(self.value_changed.emit)

        elif isinstance(prop := self.__dict__.get(key), Property):
            prop.value = value
            return

        #print('__setattr__', self.__class__, key, value)

        super().__setattr__(key, value)

    def __getitem__(self, key):
        if isinstance(prop := self.__dict__.get(key), Property):
            return prop
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        if isinstance(prop := self.__dict__.get(key), Property):
            with signal_blocked:
                prop.value = value
        else:
            raise KeyError(key)

    def __iter__(self):
        return (v for v in self.__dict__.values() if isinstance(v, Property))

    def __repr__(self):
        text = 'PropertyGroup(\n'
        for k, v in self.items():
            text += f'    {k}: {v}\n'
        text+= ')'
        return text


class PropertySignals(QObject):
    value_changed = Signal(object)


class Property:
    __slots__ = ('name', 'icon', '_value', 'default_value', 'widgets', '_signals')

    def __init__(self, 
            value: object, 
            name: (str | None)=None,
            icon: (str | object)=None,
            connect: (Signal | None)=None):

        self._value = value
        self._signals = PropertySignals()

        self.default_value = value
        self.name = name
        self.widgets = set()
        self.icon = icon

        if connect:
            self.value_changed.connect(connect)

    @property
    def value_changed(self):
        return self._signals.value_changed

    def norm_value(self, value):
        return value

    @property
    def value(self):
        return self.norm_value(self._value)

    @value.setter
    def value(self, value):
        value = self.norm_value(value)
        self._set_value(value)

    def _set_value(self, value, widget=None):
        self._value = value
    
        #print('_set_value', value, widget)

        for wgt in self.widgets:
            if wgt is widget:
                continue

            with signal_blocked(wgt):
                wgt.set_value(value)
        
        self.value_changed.emit(self.value)

    def to_widget(self, widget, parent=None, name=None, icon=None, **kargs):
        """ if widget is going to replace it"""

        if name is None:
            name = self.name
        
        if icon is None:
            icon = self.icon

        # params = {}
        # for attr, value in kargs.items():
        #     if value is None and hasattr(self, attr):
        #         value = getattr(self, attr)

        #     params[attr] = value

        #print(params)
        #print(dir(widget.__init__))

        widget_instance = widget(parent=parent, value=self.value, name=name, **kargs)
        widget_instance.default_value = self.default_value
        
        widget_instance.value_changed.connect(lambda x: self._set_value(x, widget=widget_instance))
        widget_instance.destroyed.connect(lambda: self.widgets.remove(widget_instance))

        self.widgets.add(widget_instance)

        #print(widget)

        return widget_instance


class IntProperty(Property):
    __slots__ = ('min', 'max', 'step', 'unit')

    def __init__(self,
            value: int=0,
            name: (str | None)=None,
            icon: (str | object)=None,
            min: (float | int | None)=None,
            max: (float | int | None)=None,
            step: (float | int)=1,
            unit: str='',
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, icon=icon, connect=connect)

        self.min = min
        self.max = max
        self.step = step
        self.unit = unit

    def to_widget(self, parent=None, name=None, emboss=True, slider=False,
            arrows=True, offset=True, icon=None):

        from canvas.ui.property.widget.numeric import IntWidget

        return super().to_widget(IntWidget, parent=parent, name=name, min=self.min, max=self.max,
            step=self.step, unit=self.unit, emboss=emboss, slider=slider, arrows=arrows, offset=offset, icon=icon)


class IntArrayProperty(Property):
    __slots__ = ('min', 'max', 'step', 'size', 'unit')

    def __init__(self,
            value: (list|tuple)=None,
            name: (str | None)=None,
            icon: (str | object)=None,
            min: (float | int | None)=None,
            max: (float | int | None)=None,
            step: (float | int)=1,
            size: int=3,
            unit: str='',
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, icon=icon, connect=connect)

        self.min = min
        self.max = max
        self.step = step
        self.size = size
        self.unit = unit

        if value is not None:
            self.size = len(value)

    def to_widget(self, parent=None, name=None, unit='', emboss=True, slider=False,
            arrows=True, offset=True, icon=None):

        from canvas.ui.property.widget.array import IntArrayWidget

        return super().to_widget(IntArrayWidget, parent=parent, name=name, min=self.min, max=self.max,
            step=self.step, unit=unit, emboss=emboss, slider=slider, arrows=arrows, offset=offset, icon=icon)


class FloatArrayProperty(IntArrayProperty):
    def to_widget(self, parent=None, name=None, unit='', emboss=True, slider=False,
            arrows=True, offset=True, icon=None):

        from canvas.ui.property.widget.array import FloatArrayWidget

        return Property.to_widget(self, FloatArrayWidget, parent=parent, name=name, min=self.min, max=self.max,
            step=self.step, unit=unit, emboss=emboss, slider=slider, arrows=arrows, offset=offset, icon=icon)


class FloatProperty(Property):
    __slots__ = ('min', 'max', 'step', 'unit', 'decimals')

    def __init__(self,
            value: float=0.0,
            name: (str | None)=None,
            icon: (str | object)=None,
            min: (float | int | None)=None,
            max: (float | int | None)=None,
            step: (float | int)=0.001,
            decimals: int=3,
            unit: str='',
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, icon=icon, connect=connect)

        self.min = min
        self.max = max
        self.step = step
        self.unit = unit
        self.decimals = decimals

    def to_widget(self, parent=None, name=None, emboss=True, slider=False,
            arrows=True, offset=True, icon=None):

        from canvas.ui.property.widget.numeric import FloatWidget

        return super().to_widget(FloatWidget, parent=parent, name=name, min=self.min, max=self.max, step=self.step,
            decimals=self.decimals, unit=self.unit, emboss=emboss, slider=slider, arrows=arrows, offset=offset, icon=icon)


class StringProperty(Property):
    __slots__ = ('icon',)

    def __init__(self,
            value: str='',
            name: (str | None)=None,
            icon: (str | object)=None,
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, icon=icon, connect=connect)

        self.icon = icon

    def to_widget(self, parent=None, name=None, icon=None, emboss=True):
        from canvas.ui.property.widget.string import StringWidget

        return super().to_widget(StringWidget, parent=parent, name=name, emboss=emboss, icon=icon)


class PathProperty(Property):
    def __init__(self,
            value: (Path|str)=Path(),
            name: (str | None)=None,
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, connect=connect)

    def to_widget(self, parent=None, name=None, icon=None):
        from canvas.ui.property.widget.path import PathWidget

        return super().to_widget(PathWidget, parent=parent, name=name)


class BoolProperty(Property):
    def __init__(self,
            value: bool=False,
            name: (str | None)=None,
            icon: (str | object)=None,
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, icon=None, connect=connect)

    def to_widget(self, parent=None, name=None, icon=None, emboss=None):
        """ if widget is going to replace it"""

        from canvas.ui.property.widget.bool import BoolWidget

        return super().to_widget(BoolWidget, parent=parent, name=name, icon=icon, emboss=emboss)


class ColorProperty(Property):
    def __init__(self,
            value: (QColor|tuple|list)=QColor(),
            name: (str | None)=None,
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name,  connect=connect)

    def norm_value(self, value):
        if isinstance(value, list|tuple):
            value = QColor(*value)
        return value

    def to_widget(self, parent=None, name=None, emboss=None):
        """ if widget is going to replace it"""

        from canvas.ui.property.widget.color import ColorWidget

        return super().to_widget(ColorWidget, parent=parent, name=name, emboss=emboss)


class EnumProperty(Property):
    __slots__ = ('icon', 'exclusive', '_items')

    items_changed = Signal(list)

    def __init__(self,
            value: object,
            name: (str | None)=None,
            icon: (str | object)=None,
            items: (list | tuple)=None,
            exclusive: bool=True,
            connect: (Signal | None)=None):
        
        super().__init__(value=value, name=name, icon=icon, connect=connect)

        self._items = items
        self.exclusive = exclusive


    def _set_items(self, items, widget=None):
        self._items = items
        for wgt in self.widgets:
            if wgt is widget:
                continue

            with signal_blocked(wgt):
                wgt.set_items(items)
        
        #self.items_changed.emit(self.items)

        if self.value not in items:
            self.value = items[0]

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, items):
        
        self._set_items(items)

    def to_widget(self, parent=None, name=None, expand=None, icon=None, text=''):
        """ if widget is going to replace it"""

        from canvas.ui.property.widget.enum import EnumWidget, Combobox

        if expand is None:
            expand = len(self.items) < 4

        if expand:
            widget_instance = super().to_widget(EnumWidget, parent=parent, name=name, items=self.items,
                exclusive=self.exclusive, icon=icon)
        else:
            widget_instance = super().to_widget(Combobox, parent=parent, name=name, items=self.items,
                exclusive=self.exclusive, icon=icon, text=text)

        widget_instance.items_changed.connect(lambda x: self._set_items(x, widget=widget_instance))

        return widget_instance