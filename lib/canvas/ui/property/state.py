

from PySide6.QtCore import QPoint
from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.state import WidgetState


class PropertyState(WidgetState):
    def __init__(self, style):
        super().__init__(style)

        self.under_mouse = False
        self.hover_edit = False
        self.is_down = False


class BoolState(PropertyState):
    def __init__(self, style):
        super().__init__(style)


class ColorState(PropertyState):
    def __init__(self, style):
        super().__init__(style)


class EditableState(PropertyState):
    def __init__(self, style):
        super().__init__(style)

        self.is_edited = False
        self.is_dragged = False
        


class IntState(EditableState):
    def __init__(self, style):
        super().__init__(style)

        self.hover_right_button = False
        self.hover_left_button = False

        self.is_right_down = False
        self.is_left_down = False

        self.pos = QPoint()
        self.press_value = 0


class FloatState(IntState):
    def __init__(self, style):
        super().__init__(style)


class PathState(EditableState):
    def __init__(self, style):
        super().__init__(style)

        self.under_path_icon = False
        self.is_path_icon_down = False


class EnumItemState:
    def __init__(self, widget):
        self.widget = widget
    
    @property
    def checked(self):
        return self.widget in self.widget.parent().state().checked_items

    @property
    def under_mouse(self):
        return self.widget is self.widget.parent().state().item_under_mouse


class EnumState(PropertyState):
    def __init__(self, parent):
        super().__init__(parent)

        self.item_under_mouse = None
        self.press_state = None
        self.press_value = None
        self.checked_items = set()
        #self.edited = False
    
    @property
    def drop_down(self):
        #return self.parent.drop_down().state().shown
        return self.widget.drop_down().is_visible()
        #self.parent.drop_down().state().shown


class ComboboxState(PropertyState):
    def __init__(self, style):
        super().__init__(style)

        self.item_under_mouse = None
        self.press_state = None
        self.press_value = None
        self.checked_items = set()
        #self.edited = False
    
    @property
    def drop_down(self):
        #return self.parent.drop_down().state().shown
        return self.parent.drop_down().is_visible()
        #self.parent.drop_down().state().shown


class VectorItemState:
    def __init__(self, widget):
        self.widget = widget
    
    @property
    def under_mouse(self):
        return self.widget is self.widget.parent().state().item_under_mouse


class ArrayState(PropertyState):
    def __init__(self, parent):
        super().__init__(parent)