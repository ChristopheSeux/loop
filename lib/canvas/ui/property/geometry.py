
from PySide6.QtCore import Qt, QRect, QRectF, QMargins, QMarginsF, QPoint, QPointF, QSize

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.geometry import WidgetGeometry
from canvas.ui.shape import RectShape


class PropertyGeometry(WidgetGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.icon_rect = QRect()
        self.text_rect = QRect()
        self.label_rect = QRect()
        self.field_rect = QRect()

        self.rect_shape = RectShape()

    # def set_corners(self):
    #     self.corners = [False, False, False, False]

    @property
    def icon_size(self):
        return self.widget.icon_size()

    @property
    def icon_width(self):
        return self.icon_size.width()

    @property
    def icon_height(self):
        return self.icon_size.height()

    def update(self):
        super().update()

        #self.content_rect = QRect(self.rect) - QMargins(0, 0, 1, 1)
        
        self.icon_rect = QRect(QPoint(), self.icon_size)
        self.icon_rect.move_center(QPoint(self.icon_width*0.5+3, self.height*0.5-1))

        #if self.icon():
        #    self._content_rect -= QMargins(self._icon_size.width() + 2, 0, 0, 0)
        split_width = self.width * self.style.split_percentage
        self.label_rect = self.content_rect.adjusted(0, 0, self.width-split_width, 0)
        self.field_rect = self.content_rect.adjusted(0, 0, split_width, 0)

        self.text_rect = QRect(self.content_rect).adjusted(8, 0, -8, -2) #- QMargins(0, 0, 10, 10)
        if self.widget.icon():
            self.text_rect -= QMargins(self.icon_width + 4, 0, 0, 0)

        self.rect_shape.update(self.rect, radius=self.border_radius, 
            pen_width=self.style.border.width)

    @property
    def clip_rect(self):
        if not self.style.collapsible:
            return None

        margins = [0, 0, 0, 0]

        if not self.corners[-1] and not self.corners[0]:
            margins[0] = 1
        if not self.corners[0] and not self.corners[1]:
            margins[1] = 1
        if not self.corners[1] and not self.corners[2]:
            margins[2] = 1
        if not self.corners[2] and not self.corners[3]:
            margins[3] = 1
        
        return self.rect.margins_removed(QMargins(*margins))


class BoolGeometry(PropertyGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.box_rect = QRect()
        self.box_shape = None

        #rself.style['box_size'].value_changed.connect(self.update)
    
    @property
    def border_radius(self):
        corners = self.corners
        if not self.widget.emboss():
            corners = [True, True, True, True]

        radius = self.height/2 * self.style.border.roundness
        return [c * r for c, r in zip(corners, (radius,)*4)]

    def update(self):
        super().update()

        self.text_rect = QRect(self.content_rect).adjusted(0, -1, 0, -1) #- QMargins(0, 0, 10, 10)
        box_size = QSize(*self.style.box_size)

        if self.style.icon:
            self.text_rect -= QMargins(self.icon_width + 4, 0, 0, 0)
        if self.style.show_checkbox:
            self.text_rect -= QMargins(box_size.width() + 4, 0, 0, 0)

        pen_width = self.style.border.width #[self.style.border.width, 0, 0, self.style.border.width]
        #borders = [True, False, False, True]

        self.box_rect = QRect(QPoint(self.rect.x(), self.height*0.5-box_size.height()*0.5), box_size)
        self.box_shape = RectShape(self.box_rect, pen_width=pen_width, radius=self.border_radius)


class EditableGeometry(PropertyGeometry):
    def __init__(self, style):
        super().__init__(style)


class IntGeometry(EditableGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.left_button_rect = QRect()
        self.right_button_rect = QRect()

    def update(self):
        super().update()

        #print('update NumericGeometry')

        rect = QRect(self.rect) - QMargins(0, 0, 1, 1)
        arrow_size = self.style.button_size

        self.text_rect = QRect(rect).adjusted(arrow_size+2, 0, -arrow_size-2, 0)
        self.left_button_rect = QRect(rect.x(), rect.y(), arrow_size, rect.height())
        self.right_button_rect = QRect(rect.width() - arrow_size, rect.y(), arrow_size, rect.height())

        #print('left_button_rect', self.left_button_rect)
        #print('arrow_size', arrow_size)


class FloatGeometry(IntGeometry):
    def __init__(self, style):
        super().__init__(style)


class PathGeometry(PropertyGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.path_icon_rect = QRect()

    def update(self):
        super().update()

        rect = QRect(self.rect) - QMargins(0, 0, 1, 1)

        self.text_rect = QRect(self.text_rect) - QMargins(0, 0, self.icon_width, 0)
        self.path_icon_rect = QRect(rect.width() - self.icon_width - 2, rect.y(),
            self.icon_width, rect.height())


class ArrayGeometry(PropertyGeometry):
    def __init__(self, style):
        super().__init__(style)

        self._corners = [True, True, True, True]

    @property
    def corners(self):
        return self._corners
    
    @corners.setter
    def corners(self, corners):
        self._corners = corners
        items = self.widget.items()

        #print(items)
        for i, item in enumerate(items):
            if len(items)==1:
                corners = [True, True, True, True]
            elif i == 0:
                corners = [self._corners[0], self._corners[1], False, False]
            elif 0 < i < len(items)-1:
                corners = [False, False, False, False]
            else:
                corners = [False, False, self._corners[2], self._corners[3]]


            item.style().geometry.corners = corners


class ColorGeometry(PropertyGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.box_rect = QRect()
        self.box_shape = None

    def update(self):
        super().update()

        self.text_rect = QRect(self.content_rect).adjusted(0, -1, 0, -1) #- QMargins(0, 0, 10, 10)

        #border_radius = self.style.border_radius
        box_size = self.style.box_size
        self.box_rect = QRect(QPoint(self.rect.x(), self.height*0.5-box_size.height()*0.5), box_size)
        self.box_shape = RectShape(self.box_rect, radius=self.border_radius)


class ComboboxGeometry(PropertyGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.arrow_rect = QRect()
        #self.rect_shape = QRect()

    def update(self):
        super().update()

        self.text_rect = QRect(self.rect).adjusted(10, 0, -18, -2)
        self.arrow_rect = QRect(
            QPoint(self.rect.right()-self.icon_width, self.rect.center().y()-self.icon_height*0.5),
            QSize(16, 16))
            

class EnumGeometry(PropertyGeometry):
    def __init__(self, parent):
        super().__init__(parent)

        self.item_rects = []
        self.item_rect_shapes = []
    
    def update(self):
        super().update()

        #print('EnumWidget Update')

        #geo = self.style().geometry

        items = self.widget.items()
        item_width = self.width / len(items)
        items_x = [round(i*item_width) for i in range(0, len(items)+1)]

        #shadow_offset = self.style.shadow_offset

        self.item_rect_shapes.clear()
        self.item_rects.clear()
        for i, item in enumerate(items):
            item_rect = QRect(QPoint(items_x[i], 0), QPoint(items_x[i+1]-1, self.height))
            self.item_rects.append(item_rect)

            # if i == 0:
            #     corners = [True, False, False, True]
            # elif i == len(items)-1:
            #     corners = [False, True, True, False]
            # else:
            #     corners = [False, False, False, False]
            
            # border_radius = [c * r for c, r in zip(corners, (self.border_radius,)*4)]

            self.item_rect_shapes.append(RectShape(item_rect, radius=self.border_radius))