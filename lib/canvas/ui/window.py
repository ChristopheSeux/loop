
from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt, QObject, QEvent
from PySide6.QtGui import QPainter, QCursor, QPalette, QColor
from PySide6.QtWidgets import QApplication, QMenu, QScrollArea, QSizePolicy, QFrame

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.style import MenuStyle
from canvas.ui.property import PropertyGroup, EnumProperty
from canvas.ui.base import BaseWidget
from canvas.ui.layout import VBoxLayout
from canvas.ui.frame import VBoxFrame, HBoxFrame, FormFrame

from canvas.ui.utils import rename_signals
from canvas.ui.theme import Theme
from canvas.ui.container import Panel, SubPanel, GroupBox
from canvas.ui.button import PushButton
from canvas.ui.style import MediumFrameStyle

from itertools import groupby
from functools import partial
import math

#QScrollArea

#Øself.setWidget(container)

class ThemeWidget(VBoxFrame):
    def __init__(self, theme, parent=None):
        super().__init__(parent=parent, margin=5, spacing=2)

        self.theme = theme
        self.widget_count = 0
        self.set_panels(theme)

        print(f'ThemeWidget create {self.widget_count} Widgets')      

        self.set_style(MediumFrameStyle(self))

    def set_panels(self, theme):
        for prop_name, properties in theme.__dict__.items():
            if prop_name == 'parent' or not isinstance(properties, Theme):
                continue
                            
            panel = Panel(self, title=properties.name)
            panel.first_shown.connect(lambda p=properties, w=panel: self.set_widgets(p, parent=w))
            panel.first_shown.connect(lambda p=properties, w=panel: self.set_sub_panels(p, parent=w))

    def set_sub_panels(self, panel, parent):
        for prop_name, properties in panel.__dict__.items():
            if prop_name == 'parent' or not isinstance(properties, PropertyGroup):
                continue
            
            sub_panel = SubPanel(parent, title=properties.name, spacing=0)

            sub_panel.first_shown.connect(lambda p=properties, w=sub_panel: self.set_widgets(p, parent=w))
            sub_panel.first_shown.connect(lambda p=properties, w=sub_panel: self.set_group_box(p, parent=w))
            
    def set_group_box(self, sub_panel, parent):
        for prop_name, properties in sub_panel.__dict__.items():
            if prop_name == 'parent' or not isinstance(properties, PropertyGroup):
                continue
            
            title = properties.name
            if not title.endswith(':'):
                title = f'{title} :'
            group_box = GroupBox(parent, title=title)

            group_box.first_shown.connect(lambda p=properties, w=group_box: self.set_widgets(p, parent=w))
            group_box.first_shown.connect(lambda p=properties, w=group_box: self.set_group_box(p, parent=w))

            #self.set_widgets(properties, group_box)
            #self.set_group_box(properties, parent=group_box)

    def set_widgets(self, properties, parent):
        self.set_updates_enabled(False)
        group_properties = sorted(iter(properties), key=lambda x : x.name)
        group_properties = groupby(group_properties, key=lambda x : x.name.split(' ')[0])

        for group_name, properties in group_properties:
            properties = list(properties)

            properties[0].to_widget(parent)
            self.widget_count += 1

            if len(properties) > 1:
                for prop in properties[1:]:
                    self.widget_count += 1
                    w = prop.to_widget(parent, name=prop.name.replace(f'{group_name} ', ''))

        parent.layout().add_spacer(0, 8)
        self.set_updates_enabled(True)
        #parent.layout().collapse()

class ThemeWindow(VBoxFrame):
    def __init__(self, theme, parent=None):
        super().__init__(parent, spacing=0)

        self.set_style(MediumFrameStyle(self))

        self.header = HBoxFrame(margin=5, spacing=0, parent=self, collapsed=True)

        self.properties = PropertyGroup(
            theme = EnumProperty('Default', items=('Default',))
        )

        self.properties['theme'].to_widget(self.header, expand=False)
        self.new_theme = PushButton(parent=self.header, icon='plus')
        self.remove_theme = PushButton(parent=self.header, icon='minus')

        #self.header.collapse()

        self.theme_widget = ThemeWidget(theme, parent=self)
        scroll_area = QScrollArea(self)
        scroll_area.set_frame_shape(QFrame.NoFrame)
        #scroll_area.paint_event = lambda s, p : None
        scroll_area.set_widget_resizable(True)
        scroll_area.set_widget(self.theme_widget)

        self.add_widget(scroll_area)

class Menu(QMenu):
    def __init__(self, parent):
        super().__init__(parent)


