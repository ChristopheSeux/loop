

def rename_signals(self):
    for key in list(self.__dict__.keys()):
        norm_key = ''.join([f'_{c.lower()}' if c.isupper() else c for c in key]).lstrip('_')
        setattr(self, norm_key, self.__dict__.pop(key))