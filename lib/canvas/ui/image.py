from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt, QObject, QEvent
from PySide6.QtGui import QPainter, QCursor, QPalette, QColor
from PySide6.QtWidgets import QApplication, QLineEdit, QLabel, QWidget, QFrame, QGraphicsDropShadowEffect

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.style import IconLabelStyle
from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.base import BaseWidget
#from canvas.ui.frame import Frame
from canvas.ui.behaviour import FocusBlocker

import math





class IconLabel(BaseWidget, QWidget):
    def __init__(self, parent=None, icon=None, icon_size=(16, 16)):
        QWidget.__init__(self)
        BaseWidget.__init__(self, parent=parent)

        self._style = IconLabelStyle(self)
        self._icon = None
        self._icon_size = None
        self.set_icon_size(icon_size)
        self.set_policy(('Fixed', 'Fixed'))

        #print(self._icon_size)

    def icon(self):
        return self._icon

    def set_icon(self, icon):
        self._icon = icon

    def icon_size(self):
        return self._icon_size

    def set_icon_size(self, icon_size):
        if isinstance(icon_size, (list, tuple)):
            self._icon_size = QSize(*icon_size)
            return

        self._icon_size = QSize(icon_size)

    def minimum_size_hint(self):
        return self.size_hint()

    def size_hint(self):
        return self._icon_size

    def paint_event(self, event):
        painter = QPainter(self)

        if not self._icon:
            return

        #painter.set_render_hint(QPainter.Antialiasing)

        self._icon.paint(painter, self.rect())