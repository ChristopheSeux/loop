
import logging
from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QFont, QFontDatabase
from PySide6.QtCore import QObject, Signal

from __feature__ import snake_case # pylint: disable=E0401, W0611


from canvas import constant
from canvas.ui.theme import DefaultTheme, Icons
from canvas.ui.utils import rename_signals

import sys



class Application(QApplication):
    def __init__(self):
        super().__init__()

        rename_signals(self)

        #self._qtapp = QApplication.instance() or QApplication()
        self.set_style('Fusion')
        self.set_application_name('Canvas')

        self._theme = DefaultTheme()

        self.set_application_font()

        #self.set_application_font()
        self.theme().font.value_changed.connect(self.set_application_font)

        #font = QFont()
        #font.set_pixel_size(13)
        #self.set_font(font)

        # print(dir(font))
        # print(font.family())
        # print(font.style_name())
        # print(font.style_hint())
        # print(font.weight())

        #print(self.__dict__)

        #self.set_default_font()
        #constant.APP = self
    
    def set_application_font(self):
        print(f'Set Application font')

        font_theme = self.theme().font
        font_path = (font_theme.folder / font_theme.font / f'{font_theme.font}-{font_theme.family}').with_suffix('.ttf')
        font_id = QFontDatabase.add_application_font(str(font_path))

        # Check if the font was loaded successfully
        if font_id != -1:
            # Get the family name of the loaded font
            font_info = QFontDatabase.application_font_families(font_id)

            font_family = font_info[0]  # Assuming there's only one family in the font

            # Create a QFont object with the loaded font
            font = QFont(font_family)

            font.set_pixel_size(font_theme.pixel_size)
            #font.set_gamma(1.8)
            #font.set_kerning(False)
            
            font.set_weight(getattr(QFont, font_theme.weight.replace(' ', '')))
            #font.set_hinting_preference(QFont.PreferNoHinting)
            #font.set_style_strategy(QFont.PreferQuality)
            #font.set_style_strategy(QFont.NoSubpixelAntialias)
            if font_theme.hinting:
                font.set_hinting_preference(QFont.PreferFullHinting)
            else:
                font.set_hinting_preference(QFont.PreferNoHinting)
            #font.set_style_strategy(QFont.PreferQuality|QFont.PreferAntialias)

            self.set_font(font)
            #self.update()
        else:
            print('Could not load default font')


    def theme(self):
        return self._theme

    def icons(self):
        return self.theme().icons

    def set_theme(self):
        self._theme = Theme()
    
    def exec(self):
        sys.exit(super().exec())