

from PySide6.QtCore import QRect, QSize, Qt, QPoint, QMargins, Signal
from PySide6.QtWidgets import (QApplication, QSizePolicy, QStyleOption, QLabel, 
    QGraphicsDropShadowEffect, QFrame)
from PySide6.QtGui import QPainter, QColor

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.style import WidgetStyle, Style
from canvas.ui.utils import rename_signals
#from canvas.ui.property import ColorProperty, IntProperty, BoolProperty


class BaseWidget:
    first_shown = Signal()

    # Allow the post init method to be called after the init of child class
    def __init_subclass__(cls, *args, **kwargs):
        super().__init_subclass__(*args, **kwargs)
        def new_init(self, *args, init=cls.__init__, **kwargs):
            init(self, *args, **kwargs)
            if cls is type(self) and hasattr(self, 'post_init'):
                self.post_init()
        cls.__init__ = new_init

    def __init__(self, parent=None, align=None, size=None, policy=None):
        
        self._parent = parent
        self._style = WidgetStyle(self)
        self._shown = False

        if policy is not None:
            self.set_policy(policy)
    
        if size is not None:
            self.set_size(size)
        
        #self.parent_to(parent)
        
    #     if padding:
    #         self.set_content_margins(padding)

    # def set_content_margins(self, *margin):
    #     if len(margin) == 1:
    #         margin = [margin]*4
    #     elif len(margin) == 2:
    #         margin = [margin]*2
        
        #super().set_content_margins(margin)
    def post_init(self):
        self.parent_to(self._parent)

    def state(self):
       return self.style().state

    #def geometry(self):
    #    return self._geometry

    def style(self):
        return self._style

    def set_style(self, *args, **kargs):
        for arg in args:
            if isinstance(arg, Style):
                self._style = arg

        for k, v in kargs.items():
            setattr(self.style(), k, v)

    def resize_event(self, event):
        #print('Resize Event')
        self.style().geometry.update()
        super().resize_event(event)

    def set_size(self, size):
        width, height = size
        hp, vp = None, None

        if width ==-1:
            hp = 'Expanding'
        elif width is not None:
            self.set_fixed_width(width)
            hp = 'Fixed'
        if height ==-1:
            vp = 'Expanding'
        elif height is not None:
            self.set_fixed_height(height)
            vp = 'Fixed'
        
        self.set_policy((hp, vp))

    def set_policy(self, policy):
        size_policy = self.size_policy()
        if isinstance(policy, (tuple, list)):
            hp, vp = policy
            if hp:
                size_policy.set_horizontal_policy(getattr(QSizePolicy, hp))
            if vp:
                size_policy.set_vertical_policy(getattr(QSizePolicy, vp))
        else:
            size_policy.set_horizontal_policy(getattr(QSizePolicy, policy))
        
        self.set_size_policy(size_policy)

    def set_alignment(self, alignment):
        self.layout().set_alignment(alignment)
        # if isinstance(alignment, str):
        #     alignment = alignment.split('|')
        
        # print(reduce(ior, (getattr(Qt, f'Align{a}') for a in alignment)))
        # self.layout().set_alignment(reduce(ior, (getattr(Qt, f'Align{a}') for a in alignment)))
        
    def parent_to(self, parent):
        if hasattr(parent, 'layout') and not hasattr(parent, 'add_widget'):
            parent = parent.layout()

        if not parent: 
            return

        #if hasattr(parent, 'add_row'):
        #    parent.add_row(self.name(), self)
        #else:
        parent.add_widget(self)

    def add_widget(self, widget):
        self.layout().add_widget(widget)
    
    def show_event(self, event):
        super().show_event(event)
        
        if not self._shown:
            self._shown = True
            self.first_shown.emit()