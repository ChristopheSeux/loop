
from PySide6.QtCore import QRect, QMargins, QSize, QPoint
from PySide6.QtGui import QColor, QIcon, QPainter
from PySide6.QtWidgets import QPushButton, QApplication

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.style import ButtonStyle
from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.base import BaseWidget
from canvas.ui.layout import HBoxLayout

from canvas.ui.property.widget.base import PropertyLabel

import math
#from canvas.ui.theme import Theme


class PushButton(BaseWidget, QPushButton):
    def __init__(self, parent=None, emboss=True, icon=None, text='', size=None):
        QPushButton.__init__(self, text=text)
        BaseWidget.__init__(self, parent=parent)

        self._style = ButtonStyle(self)
        self._emboss = emboss

        self.set_layout(HBoxLayout())
        self._label_widget = PropertyLabel(self)

        self.set_icon(icon)

        #print(self.style())
        
        #self.set_minimum_size(22, 22)
        if size is not None:
            self.set_size(size)
        
    def resize_event(self, event):
        self.style().geometry.update()
        super().resize_event(event)

    def minimum_size_hint(self):
        size = super().size_hint()
        width = size.width()
        if self.icon() and not self.text():
            width = 26
        return QSize(width, 24)

    def emboss(self):
        return self._emboss
    
    def set_emboss(self, emboss):
        self._emboss = emboss
    
    def set_icon(self, icon):
        if icon:
            if not isinstance(icon, QIcon):
                theme = QApplication.instance().theme()

                if isinstance(icon, str) and icon in theme.icons:
                    icon = theme.icons[icon]
                else:
                    icon = QIcon(str(icon))

            super().set_icon(icon)

            if not self.text():
                self.set_policy(('Fixed', 'Fixed'))
        else:
            self.set_policy(('Preferred', 'Fixed'))

    def enter_event(self, event):
        self.state().under_mouse = True
        super().enter_event(event)
        #self.update()

    def leave_event(self, event):
        self.state().under_mouse = False
        super().leave_event(event)
        #self.update()

    def size_hint(self):
        if self.text():
            size = super().size_hint()
            return QSize(size.width(), self.minimum_height())
        else:
            return QSize(self.minimum_width(), self.minimum_height())

    def mouse_press_event(self, event):
        super().mouse_press_event(event)
        #self._pos = event.pos()
        self.state().is_down = True
        self.update()

    def mouse_release_event(self, event):
        super().mouse_release_event(event)
        self.state().is_down = False
        self.update()

    def paint_event(self, event):
        painter = QPainter(self)

        self.style().draw_background(painter)
        self.style().draw_foreground(painter)

