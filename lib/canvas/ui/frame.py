
from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt
from PySide6.QtGui import QColor, QPainter, QBrush
from PySide6.QtWidgets import QApplication, QFrame, QWidget, QScrollArea, QSizePolicy

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.base import BaseWidget
from canvas.ui.layout import HBoxLayout, VBoxLayout, StackedLayout, FormLayout, PropertyLayout
from canvas.ui.style import BorderStyle, ShadowStyle, FrameStyle, FrameGeometry
import math


class Frame(BaseWidget, QWidget):
    def __init__(self, parent=None, color=None, size=None):
        QWidget.__init__(self)
        BaseWidget.__init__(self, parent=parent, size=size)

        self._style = FrameStyle(self)
        #self._geometry = FrameGeometry(self)

        if color:
            if isinstance(color, (tuple, list)):
                color = QColor(*color)
            self.style().background_color = color

        #self.background_color = color#QColor(40, 40, 40)#QColor(32, 32, 34)
    # def layout(self):
    #     return super().layout()
    
    # @layout.setter
    # def layout(self, layout):
    #     self.

    def paint_event(self, event):
        painter = QPainter(self)
        self.style().draw_background(painter)


class VBoxFrame(Frame):
    def __init__(self, parent=None, color=None, size=None, spacing=0, margin=0, align='Top', 
            property_split=True, collapsed=False):
        super().__init__(parent=parent, size=size, color=color)

        self._property_split = property_split

        self.set_layout(VBoxLayout(spacing=spacing, margin=margin, align=align, collapsed=collapsed))

        #self.set_size_policy(QSizePolicy.Expanding, QSizePolicy.Maximum)

    def set_property_split(self, value):
        self._property_split = value

    def property_split(self):
        return self._property_split

    # def paint_event(self, event):
    #     #return super().paint_event(event)
    #     painter = QPainter(self)


    #     color = QColor(36,36,36)#theme.background_color

    #     fill = RectShape(self.rect())
    #     fill.paint(painter, brush=color)


class HBoxFrame(Frame):
    def __init__(self, parent=None, color=None, size=None, spacing=0, margin=0, align=None, collapsed=False):
        super().__init__(parent=parent, size=size, color=color)

        self.set_layout(HBoxLayout(spacing=spacing, margin=margin, align=align, collapsed=collapsed))

        self.set_size_policy(QSizePolicy.Expanding, QSizePolicy.Maximum)


class StackedFrame(Frame):
    def __init__(self, parent=None, color=None, size=None, stacking_mode='StackOne'):
        super().__init__(parent=parent, size=size, color=color)

        self.set_layout(StackedLayout(stacking_mode=stacking_mode))

        #print(self.layout().stacking_mode())


# class Separator(BaseWidget, QFrame):
#     def __init__(self, parent=None, size=(5, 5)):
#         QFrame.__init__(self)
#         BaseWidget.__init__(self, parent=parent, size=size)


class FormFrame(Frame):
    def __init__(self, parent=None, color=None, size=None, spacing=0, margin=0, align='Top', collapsed=False):
        super().__init__(parent, color=color)

        self.set_layout(FormLayout(spacing=spacing, margin=margin, align=align, collapsed=collapsed))

    def add_row(self, widget, label=None):
        return self.layout().add_row(widget, label)


class PropertyFrame(Frame):
    def __init__(self, parent=None, color=None, size=None, spacing=0, margin=0, align='Top'):
        super().__init__(parent, color=color)

        self.set_layout(PropertyLayout(spacing=spacing, margin=margin, align=align))

    def add_widget(self, widget, label=None):
        return self.layout().add_widget(widget, label=label)