

from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt, QObject, QEvent
from PySide6.QtGui import QPainter, QCursor, QPalette, QColor
from PySide6.QtWidgets import QApplication, QLineEdit, QLabel, QWidget, QFrame, QGraphicsDropShadowEffect

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.style import WidgetStyle, LineEditStyle, TextLabelStyle,TitleLabelStyle
from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.base import BaseWidget
#from canvas.ui.frame import Frame
from canvas.ui.behaviour import FocusBlocker
from canvas.ui.utils import rename_signals

import math



class TextLabel(BaseWidget, QWidget):
    def __init__(self, parent=None, text=''):
        QWidget.__init__(self)
        BaseWidget.__init__(self, parent=parent)

        self._style = TextLabelStyle(self)
        self._text = ''

        self.set_text(text)

       #print(1, self.text())

        #parent.add_widget(self)

        #self.shadow = QGraphicsDropShadowEffect()
        #self.shadow.set_blur_radius(3)  # Adjust the blur radius as needed
        #self.shadow.set_color(QColor(255, 0, 0, 250))  # Shadow color
        #self.shadow.set_offset(1, 1)  # Shadow offset (x, y)

        #self.set_graphics_effect(self.shadow)

    def text(self):
        return self._text

    def set_text(self, text):
        self._text = text

    def minimum_size_hint(self):
        size = super().minimum_size_hint()
        return QSize(0, size.height())

    def size_hint(self):
        font_metrics = self.font_metrics()
        rect = font_metrics.bounding_rect(self.text())
        return QSize(rect.width()+5, rect.height())

    def paint_event(self, event):
        painter = QPainter(self)
        font_metrics = painter.font_metrics()

        if not self.text():
            return
        
        painter.set_render_hint(QPainter.Antialiasing)

        elided_text = font_metrics.elided_text(self.text(), Qt.ElideRight, self.width()-2)
        #painter.drawText(QPoint(0, fontGeometry.ascent()), elided_text)

        color = self.style().text_color
        self.style().draw_text(painter, self.rect(), text=elided_text, align=('Left', 'VCenter'), color=color)


class TitleLabel(TextLabel):
    def __init__(self, parent=None, text=''):
        super().__init__(parent=parent, text=text)

        self._style = TitleLabelStyle(self)


class LineEdit(BaseWidget, QLineEdit):
    def __init__(self, parent=None, text='', emboss=True):

        QLineEdit.__init__(self)
        rename_signals(self)

        BaseWidget.__init__(self, parent=parent)

        self._style = LineEditStyle(self)
        #self._geometry = LineEditGeometry(self)
        self._emboss = True

        self.set_text(text)
        self.set_frame(False)
        self.set_emboss(emboss)

    def emboss(self):
        return self._emboss
    
    def set_emboss(self, emboss):
        self._emboss = emboss

    def resize_event(self, event):
        super().resize_event(event)
        self.style().geometry.update()

    def paint_event(self, event):
        painter = QPainter(self)

        self.style().draw_background(painter)
        super().paint_event(event)