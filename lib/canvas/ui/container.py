from PySide6.QtCore import QRect, QMargins, QSize, QPoint, Signal, Qt
from PySide6.QtGui import QColor, QPainter, QBrush
from PySide6.QtWidgets import (QApplication, QFrame, QWidget, QSplitter, QSplitterHandle, 
    QScrollArea, QScrollBar, QSizePolicy, QGraphicsDropShadowEffect, QPushButton)

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.base import BaseWidget
from canvas.ui.style import GroupBoxStyle, WidgetStyle, FrameStyle, TabStyle, PanelStyle, SubPanelStyle
from canvas.ui.layout import HBoxLayout, VBoxLayout, StackedLayout, GridLayout
#from canvas.ui.property.bool import BoolWidget
from canvas.ui.text import TextLabel, TitleLabel
from canvas.ui.frame import VBoxFrame, HBoxFrame, FormFrame, StackedFrame
from canvas.ui.image import IconLabel

import math







class PanelHeaderLabel(TitleLabel):
    def __init__(self, parent=None, text=''):
        super().__init__(parent=parent, text=text)

        self._style = PanelHeaderLabelStyle(self)


class SubPanelHeadedLabel(TitleLabel):
    def __init__(self, parent=None, text=''):
        super().__init__(parent=parent, text=text)

        self._style = SubPanelHeaderLabelStyle(self)


class GroupBoxHeaderLabel(TitleLabel):
    def __init__(self, parent=None, text=''):
        super().__init__(parent=parent, text=text)

        self._style = GroupHeaderLabelStyle(self)


class PanelHeader(HBoxFrame):
    def __init__(self, panel, parent=None, name=''):
        super().__init__(parent, margin=(5, 0, 5, 0))

        self._panel = panel
        #self._style = panel.style()

        self._left_layout = HBoxLayout(self)
        self._expand_icon = IconLabel(parent=self, icon=None)
        self.layout().add_spacing(3)
        self._title_widget = TitleLabel(parent=self, text=name)
        #self._title_widget
        #self._title_widget.set_style(panel.style().header)
        self._title_widget.set_attribute(Qt.WA_TransparentForMouseEvents)

        self._right_layout = HBoxLayout(self)

        #print(self._title_widget.style().text_color)
        # self._title_widget.set_policy(('Expanding', 'Expanding'))

        # print(self._title_widget.rect())
        # #self.layout().add_widget(QPushButton(text="test"))

        self.set_policy(('Fixed', 'Fixed'))
        # print(self.size_hint())
    
    def title_widget(self):
        return self._title_widget
    
    def mouse_press_event(self, event):
        self._panel.set_expanded(not self.state().expanded)

    def panel(self):
        return self._panel

    def style(self):
        return self.panel().style()

    def state(self):
        return self.panel().state()

    def left_layout(self):
        return self._left_layout

    def right_layout(self):
        return self._right_layout

    def size_hint(self):
      size = super().size_hint()
      #print(self, self.style().header_height)
      return QSize(size.width(), self.style().header_height)

    def set_expand_icon(self, icon):
        self._expand_icon.set_icon(icon)
    
    def paint_event(self, event):
        return

    def resize_event(self, event):
        self.style().geometry.update()
        super().resize_event(event)

        
class Panel(VBoxFrame):
    def __init__(self, parent=None, title='', spacing=4, margin=6):
        super().__init__(parent)
        
        #print(self, spacing)

        self._header = PanelHeader(self, parent=super().layout())
        self._body = VBoxFrame(super().layout(), color=QColor(0, 0, 0, 0))
        self._form = FormFrame(self._body, color=QColor(0, 0, 0, 0), margin=margin, spacing=spacing)

        self._title = ''
        self._style = PanelStyle(self)
        self.header().title_widget()._style = self._style.header_label

        self.set_size_policy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        self.set_expanded(False)
        self.set_title(title)
        #self.set_contents_margins(5, 5, 5, 5)

        #self.parent_to(parent)
    
    def set_title(self, title):
        self.header().title_widget().set_text(title)

    def title(self):
        return self.header().title_widget().text()

    def header(self):
        return self._header

    def form(self):
        return self._form

    def body(self):
        return self._body

    def set_expanded(self, expanded=True):
        if expanded:
            icon = self.style().icons['arrow_down']
        else:
            icon = self.style().icons['arrow_right']

        self._header.set_expand_icon(icon)
        
        self.style().state.expanded = expanded
        self._body.set_visible(expanded)
        #self.adjust_size()

        #self.update()

    # def set_name(self, name):
    #     self._header._title_widget.set_text(name)
    
    # def name(self):
    #     return self._header._title_widget.text()

    def layout(self):
        return self._form.layout()

    def paint_event(self, event):
        painter = QPainter(self)

        self.style().draw_background(painter)
        self.style().draw_header(painter)

    def resize_event(self, event):
        self.style().geometry.update()
        super().resize_event(event)

        #print(self.style().header_height)
        #self._header.update_geometry()




class SubPanel(Panel):
    def __init__(self, parent=None, title='', margin=6, spacing=4):
        super().__init__(parent, title=title, margin=margin, spacing=spacing)
        
        self._style = SubPanelStyle(self)
        self.set_contents_margins(10, 0, 5, 0)


class GroupBox(FormFrame):
    def __init__(self, parent=None, title=''):
        super().__init__(parent)

        self._style = GroupBoxStyle(self)
        self._title = title
        #self._title_widget = GroupBoxLabel(self)

        self.set_contents_margins(0, 20, 0, 0)

    def title(self):
        return self._title

    def set_title(self, title):
        self._title = str(title)

    def paint_event(self, event):
        #print('Update', self.title())
        painter = QPainter(self)
        self.style().draw(painter)


class ScrollBar(QScrollBar):
    def __init__(self, parent=None):
        super().__init__(parent)

    def paint_event(self, event):
        painter = QPainter(self)
        painter.set_render_hint(QPainter.Antialiasing)

        # Background
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QColor(192, 192, 192))
        painter.draw_rect(self.rect())

        #print(self, self.parent(), self.parent().parent())
        #print(dir(self.parent()))
        
        height = self.parent().parent().viewport().height()

        print(height, self.height())

        # Calculate the handle position and height
        if self.maximum() > 0:
            print('------>1')
            visible_ratio = self.page_step() / (self.maximum() + self.page_step())
            print("visible_ratio", visible_ratio)
            handle_height = int(visible_ratio * self.height())
            print("handle_height", handle_height)

            #total_range = self.height() - handle_height
            #handle_pos = int((total_range * self.value()) / (self.maximum() - self.page_step()))

            handle_pos = (self.value() / self.maximum()) * visible_ratio * height

            print(self.value(), self.page_step(), handle_pos)
            #print(self.maximum())
        else:
            print('------>2')
            handle_height = height
            handle_pos = 0

        handle_rect = QRect(2, handle_pos, self.width()-4, handle_height)#self.rect().adjusted(2, handle_pos + 2, -2, handle_pos + handle_height - 2)
        

        # Handle
        painter.set_pen(Qt.NoPen)
        painter.set_brush(QColor(0, 0, 255))  # Blue color
        painter.draw_rounded_rect(handle_rect, 4, 4)

        #event.accept()


class CustomScrollArea(QScrollArea):
    def __init__(self, parent=None):
        super().__init__(parent)

        # Create a QWidget to act as the scroll area's viewport
        viewport = QWidget()
        self.set_widget(viewport)

        # Create a layout for the viewport
        layout = QVBoxLayout()
        viewport.set_layout(layout)

        # Add some content to the scroll area
        for i in range(20):
            label = QLabel(f"Item {i}")
            layout.add_widget(label)

        # Set properties for the scroll area
        self.set_horizontal_scroll_bar_policy(Qt.ScrollBarAlwaysOff)
        self.set_vertical_scroll_bar_policy(Qt.ScrollBarAlwaysOn)
        self.set_widget_resizable(True)

        # Create a custom vertical scroll bar
        vbar = ScrollBar(self)
        self.set_vertical_scroll_bar(vbar)


class HTabItem(BaseWidget, QWidget):
    def __init__(self, parent, name='', tab=None):
        QWidget.__init__(self)
        BaseWidget.__init__(self, parent=parent)

        self._tab_widget = parent

        self.parent().current_changed.connect(self.update)

        #print('name=', name)
        self._name = name
        self._tab = tab

        self._style = parent.style()
        #self._state = TabItemState(self)
        #self._geometry = HTabItemGeometry(self)

        self.set_policy('Fixed')

    def tab_widget(self):
        return self._tab_widget

    def is_current(self):
        return self._tab is self._tab_widget.current_widget()

    def name(self):
        return self._name
    
    def tab(self):
        return self._tab

    def size_hint(self):
        font_metrics = self.font_metrics()
        rect = font_metrics.bounding_rect(self._name)
        return QSize(rect.width() + 16, self.height())

    def enter_event(self, event):
        self.state().under_mouse = True
        self.update()

    def leave_event(self, event):
        self.state().under_mouse = False
        self.update()

    def mouse_press_event(self, event):
        self._tab_widget.set_current_widget(self._tab)

    def paint_event(self, event):
        painter = QPainter(self)

        #print(self.rect())
        self.style().draw_item_background(painter, self)
        self.style().draw_item_text(painter, self)

    def resize_event(self, event):
        self.style().geometry.update()
        super().resize_event(event)


class HTabWidget(VBoxFrame):
    current_changed = Signal(int)
    def __init__(self, parent=None):
        super().__init__(parent)
        
        self._style = TabStyle(self)
        #self._geometry = TabGeometry(self)
        #self._state = TabState(self)

        self.header = HBoxFrame(self, size=(None, 32), color=self.style().header_color, margin=(7, 7, 7, 0), spacing=2)
        self.header.layout().set_alignment('Left')

        #self.header = HBoxFrame(self, size=(None, 32), color=self.style().header_color, margin=(7, 7, 7, 0), spacing=2)

        self.body = StackedFrame(self)
        self.body.layout().current_changed.connect(self.current_changed.emit)

    def current_widget(self):
        return self.body.layout().current_widget()

    def set_current_widget(self, widget):
        self.body.layout().set_current_widget(widget)

    def add_tab(self, tab, name=''):
        self.body.add_widget(tab)
        self.header.add_widget(HTabItem(self, tab=tab, name=name))

        #self.header.layout().add_stretch()

#class VTabWidget(Frame):

class SplitterStyle(FrameStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.set_theme(QApplication.instance().theme().containers.splitter)
        self.background_color = QColor(10, 10, 10)

    def draw(self, painter):
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.set_pen(Qt.NoPen)
        painter.draw_rect(self.rect)


class SplitterHandle(QSplitterHandle):
    def __init__(self, orientation, parent):
        super().__init__(orientation, parent)

        self._splitter = parent
        self.set_attribute(Qt.WA_OpaquePaintEvent)

    def splitter(self):
        return self._splitter

    def style(self):
        return self.splitter().style()

    def paint_event(self, event):
        painter = QPainter(self)
        self.style().draw(painter)


class Splitter(BaseWidget, QSplitter):
    def __init__(self, orientation=None, parent=None):
        QSplitter.__init__(self)
        BaseWidget.__init__(self, parent=parent)

        self._style = SplitterStyle(self)

        if orientation:
            self.set_orientation(getattr(Qt, orientation))
        
        self.set_attribute(Qt.WA_OpaquePaintEvent)
    
    def create_handle(self):
        return SplitterHandle(self.orientation(), self)


