
from PySide6.QtCore import QRect, QSize, Qt, QPoint, QMargins, Signal
from PySide6.QtWidgets import QApplication, QSizePolicy, QStyleOption, QLabel, QWidget, QGraphicsDropShadowEffect
from PySide6.QtGui import QPainter, QColor, QPixmap, QPalette, QImage, QFont, QBrush, QPen

from __feature__ import snake_case # pylint: disable=E0401, W0611

from canvas.ui.property.core import PropertyGroup
from canvas.ui.shape import RectShape#, TextShape
from canvas.ui.geometry import *
from canvas.ui.state import *
from canvas.ui.theme import Theme
#from canvas.ui.property import ColorProperty, IntProperty, BoolProperty

from operator import ior
from functools import reduce


class Style(PropertyGroup):
    """Base Class for storing all property and drawing methods of a widget"""
    def __init__(self, widget, parent=None):
        super().__init__()

        self._parent = parent
        self._widget = widget
        self._geometry = Geometry(self)
        self._state = State(self)

        #self.value_changed.connect(widget.update)

    @property
    def parent(self):
        return self._parent

    @property
    def widget(self):
        return self._widget

    @property
    def geometry(self):
        return self._geometry

    @property
    def state(self):
        return self._state

    def set_theme(self, theme):
        theme_dict = {k: v for k, v in theme.__dict__.items() if not isinstance(v, Theme)}
        self.__dict__.update(theme_dict)

        for key, value in theme.__dict__.items():
            if key == 'parent':
                continue

            if isinstance(value, PropertyGroup) and hasattr(self, key):
                getattr(self, key).set_theme(value)

        theme.value_changed.connect(lambda : self.update())

    def update(self):

        #return
        # Update children style
        #for value in self.values():
        #    if isinstance(value, Style):
        #        value.update()
            
        self.geometry.update()
        self.widget.update()


class FrameStyle(Style):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        #theme = QApplication.instance().theme().containers.background_frame

        self._geometry = FrameGeometry(self)

        self.border = BorderStyle(widget, parent=self)
        self.drop_shadow = ShadowStyle(widget, parent=self)

        self.background_color = QColor(0, 0, 0, 0)

        #self.set_theme(theme)
        self.border.width = 0
        self.border.color = QColor(0, 0, 0, 0)
        self.border.roundness = 0

        #print(theme)

    @property
    def contents_margins(self):
        return self.widget.contents_margins()

    @property
    def rect(self):
        return self.widget.rect()

    @property
    def width(self):
        return self.widget.width()

    @property
    def height(self):
        return self.widget.height()

    def draw_background(self, painter, color=None):
        geo = self.geometry
        if color is None: # Determine by the status
            color = QColor(self.background_color)

        geo.rect_shape.paint(painter, brush=color, pen=self.border.color,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    # def update(self):
    #     #print('Update shadow')
    #     self.drop_shadow.update()
    #     self.geometry.update()
    #     self.widget.update()

class MediumFrameStyle(FrameStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        theme = QApplication.instance().theme().containers.medium_frame
        self.set_theme(theme)


class WidgetStyle(FrameStyle):
    """Base Class for storing all property and drawing methods of a widget"""
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.text_color = QColor(235, 235, 235)
        #self.text_shadow = ShadowStyle(widget)
        self.border = BorderStyle(widget)

        self._collapsible = True

    def collapsible(self):
        return self._collapsible

    @property
    def icons(self):
        return QApplication.instance().theme().icons

    @property
    def layout(self):
        return self.widget.parent().layout()

    # @property
    # def geometry(self):
    #     return self.widget.geometry()

    # @property
    # def state(self):
    #     return self.widget.state()

    #def update(self):
    #    self.text_shadow.update()
    #    super().update()

    def draw_text(self, painter, rect, text='', color=None, align='Left|VCenter',
            clip=None, font=None):

        if not text:
            return

        if not isinstance(align, (list, tuple)):
            align = align.split('|')

        align = reduce(ior, (getattr(Qt, f'Align{a}') for a in align))

        painter.save()

        if font is None:
            font = QApplication.instance().font()

        elif isinstance(font, (int, float)):
            font_size, font = font, self.widget.font()
            font.set_pixel_size(font_size)  

        if clip:
            painter.set_clip_rect(clip)

        #{font.set_hinting_preference(QFont.PreferFullHinting)
        painter.set_font(font)

        painter.set_pen(color)
        painter.draw_text(rect, align, text)

        painter.restore()


class ShadowStyle(Style):
    def __init__(self, widget=None, parent=None):
        super().__init__(widget, parent=parent)

        self._effect = QGraphicsDropShadowEffect()
        #self.widget.set_graphics_effect(self.effect)

        self.color = QColor(255, 0, 0, 255)
        self.offset = 1,1
        self.blur = 5

        #self.update()

    @property
    def effect(self):
        return self._effect

    def set_theme(self, theme):
       super().set_theme(theme)
       self.update()

    def update(self):

        #print('Update Shadow', self.widget, self.color)

        if self.color.alpha() == 0:
            self._effect = None
            self.widget.set_graphics_effect(None)
        
        self._effect = QGraphicsDropShadowEffect()

        self.effect.set_blur_radius(self.blur)
        self.effect.set_color(self.color)
        self.effect.set_offset(*self.offset)

        self.widget.set_graphics_effect(self.effect)


class BorderStyle(Style):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.color = QColor(75, 75, 75)
        self.roundness = 0.25
        self.width = 1
        self.lighting_angle = 180
        self.lighting_strength = [0, 0]


class IconLabelStyle(Style):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.text_color = QColor(235, 235, 235)


class LabelStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.text_color = QColor(235, 235, 235)
        self.pixel_size = 14
        self.weight = 'regular'

        self.shadow = ShadowStyle(widget)

        self.shadow.set_theme(QApplication.instance().theme().font.shadow.text)

    def update(self):
        super().update()
        print('Update Label', self.widget.text(), self.text_color)


class TextLabelStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.text_color = QColor(235, 235, 235)
        self.drop_shadow.set_theme(QApplication.instance().theme().font.shadow.text)


class TitleLabelStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.text_color = QColor(240, 240, 240)
        self.drop_shadow.set_theme(QApplication.instance().theme().font.shadow.title)


class MenuStyle(Style):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._state = MenuState(self)


class ButtonStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._state = ButtonState(self)
        self._geometry = ButtonGeometry(self)

        self.drop_shadow = ShadowStyle(widget, parent=self)

        #print()

        self.set_theme(QApplication.instance().theme().widgets.button)
        #self.border.set_theme(QApplication.instance().theme().widgets.button.border)

        self.text_alignment = 'Center'

        #print(QApplication.instance().theme().widgets.button.border.__dict__)

    def draw_frame(self, painter, color=None):
        geo = self.geometry
        painter.save()

        if color is None: # Determine by the status
            color = QColor(self.background_color)
            if self.widget.menu():
                color = QColor(24, 24, 24)

            if self.state.is_down:
                color = QColor(self.pressed_color)
            elif self.state.under_mouse:
                #(lightness**2 + lightness*0.5)*0.5 * 255
                color = color.lighter(150) # TODO Better way of find best hover color

            elif not self.emboss: # Normal State
                return painter.restore()

        #print(self.border.lighting_angle, self.border.lighting_strength)

        geo.rect_shape.paint(painter, brush=color, pen=self.border.color, borders=geo.borders,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

        #self.geometry.rect_shape.paint(painter, brush=QColor(255, 0, 0))

        painter.restore()

    @property
    def name(self):
        return self.widget.text()

    @property
    def icon(self):
        return self.widget.icon()

    @property
    def emboss(self):
        return self.widget.emboss()

    def draw_name(self, painter, name=None):
        if name is None:
            name = self.name
        
        if not name:
            return
        
        color = QColor(self.text_color)
        if self.state.is_down or self.state.under_mouse:
            color.set_alpha(255)
            #color = QColor(255,0,0)

        #text_shape = TextShape(self.text_rect, self.text, ('Center', 'VCenter'))
        self.draw_text(painter, self.geometry.text_rect, text=name,
            align=(self.text_alignment, 'VCenter'), color=color)

    def draw_icon(self, painter):
        painter.save()
        #print(self, color)
        if not self.state.under_mouse:
            painter.set_opacity(0.85)

        if self.icon:
            self.icon.paint(painter, self.geometry.icon_rect)
        
        painter.restore()

    def draw_background(self, painter):
        self.draw_frame(painter)
        self.draw_icon(painter)
    
    def draw_foreground(self, painter):
        self.draw_name(painter, self.name)


class LineEditStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        #self._state = LineEditState(self)
        self._geometry = LineEditGeometry(self)

        #self.set_theme(QApplication.instance().theme().widgets.text)

        #self['text_color'].value_changed.connect(self.update_palette)
        #self['selected_background_color'].value_changed.connect(self.update_palette)

        #self.text_color = 
        self.text_color = QColor(200, 200, 200, 255)
        self.selected_background_color = QColor(56, 56, 56, 255)

        self.update_palette()

    #@property
    #def background_color(self):
    #""    palette = self.widget().palette()
    #    palette.set_color(QPalette.Base, QColor(0, 0, 0, 0))
    #    self.widget().set_palette(palette)

    def update_palette(self):
        palette = QPalette()
        palette.set_color(QPalette.Text, self.text_color)
        palette.set_color(QPalette.Highlight, self.selected_background_color)
        palette.set_color(QPalette.Base, QColor(0, 0, 0, 0))

        self.widget.set_palette(palette)

    def draw_background(self, painter, color=None):

        if color is None: # Determine by the status
            color = QColor(self.background_color)
            if not self.widget.emboss(): # Normal State
                return
        
        self.geometry.rect_shape.paint(painter, brush=color, pen=self.border.color)


class PanelStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.drop_shadow = ShadowStyle(widget, parent=self)
        self.header_label = LabelStyle(widget.header().title_widget(), parent=self)

        self.header_height = 28
        self.header_color = QColor(0, 0, 0, 0)


        self._state = PanelState(self)
        self._geometry = PanelGeometry(self)

        #print(self.geometry)
        #print(self.widget.style().__class__)

        theme = QApplication.instance().theme().containers.panel
        self.set_theme(theme)
        self.header_label.set_theme(theme.header_label)

        

    def draw_background(self, painter, color=None):
        color = color or self.background_color
        geo = self.geometry

        geo.body_shape.paint(painter, brush=self.background_color, pen=self.border.color, borders=geo.borders,
            lighting_angle=self.border.lighting_angle, lighting_strength=self.border.lighting_strength)

    def draw_header(self, painter):
        self.geometry.header_shape.paint(painter, brush=self.header_color, pen=self.border.color)

    def update(self):
        self.drop_shadow.update()
        self.geometry.update()
        self.widget.update()

        self.header_label.update()
        # self.widget.header().update_geometry()

        #self.widget.header().geometry().update()
        #self.widget.header().title_widget().update()
        #self.widget.update()

        #self.header_style.update()
        #self.widget.header().style().geometry.update()
        #self.widget.header().style().update()
        return
        super().update()

        #print('Update')
        #self.drop_shadow.update()
        #self.geometry.update()
        #self.widget.header().update_geometry()
        #self.widget.update()
        #self.widget.header().update()

        #return


class SubPanelStyle(PanelStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self.set_theme(QApplication.instance().theme().containers.subpanel)

        #self.shadow.set_color(QColor(0, 0, 0, 0)) 


class GroupBoxStyle(WidgetStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        #self.drop_shadow.set_theme(QApplication.instance().theme().widgets.shadow.drop)
        self.set_theme(QApplication.instance().theme().containers.group_box)
    
    def update(self):
        #print('Update shadow')
        self.drop_shadow.update()
        #print(self.drop_shadow.color)
        self.geometry.update()
        self.widget.update()

    def draw(self, painter):
        painter.set_brush(QBrush(self.background_color, Qt.SolidPattern))
        painter.set_pen(Qt.NoPen)
        painter.draw_rect(self.rect)

        # painter.set_pen(QColor(255, 255, 255, 20))
        # painter.set_brush(Qt.NoBrush)

        # painter.draw_rect(self.rect.adjusted(0, 0, -1, -5))

        #self.draw_text(self.rect)
        self.draw_text(painter, self.rect.adjusted(10, 0, 0, 0), text=self.widget.title(), color=self.text_color, align='Left|Top')


class TabStyle(FrameStyle):
    def __init__(self, widget, parent=None):
        super().__init__(widget, parent=parent)

        self._state = TabState(self)
        self._geometry = TabGeometry(self)

        self.set_theme(QApplication.instance().theme().containers.tab)

    def draw_frame(self, painter):

        #if color is None: # Determine by the status
        #    color = QColor(self.body_color)

        self.geometry.body_shape.paint(painter, brush=self.body_color)

    def draw_item_background(self, painter, item):
        if item.state().active:
            color = self.item_color
        elif item.state().under_mouse:
            color = self.item_color.darker(150)
        else:
            return

        item.geometry().shape.paint(painter, brush=color)

    def draw_item_text(self, painter, item):

        #if color is None: # Determine by the status
        #    color = QColor(self.body_color)
        #print(item.name)
        shadow_color = None
        if item.state().active:
            color = self.text_color
            shadow_color = QColor(0, 0, 0, 175)
        elif item.state().under_mouse:
            color = self.text_color.darker(133)
        else:
            color = self.text_color.darker(150)

        self.draw_text(painter, item.rect(), text=item.name(), color=color, align='Center')