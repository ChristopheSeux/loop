

class State:
    """Base Class for storing all state of a widget"""
    def __init__(self, style, **kargs):
        self.style = style

    @property
    def widget(self):
       return self.style.widget


class WidgetState(State):
    def __init__(self, style):
        super().__init__(style)


class ButtonState(WidgetState):
    def __init__(self, style):
        super().__init__(style)

        self.under_mouse = False
        self.is_down = False


class MenuState(WidgetState):
    def __init__(self, parent):
        super().__init__(parent)

        self.shown = False
        

class PanelState(WidgetState):
    def __init__(self, style):
        super().__init__(style)

        self.expanded = False


class TabState(State):
    def __init__(self, style):
        super().__init__(style)

        #self.open = False


class TabItemState(State):
    def __init__(self, style):
        super().__init__(style)

        self.under_mouse = False
    
    @property
    def active(self):
        return self.widget.is_current()