

from PySide6.QtCore import Qt, QRect, QRectF, QMargins, QMarginsF, QPoint, QPointF, QSize
from __feature__ import snake_case

from canvas.ui.shape import RectShape


class Geometry:
    """Base Class for storing all geometry of a widget"""
    def __init__(self, style, **kargs):
        self.style = style
        self.rect_shape = RectShape()
        #self.content_rect = QRect()
        #self.text_rect = QRect()
    @property
    def widget(self):
        return self.style.widget

    @property
    def state(self):
        return self.style.state

    @property
    def layout(self):
        return self.widget.parent()

    @property
    def contents_margins(self):
        return self.widget.contents_margins()

    @property
    def rect(self):
        return self.widget.rect()

    @property
    def width(self):
        return self.widget.width()

    @property
    def height(self):
        return self.widget.height()
    
    def update(self):
        self.content_rect = QRect(self.rect) - self.contents_margins
        #self.widget.update_geometry()


class WidgetGeometry(Geometry):
    def __init__(self, style):
        super().__init__(style)

        self.corners = [True, True, True, True]

    @property
    def border_radius(self):
        radius = self.height/2 * self.style.border.roundness
        return [c * r for c, r in zip(self.corners, (radius,)*4)]

    @property
    def borders(self):
        borders = [True, True, True, True]
        if not self.corners[1] and not self.corners[2]:
            borders[1] = False
        
        if not self.corners[2] and not self.corners[3]:
            borders[2] = False
        
        return borders


class FrameGeometry(Geometry):
    def __init__(self, style):
        super().__init__(style)

        self.rect_shape = RectShape()
        self.corners = [True, True, True, True]
    
    @property
    def border_radius(self):
        radius = self.height/2 * self.style.border.roundness
        return [c * r for c, r in zip(self.corners, (radius,)*4)]

    def update(self):
        super().update()
        self.rect_shape.update(self.rect, radius=self.border_radius, pen_width=self.style.border.width)


class LineEditGeometry(WidgetGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.rect_shape = RectShape()

    def update(self):
        self.rect_shape.update(self.rect, radius=self.border_radius, pen_width=self.style.border.width)


class ButtonGeometry(WidgetGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.icon_size = QSize(16, 16)
        self.icon_width = 16

    def update(self):
        super().update()

        self.rect_shape.update(self.rect, radius=self.border_radius, pen_width=self.style.border.width)
        self.content_rect = QRect(self.rect) - QMargins(0, 0, 2, 2)
        self.icon_rect = QRect(QPoint(), self.icon_size)

        if self.widget.text():
            self.icon_rect.move_center(QPoint(self.icon_width*0.5, self.height*0.5-1))
        else:
            self.icon_rect.move_center(QPoint(self.width*0.5, self.height*0.5-1))

        self.text_rect = QRect(self.content_rect) #- QMargins(0, 0, 0, 1)
        if self.style.icon:
            self.text_rect -= QMargins(self.icon_width + 4, 0, 0, 0)
            

class PanelGeometry(WidgetGeometry):
    def __init__(self, style):
        super().__init__(style)

        self.header_rect = QRect()
        self.body_shape = RectShape()
        self.rect_shape = RectShape()
        self.header_shape = RectShape()

    @property
    def border_radius(self):
        radius = self.header_rect.height()/2 * self.style.border.roundness
        return [c * r for c, r in zip(self.corners, (radius,)*4)]

    def update(self):
        super().update()

        self.body_shape.update(self.rect, radius=self.border_radius, pen_width=self.style.border.width)
        self.rect_shape.update(self.rect, radius=self.border_radius)
        self.header_rect = self.rect.adjusted(0, 0, 0, -(self.height-self.style.header_height))

        border_radius = self.border_radius
        if self.state.expanded:
            border_radius[2] = 0
            border_radius[3] = 0

        self.header_shape.update(self.header_rect, radius=border_radius)


class HTabItemGeometry(Geometry):
    def __init__(self, widget=None):
        super().__init__(widget)

        self.shape = RectShape()

    def update(self):
        self.shape.update(self.rect, radius=(4, 4, 0, 0))


class TabGeometry(Geometry):
    def __init__(self, style):
        super().__init__(style)

        self.header_rect = QRect()
        self.body_shape = RectShape()
        self.rect_shape = RectShape()
        self.corners = [True, True, True, True]

    @property
    def border_radius(self):
        radius = self.height/2 * self.style.border.roundness
        return [c * r for c, r in zip(self.corners, (radius,)*4)]

    def update(self):
        super().update()

        self.body_shape.update(self.rect, radius=self.border_radius)
        self.rect_shape.update(self.rect, radius=self.border_radius)
        self.header_rect = self.rect.adjusted(0, 0, 0, -(self.height-self.style.header_height))