
from PySide6.QtGui import QPainter, QPainterPath, QColor, QBrush, QPen
from PySide6.QtCore import Qt, QRect, QRectF, QMargins, QMarginsF

from __feature__ import snake_case


'''
class TextShape:
    def __init__(self, rect, text, alignment=None, color=None):

        self.rect = rect
        self.text = text
        self._alignment = None
        self.color = color
        self.shadow_color = shadow_color
        self.shadow_offset = shadow_offset

        self.alignment = alignment

    @property
    def alignment(self):
        return self._alignment
    
    @alignment.setter
    def alignment(self, alignment):
        if not isinstance(alignment, (list, tuple)):
            alignment = [alignment]

        from operator import ior
        from functools import reduce

        self._alignment = reduce(ior, (getattr(Qt, f'Align{a}') for a in alignment))


    def paint(self, painter, color=None,  clip=None):
        painter.save()

        self.color = color
        self.set_shadow_color(shadow_color)
        self.set_shadow_offset(shadow_offset)

        if self.shadow_color():
            offset = self.shadow_offset()
            rect = self.rect().adjusted(offset, offset, offset, offset)
            painter.set_pen(self.shadow_color())
            painter.draw_text(rect, self.alignment(), self.text())

        if clip:
            painter.set_clip_rect(clip)

        painter.set_pen(self.color())
        painter.draw_text(self.rect(), self.alignment(), self.text())

        painter.restore()
'''

class RectShape:
    def __init__(self, rect=None, radius=None, pen_width=0):
        
        self._rect = None
        self._radius = [0, 0, 0, 0]
        self._pen_width = 0
        #self._borders = [True, True, True, True]

        self.top_path = QPainterPath()
        self.right_path = QPainterPath()
        self.bottom_path = QPainterPath()
        self.left_path = QPainterPath()
        
        self.fill_path = QPainterPath()

        self.update(rect, radius, pen_width)

    @property
    def box_paths(self):
        return (self.top_path, self.right_path,
            self.bottom_path, self.left_path)

    @property
    def rect(self):
        return self._rect

    @rect.setter
    def rect(self, rect):
        self._rect = rect

    @property
    def borders(self):
        return self._borders

    @borders.setter
    def borders(self, borders):
        self._borders = borders

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, radius):
        if radius is not None:
            if isinstance(radius, (float, int)):
                radius = [radius] * 4
            self._radius[:] = radius

    @property
    def pen_width(self):
        return self._pen_width

    @pen_width.setter
    def pen_width(self, width=None):
        self._pen_width = width
        # if width is not None:
        #     if isinstance(width, (float, int)):
        #         width = [width] * 4
        #     self._pen_width[:] = [w or 0 for w in width]
    
    def copy(self):
        rect_shape = RectShape()
        rect_shape._rect = QRect(self._rect)
        rect_shape._radius = self._radius
        rect_shape._pen_width = self._pen_width

        rect_shape.top_path = QPainterPath(self.top_path)
        rect_shape.right_path = QPainterPath(self.right_path)
        rect_shape.bottom_path = QPainterPath(self.bottom_path)
        rect_shape.left_path = QPainterPath(self.left_path)
        
        rect_shape.fill_path = QPainterPath(self.fill_path)

        return rect_shape


    def updated(self, rect=None, radius=None, pen_width=None):
        rect_shape = self.copy()
        rect_shape.update(rect=rect, radius=radius, pen_width=pen_width)

        return rect_shape

    def update(self, rect=None, radius=None, pen_width=None, borders=None):
        if rect is not None:
            self.rect = rect
        if radius is not None:
            self.radius = radius
        if pen_width is not None:
            self.pen_width = pen_width
        if borders is not None:
            self.borders = borders

        if self.rect is None:
            return

        self.top_path.clear()
        self.right_path.clear()
        self.bottom_path.clear()
        self.left_path.clear()

        #pen_width = pen_width = [max(b, 1) for b in self.pen_width]
        rect = QRectF(self.rect) - QMarginsF(*(self.pen_width,)*4) * 0.5

        #TODO optimise when 0 is 0
        #if all(b == 0 for b in self.radius):

        x, y = rect.left(), rect.top()
        w, h = rect.width(), rect.height()
        r1, r2, r3, r4 = self.radius
        R1, R2, R3, R4 = r1*2, r2*2, r3*2, r4*2

        self.top_path.move_to(x+r1, y)

        self.top_path.line_to(x + w-r2, y)
        if R2:
            self.top_path.arc_to(x + w-R2, y, R2, R2, -270, -45)

        self.right_path.move_to(self.top_path.current_position())
        if R2:
            self.right_path.arc_to(x + w-R2, y, R2, R2, -315, -45)

        self.right_path.line_to(x + w, y + h-r3)
        if R3:
            self.right_path.arc_to(x + w-R3, y + h-R3, R3, R3, 0, -45)

        self.bottom_path.move_to(self.right_path.current_position())
        if R3:
            self.bottom_path.arc_to(x + w-R3, y + h-R3, R3, R3, 315, -45)

        self.bottom_path.line_to(x + r4, y + h)
        if R4:
         self.bottom_path.arc_to(x, y + h-R4, R4, R4, -90, -45)

        self.left_path.move_to(self.bottom_path.current_position())
        if R4:
            self.left_path.arc_to(x, y + h-R4, R4, R4, -180+45, -45)
        
        self.left_path.line_to(x, y+r1)
        if R1:
            self.left_path.arc_to(x, y, R1, R1, 180, -45)

        top_path_corner = QPainterPath()
        top_path_corner.move_to(self.left_path.current_position())
        top_path_corner.arc_to(x, y, R1, R1, -180-45, -45)
        top_path_corner.connect_path(self.top_path)
        self.top_path = top_path_corner

        self.fill_path = QPainterPath(self.top_path)
        self.fill_path.connect_path(self.right_path)
        self.fill_path.connect_path(self.bottom_path)
        self.fill_path.connect_path(self.left_path)

    def get_border_colors(self, pen, brush, borders=None, lighting_angle=0, lighting_strength=(0, 0)):
        hue = pen.color().hue_f()
        sat = pen.color().saturation_f()
        value = pen.color().value_f()

        if borders is None:
            borders = [True, True, True, True]

        lighter_color = pen.color().from_hsv_f(hue, sat, value * (1 + lighting_strength[0]))
        darker_color = pen.color().from_hsv_f(hue, sat, value * (1 + lighting_strength[1]))

        if lighting_angle == 0:
            colors = [darker_color, darker_color, lighter_color, darker_color]
        elif lighting_angle == 45:
            colors = [darker_color, darker_color, lighter_color, lighter_color]
        elif lighting_angle == 90:
            colors = [darker_color, darker_color, darker_color, lighter_color]
        elif lighting_angle == 135:
            colors = [lighter_color, darker_color, darker_color, lighter_color]
        elif lighting_angle == 180:
            colors = [lighter_color, darker_color, darker_color, darker_color]
        elif lighting_angle == 225:
            colors = [lighter_color, lighter_color, darker_color, darker_color]
        elif lighting_angle == 270:
            colors = [darker_color, lighter_color, darker_color, darker_color]
        elif lighting_angle == 315:
            colors = [darker_color, lighter_color, lighter_color, darker_color]

        return colors
        #return [c if b else brush.color() for b, c in zip(borders, colors)]

    def paint(self, painter, brush=None, pen=None, borders=None, lighting_angle=0, lighting_strength=(0, 0), clip=None):
        painter.save()

        pen = QPen(pen or Qt.NoPen)
        brush = QBrush(brush or Qt.NoBrush)

        painter.set_render_hint(QPainter.Antialiasing, True)

        if clip:
            painter.set_clip_rect(clip, Qt.IntersectClip)

        painter.set_brush(brush)
        painter.set_pen(Qt.NoPen)

        if brush.style() is not Qt.NoBrush:
            painter.draw_path(self.fill_path)

        painter.set_brush(Qt.NoBrush)

        border_colors = self.get_border_colors(pen, brush, borders, lighting_angle, lighting_strength)

        #print('border_colors', border_colors)
        if self.pen_width:
            for border_color, border_path in zip(border_colors, self.box_paths):
                painter.set_pen(QPen(border_color, self.pen_width))
                painter.draw_path(border_path)

        painter.restore()


class WidgetPainter(QPainter):
    def __init__(self, widget):
        super().__init__(widget)
    
    def _draw_rounded_rect(self, rect, corners, radius=0, width=None):
        if all(corners):
            return self.draw_rounded_rect(rect, radius, radius)
        if not any(corners) or radius == 0:
            return self.draw_rect(rect)
        
        x, y = rect.left(), rect.top()
        w, h = rect.width(), rect.height()
        double_radius = 2 * radius

        path = QPainterPath()
        if corners[0]:
            #path.move_to(x, y-radius)
            path.arc_to(x, y, double_radius, double_radius, 180, -90)
        else:
            path.move_to(x, y)

        if corners[1]:
            path.line_to(x + w - radius, y)
            path.arc_to(x + w-double_radius, y, double_radius, double_radius, 90, -90)
        else:
            path.line_to(x + w, y)

        if corners[2]:
            path.line_to(x + w, y + h - radius)
            path.arc_to(
                x + w-double_radius, y + h-double_radius,
                double_radius, double_radius, 0, -90)
        else:
            path.line_to(x + w, y + h)

        if corners[3]:
            path.line_to(x + radius, y + h)
            path.arc_to(x, y + h-double_radius, double_radius, double_radius, -90, -90)
        else:
            path.line_to(x, y + h)
        
        if corners[0]:
            path.line_to(x, y+radius)
        else:
            path.line_to(x, y)

        painter.draw_path(path)

    def _draw_background_shadow(self, painter):
        painter.save()

        theme = self.theme()
        border_radius = theme.border_radius
        content_rect = QRectF(self.rect()) - QMarginsF(0, 0, 1, 1)
        
        painter.set_pen(Qt.NoPen)

        # Draw Shadow
        shadow_color = QColor(0, 0, 0, 80)
        if self._is_down:
            shadow_color = QColor(255, 255, 255, 10)
        painter.set_brush(shadow_color)
        self._draw_rounded_rect(painter, content_rect.adjusted(1, 1, 1, 1), self._corners, border_radius)

        painter.restore()

    def _draw_background(self, painter, color=None):
        painter.save()

        theme = self.theme()

        #pen_width = 1
        #half_pen_width = pen_width*0.5
        border_radius = theme.border_radius
        
        painter.set_pen(Qt.NoPen)

        if color is not None:
            painter.set_brush(QBrush(color, Qt.SolidPattern))
            self._draw_rounded_rect(painter, self._content_rect, self._corners, border_radius)
            painter.restore()
            return

        # Draw Background
        if self._emboss:
            painter.set_brush(QBrush(theme.background_color, Qt.SolidPattern))
            self._draw_rounded_rect(painter, self._content_rect, self._corners, border_radius)

        if self._is_down:
            painter.set_brush(QBrush(QColor(0, 0, 0, 120), Qt.SolidPattern))
            self._draw_rounded_rect(painter, self._content_rect, self._corners, border_radius)

        elif self._under_mouse:
            lightness = theme.background_color.lightness() /255
            over_alpha = (lightness**2 + lightness*0.5)*0.5 * 255
            painter.set_brush(QBrush(QColor(255, 255, 255, over_alpha), Qt.SolidPattern))
            self._draw_rounded_rect(painter, self._content_rect, self._corners, border_radius)
        
        painter.restore()

    def _draw_name(self, painter):
        if not self.name():
            return
        
        painter.save()

        theme = self.theme()

        # Draw Shadow
        painter.set_pen(QColor(0, 0, 0, 150))
        painter.draw_text(self._text_rect - QMargins(1, 2, 0, 0), Qt.AlignLeft|Qt.AlignVCenter, self.name())

        text_color = theme.text_color
        if self._is_down or not self._under_mouse:
            text_color = QColor(text_color)
            text_color.set_alpha(230)

        painter.set_pen(text_color)
        painter.draw_text(self._text_rect, Qt.AlignLeft|Qt.AlignVCenter, self.name())

        painter.restore()
        
    def _draw_value(self, painter):
        painter.save()

        theme = self.theme()

        if self.name():
            align = Qt.AlignRight|Qt.AlignVCenter
            shadow_rect = self._text_rect - QMargins(0, 2, -1, 0)
        else:
            align = getattr(Qt, self._text_alignment)|Qt.AlignVCenter
            shadow_rect = self._text_rect - QMargins(2, 2, 0, 0)

        # Draw Shadow
        painter.set_pen(QColor(0, 0, 0, 150))
        painter.draw_text(shadow_rect, align, str(self.value()))

        # Draw Value
        painter.set_pen(theme.value_color)
        painter.draw_text(self._text_rect, align, str(self.value()))

        painter.restore()

    def _draw_arrows(self, painter):
        if not self.arrows() or not self._under_mouse:
            return

        painter.save()

        icons = QApplication.instance().icons()
        theme = self.theme()
        border_radius = theme.border_radius

        painter.set_pen(Qt.NoPen)

        arrow_normal_color = theme.arrow_background_color#.copy()
        arrow_hover_color = arrow_normal_color.lighter(150)

        arrow_background_color = arrow_normal_color
        if self._hover_left_button:
            arrow_background_color = arrow_hover_color
        
        painter.set_brush(QBrush(arrow_background_color, Qt.SolidPattern))

        corners = (self._corners[0], False, False, self._corners[-1])
        self._draw_rounded_rect(painter, self._left_arrow_rect, corners, border_radius)

        arrow_background_color = arrow_normal_color
        if self._hover_right_button:
            arrow_background_color = arrow_hover_color

        painter.set_brush(QBrush(arrow_background_color, Qt.SolidPattern))
        corners = (False, self._corners[1], self._corners[2], False)
        self._draw_rounded_rect(painter, self._right_arrow_rect, corners, border_radius)

        right_icon_rect = QRect(self._right_arrow_rect)
        right_icon_rect.set_size(QSize(16, 16))
        right_icon_rect.move_center(self._right_arrow_rect.center() - QPoint(0, 1))
        icons['arrow_right_small'].paint(painter, right_icon_rect)

        left_icon_rect = QRect(self._left_arrow_rect)
        left_icon_rect.set_size(QSize(16, 16))
        left_icon_rect.move_center(self._left_arrow_rect.center() - QPoint(0, 1))
        icons['arrow_left_small'].paint(painter, left_icon_rect)

        painter.restore()