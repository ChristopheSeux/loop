
from PySide6.QtGui import QIcon, QColor, QPixmap, QPainter
from PySide6.QtCore import QSize, Qt, QByteArray
from PySide6.QtWidgets import QApplication
from PySide6.QtSvg import QSvgRenderer
from __feature__ import snake_case # pylint: disable=E0401, W0611

from pathlib import Path
import re
#from canvas.core.container import Container
from canvas.ui.property import PropertyGroup
from canvas.ui.property.core import (ColorProperty, IntProperty, BoolProperty, EnumProperty,
    StringProperty, IntArrayProperty, FloatArrayProperty, PathProperty, FloatProperty)

from canvas import constant
#from canvas.ui.base import WidgetTheme


class Theme(PropertyGroup):
    def __init__(self, parent=None, id=None, name=''):
        super().__init__(parent)

        self._id = id
        self.name = name

        #)




class ShadowTheme(Theme):
    def __init__(self, parent=None, name='Shadow'):
        super().__init__(parent, name=name)

        self.color = ColorProperty((0, 0, 0, 0))
        self.offset = IntArrayProperty([1, 1], min=0, max=3, unit='px')
        self.blur = IntProperty(3, min=0, max=5, unit='px')


class LabelTheme(Theme):
    def __init__(self, parent=None, name='Label'):
        super().__init__(parent, name=name)

        self.shadow = ShadowTheme(self)
        self.text_color = ColorProperty((230, 230, 230))
        self.pixel_size = IntProperty(14, min=8, max=18, unit='px')
        self.weight = EnumProperty('Normal', items=('Thin','Extra Light', 'Light', 'Normal',
            'Medium', 'Demi Bold', 'Bold', 'Extra Bold', 'Black'))


class BorderTheme(Theme):
    def __init__(self, parent=None, name="Border"):
        super().__init__(parent, name=name)

        self.color = ColorProperty((75, 75, 75))
        self.roundness = FloatProperty(0.25, min=0, max=1)
        self.width = IntProperty(1, min=0, max=4, unit='px')
        self.lighting_angle = IntProperty(180, min=0, max=360, step=45)
        self.lighting_strength = FloatArrayProperty([0, 0], min=-1, max=1, step=0.01)
        #self.lighting_bottom_strength = FloatProperty(0, min=0, max=1, step=0.01)

'''
class WidgetTheme(Theme):
    """Base Class for storing all theme property of a widget"""
    def __init__(self, parent=None):
        super().__init__(parent)

        self.background_color = ColorProperty((84, 84, 84))
        #self.border.color = ColorProperty((75, 75, 75))
        self.text_color = ColorProperty((210, 210, 210))
        self.value_color = ColorProperty((240, 240, 240))
        #self.border_radius = IntProperty(3, min=0, max=8, unit='px')
        #self.border_width = IntProperty(1, min=0, max=2, unit='px')
        #self.emboss = BoolProperty(True, name="Emboss")
        #self.border_lighting_source = EnumProperty('top_left', items=constant.LIGHTING_SOURCE)
        #self.border_lighting_strength = FloatProperty(0, min=0, max=1, step=0.01)

    def copy(self):
        return self.__class__()
'''

class TextTheme(Theme):
    def __init__(self, parent=None, name="Text"):
        super().__init__(parent, name=name)

        self.background_color = ColorProperty((17, 17, 17))
        self.selected_background_color = ColorProperty((56, 100, 159))
        #self.emboss = BoolProperty(True, name="Emboss")


class WidgetTheme(Theme):
    def __init__(self, parent=None, name="Widget"):
        super().__init__(parent, name=name)

        self.background_color = ColorProperty((48, 48, 48))
        self.text_color = ColorProperty((220, 220, 220))
        self.value_color = ColorProperty((230, 230, 230))
        self.border = BorderTheme(self)
        #self.shadow_color = ColorProperty((0, 0, 0, 125))
        #self.shadow_offset = IntProperty(1, min=0, max=3, unit='px')
        #self.font_size = IntProperty(10, min=7, max=14, unit='px')


class PropertyTheme(WidgetTheme):
    def __init__(self, parent=None, name="Widget"):
        super().__init__(parent, name=name)

        self.drop_shadow = ShadowTheme(self, name="Drop Shadow")
        self.text_shadow = ShadowTheme(self, name="Text Shadow")

        self.drop_shadow.color = (0, 0, 0, 50)
        self.text_shadow.color = (0, 0, 0, 150)


class ButtonTheme(PropertyTheme):
    def __init__(self, parent=None, name='Button'):
        super().__init__(parent, name=name)
        
        self.pressed_color = ColorProperty((56, 100, 159))

        self.background_color = (75, 75, 75)
        self.text_color = (200, 200, 200)
        self.border.color = (75, 75, 75)
        self.border.width = 1
        self.border.lighting_angle = 180
        self.border.lighting_strength = [0.25, -0.25]


class EditableTheme(PropertyTheme):
    def __init__(self, parent=None, name='Editable'):
        super().__init__(parent, name=name)

        self.selected_background_color = ColorProperty((56, 100, 159))
        self.edit_color = ColorProperty((17, 17, 17))


class StringTheme(EditableTheme):
    def __init__(self, parent=None, name='String'):
        super().__init__(parent, name=name)

        self.background_color = (17, 17, 17)
        self.border.color = (35, 35, 35)
        

class PathTheme(EditableTheme):
    def __init__(self, parent=None, name='Path'):
        super().__init__(parent, name=name)


class IntTheme(EditableTheme):
    def __init__(self, parent=None, name='Int'):
        super().__init__(parent, name=name)

        self.background_color = (85, 85, 85)
        self.border.color = (55, 55, 55)

        self.slider_color = ColorProperty((56, 100, 159))
        self.button_color = ColorProperty((85, 85, 85))
        #self.arrow_background_hover_color = ColorProperty((255, 255, 255, 50))
        self.button_size = IntProperty(14, min=8, max=32, unit='px')


class IntVectorTheme(IntTheme):
    def __init__(self, parent=None, name='Int Vector'):
        super().__init__(parent, name=name)


class FloatTheme(IntTheme):
    def __init__(self, parent=None, name='Float'):
        super().__init__(parent, name=name)


class FloatVectorTheme(FloatTheme):
    def __init__(self, parent=None, name='Float Vector'):
        super().__init__(parent, name=name)


class BoolTheme(PropertyTheme):
    def __init__(self, parent=None, name='Bool'):
        super().__init__(parent, name=name)

        self.box_color = ColorProperty((78, 78, 78))
        self.box_size = IntArrayProperty((17, 16), min=8, max=32)
        self.check_icon = StringProperty('check')
        self.enabled_color = ColorProperty((56, 100, 159))
        self.text_alignment = EnumProperty('Auto', items=constant.ALIGNMENTS)
        #self.emboss = EnumProperty('Auto', items=constant.EMBOSS)

        self.border.color = self.box_color
        self.border.lighting_angle = 180
        self.border.lighting_strength = (0.25, -0.25)


class EnumTheme(PropertyTheme):
    def __init__(self, parent=None, name='Enum'):
        super().__init__(parent, name=name)
        
        self.background_color = (30, 30, 30)
        self.border.color = (70, 70, 70)

        self.item_color = ColorProperty((30, 30, 30))
        self.checked_color = ColorProperty((56, 100, 159))
        #self.compact_color = ColorProperty((17, 17, 17))


class ComboboxTheme(EnumTheme):
    def __init__(self, parent=None, name='Combobox'):
        super().__init__(parent, name=name)

        self.text_color = (190, 190, 190, 255)
        

class ColorTheme(PropertyTheme):
    def __init__(self, parent=None, name='Color'):
        super().__init__(parent, name=name)


class WidgetsTheme(Theme):
    def __init__(self, parent=None, name='Widgets'):
        super().__init__(parent, name=name)

        #self.default = WidgetTheme(self)
        self.text = TextTheme(self)
        #self.shadow = WidgetShadowTheme(self)
        #self.property = WidgetTheme(self)

        self.int = IntTheme(self)
        self.float = FloatTheme(self)
        self.bool = BoolTheme(self)
        self.string = StringTheme(self)
        self.path = PathTheme(self)
        self.color = ColorTheme(self)
        self.button = ButtonTheme(self)
        self.enum = EnumTheme(self)
        self.combobox = ComboboxTheme(self)

        self.int_vector = IntVectorTheme(self)
        self.float_vector = FloatVectorTheme(self)


class FrameTheme(Theme):
    def __init__(self, parent=None, name='Frame'):
        super().__init__(parent, name=name)
        
        self.border = BorderTheme(self)

        self.background_color = ColorProperty((40, 40, 40))
        #self.shadow = ShadowTheme(self)

        self.border.roundness = 0
        self.border.width = 0
        self.border.color = (0, 0, 0, 0)


class TabTheme(FrameTheme):
    def __init__(self, parent=None, name='Tab'):
        super().__init__(parent, name=name)
        
        self.background_color = ColorProperty((48, 48, 48))
        self.border_radius = IntProperty(4, min=0, max=8, unit='px')
        self.tab_alignment = EnumProperty('Auto', items=constant.ALIGNMENTS)

        self.header_height = IntProperty(28, min=20, max=32, unit='px')
        self.header_color = ColorProperty((24, 24, 24))
        self.body_color = ColorProperty((61, 61, 61))

        self.item_color = ColorProperty((48, 48, 48))
        self.text_color = ColorProperty((220, 220, 220))


class PanelTheme(FrameTheme):
    def __init__(self, parent=None, name='Panel'):
        super().__init__(parent, name=name)
        
        self.header_label = LabelTheme(self, name='Header')
        self.drop_shadow = ShadowTheme(self, name="Drop Shadow")

        self.header_height = IntProperty(28, min=20, max=32, unit='px')
        self.header_color = ColorProperty((0, 0, 0, 0))
        #self.text_color = ColorProperty((200, 200, 200))

        self.background_color = (55, 55, 55)
        self.border.color = (61, 61, 61)
        self.border.roundness = 0.2
        self.border.width = 1

        self.drop_shadow.color = (0, 0, 0, 75)




class SubPanelTheme(PanelTheme):
    def __init__(self, parent=None, name='Sub Panel'):
        super().__init__(parent, name=name)

        self.header_color = ColorProperty((38, 38, 38))
        self.background_color = ColorProperty((48, 48, 48))
        #self.text_color = ColorProperty((200, 200, 200))

        self.border.width = 0

        #self.header = LabelTheme(self, name='Header')
        #self.text_shadow = ShadowTheme(self)


class GroupBoxTheme(FrameTheme):
    def __init__(self, parent=None, name='Group Box'):
        super().__init__(parent, name=name)

        self.background_color = ColorProperty((48, 48, 48))
        #self.text_color = ColorProperty((200, 200, 200))

        self.border.width = 0

        #self.header = LabelTheme(self, name='Header')


class ContainersTheme(Theme):
    def __init__(self, parent=None, name='Container'):
        super().__init__(parent, name=name)
        
        #self.default = FrameTheme(self)
        self.background_frame = FrameTheme(self, name='Background Frame')
        self.medium_frame = FrameTheme(self, name='Medium Frame')
        self.foreground_frame = FrameTheme(self, name='Foreground Frame')

        self.panel = PanelTheme(self)
        self.subpanel = SubPanelTheme(self)
        self.group_box = GroupBoxTheme(self)
        self.tab = TabTheme(self)


class MainWindowTheme(Theme):
    def __init__(self, parent=None, name='Main Window'):
        super().__init__(parent, name=name)

        self.background_color = ColorProperty((36, 36, 36))


class MenuTheme(Theme):
    def __init__(self, parent=None, name='Menu'):
        super().__init__(parent, name=name)

        self.background_color = ColorProperty((24, 24, 24))


class WindowsTheme(Theme):
    def __init__(self, parent=None, name='Windows'):
        super().__init__(parent, name=name)

        self.main_window = MainWindowTheme(self)
        self.menu = MenuTheme(self)


class FontShadowTheme(Theme):
    def __init__(self, parent=None, name="Widget Shadow"):
        super().__init__(parent, name=name)

        self.title = ShadowTheme(self, name="Title")
        self.text = ShadowTheme(self, name="Text")
        #self.widget = ShadowTheme(self, name="Widget")


class FontTheme(Theme):
    def __init__(self, parent=None, name='Font'):
        super().__init__(parent, name=name)

        app = QApplication.instance()
        
        self.folder = PathProperty(value=constant.FONT_DIR)
        self.font = EnumProperty('Segoe UI', items=['Segoe UI'], connect=self.set_family_items)
        self.family = EnumProperty('Regular', items=['Regular'])
        self.pixel_size = IntProperty(14, min=8, max=18, unit='px')
        self.weight = EnumProperty('Normal', items=('Thin','Extra Light', 'Light', 'Normal',
            'Medium', 'Demi Bold', 'Bold', 'Extra Bold', 'Black'))

        #self.weight = EnumProperty(500, min=100, max=900, step=100)
        self.hinting = BoolProperty(False)

        self.shadow = FontShadowTheme(self, name='Shadow')

        #self.label_shadow = ShadowTheme(self, name="Label Shadow")
        #self.value_shadow = ShadowTheme(self, name="Value Shadow")

        self.set_font_items()
        self.set_family_items()

    def set_font_items(self):
        #print([f.name for f in self.folder.iterdir()])
        self['font'].items = [f.name for f in self.folder.iterdir()]


    def set_family_items(self):
        #print([f.stem for f in (self.folder/self.font).glob('*.ttf')])
        #family_names = 
        self['family'].items = [f.stem.replace(f'{f.parent.name}-', '') for f in (self.folder/self.font).glob('*.ttf')]
        #app.set_application_font
        
        
        #for font_file in self.folder.glob('*.ttf'):


class DefaultTheme(Theme):
    def __init__(self, parent=None):
        super().__init__(parent, id='themes/default')
        
        self.font = FontTheme(self)
        self.windows = WindowsTheme(self)
        self.widgets = WidgetsTheme(self)
        self.containers = ContainersTheme(self)
        
        self.icons = Icons(directory=constant.ICON_DIR / 'default')

    @property
    def registery_key(self):
        return QApplication


class Icon(QIcon):
    def __init__(self, filepath, name=None, size=None):
        filepath = Path(filepath)

        if filepath.suffix == '.svg':
            icon_xml = filepath.read_text()

            #icon_xml = icon_xml.replace('opacity="0.5"', 'opacity="1"')
            #icon_xml = icon_xml.replace('#000', 'rgb(255, 255, 128)')
            #icon_xml = icon_xml.replace('black', 'rgb(255, 255, 128)')
            #icon_xml = icon_xml.replace('#999', 'rgb(255, 255, 128)')

            # create svg renderer with edited contents
            icon_svg =  QSvgRenderer(QByteArray(icon_xml))
            # create pixmap target (could be a QImage)

            icon_pixmap = QPixmap(icon_svg.default_size())
            icon_pixmap.fill(Qt.transparent)
            # create painter to act over pixmap
            icon_painter = QPainter(icon_pixmap)
            # use renderer to render over painter which paints on pixmap
            icon_svg.render(icon_painter)

            icon_painter.end()

            super().__init__(icon_pixmap)

        else:
            super().__init__(str(filepath))

        self.filepath = Path(filepath)
        self.size = size
        self.name = name

        #self.shadow_offset = 1
        self.shadow_opacity = 0.5
        self.shadow_color = QColor(0, 0, 0)

    def add_file(self, icon_path, size):
        super().add_file(str(icon_path), size)

    
class Icons:
    def __init__(self, directory=None, load=True):
        super().__init__()

        self._dict = {}

        self.directory = None

        if directory:
            self.directory = Path(directory)
        #self.directory = Path(directory)
        self.naming_reg = re.compile(r'([_\w]+)_([\d]+)')

        if load:
            self.load()

    def to_dict(self):
        return self._dict

    def values(self):
        return self.to_dict().values()

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.values()[key]

        elif isinstance(key, str):
            return self._dict[key]
    
    def get(self, key):
        return self._dict.get(key)

    def __contains__(self, key):
        return key in self._dict

    def add(self, icon_path):
        icon_path = Path(icon_path)
        icon_name = icon_path.name
        icon_size = QSize(16, 16)
        res = self.naming_reg.findall(icon_name)
        #print(res)
        if res:
            icon_name, size = res[0]
            icon_size = QSize(int(size), int(size))

        if icon_name in self._dict:
            self._dict[icon_name].add_file(icon_path, icon_size)
        else:
            theme_icon = Icon(icon_path, name=icon_name, size=icon_size)
            self._dict[theme_icon.name] = theme_icon

    def clear(self):
        #TODO Implement clear method swap to empty icons
        return

    def load(self):
        self.clear()

        for icon_path in self.directory.rglob('*.*'):
            self.add(icon_path)