
from PySide6.QtCore import QObject, QEvent, Signal
from PySide6.QtWidgets import QApplication, QStyle

from __feature__ import snake_case # pylint: disable=E0401, W0611


class signal_blocked:
    def __init__(self, widget):
        self.widget = widget
        self.signals_blocked = widget.signals_blocked()

    def __enter__(self):
        self.widget.block_signals(True)
        return self.widget

    def __exit__(self, type, value, traceback):
        if not self.signals_blocked:
            self.widget.block_signals(False)
            

class FocusBlocker(QObject):
    focus_out = Signal()
    def __init__(self, modal_widget, install=False):
        super().__init__()
        self.modal_widget = modal_widget
        self.blocking = False

        if install:
            self.install()
        
        self.focus_out.connect(self.remove)
    
    def install(self):
        QApplication.instance().install_event_filter(self)

    def remove(self):
        QApplication.instance().remove_event_filter(self)

    def event_filter(self, obj, event):
        if event.type() in (QEvent.Enter, QEvent.HoverEnter) and obj is not self.modal_widget:
            return True

        if event.type() == QEvent.MouseButtonPress and not self.modal_widget.under_mouse():
            self.focus_out.emit()
            return True

        return False