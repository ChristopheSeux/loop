from PySide6.QtWidgets import (QBoxLayout, QHBoxLayout, QVBoxLayout, QStackedLayout, QSpacerItem,
    QGridLayout, QLayout, QLayoutItem)
from PySide6.QtCore import Qt, QSize, QPoint

from __feature__ import snake_case # pylint: disable=E0401, W0611

from functools import reduce
from operator import ior
from itertools import groupby

from canvas.ui.text import TextLabel


class AbstractLayout():
    def __init__(self, parent=None, spacing=0, margin=0, align=None):
        
        self.set_spacing(spacing)
        self.set_margin(margin)

        if align:
           self.set_alignment(align)

        self.parent_to(parent)

    def parent_to(self, parent):
        if parent and parent.layout():
            parent.layout().add_layout(self)
        elif parent:
            parent.set_layout(self)

    def set_margin(self, *margin):
        if len(margin)==1 and isinstance(margin[0], (list, tuple)):
            margin = margin[0]

        if len(margin)==1:
            margin = margin*4
        elif len(margin)==2:
            margin = margin*2

        self.set_contents_margins(*margin)

    def set_alignment(self, alignment):
        if isinstance(alignment, str):
            alignment = alignment.split('|')
        super().set_alignment(reduce(ior, (getattr(Qt, f'Align{a}') for a in alignment)))
    
    def clear(self):
        for item in reversed(self.items()):
            widget = item.widget()
            if widget:
                self.remove_widget(widget)
                widget.set_parent(None)
            else:
                self.remove_item(item)

    def items(self):
        return [self.item_at(index) for index in range(self.count())]

    def widgets(self):
        return [item.widget() for item in self.items() if item.widget()]

    def add_spacer(self, width=0, height=0):
        self.add_item(QSpacerItem(width, height))

    def horizontal_collapse(self, collapsed=True):
        self._is_collapsed = collapsed

        for is_spacer, groups in groupby(self.items(), lambda x: isinstance(x, QSpacerItem)):
            if is_spacer:
                continue
            
            widgets = [i.widget() for i in groups]

            for index, widget in enumerate(widgets):
                if not widget or not hasattr(widget, 'geometry'):
                    continue

                if not collapsed or len(widgets)==1:
                    corners = [True, True, True, True]
                elif index == 0:
                    corners = [True, False, False, True]
                elif index == len(widgets)-1:
                    corners = [False, True, True, False]
                else:
                    corners = [False, False, False, False]
                
                widget.geometry().corners = corners
                widget.update()

    def vertical_collapse(self, collapsed=True):
        self._is_collapsed = collapsed

        for is_spacer, groups in groupby(self.items(), lambda x: isinstance(x, QSpacerItem)):
            if is_spacer:
                continue

            widgets = [i.widget() for i in groups]

            for index, widget in enumerate(widgets):
                # if not widget or not hasattr(widget, 'geometry'):
                #     print(f'Widget {widget} has no geometry')
                #     continue

                if not collapsed or len(widgets)==1:
                    corners = [True, True, True, True]
                elif index == 0:
                    corners = [True, True, False, False]
                elif 0 < index < len(widgets)-1:
                    corners = [False, False, False, False]
                else:
                    corners = [False, False, True, True]
                                
                widget.style().geometry.corners = corners


class BoxLayout(AbstractLayout, QBoxLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, direction='LeftToRight', collapsed=False):
        QBoxLayout.__init__(self, getattr(QBoxLayout, direction))
        AbstractLayout.__init__(self, parent=parent, spacing=spacing, margin=margin, align=align)

        self._collapsed = False

        self.set_collapsed(collapsed)

    def collapsed(self):
        return self._collapsed
    
    def set_direction(self, direction):
        if isinstance(direction, str):
            direction = getattr(QBoxLayout, direction)
        
        super().set_direction(direction)

    def add_widget(self, widget):
        super().add_widget(widget)

        if not self.collapsed():
            return

        self.set_collapsed(self.collapsed())

    def set_collapsed(self, collapsed=True):
        self._collapsed = collapsed
        vertical = self.direction() in (QBoxLayout.TopToBottom, QBoxLayout.BottomToTop)

        item_groups = []
        item_groups.append(item_group := [])
        for item in self.items():
            widget = item.widget()
            if not widget or not widget.style() or not widget.style().collapsible:
                item_groups.append(item_group := [])
            else:
                item_group.append(item)
        
        for item_group in item_groups:
            widgets = [i.widget() for i in item_group]

            for index, widget in enumerate(widgets):
                if not self.collapsed() or len(widgets) == 1:
                    corners = [True, True, True, True]
                elif index == 0:
                    if vertical:
                        corners = [True, True, False, False]
                    else:
                        corners = [True, False, False, True]
                elif 0 < index < len(widgets)-1:
                    corners = [False, False, False, False]
                else:
                    if vertical:
                        corners = [False, False, True, True]
                    else:
                        corners = [False, True, True, False]
                
                widget.style().geometry.corners = corners

    #def show_event(self, event):
    #    self.set_collapsed()

    # def add_widget(self, widget):

    #     #if self.collapsed():
    #         #self.set_collapsed()

    #         #print('add_widget', widget)
    #     if hasattr(widget, 'geometry'):
    #         widget.geometry().corners = [True, False, False, False]

    #     super().add_widget(widget)
    #     return widget
    


class HBoxLayout(BoxLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, collapsed=False):
        super().__init__(parent=parent, spacing=spacing, margin=margin,
            align=align, direction='LeftToRight', collapsed=collapsed)

    def expanding_directions(self):
       return Qt.Horizontal

    def collapse(self, collapsed=True):
        self.horizontal_collapse(collapsed)

    def set_geometry(self, rect):
        super().set_geometry(rect)

        if not self.collapsed():
            return

        # Implement collapse
        for i in range(self.count()):
            item = self.item_at(i)
            rect = item.geometry()
            item.widget().set_maximum_width(16777214)
            rect.set_right(rect.right()+1)
            item.set_geometry(rect)

class VBoxLayout(BoxLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align='Top', collapsed=False):
        super().__init__(parent=parent, spacing=spacing, margin=margin, 
            align=align, direction='TopToBottom', collapsed=collapsed)

        #self.set_alignment (Qt.AlignBottom)

    def expanding_directions(self):
        return Qt.Vertical

    def collapse(self, collapsed=True):
        self.vertical_collapse(collapsed)

    # def set_geometry(self, rect):
    #     super().set_geometry(rect)

    #     if not self.collapsed():
    #         return

    #     for i in range(self.count()):
    #         item = self.item_at(i)
    #         rect = item.geometry()
    #         row, _, _, _ = self.get_item_position(i)
    #         #rect.translate(QPoint(0, -row))
    #         rect.set_bottom(rect.bottom()+1)
    #         item.set_geometry(rect)


class StackedLayout(AbstractLayout, QStackedLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, stacking_mode='StackOne'):
        QStackedLayout.__init__(self, parent=parent)
        AbstractLayout.__init__(self, parent=parent, spacing=spacing, margin=margin, align=align)

        self.set_stacking_mode(getattr(QStackedLayout, stacking_mode))
        self.current_changed = self.__dict__.pop('currentChanged') if 'currentChanged' in self.__dict__ else self.current_changed


class GridLayout(AbstractLayout, QGridLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None):
        QGridLayout.__init__(self, parent=parent)
        AbstractLayout.__init__(self, parent=parent, spacing=spacing, margin=margin, align=align)


class PropertyLayoutItem(QLayoutItem):
    def __init__(self, widget, label):
        super().__init__()

        self.label = label
        self.widget = widget

    #def expanding_directions(self):
    #    return q_size_policy(0, 0)

    #def has_height_for_width(self):
    #    return true

    #def height_for_width(self, width):
    #    return self.size_hint().height()

    def set_geometry(self, rect):
        self.widget.set_geometry(rect)

    def size_hint(self):
        return self.widget.size_hint()

    def minimum_size(self):
        return self.size_hint()


class PropertyLayout(AbstractLayout, QLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align='Top'):
        #super().__init__(parent=parent, spacing=spacing, margin=margin, align=align)

        QLayout.__init__(self, parent=parent)
        AbstractLayout.__init__(self, parent=parent, spacing=spacing, margin=margin, align=align)

        self._items = []
        self._split_factor = 0.4

    def split_factor(self):
        return self._split_factor
    
    def set_split_factor(self, split_factor):
        self._split_factor = split_factor

    def items(self):
        return self._items

    def count(self):
        return len(self.items())

    def size_hint(self):
        return QSize(-1, -1)
        if self.parent():
            return self.parent().size()
    
    def set_geometry(self, rect):
        #super().set_geometry(rect)

        if not self.items():
            return

        width = self.parent().rect().width()
        label_width = width * self.split_factor()
        widget_width = width - label_width

        height = 0
        for item in self.items():
            rect = label.geometry()
            rect.move_to(0, height)
            rect.set_width(label_width)
            label.set_geometry(rect)

            rect = widget.geometry()
            rect.move_to(widget_width, height)
            rect.set_width(widget_width)
            widget.set_geometry(rect)

            height += rect.height() -1

    def item_at(self, index):
        if 0 <= index < self.count():
            return self.items()[index]

    def take_at(self, index):
        item = self.item_at(index)
        if item:
            self.remove_item(item)#.set_parent(None)

        return item

    def add_item(self, item):
       #self.add_widget(item)
       self.items().append(item)

    def add_widget(self, widget, label=None):
        if label is None:
            label = widget.name()

        label = TextLabel(text=label)
        #widget = prop.to_widget()

        self.items().append(PropertyLayoutItem(widget, label))


class FormLayout(GridLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align='Top', collapsed=False):
        super().__init__(parent=parent, margin=margin, align=align)

        #print(self, spacing, self.spacing())

        self._property_split = True
        self._collapsed = False

        self.set_column_stretch(0, 2)
        self.set_column_stretch(1, 3)

        self.set_horizontal_spacing(4)
        self.set_vertical_spacing(spacing)

        self.set_collapsed(collapsed)
    
    def set_collapsed(self, collapsed=True):
        self._collapsed = collapsed

        item_groups = []
        item_groups.append(item_group := [])
        for item in self.items():
            widget = item.widget()
            if not widget or not widget.style() or not hasattr(widget.style(), "collapsible") or widget.style().collapsible:
                item_groups.append(item_group := [])
            else:
                item_group.append(item)
        
        for item_group in item_groups:
            widgets = [i.widget() for i in item_group]

            for index, widget in enumerate(widgets):
                if not self.collapsed() or len(widgets) == 1:
                    corners = [True, True, True, True]
                elif index == 0:
                    corners = [True, True, False, False]
                elif 0 < index < len(widgets)-1:
                    corners = [False, False, False, False]
                else:
                    corners = [False, False, True, True]
                                
                widget.style().geometry.corners = corners

    def collapsed(self):
        return self._collapsed


    # def add_item(self, item):
    #    super().add_item(item)
    #    index = self.row_count() -1
    #    row, _, _, _ = self.get_item_position(index)

    #    return item


    def set_geometry(self, rect):
        super().set_geometry(rect)

        if not self.collapsed():
            return

        for i in range(self.count()):
            item = self.item_at(i)
            #print(item.geometry())
            rect = item.geometry()


            row, _, _, _ = self.get_item_position(i)
            #rect.translate(QPoint(0, -row))
            rect.set_bottom(rect.bottom()+1)
            item.set_geometry(rect)

            #print(item.widget(), rect, self.get_item_position(i))
            #item.set_geometry(rect.adjusted(0, -row, 0, row))

        #     _, _, row, col = self.get_item_position(i)
        #     print(row)
        #     # rect.move_to(0, height)
        #     item.set_geometry(rect.adjusted(0, -row, 0, row))

        #     # height += rect.height() -1
        

    def add_spacer(self, width=1, height=1):
        self.add_item(QSpacerItem(width, height), self.row_count(), 1)

    def items(self):
        items = [self.item_at_position(index, 1) for index in range(self.row_count())]

        return [item for item in items if item]

    def property_split(self):
        return self._property_split

    def add_widget(self, widget):
        if hasattr(widget, 'name'):
            self.add_row(widget, widget.name())
        else:
            self.add_row(widget)

        widget.set_maximum_height(16777214)

        self.set_collapsed(self.collapsed())
        
    def add_row(self, widget, label=None):
        row_count = self.row_count() #+ 1
        if label:
            text_label = TextLabel(text=label)
            super().add_widget(text_label, row_count, 0, Qt.AlignRight)
            super().add_widget(widget, row_count, 1)
        else:
            super().add_widget(widget, row_count, 0, 1, 0)
        
        