
from pathlib import Path

MODULE_DIR = Path(__file__).parent
RESOURCE_DIR = MODULE_DIR / 'resources'
ICON_DIR = RESOURCE_DIR / 'icons'
FONT_DIR = RESOURCE_DIR / 'fonts'
APP = None

ALIGNMENTS = ('Auto', 'Left', 'Right', 'Center')
EMBOSS = ('Auto', 'On', 'Off')
LIGHTING_SOURCE = ('top_left', 'top', 'top_right', 'right', 
    'bottom_right', 'bottom', 'bottom_left', 'left')
LIGHTING_ANGLE = ('0°', '45°', '90°')