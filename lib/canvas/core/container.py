

class Container:
    def __init__(self, parent=None):
        self.parent = parent

        try:
            self._values = []    
        except AttributeError: # _values has been overrided by a property
            pass

    # @property
    # def data(self):
    #     return self._data

    # @data.setter
    # def data(self, data):
    #     self._data.clear()
    #     self._data.update(data)
    def clear(self):
        self._values.clear()

    def items(self):
        return self.to_dict().items()
    
    def values(self):
        return self.to_dict().values()

    def keys(self):
        return self.to_dict().keys()

    def add(self, item):
        self._values.append(item)
        return item
    
    def remove(self, item):
        self._values.remove(item)

    def __iter__(self):
        return self._values.__iter__()
    
    def __len__(self):
        return self._values.__len__()
    
    def to_dict(self):
        return {item.name: item for item in self}

    def get(self, name=None, fallback=None, **kargs):
        if name is not None:
            return next((item for item in self if item.name==name), None)

        for k, v in kargs.items():
            if v is not None:
                return next((item for item in self if getattr(item, k)==v), None)
        
        return fallback