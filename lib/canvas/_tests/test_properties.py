
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication, QVBoxLayout, QScrollArea)
from PySide6.QtCore import (Qt, QRect, QMargins)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon
import sys


from pathlib import Path
from time import perf_counter

src_dir = Path(__file__).parents[2] #/ 'lib'

print(src_dir)
sys.path.append(str(src_dir))

from canvas.ui.application import Application
from canvas.ui.frame import VBoxFrame
from canvas.ui.text import TextLabel
from canvas.ui.container import Panel, SubPanel, HTabWidget, FormFrame
from canvas.ui.button import PushButton
from canvas.ui.property import (IntProperty, FloatProperty, StringProperty, BoolProperty,
    EnumProperty, PropertyGroup, PathProperty, IntArrayProperty)

from canvas.ui.window import ThemeWindow


from __feature__ import snake_case#, true_property


class MyPanel(Panel):
    def __init__(self, parent=None):
        super().__init__(parent, title='My Panel')

        theme = QApplication.instance().theme()
        icons = theme.icons

        self.properties = PropertyGroup(
            int=IntProperty(1, name='Render'),
            float=FloatProperty(1.5, name='Render', min=0, max=10),
            string=StringProperty('My Text', name='My String', icon=icons['video']),
            bool=BoolProperty(True, name='My Bool'),
            path=PathProperty('./my_path.txt', name='My Path'),
            enum=EnumProperty('Item2', items=('Item1', 'Item2', 'Items3'), name='My Enum Expand', exclusive=True)
        )
        #self.set_contents_margins(10, 10 ,10, 10)

        theme = QApplication.instance().theme().widgets.bool
        theme.save()

        self.properties.to_widgets(self)

        self.properties.combo = EnumProperty('Item2', items=('Item1', 'Item2', 'Items3'), name='My Enum Compact')
        self.properties['combo'].to_widget(self, expand=False)

        button = PushButton(self, text='My Button')
        

        self.properties.value_changed.connect(print)

        #print(list(self.properties['int'].widgets)[0].style().drop_shadow.__class__)

        int_widget = list(self.properties['int'].widgets)[0]

        print(int_widget.style().drop_shadow)

        int_widget.style().update()

        return
        #self.set_minimum_height(256)

        # self.scroll_area = QScrollArea()
        # self.scroll_area.set_widget_resizable(True)
        # self.scroll_area.set_widget(self)
        #self.layout().set_collapsed()



        #self.properties['properties'].

        self.int = IntProperty(1, name='Viewport').to_widget(parent=self)
        self.float = FloatProperty(1.5, name='Render', min=0, max=10).to_widget(parent=self, slider=True)
        self.string = StringProperty('test', name='').to_widget(parent=self, icon=icons['video'])
        self.btn = PushButton(text='Push Me', parent=self, icon=icons['movie'])
        self.bool = BoolProperty(True, name='My Bool Icon and Text').to_widget(parent=self, icon=icons['play'])

        #self.layout().collapse()

        BoolProperty(True, name='My Bool Text Only').to_widget(parent=self)
        BoolProperty(True, name='My Bool Icon Only').to_widget(parent=self, icon=icons['play'], name='')

        EnumProperty('Item2', items=('Item1', 'Item2', 'Items3'), name='My Enum Expanded', exclusive=True).to_widget(parent=self, expand=True)

        EnumProperty('Item2', items=(f'Item{i}' for i in range(15)), name='My Enum Compact', exclusive=True).to_widget(parent=self, expand=False)

        #self.layout().add_stretch()

        self.btn.clicked.connect(self.change_color)
        #self.prop.to_widget(self.layout())

        #self.int_widget = IntWidget(parent=self.layout(), name='My Int', min=0, max=10)
        #self.float_widget = FloatWidget(parent=self.layout(), name='My Float')
        #self.bool_widget = BoolWidget(parent=self.layout(), name='My Bool')
        #self.string_widget = StringWidget(parent=self.layout(), name='My String')

    def change_color(self):
        self.int.style().background_color = QColor('red')

        for widget in QApplication.instance().top_level_widgets():
            widget.update()


class Widget(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent, margin=8, spacing=3)

        self.panel = MyPanel(self)
        self.sub_panel = SubPanel(self.panel, title='My Sub Panel')

        EnumProperty('Item2', items=[f'Item{i}' for i in range(10)], name='My Enum').to_widget(self.sub_panel, expand=False)

        self.set_alignment('Top')

        self.layout().add_stretch()

        self.resize(256, 256)
        #self.layout().add_stretch()


class Window(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.tab_widget = HTabWidget(self)
        self.tab_widget.add_tab(Widget(), name='My First Tab')
        self.tab_widget.add_tab(Widget(), name='My Second Tab')
        #self.set_layout(FormFrame)

        #IntArrayProperty([1,1,1]).to_widget(self)
        #self.add_row(IntProperty(1).to_widget(), 'toto')
        #TextLabel(text='My Label', parent=self)

        #self.properties = PropertyGroup(
        #    IntArrayProperty([1,1,1]),
        #    IntProperty(1, name='My Int')
        #)

        #self.properties.to_widgets(self)
        self.resize(512, 512)
        #self.layout().add_stretch()

app = Application()

t0 = perf_counter()
window = ThemeWindow(theme=app.theme())
print(f"Creating Window took {perf_counter()-t0} second")
#window = Widget()
window.resize(512, 768)
#window = Window()
window.show()


app.exec()