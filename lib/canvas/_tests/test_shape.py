
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication, QVBoxLayout)
from PySide6.QtCore import (Qt, QRect, QMargins, QSize)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon
import sys


from pathlib import Path

src_dir = Path(__file__).parents[2] #/ 'lib'

print(src_dir)
sys.path.append(str(src_dir))

from canvas.ui.application import Application
from canvas.ui.frame import VBoxFrame
from canvas.ui.shape import RectShape

from __feature__ import snake_case#, true_property


class Widget(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent)

    def size_hint(self):
        return QSize(150, 150)

    def paint_event(self, event):
        painter = QPainter(self)

        border_radius = 20, 10, 0, 15

        shape = RectShape(self.rect().adjusted(10,10,-10,-10), radius=border_radius, pen_width=1)
        print(shape.fill_path)
        shape.paint(painter, brush=QColor(255,0,0), pen=QColor(0,0,0,255))

        #self.properties = PropertyGroup(
        #    int_prop=IntProperty(1, parent=self)




app = Application()
window = Widget()
window.show()

app.exec()