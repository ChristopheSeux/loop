
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication, QVBoxLayout, QStackedLayout, QLayout)
from PySide6.QtCore import (Qt, QRect, QMargins, QSize, QEvent)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon, QCursor
import sys


from pathlib import Path

from __feature__ import snake_case#, true_property

from pathlib import Path

src_dir = Path(__file__).parents[2] #/ 'lib'

print(src_dir)
sys.path.append(str(src_dir))

from canvas.ui.property import IntProperty
from canvas.ui.application import Application


class OverlayLayout(QLayout):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._items = []
    
    def items(self):
        return self._items
    
    def add_item(self, item):
       #self.add_widget(item)
       self.items().append(item)

    def add_layout(self, layout):
        self.add_item(layout)

    def count(self):
        return len(self.items())

    def size_hint(self):
        return QSize(-1, -1)
        if self.parent():
            return self.parent().size()
    
    def set_geometry(self, rect):
        super().set_geometry(rect)

        if not self.count():
            return

        #w = rect.width() - (self.items().count() - 1) * spacing()
        #h = rect.height() - (self.items().count() - 1) * spacing()
        height = 0
        for i in range(self.count()):
            print(i)
            item = self.item_at(i)
            rect = item.geometry()
            rect.move_to(0, height)
            #rect.set_top(rect.top()-i*4)
            item.set_geometry(rect)
            #geom(rect.x() + i * spacing(), r.y() + i * spacing(), w, h)
            #item.setGeometry(geom)
            height += rect.height() -1

    def item_at(self, index):
        if 0 <= index < self.count():
            return self.items()[index]

    def take_at(self, index):
        item = self.item_at(index)
        if item:
            self.remove_item(item)#.set_parent(None)

        return item


class OverlayWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.set_layout(OverlayLayout())


class Widget(OverlayWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        btn = IntProperty(1, name='My Button')
        btn.to_widget(self.layout())

        btn = IntProperty(2, name='My Button2')
        btn.to_widget(self.layout())

        #btn = IntProperty(name='My Button2')
        #btn.set_fixed_height(20)
        #self.layout().add_stretch()
        #self.layout().add_widget(btn)

        #btn.set_fixed_height(20)







app = Application()
window = Widget()
window.show()

sys.exit(app.exec())