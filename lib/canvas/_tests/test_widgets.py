
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication, QVBoxLayout)
from PySide6.QtCore import (Qt, QRect, QMargins)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon
import sys


from pathlib import Path

src_dir = Path(__file__).parents[2] #/ 'lib'

print(src_dir)
sys.path.append(str(src_dir))

from canvas.ui.application import Application
from canvas.ui.property.widget import IntWidget, BoolWidget, StringWidget, FloatWidget
from canvas.ui.frame import FormFrame, PropertyFrame, VBoxFrame
from canvas.ui.button import PushButton

from __feature__ import snake_case#, true_property




class Widget(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent, margin=5)

        #self.set_layout(QVBoxLayout())

        self.int_widget = IntWidget(parent=self, name='My Int', min=0, max=10)
        self.float_widget = FloatWidget(parent=self, name='My Float')
        self.bool_widget = BoolWidget(parent=self, name='My Bool')
        self.string_widget = StringWidget(parent=self, name='My String')

        PushButton(parent=self, text='My Button')



app = Application()
window = Widget()
window.show()

sys.exit(app.exec())