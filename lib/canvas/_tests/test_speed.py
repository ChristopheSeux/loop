
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication, QVBoxLayout, QScrollArea)
from PySide6.QtCore import (Qt, QRect, QMargins)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon
import sys


from pathlib import Path
from time import perf_counter

src_dir = Path(__file__).parents[2] #/ 'lib'

print(src_dir)
sys.path.append(str(src_dir))

from canvas.ui.application import Application
from canvas.ui.frame import VBoxFrame
from canvas.ui.text import TextLabel
from canvas.ui.container import Panel, SubPanel, HTabWidget, FormFrame
from canvas.ui.button import PushButton
from canvas.ui.property import (IntProperty, FloatProperty, StringProperty, BoolProperty,
    EnumProperty, PropertyGroup, PathProperty, IntArrayProperty)

from canvas.ui.window import ThemeWindow


from __feature__ import snake_case#, true_property


class Widget(VBoxFrame):
    def __init__(self, parent=None):
        super().__init__(parent, margin=8, spacing=3)

        for i in range(0, 100):
            #EnumProperty('1', items=('1', '2', '3')).to_widget(parent=self)
            IntArrayProperty([1, 1]).to_widget(self)


        self.resize(256, 256)
        #self.layout().add_stretch()

app = Application()

t0 = perf_counter()
window = Widget()
print(f"Creating Window took {perf_counter()-t0} second")
window.resize(512, 768)
#window = Window()
window.show()


app.exec()