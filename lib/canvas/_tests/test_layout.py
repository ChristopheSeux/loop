
from PySide6.QtWidgets import (QSplitter, QSplitterHandle, QPushButton, QHBoxLayout, 
    QWidget, QFrame, QApplication, QVBoxLayout, QStackedLayout, QLayout)
from PySide6.QtCore import (Qt, QRect, QMargins, QSize, QEvent)
from PySide6.QtGui import QColor, QPainter, QBrush, QPen, QIcon, QCursor
import sys


from pathlib import Path

from __feature__ import snake_case#, true_property


class BorderWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.set_attribute( Qt.WA_TransparentForMouseEvents )
        self.set_mouse_tracking(True)
    
    def mouse_press_event(self, event):
        print(event)

    # def event(self, event):
    #     event.accept()

    #     return True
    #     print(event, event.type())

    #     if event.type() is QEvent.MouseMove:
    #         print('11')
    #         return True

    #     return super().event(event)

    # def mouse_move_event(self, event):
    #     print(event)

    def paint_event(self, event):

        w = QApplication.widget_at(QCursor.pos())
        #w = self.parent().background_widget().layout().widget_at(QCursor.pos())
        if not w:
            return

        print(w.geometry())
        #print(self.map_from(w, w.rect().top_left()))
        
        #self.parent().background_widget().layout().items()

        painter = QPainter(self)

        painter.set_brush(Qt.NoBrush)
        painter.set_pen(QColor(255, 0, 0))
        painter.draw_rect(w.geometry().adjusted(1, 1, -1 ,-1))


class VBoxWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.set_layout(QVBoxLayout())


class OverlayLayout(QLayout):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._items = []
    
    def items(self):
        return self._items
    
    def add_item(self, item):
       #self.add_widget(item)
       self.items().append(item)

    def add_layout(self, layout):
        self.add_item(layout)

    def count(self):
        return len(self.items())

    def size_hint(self):
        return QSize(-1, -1)
        if self.parent():
            return self.parent().size()
    
    def set_geometry(self, rect):
        super().set_geometry(rect)
        for item in self.items():
            item.set_geometry(rect)

    def item_at(self, index):
        if 0 <= index < self.count():
            return self.items()[index]

    def take_at(self, index):
        item = self.item_at(index)
        if item:
            self.remove_item(item)#.set_parent(None)

        return item


class OverlayWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.set_layout(OverlayLayout())

        self._background_widget = None
        self._foreground_widget = None
        
    def background_widget(self):
        return self._background_widget

    def foreground_widget(self):
        return self._foreground_widget

    def set_background_widget(self, widget):
        if self.background_widget():
            self.background_widget().set_parent(None)
        
        foreground_widget = self.foreground_widget()
        if foreground_widget:
            self.background_widget().set_parent(None)

        self._background_widget = widget
        super().layout().add_widget(widget)

        if foreground_widget:
            super().layout().add_widget(foreground_widget)

    def set_foreground_widget(self, widget):
        if self.foreground_widget():
            self.foreground_widget().set_parent(None)

        self._foreground_widget = widget
        super().layout().add_widget(widget)

    def layout(self):
        if self.background_widget():
            return self.background_widget().layout()

    '''
    def set_geometry(self, rect):
        print(rect)
        super().set_geometry(rect)

        if not self.count():
            return

        #w = rect.width() - (self.items().count() - 1) * spacing()
        #h = rect.height() - (self.items().count() - 1) * spacing()
    
        for i in range(self.count()):
            item = self.item_at(i)
            rect = item.geometry()
            rect.set_top(rect.top()-i*4)
            item.set_geometry(rect)
            #geom(rect.x() + i * spacing(), r.y() + i * spacing(), w, h)
            #item.setGeometry(geom)
            '''

class Widget(OverlayWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        btn = QPushButton(text='My Button')
        btn.set_minimum_height(10)


        self.set_background_widget(VBoxWidget())
        self.set_foreground_widget(BorderWidget())

        #self.layout().add_stretch()
        self.layout().add_widget(btn)






app = QApplication()
app.set_style('Fusion')

window = Widget()
window.show()

sys.exit(app.exec())